/**
 * 
 * Select Field
 * 
 */

import React from 'react';
import styles from './styles';
import { FormControl, InputLabel, Select, MenuItem } from '@mui/material';

export default function selectField({ input, label, title, required, metaData, options, variant, disabled, children, style, meta: { touched, error, warning, data } }) {

    const classes = styles();

    const { name, value } = input;
    const isPreDefinedSet = Array.isArray(options);
    return (
        <div className={classes.selectField} style={style || {}}>
            {children || ''}
            {/* <FormControl variant={variant || null} className={classes.formControl}> */}
            <label className={classes.label}>{label ? label : title ? title : ''}</label>
            <Select
                name={name}
                fullWidth
                disabled={disabled}
                required={required}
                value={value}
                onChange={(e) => { input.onChange(e.target.value) }}
                className={classes.Field}
                variant="standard"
                placeholder="Choose Option"
                displayEmpty>
                <MenuItem value="" disabled={input.value ? true : false} style={{ color: 'gray' }}>Choose Option</MenuItem>
                {isPreDefinedSet ? (options || []).map((opt, index) => {
                    return <MenuItem
                        key={index} disabled={opt.disabled || false} value={opt && opt.value ? opt.value : opt.name} style={{ color: 'gray' }}>{opt && opt.label ? opt.label : opt.title}</MenuItem>
                }) :
                    (metaData[options] || []).map((opt, index) => {
                        return <MenuItem key={index} disabled={opt.disabled || false} value={opt && opt.value != null
                            && opt.value || opt}>{opt && opt.label != null && opt.label || opt}</MenuItem>
                    })
                }
            </Select>
            {/* </FormControl> */}
            <div className={classes.error}>{touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</div>
        </div>
    )
}

