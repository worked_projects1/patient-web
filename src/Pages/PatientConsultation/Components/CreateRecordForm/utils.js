
import TextArea from '../TextArea';
import selectField from '../SelectField';

export const ImplementationFor = {
    textarea: TextArea,
    select: selectField,
};

