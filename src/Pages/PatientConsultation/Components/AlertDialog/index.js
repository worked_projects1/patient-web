/**
 * 
 * Alert Dialog
 * 
 */

import React, { useState } from 'react';
import { Grid, Typography, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material';
import Button from '@mui/material/Button';

import Styles from './styles';

export default function AlertDialog({ title, children, btnLabel1, btnLabel2, description, onConfirm, onConfirmPopUpClose, openDialog, closeDialog, heading }) {

  const classes = Styles();
  const [open, setOpen] = useState(false);

  const close = () => {
    setOpen(false);
    if (closeDialog) {
      closeDialog();
    }
  }

  const confirm = () => {
    onConfirm();
    if (onConfirmPopUpClose) {
      close();
    }
  }


  return (<div>
    {children && children(() => setOpen(!open))}
    <Dialog
      open={open || openDialog || false}
      onClose={close}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description">
      <DialogTitle id="alert-dialog-title" >{title}</DialogTitle>
      {typeof heading === 'function' && heading() || heading && <Grid className={classes.gridHeader}>
        <Typography variant="h6" gutterBottom component="span"><b>{heading || ''}</b></Typography>
      </Grid> || null}
      <DialogContent>
        <DialogContentText id="alert-dialog-description" className={classes.description}>
          <span dangerouslySetInnerHTML={{ __html: description }} />
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={close} color="primary" className={classes.button}>
          {btnLabel2}
        </Button>
        {btnLabel1 ? <Button onClick={confirm} color="primary" autoFocus className={classes.button}>
          {btnLabel1}
        </Button> : null}
      </DialogActions>
    </Dialog>
  </div>);
}