
/**
 * 
 * Home
 * 
 */



import React from 'react';
import styled from 'styled-components';
import styles from './styles';
import api from '../../utils/api';
import { Grid } from '@mui/material';
import Button from '@mui/material/Button';
import CancelPage from './CancelPage';
import ModalRecordForm from './Components/ModalRecordForm';
import ConfirmPage from './ConfirmPage';
import SchedulePage from './SchedulePage';
import schema from '../../routes/schema';

const appoinment = schema().Appoinment_form().columns;



const H1 = styled.h1`
   font-size: 2rem;
   font-weight: 400;
   text-align: center;
   color:white
 `;

function PatientConsultation(props) {
  const { decryptedText } = props;
  const [open, setOpen] = React.useState(false);
  const [busy, setBusy] = React.useState(false);
  const [cancel, setcancel] = React.useState(false);
  const [confirm, setconfirm] = React.useState(false);
  const [schedule, setschedule] = React.useState(false);
  const classes = styles();


  var remove_str = decryptedText.split('&');
  var data_req = remove_str[1].split('=')[1];
  var id = remove_str[3].split('=')[1];
  var my_day = new Date(data_req);


  const handleCancel = (submitData, dispatch, { form }) => {
    const reason = submitData.reason;
    const notes = submitData.other;
    setBusy(true);
    api.put(`/reminderResponse/${id}`, Object.assign({}, { status: 'Canceled', reason, notes })).then((response) => {
      const cancel_data = response.data.appointmentStatus;
      setcancel(cancel_data);
    }).finally(() => {
      setBusy(true);
    })
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSchedule = (data, dispatch, { form }) => {
    const reason = data.reason;
    const notes = data.other;
    setBusy(true);
    api.put(`/reminderResponse/${id}`, Object.assign({}, { status: 'Need to reschedule', reason, notes })).then((response) => {
      const schedule_data = response.data.appointmentStatus;
      setschedule(schedule_data);
    }).finally(() => {
      setBusy(false);
    })
  }

  const handleConfirm = async () => {
    setBusy(true);
    await api.put(`/reminderResponse/${id}`, Object.assign({}, { status: 'Confirmed' })).then((response) => {
      const conf_data = response.data.appointmentStatus;
      setconfirm(conf_data);
    }).finally(() => {
      setBusy(false);
    });
  }
    

  return (
    <>
      {confirm ? <ConfirmPage decryptedText={decryptedText} /> :
        cancel ? <CancelPage decryptedText={decryptedText} /> :
          schedule ? <SchedulePage decryptedText={decryptedText} /> :
            <div className={classes.body} style={{ padding: '5px' }}>
              <H1>Please manage your dental appointment</H1>
              <div className={classes.space}></div>
              <div className={classes.schedule_time}>
                <Grid item align="center">
                  <div className={classes.schedule_table}>Schedule for</div>
                  <div className={classes.schedule_table}>{remove_str[0].split('=')[1]}</div>
                  <div className={classes.schedule_table}>{my_day.toDateString()} at {remove_str[2].split('=')[1]}</div>
                </Grid>
              </div>

              <Grid item align="center">
                <Button onClick={() => handleConfirm()} className={classes.button}> I WILL BE THERE </Button>
              </Grid>
              <Grid item align="center" xs={12}>
                <ModalRecordForm
                  fields={appoinment}
                  title="Reschedule"
                  btnLabel2="submit"
                  label="Reason"
                  onSubmit={handleSchedule}
                  className={classes.appoinment}
                >
                  {(open) => <Button type="button" className={classes.button} onClick={open} >
                    I NEED TO RESCHEDULE
                  </Button>}
                </ModalRecordForm>
              </Grid>
              <Grid item align="center">
                <ModalRecordForm
                  fields={appoinment}
                  title="Cancel Consultation"
                  label="Reason"
                  btnLabel2="submit"
                  onSubmit={handleCancel}
                  className={classes.appoinment}
                >
                  {(open) => <Button type="button" className={classes.button} onClick={open} >
                    CANCEL CONSULTATION
                  </Button>}
                </ModalRecordForm>
              </Grid>

            </div>}
    </>
  );

}
export default PatientConsultation;