
/**
 * 
 * Confirm thanks 
 * 
 */



 import React from 'react';
 import styled from 'styled-components';
 import CheckCircleOutlinedIcon from '@mui/icons-material/CheckCircleOutlined';
 import { Grid } from '@mui/material';
 import styles from './styles';
 
 const H1 = styled.h1`
  font-size: 2rem;
  font-weight: 400;
  text-align: center;
  color:white
 `;
 
 function ConfirmPage(props) {
   const { decryptedText } = props;
   const classes = styles();
   var remove_str = []
   var data_req = []
   var id = []
   var my_day = new Date()

   remove_str = decryptedText.split('&');
   data_req = remove_str[1].split('=')[1];
   id = remove_str[3].split('=')[1];
   my_day = new Date(data_req);
 
 
   return (
   
    <Grid xs={12} justify="center" container style={{display: 'flex',justifyContent: 'center', verticalAlign: 'middle', backgroundColor: '#98999a', flexDirection:'column',height: '94vh'}} className={classes.body1}>
         <H1>Thanks for your response</H1>
         <Grid className={classes.status_confirm1}>
            <Grid item align="center">
           <div className={classes.schedule_confirm}> <CheckCircleOutlinedIcon style={{ color: "green", fontSize: "35px" }} /></div>
           <><div className={classes.schedule_confirm}>Your appointment is confirmed</div>
           <div className={classes.schedule_confirm}>{remove_str[0].split('=')[1]}</div>
           <div className={classes.schedule_confirm}>{my_day.toDateString()} at {remove_str[2].split('=')[1]}</div></>
 
          </Grid>
         </Grid>
     </Grid>
 
   );
 
 }
 export default ConfirmPage;