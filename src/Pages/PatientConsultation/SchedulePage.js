
/**
 * 
 * Home
 * 
 */



import React from 'react';
import styled from 'styled-components';
import ScheduleOutlinedIcon from '@mui/icons-material/ScheduleOutlined';
import { Grid } from '@mui/material';
import styles from './styles';

const H1 = styled.h1`
 font-size: 2rem;
 font-weight: 400;
 text-align: center;
 color:white
`;

function SchedulePage(props) {
  const { decryptedText } = props;
  const classes = styles();
  var remove_str = decryptedText.split('&');
  var data_req = remove_str[1].split('=')[1];
  var id = remove_str[3].split('=')[1];
  var my_day = new Date(data_req);


  return (
      <Grid xs={12} justify="center" container style={{display: 'flex',justifyContent: 'center', verticalAlign: 'middle', backgroundColor: '#98999a', flexDirection:'column',height: '94vh'}} className={classes.body1}>
        <H1>Thanks for your response</H1>
        <Grid className={classes.status_schedule}>
          <Grid item align="center">
            <div className={classes.schedule_schedule}> <ScheduleOutlinedIcon style={{ color: "blue", fontSize: "32px" }} /></div>
            <div className={classes.schedule_schedule}>Our staff will be reaching  you to reschedule the appointment</div>
          </Grid>
        </Grid>
      </Grid>
  );

}
export default SchedulePage;