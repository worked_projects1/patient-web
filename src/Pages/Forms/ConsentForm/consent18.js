
/**
 * 
 *  Consent18
 * 
 */
 import * as React from 'react';
 import { useState, useEffect, useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Grid, Button, } from '@mui/material';
 import Styles from './styles';
 import { ImplementationFor } from '../Components/CreateRecordForm/utils'
 import { Field, reduxForm, formValueSelector } from 'redux-form'

 /**
  * 
  * @param {object} props 
  * @returns 
  */
  
 function Consent18(props) {
    const { handleSubmit, destroy,change,dispatch,initialValues } = props;
 
     const classes = Styles();
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState("");
 
     useEffect(() => {
         return () => destroy();
     }, []);
 
     dispatch(change('staff1',initialValues.providerFirst+ ' ' + initialValues.providerLast))

     const handleSignature = (val, eventValue) => {
         val == "signature1" && setSignature1(eventValue)
         val == "signature2" && setSignature2(eventValue)
         val == "signature3" && setSignature3(eventValue)
     }
 
     return <Grid className={classes.firstContainer}  >
         <Grid className={classes.secContainer} >
             <Grid item className={classes.EditForm} justify="center">
                 <span className={classes.formTitle}>Oral Surgery And Dental Extractions Consent Form</span>
             </Grid>
             <Grid item className={classes.thirdContainer}>
                 <form onSubmit={handleSubmit} id='consent18'>
 
                     <Grid item spacing={12} className={classes.content} justify="space-between">
                         <Grid>
                                 
                             <p>I understand that oral surgery and/or dental extractions include inherent risks such as, but not limited to, the following: </p>

                             <ul>
                                <li style={{padding: '10px 0px'}}>
                                This would include injuries causing numbness of the lips; the tongue; and any tissues of the mouth, cheeks, and/or face. The potential numbness may be of a temporary nature, lasting a few days, weeks, or months, or could possibly be permanent. Numbness could be the result of surgical procedures or anesthetic administration
                                </li>
                                <li style={{padding: '10px 0px'}}>
                                Some moderate bleeding may last several hours. If it is profuse, you must contact us as soon as possible. Some swelling is normal, but if it is severe, you should notify us. Swelling usually starts to subside after about 48 hours. Bruises may persist for a week or so. 
                                </li>
                                <li style={{padding: '10px 0px'}}>
                                This occurs on occasion when teeth are extracted and is a result of a blood clot not forming properly during the healing process. Dry sockets can be extremely painful if not treated.
                                </li>
                                <li style={{padding: '10px 0px'}}>
                                In some cases, the root tips of maxillary (upper) teeth lie in close proximity to sinuses. Occasionally during extraction or surgical procedures the sinus membrane may be perforated. Should this occur, it may be necessary to have the sinus surgically closed. Root tips may need to be retrieved from the sinus. 
                                </li>
                                <li style={{padding: '10px 0px'}}>
                                No matter how carefully surgical sterility is maintained, it is possible, because of the existing nonsterile oral environment, for infections to occur postoperatively. At times these may be of a serious nature. Should severe swelling occur,
                                particularly if accompanied by fever or malaise, professional attention should be received as soon as possible.
                                </li>
                                <li style={{padding: '10px 0px'}}>
                                Although extreme care will be used, the jaw, tooth toots, bone spicules, or instruments used in the extraction procedure may fracture or be fractured, requiring retrieval and possibly referral to a specialist. A decision may be made to leave a small piece of root, bone fragment, or instrument in the jaw when removal may require additional extensive surgery that could cause more harm
                                and add to the risk of complications. 
                                </li>
                                <li style={{padding: '10px 0px'}}>
                                This could occur at times no matter how carefully surgical and/or extraction procedures are performed. 
                                </li>
                                <li style={{padding: '10px 0px'}}> 
                                Because of the normal existence of bacteria in the oral cavity, the tissues of the heart, as a result of reasons known or unknown, may be susceptible to bacterial infection transmitted through blood vessels, and bacterial endocarditis (an
                                infection of the heart) could occur. It is my responsibility to inform the dentist of any known or suspected heart problems. 
                                </li>
                                <li style={{padding: '10px 0px'}}>
                                Reactions, either mild or severe, may possibly occur from anesthetics or other medications administered or prescribed. All prescription drugs must be taken according to instructions. Women using oral contraceptives must be aware that
                                antibiotics can render these contraceptives ineffective. Other methods of contraception must be utilized during the treatment period. 
                                </li>
                                <li style={{padding: '10px 0px'}}>
                                It is my responsibility to seek attention should any undue circumstances occur postoperatively, and I shall diligently follow any preoperative and postoperative
                                instructions given to me. 
                                </li>
                             </ul>

                            <p><b>Informed Consent: </b>As a patient, I have been given the opportunity to ask any questions regarding the nature and purpose of surgical treatment and have received answers to my satisfaction. I do voluntarily assume any and all possible risks, including the risk of harm, if any, that may be associated with any phase of this treatment in hopes of obtaining the desired results, which may or may not be achieved. No guarantees or promises have been made to me concerning my recovery and results of the treatment to be rendered to me.
                            The fee(s) for this service have been explained to me and are satisfactory. By signing this form, I am freely giving my consent to allow and authorize Dr .
                                <Grid item xs={6} style={{ display: 'inline-block', margin: '5px 10px', lineHeight: '1rem' }}>
                                    <Field
                                        name={"staff1"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                        disabled={true}
                                    />
                                </Grid>
                            and his associates to render any treatments necessary or advisable to my dental conditions, including any and all anesthetics and/or
                            medications
                            </p>
                            
                            <Grid  className={classes.flexContainer}>
                            <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                                <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Patient Name:</span></Grid>
                                <Grid  item style={{ margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                    name={"patientName"}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    required={true}
                                    />
                                </Grid>
                            </Grid>
                            </Grid>
                            <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                            <Grid item  md={6} className={classes.signatureContainer}>
                                <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                    
                                    <Field
                                        name={"signature1"}
                                        type={"text"}
                                        autoFocus={true}
                                        component={ImplementationFor["signature"]}
                                        id={"signature1"}
                                        onChange={(e) => handleSignature("signature1", e)}
                                    /></Grid>
                                    <Grid>Patient Signature</Grid>
                                </Grid>
                                <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                                    <Grid item md={12} className={classes.DateContainer1}>
                                        <Field
                                            name={"date1"}
                                            type={"text"}
                                            component={ImplementationFor["date"]}
                                            required={true}
                                        />
                                        
                                    </Grid>
                                    <Grid>Date</Grid>
                                    </Grid>
                            </Grid>

                             <Grid style={{ textAlign: "center", padding: '20px 0px' }}>
                                <Button className={classes.button} type="submit" form="consent18">Submit</Button>
                             </Grid>
                         </Grid>
                     </Grid>
                 </form>
             </Grid >
         </Grid >
     </Grid>
 
 }
  
 Consent18.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent18',
     enableReinitialize: true,
     touchOnChange: true,
     touchOnBlur: true,
     destroyOnUnmount: true,
     forceUnregisterOnUnmount: true
 })(Consent18);
 