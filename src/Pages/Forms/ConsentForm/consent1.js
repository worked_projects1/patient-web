
/**
 * 
 *  Consent1
 * 
 */
 import * as React from 'react';
 import { useState, useEffect } from 'react';
 import PropTypes from 'prop-types';
 import { Grid, Button, } from '@mui/material';
 import Styles from './styles';
 import { ImplementationFor } from '../Components/CreateRecordForm/utils'
 import { Field, reduxForm } from 'redux-form'
 
 /**
  * 
  * @param {object} props 
  * @returns 
  */
 
 
 function Consent1(props) {
     const { handleSubmit, destroy,change,dispatch, initialValues } = props;
 
     const classes = Styles();
    //  const [signature1, setSignature1] = useState("");
    //  const [signature2, setSignature2] = useState("");
    //  const [signature3, setSignature3] = useState("");
 
     dispatch(change('staff',initialValues.providerFirst+ ' ' + initialValues.providerLast))
 
     useEffect(() => {
         return () => destroy();
     }, []);
 
    //  const handleSignature = (val, eventValue) => {
    //      val == "signature1" && setSignature1(eventValue)
    //      val == "signature2" && setSignature2(eventValue)
    //      val == "signature3" && setSignature3(eventValue)
    //  }
 
     return <Grid className={classes.firstContainer}  >
         <Grid className={classes.secContainer} >
             <Grid item className={classes.EditForm} justify="center">
                 <span className={classes.formTitle}>Biopsy and Soft Tissue Consent Form</span>
             </Grid>
             <Grid item className={classes.thirdContainer}>
                 <form onSubmit={handleSubmit} id='consent1'>
 
                     <Grid item spacing={12} className={classes.content} justify="space-between">
                         <Grid>
                             <p><b>SECTION I.</b> Patient Information</p> 
                         <Grid className={classes.flexContainer}>
                         <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                             <Grid item  style={{ display: 'inline-block'}}><span>Patient's Name:</span></Grid>
                                 <Grid style={{ display: 'inline-block', margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                     <Field
                                             name={"patientName"}
                                             type={"text"}
                                             required={true}
                                             component={ImplementationFor["input"]}
                                         />
                             </Grid>
                         </Grid>
                         </Grid>
                             <p>1. I hereby authorize and direct 
                                 <Grid item xs={12} style={{ display: 'inline-block', margin: '5px 10px',  lineHeight: '1rem' }}>
                                     <Field
                                         name={"staff"}
                                         type={"text"}
                                         component={ImplementationFor["input"]}
                                         required={true}
                                         disabled={true}
                                     />
                                 </Grid>
                                 and the assistants of his/her choice to perform the following operation(s) or procedure(s):</p>
                             <p>
                                 2. I hereby authorize and direct the above named surgeon or other physician and/or associates and assistants to provide or arrange for the provision of such additional services or related procedures that are deemed necessary or advisable, including, but not limited to, pathology and radiology services.</p>
                             <p>
                                 3. All operations and procedures may involve risks of unsuccessful results, complications, injury, or even death, from known and unforeseen causes. I have the right to be informed of such risks, as well as the nature of the operation or procedure, the expected benefits or effects of such operation or procedure, and the available alternative methods of treatment
                                 and their risks and benefits. I also have the right to be informed as to whether my physician has any independent medical research or economic interests related to the
                                 performance of the proposed operation or procedure. Except in cases of emergency, I have the right to receive this information and to give my consent before operations or
                                 procedures are performed. I have the right to consent to or to refuse any proposed operation or procedure at any time prior to its performance. I understand that no warranty or guarantee is made as to the result or cure. </p>
                             <p>4. My signature below confirms that I authorize the pathologist to use his/her discretion in the disposition or use of any member, organ, or other tissue removed from my person during the operation or procedure identified above.</p>
                             <p>
                                 My signature on this form indicates: (1) I have read and understood the information contained herein; (2) I have been verbally informed about this operation or procedure; (3)
                                 I have had the opportunity to ask questions regarding this operation or procedure; (4) I have received all of the information I desire; and (5) I authorize and consent to the
                                 performance of the operation or procedure. </p>
                             <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                             <Grid item  md={6} className={classes.signatureContainer}>
                                 <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                     
                                     <Field
                                         name={"signature1"}
                                         type={"text"}
                                         autoFocus={true}
                                         component={ImplementationFor["signature"]}
                                         id={"signature1"}
                                        //  onChange={(e) => handleSignature("signature1", e)}
                                     /></Grid>
                                     <Grid>Patient Signature</Grid>
                                 </Grid>
                                 <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>
 
                                     <Grid item md={12} className={classes.DateContainer1}>
                                         <Field
                                             name={"date1"}
                                             type={"text"}
                                             component={ImplementationFor["date"]}
                                             required={true}
                                         />
                                         
                                     </Grid>
                                     <Grid>Date</Grid>
                                     </Grid>
                             </Grid>
                             <p>My signature below indicates that: (1) I have read and discussed the risks, benefits, and options of anesthesia (as listed on back of form) with a representative of the anesthesia
                            department; (2) I have received answers to all of my questions; and (3) I authorize and consent to the anesthesia plan discussed. </p>
                             <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                             <Grid item  md={6} className={classes.signatureContainer}>
                                 <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                     
                                     <Field
                                         name={"signature2"}
                                         type={"text"}
                                         autoFocus={true}
                                         component={ImplementationFor["signature"]}
                                        //  onChange={(e) => handleSignature("signature2", e)}
                                     /></Grid>
                                     <Grid>Patient Signature</Grid>
                                 </Grid>
                                 <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>
 
                                     <Grid item md={12} className={classes.DateContainer1}>
                                         <Field
                                             name={"date2"}
                                             type={"date"}
                                             component={ImplementationFor["date"]}
                                             required={true}
                                         />
                                         
                                     </Grid>
                                     <Grid>Date</Grid>
                                     </Grid>
                             </Grid>
                             <p><b>SECTION II.</b> Patient Declines to be Informed </p>
                             <p>Although I have been given an opportunity to be advised as to the nature and purpose of the operation or procedure and the risks, benefits, and alternatives, I specifically decline to
                             be so advised, but I give my consent to the operation. No warranty or guarantee has been made as to the result or cure. </p>
                             <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                             <Grid item  md={6} className={classes.signatureContainer}>
                                 <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                     
                                     <Field
                                         name={"signature3"}
                                         type={"text"}
                                         autoFocus={true}
                                         component={ImplementationFor["signature"]}
                                        //  onChange={(e) => handleSignature("signature3", e)}
                                     /></Grid>
                                     <Grid>Patient Signature</Grid>
                                 </Grid>
                                 <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>
                                     <Grid item md={12} className={classes.DateContainer1}>
                                         <Field
                                             name={"date3"}
                                             type={"date"}
                                             component={ImplementationFor["date"]}
                                             required={true}
                                         />
                                         
                                     </Grid>
                                     <Grid>Date</Grid>
                                     </Grid>
                             </Grid>
 
                             <Grid style={{ textAlign: "center", padding: '20px 0px' }}>
                                 <Button className={classes.button} type="submit" form="consent1">Submit</Button>
                             </Grid>
                         </Grid>
                     </Grid>
                 </form>
             </Grid >
         </Grid >
     </Grid>
 
 }
 
 Consent1.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent1',
     enableReinitialize: true,
     touchOnChange: true,
     touchOnBlur: true,
     destroyOnUnmount: true,
     forceUnregisterOnUnmount: true
 })(Consent1);
 