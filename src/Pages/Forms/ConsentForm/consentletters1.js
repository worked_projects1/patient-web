/**
 * 
 *  ConsentLetters1
 * 
 */

 import React, { useState, useEffect, useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm } from 'redux-form';
 import { Grid, Button, } from '@mui/material';
 import Styles from './styles';
 import { ImplementationFor } from '../Components/CreateRecordForm/utils'

 /**
   * 
   * @param {object} props 
   * @returns 
   */
 
 function ConsentLetters1(props) {
    const { handleSubmit, destroy,change,dispatch,initialValues } = props;
     const classes = Styles();
     const [signature1, setSignature1] = useState("");

     useEffect(() => {
        return () => destroy();
    }, []);

     dispatch(change('staff',initialValues.providerFirst+ ' ' + initialValues.providerLast))
 
     const onChange = (val, eventValue) => {
         val == "signature1" && setSignature1(eventValue)
     }
 
    return <Grid className={classes.firstContainer}  >
            <Grid className={classes.secContainer} >
             <Grid item className={classes.EditForm} justify="center">
                 <span className={classes.formTitle}>Implant Treatment Planning Letter</span>
             </Grid>
             <Grid item className={classes.thirdContainer}>
                 <form onSubmit={handleSubmit.bind(this)} id={"letters1"}>
 
                <Grid item spacing={12} className={classes.content} justify="space-between">
                <Grid>
                    <Grid  className={classes.flexContainer}>
                    <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                        <Grid item style={{ display: 'inline-block', paddingBottom: '10px'}}>
                            <span>Date:</span></Grid> 
                            <Grid item  className={classes.LowDateContainer} style={{ textAlign: 'center', margin: '0px 10px',lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                    name={"date1"}
                                    type={"date"}
                                    component={ImplementationFor["date"]}
                                    required={true}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid  className={classes.flexContainer}>
                    <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                        <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Dear </span></Grid>
                        <Grid  item style={{  margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                            <Field
                                name={"staff"}
                                type={"text"}
                                required={true}
                                component={ImplementationFor["input"]}
                                disabled={true}
                            />
                        </Grid>
                    </Grid>
                    </Grid>
                    <Grid>Thank you for visiting our office on 
                        <Grid item xs={12} md={6} style={{display:'inline-block',margin: '5px 10px',lineHeight: '1rem'}}>
                            <Field
                                name={"date2"}
                                type={"text"}
                                component={ImplementationFor["date"]}
                                required={true}
                                />
                        </Grid>
                        for consultation, evaluation, and implant treatment recommendations. I believe that through your collaboration with us we will be able to provide you with a successful treatment. 
                    </Grid>

                    <h4>IMPORTANT READING </h4>
                    <p>Please take the time to carefully read this letter and the material in the Patient Information Booklets / Brochures and Consent Forms that we gave you at your consultation. The letter explains the course of treatment and points out special considerations that are applicable to you. </p>

                    <h4>DIAGNOSIS </h4>
                    <Grid xs={12} className={classes.flexContainer}>
                    <Grid  style={{paddingBottom: '10px'}}className={classes.radioGroup} >
                        <Grid  item item xs={12}  md={5} style={{display: 'inline-block', paddingBottom: '10px'}}><span>The basic diagnosis is :</span></Grid>
                            <Grid item xs={12} md={9} style={{  margin: '0px 15px 0px 10px', lineHeight: '1rem', padding: '10px 0px' }} className={classes.blockToInline}>
                                <Field
                                        name={"diagnosis"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                        </Grid>
                        </Grid>
                    </Grid>
                    <ol>
                        <li>
                        <Grid xs={12} className={classes.flexContainer}>
                        <Grid  style={{paddingBottom: '10px'}}className={classes.radioGroup} >
                        <Grid  item item xs={12}  md={4} style={{display: 'inline-block', paddingBottom: '10px'}}><span>You are missing :</span></Grid>
                            <Grid item xs={12} md={8} style={{  margin: '0px 15px 0px 10px', lineHeight: '1rem', padding: '10px 0px' }} className={classes.blockToInline}>
                                <Field
                                        name={"missing"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                        </Grid></Grid>
                        </Grid>
                        </li>
                        <li>
                      <Grid xs={12} className={classes.flexContainer}>
                      <Grid  style={{paddingBottom: '10px'}}className={classes.radioGroup} >
                      <Grid  item item xs={12}  md={4} style={{display: 'inline-block', paddingBottom: '10px'}}><span>Bone status :</span></Grid>
                            <Grid item xs={12} md={8} style={{  margin: '0px 15px 0px 10px', lineHeight: '1rem', padding: '10px 0px' }} className={classes.blockToInline}>
                                <Field
                                        name={"status"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                        </Grid></Grid>
                        </Grid>
                        </li>
                        <li>
                      <Grid xs={12} className={classes.flexContainer}>
                      <Grid  style={{paddingBottom: '10px'}}className={classes.radioGroup} >
                      <Grid  item item xs={12}  md={4} style={{display: 'inline-block', paddingBottom: '10px'}}><span>Oral hygiene is :</span></Grid>
                            <Grid item xs={12} md={8} style={{  margin: '0px 15px 0px 10px', lineHeight: '1rem', padding: '10px 0px' }} className={classes.blockToInline}>
                                <Field
                                        name={"oral"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                        </Grid></Grid>
                        </Grid>
                        </li>
                    </ol>

                    <p>You have told me that you would prefer to wear a fixed appliance supported by implants
                    rather than a removable one.</p>
                    <Grid className={classes.rightInput}>
                            initial
                            <Grid item xs={12} style={{margin: '5px 10px', lineHeight:'1rem'}} className={classes.blockToInline}>
                                <Field
                                    name={"section1"}
                                    required={true}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    />
                            </Grid>
                        </Grid>
                        
                    <h4>SUGGESTIONS AND TREATMENT PLAN</h4>
                    <p>As we discussed, I suggest:</p>
                    <ol>
                        <li>
                        Making models of your maxilla (upper arch) and mandible (lower arch) as well as a bite registration. 
                        </li>
                        <li>
                        Preparation of a study model and/or surgical template that will determine the most favorable position to place the implants if needed.
                        </li>
                        <li>
                        Taking a CT Scan 
                        </li>
                        <li>
                        Placing implants in the locations of:
                        <Grid item xs={12} md={12} style={{margin: '10px',lineHeight:'1rem'}}>
                        <Field
                            name={"implants1"}
                            type={"text"}
                            component={ImplementationFor["input"]}
                            required={true}
                            />
                            </Grid>
                        </li>
                        <li>
                        Healing process of approximately 4 to 6 months or as needed. 
                        </li>
                        <li>
                        After healing of the implant(s), I will place a temporary superstructure on top of the implant(s) and allow the gingiva (gum) to heal around the superstructure if needed (this point however does not apply to immediately loaded cases). 
                        </li>
                        <li>
                        After healing of the gingival (gum) tissues we will start the prosthetic treatment phase. 
                        </li>

                    </ol>
                    <p>After healing, these implants will act as anchors for your fixed appliance. </p>
                    <Grid className={classes.rightInput}>
                            initial
                            <Grid item xs={12} style={{margin: '5px 10px', lineHeight:'1rem'}} className={classes.blockToInline}>
                                <Field
                                    name={"section2"}
                                    required={true}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    />
                            </Grid>
                        </Grid>

                    <h4>POSSIBLE RISKS :</h4>
                    <p>Reasonable foreseeable risks of implant treatment, as described in the Consent Form, may include mild to moderate pain or discomfort, swelling, and bruising. The remote risks of your treatment can include infection or failure of the implants to heal solidly in the bone. </p>
                    <p>I believe the likelihood of success is sufficiently high to justify this procedure, but only you can decide whether the risks are warranted. Many implants have been successfully placed and functioning for years: supporting crowns, partial dentures, and prosthetic appliances with no problems. </p>
                    <p>We are always concerned about the prognosis of dental implants and their restorations in patients who continue to smoke cigarettes, cigars, or pipes. It is well documented in our professional literature that smoking contributes to damage of the gingiva (gums) and bone around natural teeth as well as implants. This can result in infection, bone loss, and a lower immune resistance, requiring removal of teeth and implants. If you can eliminate smoking, maintain good home care, and have maintenance hygiene appointments on a regular three-month basis with your dentist, your prognosis can be expected to be very good.</p>
                    <Grid className={classes.rightInput}>
                        initial
                        <Grid item xs={12} style={{margin: '5px 10px', lineHeight:'1rem'}} className={classes.blockToInline}>
                            <Field
                                name={"section3"}
                                required={true}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                />
                        </Grid>
                    </Grid>

                    <p><b>FEES for implants </b></p>
                    <Grid>My fee for the implant surgical procedure is $ 
                        <Grid item xs={12} md={6} style={{display:'inline-block',margin: '5px 10px',lineHeight: '1rem'}}>
                            <Field
                                name={"fee1"}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                required={true}
                                />
                        </Grid>
                        per implant. I will place
                        <Grid item xs={12} md={6} style={{display:'inline-block',margin: '5px 10px',lineHeight: '1rem'}}>
                            <Field
                                name={"order"}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                required={true}
                                />
                        </Grid>
                        implants. The total estimated fee is $
                        <Grid item xs={12} md={6} style={{display:'inline-block',margin: '5px 10px',lineHeight: '1rem'}}>
                            <Field
                                name={"fee2"}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                required={true}
                                />
                        </Grid>
                        . This fee includes the surgical procedure and the routine post surgical followup. 
                    </Grid>
                    <Grid className={classes.rightInput}>
                        initial
                        <Grid item xs={12} style={{margin: '5px 10px', lineHeight:'1rem'}} className={classes.blockToInline}>
                            <Field
                                name={"section4"}
                                required={true}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                />
                        </Grid>
                    </Grid>

                    <p><b>FEES for additional services </b>(Extractions, Bone Grafting, etc.) </p>
                    <Grid>My fee for  
                        <Grid item xs={12} md={6} style={{display:'inline-block',margin: '5px 10px',lineHeight: '1rem'}}>
                            <Field
                                name={"implants2"}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                required={true}
                                />
                        </Grid>
                        $
                        <Grid item xs={12} md={6} style={{display:'inline-block',margin: '5px 10px',lineHeight: '1rem'}}>
                            <Field
                                name={"fee3"}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                required={true}
                                />
                        </Grid>
                        The total estimated fee is $
                        <Grid item xs={12} md={6} style={{display:'inline-block',margin: '5px 10px',lineHeight: '1rem'}}>
                            <Field
                                name={"fee4"}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                required={true}
                                />
                        </Grid>
                        . This fee includes the procedure (s) mentioned in this paragraph and the routine post surgical followup. 
                    </Grid>
                    <Grid className={classes.rightInput}>
                        initial
                        <Grid item xs={12} style={{margin: '5px 10px', lineHeight:'1rem'}} className={classes.blockToInline}>
                            <Field
                                name={"section5"}
                                required={true}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                />
                        </Grid>
                    </Grid>
                
                    <h4>INSURANCE</h4>
                    <p>To date, most insurance companies exclude benefits for implants. We will assist you in the completion of any insurance forms; however, it is your responsibility to pay for this treatment and to communicate with your insurance company if your claim is disputed. </p>
                    <Grid className={classes.rightInput}>
                        initial
                        <Grid item xs={12} style={{margin: '5px 10px', lineHeight:'1rem'}} className={classes.blockToInline}>
                            <Field
                                name={"section6"}
                                required={true}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                />
                        </Grid>
                    </Grid>
                    
                    <h4>ORAL HYGIENE </h4>
                    <p>After the implants have healed, your cooperation is vital for long-term success. You must maintain good oral hygiene and visit our office for periodic examinations.</p>
                    <Grid className={classes.rightInput}>
                        initial
                        <Grid item xs={12} style={{margin: '5px 10px', lineHeight:'1rem'}} className={classes.blockToInline}>
                            <Field
                                name={"section7"}
                                required={true}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                />
                        </Grid>
                    </Grid>

                    <h4>TREATMENT OPTIONS </h4>
                    <p>Implants are one option. Other options include:</p>
                    <ol>
                        <li>
                        <Grid item xs={12} className={classes.textArea}>
                                    <Field
                                        name={"option1"}
                                        type={"text"}
                                        component={ImplementationFor["textArea"]}
                                        variant="standard"
                                        required={true}
                                    />
                                </Grid>
                        </li>
                        <li>
                        <Grid item xs={12} className={classes.textArea}>
                                    <Field
                                        name={"option2"}
                                        type={"text"}
                                        component={ImplementationFor["textArea"]}
                                        variant="standard"
                                        required={true}
                                    />
                                </Grid>
                        </li>
                        <li>
                        <Grid item xs={12} className={classes.textArea}>
                                    <Field
                                        name={"option3"}
                                        type={"text"}
                                        component={ImplementationFor["textArea"]}
                                        variant="standard"
                                        required={true}
                                    />
                                </Grid>
                        </li>
                    </ol>
                    <Grid className={classes.rightInput}>
                        initial
                        <Grid item xs={12} style={{margin: '5px 10px', lineHeight:'1rem'}} className={classes.blockToInline}>
                            <Field
                                name={"section8"}
                                required={true}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                />
                        </Grid>
                    </Grid>

                    <p>The disadvantages and risks of the non-implant treatment alternatives are: </p>
                    <ol>
                        <li>
                        <Grid item xs={12} className={classes.textArea}>
                                    <Field
                                        name={"disadvantage1"}
                                        type={"text"}
                                        component={ImplementationFor["textArea"]}
                                        variant="standard"
                                        required={true}
                                    />
                                </Grid>
                        </li>
                        <li>
                        <Grid item xs={12} className={classes.textArea}>
                                    <Field
                                        name={"disadvantage2"}
                                        type={"text"}
                                        component={ImplementationFor["textArea"]}
                                        variant="standard"
                                        required={true}
                                    />
                                </Grid>
                        </li>
                        <li>
                        <Grid item xs={12} className={classes.textArea}>
                                    <Field
                                        name={"disadvantage3"}
                                        type={"text"}
                                        component={ImplementationFor["textArea"]}
                                        variant="standard"
                                        required={true}
                                    />
                                </Grid>
                        </li>
                    </ol>
                    <Grid className={classes.rightInput}>
                        initial
                        <Grid item xs={12} style={{margin: '5px 10px', lineHeight:'1rem'}} className={classes.blockToInline}>
                            <Field
                                name={"section9"}
                                required={true}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                />
                        </Grid>
                    </Grid>

                <h4>INSTRUCTIONS</h4>
                <p>When you come for your next appointment, please bring this letter and the consent forms. After all your questions have been answered to your satisfaction, you will sign the forms. We'll make copies for your records; the originals will become part of your permanent file. </p>
                <p>I feel confident that we can provide you with successful results. Please call me if you have any questions regarding this implant treatment plan.</p>

                <Grid style={{textAlign: 'center'}}>Patient's Name:
                    <Grid item xs={12} style={{display:'inline-block',margin: '5px 10px', lineHeight:'1rem'}}>
                        <Field
                            name={"patientName2"}
                            required={true}
                            type={"text"}
                            component={ImplementationFor["input"]}
                            />
                    </Grid>
                </Grid>

                <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                    <Grid item  md={6} className={classes.signatureContainer}>
                        <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                            
                            <Field
                                name={"signature1"}
                                type={"text"}
                                autoFocus={true}
                                component={ImplementationFor["signature"]}
                                id={"signature1"}
                                onChange={(e) => onChange("signature1", e)}
                            />
                        </Grid>
                        <Grid>Patient Signature</Grid>
                    </Grid>
                    <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                        <Grid item md={12} className={classes.DateContainer1}>
                            <Field
                                name={"date3"}
                                type={"text"}
                                component={ImplementationFor["date"]}
                                required={true}
                            />
                            
                        </Grid>
                        <Grid>Date</Grid>
                    </Grid>
                </Grid>
                    <Grid style={{ textAlign: "center", padding: '20px 0px' }}>
                        <Button className={classes.button} form={"letters1"} type="submit">Submit</Button>
                    </Grid>
                </Grid>
            </Grid>

            </form>
        </Grid >
    </Grid >
</Grid>
 
 }
 
 ConsentLetters1.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'letters1',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true
 })(ConsentLetters1);