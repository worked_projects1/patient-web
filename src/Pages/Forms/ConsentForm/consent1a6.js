/**
 * 
 *  Consent1a6
 * 
 */

 import React, { useState, useEffect, useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm, formValueSelector } from 'redux-form';
 import {  Grid, Button, } from '@mui/material';
 import Styles from './styles';
 import { ImplementationFor } from '../Components/CreateRecordForm/utils'
 
 /**
   * 
   * @param {object} props 
   * @returns 
   */
 function Consent1a6(props) {
    const { handleSubmit, destroy,change,dispatch,initialValues } = props;
     const classes = Styles();
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState("");
 
     useEffect(() => {
         return () => destroy();
     }, []);

     dispatch(change('staff1',initialValues.providerFirst+ ' ' + initialValues.providerLast))
     dispatch(change('staff2',initialValues.providerFirst+ ' ' + initialValues.providerLast))
 
     const onChange = (val, eventValue) => {
         val == "signature1" && setSignature1(eventValue)
         val == "signature2" && setSignature2(eventValue)
         val == "signature3" && setSignature3(eventValue)
     }
 
     return <Grid className={classes.firstContainer}  >
         <Grid className={classes.secContainer} >
             <Grid item className={classes.EditForm} justify="center">
                 <span className={classes.formTitle}>Tooth Extractions And Related Surgery Consent Form</span>
             </Grid>
             <Grid item className={classes.thirdContainer}>
                 <form onSubmit={handleSubmit.bind(this)} id={"1a6"}>
 
                     <Grid item spacing={12} className={classes.content} justify="space-between">
                         <Grid>
                             <p><span className={classes.underlinedSpan}><b>Instructions to Patient:</b> </span> Please take this document home and read it carefully. Note any questions you might have in the area provided. Bring this back to our office at your next appointment, and the doctor will review it with you before you sign it.  </p>

                             <ul>
                                <li style={{ padding: '10px 0px' }}>
                                My dentist has recommended the following procedures:
                                <Grid item xs={12} className={classes.textArea}>
                                    <Field
                                        name={"exception"}
                                        type={"text"}
                                        component={ImplementationFor["textArea"]}
                                        variant="standard"
                                        required={true}
                                    />
                                </Grid>  
                                </li>
                                <li style={{ padding: '10px 0px' }}>
                                I have been informed of the risks and complications of the recommended oral surgical procedures, anesthesia, and the proposed drugs, including, but not limited to: pain, infection, swelling, heavy or prolonged bleeding, discoloration, numbness, and tingling of the lip, tongue, chin, gums, cheeks and teeth; pain, numbness and phlebitis (inflammation of a vein) from an intravenous and/or intramuscular injection; injury to and stiffening of the neck and facial muscles; malfunction of the adjacent facial muscles for an indefinite time; change in occlusion or temporomandibular joint (TMJ) difficulty; or injury to adjacent
                                teeth, restorations in other teeth, or adjacent soft tissues. 
                                </li>
                                <li style={{ padding: '10px 0px' }}>
                                I have further been informed of other potential complications, including, but not limited to: nausea, vomiting, allergic reaction, bone fractures, bruises, delayed healing, sinus complications, openings from the sinus into the mouth, apparent facial changes, nasal changes, the possibility of secondary surgical procedures, loss of bone and the invested teeth, non-healing of the bony segments, devitalization (nerve damage that may require root canal treatment) of teeth, and relapse. 
                                </li>
                                <li style={{ padding: '10px 0px' }}>
                                I am aware that the practice of dentistry and dental surgery is not an exact science, and I acknowledge that no guarantees have been made to me concerning the success of this procedure, the associated treatment and procedures, or the Tooth Extractions And Related Surgery Consent Form postsurgical dental procedures. I am further aware that there is a risk of failure and/or that further corrective surgery may be necessary. Such a failure and remedial procedures may involve additional fees being assessed.
                                </li>
                                <li style={{ padding: '10px 0px' }}>
                                I agree and understand I am not to have anything to eat for 8 hours before my surgery or as instructed by the Dentist. 
                                </li>
                                <li style={{ padding: '10px 0px' }}>
                                I authorize 
                                <Grid item xs={12} md={6} style={{ display: 'inline-block', margin: '5px 10px', lineHeight: '1rem' }}>
                                    <Field
                                        name={"staff1"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                        disabled={true}
                                    />
                                </Grid>
                                to perform the recommended dental procedures. I agree to the type of anesthesia that he/she has discussed with me, specifically (local) (IV sedation) or (general). I agree not to operate a motor vehicle or hazardous device for at least twenty-four (24) hours after the procedure or until fully recovered from the effects of the anesthesia or drugs given for my care. I agree not to drive home after my surgery and will have a responsible adult drive me or accompany me home after my discharge from surgery.
                                </li>
                                <li style={{ padding: '10px 0px' }}>
                                If an unforeseen condition arises in the course of treatment that calls for the performance of procedures in addition to or different from that now contemplated and I am under general anesthesia or IV sedation, I further authorize and direct 
                                <Grid item xs={12} md={6} style={{ display: 'inline-block', margin: '5px 10px', lineHeight: '1rem' }}>
                                    <Field
                                        name={"staff2"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                        disabled={true}
                                    />
                                </Grid>
                                and the associates or assistants of his/her choice, to do whatever he/she/they deem necessary and advisable under the circumstances, including the decision not to proceed with the surgical procedure.
                                </li>
                                <li style={{ padding: '10px 0px' }}>
                                I agree to cooperate with the postoperative instructions of my dentist, realizing that any deviation from the instructions or lack of cooperation could result in less than optimal results. I further agree that if I do not follow my dentist’s recommendations and advice for postoperative care, my dentist may terminate the dentist-patient relationship, requiring me to seek treatment from another dentist.
                                </li>
                                <li style={{ padding: '10px 0px' }}>
                                To my knowledge, I have given an accurate report of my health history. I have also reported any prior allergic or unusual reactions to drugs, food, insect bites, anesthetics, pollens, dust, blood or body diseases, gum or skin reactions,abnormal bleeding, or any other condition relating to my health or any problems experienced with any prior medical, dental, or other health care and treatment.
                                </li>
                                <li style={{ padding: '10px 0px' }}>
                                The fee for services has been explained to me and is acceptable, and I understand that there is no warranty or guarantee as to the result of this treatment. 
                                </li>
                                <li style={{ padding: '10px 0px' }}>
                                I realize and understand that the purpose of this document is to evidence the fact that I am knowingly consenting to the oral surgical procedures recommended by my dentist. 
                                </li>
                                <li style={{ padding: '10px 0px' }}>
                                Questions I have to ask mydentist 
                                <Grid item xs={12} className={classes.textArea}>
                                        <Field
                                            name={"questions"}
                                            type={"text"}
                                            component={ImplementationFor["textArea"]}
                                            variant="standard"
                                            required={true}
                                        />
                                    </Grid>
                                </li>
                                <li style={{ padding: '10px 0px' }}>
                                I CERTIFY THAT I HAVE READ AND FULLY UNDERSTAND THE ABOVE AUTHORIZATION AND INFORMED CONSENT TO THIS PROCEDURE AND THAT ALL OF MY QUESTIONS, IF ANY, HAVE BEEN ANSWERED. I HAVE HAD THE OPPORTUNITY TO TAKE THIS FORM HOME AND REVIEW IT BEFORE SIGNING IT.
                                </li>
                             </ul>


                            <Grid  className={classes.flexContainer}>
                            <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                            <Grid item style={{ display: 'inline-block', paddingBottom: '10px'}}>
                                    <span>Name:</span></Grid> 
                                    <Grid item  className={classes.LowDateContainer} style={{ textAlign: 'center', margin: '0px 10px',lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"patientName"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                        />
                                </Grid>
                            </Grid>
                            </Grid>
                            <Grid  className={classes.flexContainer}>
                            <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                            <Grid item style={{ display: 'inline-block', paddingBottom: '10px'}}>
                                    <span>Relationship to minor patient (if applicable):</span></Grid> 
                                    <Grid item  className={classes.LowDateContainer} style={{ textAlign: 'center', margin: '0px 10px',lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"relationShip"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        // required={true}
                                        />
                                </Grid>
                            </Grid>
                            </Grid>
                                <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                                <Grid item  md={6} className={classes.signatureContainer}>
                                    <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                        
                                        <Field
                                            name={"signature1"}
                                            type={"text"}
                                            autoFocus={true}
                                            component={ImplementationFor["signature"]}
                                            id={"signature1"}
                                            onChange={(e) => onChange("signature1", e)}
                                        /></Grid>
                                        <Grid>Patient Signature</Grid>
                                    </Grid>
                                    <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                                        <Grid item md={12} className={classes.DateContainer1}>
                                            <Field
                                                name={"date1"}
                                                type={"text"}
                                                component={ImplementationFor["date"]}
                                                id={"date1"}
                                            />
                                            
                                        </Grid>
                                        <Grid>Date</Grid>
                                        </Grid>
                                </Grid>


                             <Grid style={{ textAlign: "center", padding: '20px 0px' }}>
                                 <Button form="1a6" type="submit" className={classes.button}>Submit</Button>
                             </Grid>
                         </Grid>
                     </Grid>
 
                 </form>
             </Grid >
         </Grid >
     </Grid>
 
 }
 Consent1a6.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: '1a6',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true,
 })(Consent1a6);