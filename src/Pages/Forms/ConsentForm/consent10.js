
/**
 * 
 *  Consent10
 * 
 */
 import  * as React from 'react';
 import { useState, useEffect,useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm } from 'redux-form';
 import {  Grid, Button, } from '@mui/material';
 import Styles from './styles';
import {ImplementationFor} from '../Components/CreateRecordForm/utils'

 /**
  * 
  * @param {object} props 
  * @returns 
  */

 function Consent10(props) {
     const {  handleSubmit,destroy} = props;
     const classes = Styles();
     const [showModal, setModalOpen] = useState(false);
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState(""); 
     
     useEffect(() => {
        return () => destroy();
    }, []);
    
    const onChange = (val,eventValue) => {
    val=="signature1"&&setSignature1(eventValue)
    val=="signature2"&&setSignature2(eventValue)
    val=="signature3"&&setSignature3(eventValue)

    }

     return <Grid className={classes.firstContainer}  >
            <Grid className={classes.secContainer} >
                <Grid item className={classes.EditForm} justify="center">
                <span className={classes.formTitle}>Notice of Privacy Practices/ HIPAA Consent Form </span>
                </Grid>
                <Grid item className={classes.thirdContainer}>
                <form onSubmit={handleSubmit} id='consent10'>

                    <Grid item spacing={12} className={classes.content} justify="space-between">
                    <Grid>
                        <h4 style={{color: '#ea6225'}}>NOTICE OF PRIVACY PRACTICES</h4>
                        <p>This notice describes how your health information may be used and disclosed by our office and how you can access this information. Please review this document and sign the acknowledgement attached to this form. At our office, we have always kept your health information secure and confidential. A new law requires us to continue
                        maintaining your privacy in addition to providing you with this information and adhering to the new law set in place. This law allows us to:</p>

                        <p>Use or disclose your health information to those involved in your health treatment, i.e. a review of your file by a specialist whom we may involve in your care. </p>

                        <p>We may use or disclose your health information for payment services, i.e.we may send a report of progress with your claim to your insurance company. </p>

                        <p>We may disclose your health information with our business associates, such as a dental laboratory. Business associates have a written contract with the Doctor,which requires them to protect your privacy.</p>

                        <p>We may use your information to contact you, i.e. newsletters, educational materials or other information. We may also contact you to confirm your appointments in lieu of your absence when the confirmation call is made our office may leave a message on your answering machine or with persons who answer the contact number provided. </p>

                        <p> In an emergency, we may also disclose your health information to a family member or another person responsible for your care. </p>

                        <p>We may also release your health information if required by law. Exceptions are as follows: we will not disclose your health information without prior written knowledge.</p>

                        <p>You may request in writing that we do not disclose your health information as described above, our office will inform you if that request cannot be met. You have the right to know any of the uses or disclosures of your health information beyond the normal uses. As we will need to contact you periodically we will use whatever address or telephone number you prefer</p>

                        <p>You have the right to transfer copies of your information to another practice. You have the right to view and receive a copy of your health information. A written request is needed to prepare the documents requested. All copies of x-rays and/orrecords will ensue a reasonable fee for copies of documents.</p>

                        <p>You have the right to request an amendment or change to your health information. All requests shouldbe submitted in writing to our office. If you wish to include a statement in your file, please submit this in writing. The changes requested will be made at the discretion of our office, no documents shall be removed or altered the file can only be changed with new information. </p>

                        <p>All patients have a right to receive a copy of this notice. If any details in this notice have been changed or updated, we will notify of the changes in writing. Complaints can be filed with:</p>

                        <p> Department of Health and Human Services<br />
                        200 Independent Avenue, S.W.,Room 5009F<br />
                        Washington, DC 20201</p>

                        <p>You will not be retaliated against for filing a complaint, however should you need assistance regarding your health information privacy, please contact our office.</p>
                        
                        <p>By signing below,I acknowledge that I have received and read a copy of the Notice of Privacy Practices.I am in full understanding that the notice given to me may be updated in the future, and I will be notified of any amendments to this notice by telephone or mail.</p> 

                        <Grid  className={classes.flexContainer}>
                        
                        <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                            <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Name:</span></Grid>
                            <Grid  item style={{margin: '5px 10px',lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                    name={"patientName"}
                                    type={"text"}
                                    required={true}
                                    component={ImplementationFor["input"]}
                                />
                            </Grid>
                        </Grid>
                        </Grid>
                        <Grid  className={classes.flexContainer}>
                        
                        <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                            <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Relationship to minor patient(if applicable):</span></Grid>
                            <Grid  item style={{margin: '5px 10px',lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                    name={"relation"}
                                    type={"text"}
                                    // required={true}
                                    component={ImplementationFor["input"]}
                                />
                            </Grid>
                        </Grid>
                        </Grid>

                        <Grid style={{ textAlign: 'center' }}>Patient’s Signature :
                            <Grid item xs={12} md={6} style={{ display: 'inline-block', margin: '0px 10px' }}>
                                <Field
                                    name={"signature1"}
                                    type={"text"}
                                    component={ImplementationFor["signature"]}
                                    id={"signature1"}
                                    onChange={(e) => onChange("signature1", e)}
                                />
                            </Grid>
                            <Grid style={{ display: 'inline-block', paddingBottom: '10px' }}>
                                    <span>Date:</span>
                                    <Grid item xs={6} md={6} className={classes.DateContainer} style={{ textAlign: 'center',  margin: '0px 10px' }}>
                                        <Field
                                            name={"date1"}
                                            type={"text"}
                                            component={ImplementationFor["date"]}
                                            id={"date1"}
                                        />
                                </Grid>
                            </Grid>
                        </Grid>

                            <Grid style={{textAlign:"center", padding: '20px 0px'}}>
                            <Button className={classes.button} type="submit" form="consent10">Submit</Button>
                            </Grid>
                        </Grid> 
                    </Grid>
              
            </form>
            </Grid >
            </Grid >
        </Grid>
      
 }
 
 Consent10.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent10',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true
 })(Consent10);