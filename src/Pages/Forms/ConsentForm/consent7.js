
/**
 * 
 *  Consent7
 * 
 */
 import  * as React from 'react';
 import { useState, useEffect,useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm } from 'redux-form';
 import {  Grid, Button } from '@mui/material';
 import Styles from './styles';
import {ImplementationFor} from '../Components/CreateRecordForm/utils'

 /**
  * 
  * @param {object} props 
  * @returns 
  */

 function Consent7(props) {
     const { handleSubmit, destroy,} = props;
     const classes = Styles();
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState("");   
     
     useEffect(() => {
        return () => destroy();
    }, []);
    
    const onChange = (val,eventValue) => {
    val=="signature1"&&setSignature1(eventValue)
    val=="signature2"&&setSignature2(eventValue)
    val=="signature3"&&setSignature3(eventValue)

    }

     return <Grid className={classes.firstContainer}  >
            <Grid className={classes.secContainer} >
                <Grid item className={classes.EditForm} justify="center">
                <span className={classes.formTitle}>Halcion Information and Consent Form</span>
                </Grid>
                <Grid item className={classes.thirdContainer}>
                <form onSubmit={handleSubmit} id='consent7'>

                    <Grid item spacing={12} className={classes.content} justify="space-between">
                    <Grid>
                        <p>Taking Halcion one hour prior to your dental appointment is an excellent way to minimize or eliminate anxiety that may be associated with going to the dentist. Even though it is safe, effective, and wears off rapidly after the dental visit, you should be aware of some important precautions and considerations.
                        </p>
                        <ol>
                            <li style={{padding: '10px 0px'}}>
                            This consent form and the dental treatment consent form should be signed before you take the medication. They are invalid if signed after you take the pills. 
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            The onset of Halcion is 15 to 30 minutes. Do not drive after you have taken the
                            medication. The peak effect occurs between 1 and 2 hours. After that, it starts wearing off, and most people feel back to normal after 6 to 8 hours. For safety reasons and because people react differently, you should not drive or operate machinery the remainder of the day
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            This medication should not be used if: 
                            <ol className={classes.alphaSmall}>
                                <li>
                                You are hypersensitive to benzodiazepines (e.g., Valium, Ativan, Versa)
                                </li>
                                <li>
                                You are pregnant or breast-feeding
                                </li>
                                <li>
                                You have liver or kidney disease 
                                </li>
                                <li>
                                You are taking the medicines nefazodone antidepressant (Serzone), cimetidine (Tagamet, Tagamet HB, Novocimetine, or Peptol), or levodopa (Dopar or Larodopa) for Parkinson's disease. The following substances may prolong the effects of the Halcion: Benadryl, Phenergan, Calan (verapamil), Cardizem (diltiazem), erythromycin, HIV drugs indinavir and nelfinavir, and alcohol. There may be unusual and dangerous reactions if you are currently taking illegal drugs
                                </li>
                            </ol>
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            Side effects may include: light-headedness, headache, dizziness, visual disturbances,amnesia, and nausea. In some people, oral Halcion may not work as desired.
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            Smokers will probably notice a decrease in the medication’s ability to achieve desired results.
                            </li>
                            <li>
                            You should not eat heavily prior to your appointment. You may take the medication with a small amount of food such as juice, toast, etc. Taking it with too much food can make absorption into your system unpredictable. 
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            Nitrous oxide (laughing gas) may be used in conjunction with the Halcion and local anesthetic.
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            On the way home from the dentist, your seat in the car should be in the reclined position. When at home, lie down with the head slightly elevated. Someone should stay with you for the next several hours because of possible disorientation and possible injury from falling.
                            </li>
                        </ol>
                        <p>
                        I understand these considerations and am willing to abide by the conditions stated above.I have had an opportunity to ask questions and have had them answered to my
                        satisfaction. </p>
                        <Grid className={classes.flexContainer}>
                            <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                                <Grid item  style={{ display: 'inline-block'}}><span>Patient's Name:</span></Grid>
                                    <Grid style={{ display: 'inline-block', margin: '5px 10px',  lineHeight: '1rem' }} className={classes.blockToInline}>
                                        <Field
                                                name={"patientName"}
                                                type={"text"}
                                                required={true}
                                                component={ImplementationFor["input"]}
                                            />
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                            <Grid item  md={6} className={classes.signatureContainer}>
                                <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                    
                                    <Field
                                        name={"signature1"}
                                        type={"text"}
                                        autoFocus={true}
                                        component={ImplementationFor["signature"]}
                                        id={"signature1"}
                                        onChange={(e) => onChange("signature1", e)}
                                    /></Grid>
                                    <Grid>Patient Signature</Grid>
                            </Grid>
                            <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                                <Grid item md={12} className={classes.DateContainer1}>
                                    <Field
                                        name={"date1"}
                                        type={"text"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                    />
                                    
                                </Grid>
                                <Grid>Date</Grid>
                            </Grid>
                        </Grid>

                        <Grid style={{textAlign:"center", padding: '20px 0px'}}>
                        <Button className={classes.button} type="submit" form="consent7">Submit</Button>
                        </Grid>
                    </Grid> 
                    </Grid>
              
            </form>
            </Grid >
            </Grid >
        </Grid>
      
 }
 
 Consent7.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent7',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true
 })(Consent7);