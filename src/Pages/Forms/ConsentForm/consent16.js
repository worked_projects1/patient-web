
/**
 * 
 *  Consent16
 * 
 */
 import  * as React from 'react';
 import { useState, useEffect,useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm } from 'redux-form';
 import {  Grid,Button, } from '@mui/material';
 import Styles from './styles';
import {ImplementationFor} from '../Components/CreateRecordForm/utils'

 /**
  * 
  * @param {object} props 
  * @returns 
  */

 function Consent16(props) {
    const { handleSubmit, destroy,change,dispatch,initialValues } = props;
     const classes = Styles();
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState("");
     
     useEffect(() => {
        return () => destroy();
    }, []);

    dispatch(change('staff1',initialValues.providerFirst+ ' ' + initialValues.providerLast))
    dispatch(change('dentistName',initialValues.providerFirst+ ' ' + initialValues.providerLast))
    dispatch(change('physician',initialValues.providerFirst+ ' ' + initialValues.providerLast))
    
    const onChange = (val,eventValue) => {
    val=="signature1"&&setSignature1(eventValue)
    val=="signature2"&&setSignature2(eventValue)
    val=="signature3"&&setSignature3(eventValue)

    }

    const option = [
        {
            "label": "POSSIBLE",
            "value": "POSSIBLE"
        },
        {
            "label": "NOT POSSIBLE",
            "value": "NOT POSSIBLE"
        }
    ]

     return <Grid className={classes.firstContainer}  >
            <Grid className={classes.secContainer} >
                <Grid item className={classes.EditForm} justify="center">
                <span className={classes.formTitle}>Medical Clearance For Dental Treatment</span>
                </Grid>
                <Grid item className={classes.thirdContainer}>
                <form onSubmit={handleSubmit} id='consent16'>

                    <Grid item spacing={12} className={classes.content} justify="space-between">
                    <Grid>
                    <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>Date:</span></Grid>
                        <Grid item  className={classes.LowDateContainer} style={{ textAlign: 'center', margin: '0px 10px',lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"date1"}
                                        type={"text"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                        id={"date1"}
                                    />
                                </Grid></Grid>
                                <Grid  className={classes.flexItem} style={{paddingBottom: '10px'}}> 
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}}><span>Dentist Name:</span></Grid>
                                <Grid item  style={{ margin: '5px 10px', lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"dentistName"}
                                        required={true}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        disabled={true}
                                    />
                                </Grid></Grid>
                       </Grid>

                       <Grid  className={classes.flexContainer}>
                       <Grid  className={classes.flexItem} style={{paddingBottom: '10px'}}> 
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}}><span>Patient Name:</span></Grid>
                                <Grid item  style={{ margin: '5px 10px', lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"patientName"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                        id={"patientName"}
                                    />
                                </Grid></Grid>
                                <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>DOB:</span></Grid>
                        <Grid item  className={classes.LowDateContainer} style={{ textAlign: 'center', margin: '0px 10px',lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"dob"}
                                        type={"text"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                    />
                                </Grid></Grid>
                       </Grid>

                        <h4>PATIENT CONSENT </h4>
                        <p>I agree to the release of my medical information to the above named dentist office. </p>
                        <Grid>Patient’s Signature :
                            <Grid item xs={12} md={6} style={{ display: 'inline-block', margin: '0px 10px' }}>
                                <Field
                                    name={"signature1"}
                                    type={"text"}
                                    component={ImplementationFor["signature"]}
                                    id={"signature1"}
                                    onChange={(e) => onChange("signature1", e)}
                                />
                            </Grid>
                            <Grid style={{ display: 'inline-block', paddingBottom: '10px', margin: '0px 20px' }}>
                                    <span>Date:</span>
                                    <Grid item xs={6} md={6} className={classes.DateContainer} style={{ textAlign: 'center',  margin: '0px 10px' }}>
                                        <Field
                                            name={"date2"}
                                            type={"text"}
                                            component={ImplementationFor["date"]}
                                            id={"date2"}
                                        />
                                </Grid>
                            </Grid>
                        </Grid>

                        <Grid style={{  paddingBottom: '10px'}}>
                                <span>Dear </span>
                                <Grid item xs={12} md={6} style={{  margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"staff1"}
                                        disabled={true}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid>

                        <Grid style={{  paddingBottom: '10px'}}>
                            <span>This letter is regarding our mutual patient,</span>
                            <Grid item xs={12} md={6} style={{ margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                    name={"patient"}
                                    required={true}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    
                                />
                            </Grid>
                        </Grid>

                        <p>
                        <b>The patient is scheduled for dental treatment that will involve:</b><br />
                        Tooth extraction<br />
                        Bone graft<br />
                        Placement of a dental implant<br />
                        Maxillary sinus bone augmentation 
                        </p>

                         <Grid style={{  paddingBottom: '10px'}} containner className={classes.radioGroup}>
                                <Grid>Additional Procedure (s):</Grid>
                                <Grid item xs={12} md={7} style={{  margin: '5px 10px', lineHeight: '1rem',padding: '10px 0px' }} className={classes.blockToInline}>
                                    <Field
                                        name={"procedure"}
                                        required={true}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                       
                                    />
                                </Grid></Grid>

                        {/* <Grid> */}
                        <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>Proposed date of dental treatment:</span>
                        </Grid>
                        <Grid item  className={classes.LowDateContainer} style={{ textAlign: 'center', margin: '0px 10px',lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"date3"}
                                        type={"text"}
                                        required={true}
                                        component={ImplementationFor["date"]}
                                    />
                            </Grid>
                        </Grid>
                        </Grid>

                        <p>Most patients experience the following with the above planned procedures:
                            <Grid>
                            <Grid>
                                Bleeding:
                            </Grid>
                            <Grid container xs={12}>
                            <Grid item md={3} xs={4} className={classes.initialInput}>
                                <Field
                                    name={"minimal"}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    required={true}
                                />
                            </Grid>
                            <Grid >
                            minimal( &lt;50ml)
                            </Grid>
                            <Grid item md={3} xs={4} className={classes.initialInput}>
                                <Field
                                    name={"significant"}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    required={true}
                                />
                            </Grid>
                            <Grid >
                            significant(&gt;50ml)
                            </Grid>
                            </Grid>
                            </Grid>

                            <Grid>
                            <Grid>
                            Stress and anxiety: 
                            </Grid>
                            <Grid container xs={12}>
                            <Grid item md={3} xs={4} className={classes.initialInput}>
                                <Field
                                    name={"low"}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    required={true}
                                />
                            </Grid>
                            <Grid>
                            low
                            </Grid>
                            <Grid item md={3} xs={4} className={classes.initialInput}>
                                <Field
                                    name={"medium"}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    required={true}
                                />
                            </Grid>
                            <Grid>
                            medium
                            </Grid>
                            <Grid item md={3} xs={4} className={classes.initialInput}>
                                <Field
                                    name={"high"}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    required={true}
                                />
                            </Grid>
                            <Grid>
                            high
                            </Grid>
                            </Grid>
                            </Grid>
                        </p>

                        <p>As part of these procedures, the patient will be prescribed medications for dental surgery and pain. However, due to her current medical history, I’d like to get a medical clearance to proceed with the dental treatment. </p>

                        <p>THE PATIENT HAS INFORMED US THAT SHE IS CURRENTLY TAKING
                        <Grid item xs={12} className={classes.textArea}>
                            <Field
                                name={"exception"}
                                type={"text"}
                                component={ImplementationFor["textArea"]}
                                variant="standard"
                                required={true}
                            />
                        </Grid>
                        </p>

                        <h4>Please CHECK <span className={classes.underlinedSpan}>ALL</span> THAT APPLY:</h4>
                        <Grid className={classes.initialContainer} container>
                        <Grid className={classes.initialItem}>
                        <Grid  item xs={4} md={3} className={classes.initialInput}>
                        <Field
                            name={"treatment"}
                            type={"text"}
                            component={ImplementationFor["input"]}
                            required={true}
                            />
                        </Grid>
                        <Grid item xs={8} md={8}>
                        <span className={classes.underlinedSpan}>OK</span> to <span className={classes.underlinedSpan}>PROCEED</span> with dental treatment; <span className={classes.underlinedSpan}>NO</span> special precautions and <span className={classes.underlinedSpan}>NO</span> prophylactic antibiotics are needed.
                        </Grid> 
                        </Grid>
                        <Grid className={classes.initialItem}>
                        <Grid  item xs={4} md={3} className={classes.initialInput}>
                        <Field
                            name={"antibiotic"}
                            type={"text"}
                            component={ImplementationFor["input"]}
                            required={true}
                            />
                        </Grid>
                        <Grid item xs={8} md={8} >
                        Antibiotic prophylaxis <span className={classes.underlinedSpan}>IS</span> required for dental treatment according to the current American Heart Association and/or American Academy of
                        Orthopedic Surgeons guidelines
                        </Grid> 
                        </Grid>
                        <Grid className={classes.initialItem}>
                        <Grid  item xs={4} md={3} className={classes.initialInput}>
                        <Field
                            name={"precaution"}
                            type={"text"}
                            component={ImplementationFor["input"]}
                            required={true}
                            />
                        </Grid>
                        <Grid item xs={8} md={8} >
                        Other precautions are required: (please specify) 
                        </Grid> 
                        </Grid>
                        <Grid item xs={12} className={classes.textArea}>
                            <Field
                                name={"precaution1"}
                                type={"text"}
                                component={ImplementationFor["textArea"]}
                                variant="standard"
                                required={true}
                            />
                        </Grid>
                        <Grid className={classes.initialItem}>
                        <Grid  item xs={4} md={3} className={classes.initialInput}>
                        <Field
                            name={"noProceed"}
                            type={"text"}
                            component={ImplementationFor["input"]}
                            id={"noProceed"}
                            required={true}
                            />
                        </Grid>
                        <Grid item xs={8} md={8} >
                        <span className={classes.underlinedSpan}>DO NOT</span> proceed with treatment. (Please specify reason) 
                        </Grid> 
                        </Grid>
                        <Grid item xs={12} className={classes.textArea}>
                            <Field
                                name={"noProceed1"}
                                type={"text"}
                                component={ImplementationFor["textArea"]}
                                variant="standard"
                                required={true}
                            />
                        </Grid>
                        
                    </Grid>

                        <h4>Medications:</h4>
                        <Grid className={classes.radioGroup} container>
                            <Grid item  style={{ display: 'flex',}} >
                            Amoxicillin 500 mg 24 Tabs 
                            </Grid>
                            <Grid item style={{ display: 'inline-block',  margin: '5px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"amoxicillin"}
                                    options= {option}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                />
                                </Grid>
                        </Grid>

                        <Grid className={classes.radioGroup} container>
                            <Grid item  style={{ display: 'flex',}} >
                            Peridex Oral rinse  
                            </Grid>
                            <Grid item style={{ display: 'inline-block',  margin: '5px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"peridex"}
                                    options= {option}
                                    type={"check"}
                                    component={ImplementationFor["radio"]}
                                />
                                </Grid>
                        </Grid>

                        <Grid className={classes.radioGroup} container>
                            <Grid item  style={{ display: 'flex',}} >
                            Halcion 0.50 mg 2-4 Tabs
                            </Grid>
                            <Grid item style={{ display: 'inline-block',  margin: '5px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"halcion"}
                                    options= {option}
                                    type={"check"}
                                    component={ImplementationFor["radio"]}
                                />
                                </Grid>
                        </Grid>

                        <Grid className={classes.radioGroup} container>
                            <Grid item  style={{ display: 'flex',}} >
                            Decadron 1.5 mg 7-12 Tabs 
                            </Grid>
                            <Grid item style={{ display: 'inline-block',  margin: '5px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"decadron"}
                                    options= {option}
                                    type={"check"}
                                    component={ImplementationFor["radio"]}
                                />
                                </Grid>
                        </Grid>

                        <Grid className={classes.radioGroup} container>
                            <Grid item  style={{ display: 'flex',}} >
                            Motrin 600 mg15 Tabs 
                            </Grid>
                            <Grid item style={{ display: 'inline-block',  margin: '5px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"motrin"}
                                    options= {option}
                                    type={"check"}
                                    component={ImplementationFor["radio"]}
                                />
                                </Grid>
                        </Grid>

                        <p>IF NECESSARY, CAN YOU PLEASE MODIFY THE DOSAGE (RX) AND LET US KNOW PRIOR
                            TO HER SCHEDULED SURGERY FOR 
                            <Grid  style={{ display: 'inline-block',  margin: '5px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"dosage"}
                                    required={true}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                />
                            </Grid>.</p>
                            <Grid  className={classes.flexContainer}>
                        
                        <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                            <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Physician’s name printed:</span></Grid>
                            <Grid  item style={{   margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                    name={"physician"}
                                    type={"text"}
                                    required={true}
                                    component={ImplementationFor["input"]}
                                    disabled={true}
                                />
                            </Grid>
                        </Grid>
                        </Grid>
                        <Grid style={{textAlign: 'center'}}>Physician signature:
                            <Grid item xs={12} sm={5} style={{display:'inline-block',margin: '0px 10px'}}>
                            <Field
                                name={"signature2"}
                                type={"text"}
                                component={ImplementationFor["signature"]}
                                onChange={(e) => onChange("signature1",e)}
                                disabled={true}
                                />
                            </Grid>
                            <Grid style={{ display: 'inline-block', paddingBottom: '10px'}}>
                                <span>Date:</span>
                                <Grid item xs={6} md={6} className={classes.DateContainer} style={{ textAlign: 'center',  margin: '0px 10px' }}>
                                    <Field
                                        name={"date4"}
                                        type={"text"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                        disabled={true}
                                    />
                                </Grid></Grid>
                            </Grid>

                            <Grid style={{textAlign:"center", padding: '20px 0px'}}>
                            <Button className={classes.button} type="submit" form="consent16">Submit</Button>
                            </Grid>
                        </Grid> 
                    </Grid>
              
            </form>
            </Grid >
            </Grid >
        </Grid>
      
 }
 
 Consent16.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent16',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true
 })(Consent16);