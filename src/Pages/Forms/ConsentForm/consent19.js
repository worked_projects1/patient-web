
/**
 * 
 *  Consent19
 * 
 */
 import * as React from 'react';
 import { useState, useEffect, useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Grid, Button,} from '@mui/material';
 import Styles from './styles';
 import { ImplementationFor } from '../Components/CreateRecordForm/utils'
 import { Field, reduxForm, formValueSelector } from 'redux-form'

 /**
  * 
  * @param {object} props 
  * @returns 
  */
  
 function Consent19(props) {
     const {  handleSubmit, destroy,} = props;
 
     const classes = Styles();
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState("");
     
     useEffect(() => {
         return () => destroy();
     }, []);
 
 
     const handleSignature = (val, eventValue) => {
         val == "signature1" && setSignature1(eventValue)
         val == "signature2" && setSignature2(eventValue)
         val == "signature3" && setSignature3(eventValue)
     }
 
     return <Grid className={classes.firstContainer}  >
         <Grid className={classes.secContainer} >
             <Grid item className={classes.EditForm} justify="center">
                 <span className={classes.formTitle}>Physician-Patient Arbitration Agreement</span>
             </Grid>
             <Grid item className={classes.thirdContainer}>
                 <form onSubmit={handleSubmit} id='consent19'>
 
                     <Grid item spacing={12} className={classes.content} justify="space-between">
                         <Grid>
                                 
                            <p><b>ARTICLE 1:</b> Agreement to Arbitrate: It is understood that any dispute as to medical malpractice, that is, as to whether any medical services rendered under this contract were
                            unnecessary or unauthorized or were improperly, negligently, or incompetently rendered, will be determined by submission to arbitration as provided by California law and not by a lawsuit or resort to courts process except as California law provides for judicial review of arbitration proceedings. Both parties to this contract, by entering into it, are giving up their constitutional rights to have any such dispute decided in a court of law before a jury and instead are accepting the use of arbitration. </p>

                            <p><b>ARTICLE 2:</b> All Claims Must Be Arbitrated: It is the intention of the parties that this agreement bind all parties whose claims may arise out of or relate to treatment or service provided by the physician, including any spouse or heirs of the patient and any children, whether born or unborn, at the time of the occurrence giving rise to any claim. In the case of a pregnant mother, the term “patient” herein shall mean both the mother and the mother’s expected child or children.<br /> All claims for monetary damages exceeding the jurisdictional limit of the small claims court against the physician; the physician’s partners, associates, association, corporation, or partnership; and the employees, agent, and estates of any of them must be arbitrated, including, without limitation, claims for loss of consortium, wrongful death, emotional distress, or punitive damages. Filing of any action in any court by the physician to collect any fee from the patient shall not waive the right to compel arbitration of any malpractice
                            claim.</p>

                            <p>
                            <b>ARTICLE 3:</b> Procedures and Applicable Law: A demand for arbitration must be
                            communicated in writing to all parties. Each party shall select an arbitrator (party arbitrator) within 30 days, and a third arbitrator (neutral arbitrator) shall be selected by the arbitrators
                            appointed by the parties within 30 days of a demand for a neutral arbitrator by either party. Each party to the arbitration shall pay such party’s pro rata share of the expenses and fees of the neutral arbitrator, together with other expenses of the arbitration incurred or approved by the neutral arbitrator, not including counsel fees or witness fees, and together with expenses incurred by a party for such party’s own benefit. The parties agree that the arbitrators have the immunity of a judicial officer from civil liability when acting in the capacity of arbitrator under this contract. This immunity shall supplement, not supplant, any other applicable statutory or common law. <br />
                            Either party shall have the absolute right to arbitrate separately the issues of liability and damages upon written request to the neutral arbitrator. <br />
                            The parties agree that provisions of California law applicable to health care providers shall apply to disputes within this arbitration agreement, including, but not limited to, Code of Civil Procedure Sections 340.5 and 667.7 and Civil Code Sections 3333.1 and 3333.2. Any party may bring before the arbitrators a motion for summary judgment or summary adjudication in accordance with the Code of Civil Procedure. Discovery shall be conducted pursuant to Code of Civil Procedure section 1283.05; however, depositions may be taken
                            without prior approval of the neutral arbitrator. 
                            </p>

                            <p>
                            <b>ARTICLE 4:</b> General Provisions: All claims based upon the same incident, transaction, or related circumstances shall be arbitrated in one proceeding. A claim shall be waived and forever barred if (1) on the date notice thereof is received, the claim, if asserted in a civil action, would be barred by the applicable California statute of limitations, or (2) the claimant fails to pursue the arbitration claim in accordance with the procedures prescribed
                            herein with reasonable diligence. With respect to any matter not herein expressly provided for, the California Code of Civil Procedure provisions relating to arbitration shall govern the arbitrators
                            </p>

                            <p>
                            <b>ARTICLE 5:</b> Revocation: This agreement may be revoked by written notice delivered to the physician within 30 days of signature. It is the intent of this agreement to apply to all medical services rendered anytime for any condition. 
                            </p>

                            <p>
                            <b>ARTICLE 6:</b> Retroactive Effect: If patient intends this agreement to cover services rendered before the date it is signed (including, but not limited to, emergency treatment) patient should initial below: Effective as of the date of first medial services 
                            </p>

                            <p>
                            <Grid item xs={12} className={classes.textArea}>
                                <Field
                                    name={"exception"}
                                    type={"text"}
                                    component={ImplementationFor["textArea"]}
                                    variant="standard"
                                    required={true}
                                />
                            </Grid>
                            Patient’s or Patient Representative’s Initials
                            </p>

                            <p>
                            If any provision of the arbitration agreement is held invalid or unenforceable, the remaining provisions shall remain in full force and shall not be affected by the invalidity of any other provision.
                            </p>

                            <p>
                            I understand that I have the right to receive a copy of this arbitration agreement. By my signature below, I acknowledge that I have received a copy. 
                            </p>

                            <p>
                            <b>NOTICE:</b> BY SIGNING THIS COTRACT YOU ARE AGREEING TO HAVE ANY ISSUE OF MEDICAL MALPRACTICE DECIDED BY NEUTRAL ARBITRATION, AND YOU ARE GIVING UP YOUR RIGHT TO A JURY OR COURT TRIAL. SEE ARTICLE 1 OF THIS CONTRACT. 
                            </p>
                             
                            
                            <Grid  className={classes.flexContainer}>
                            <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                                <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Patient's Name:</span></Grid>
                                <Grid  item style={{ margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                    name={"patientName"}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    required={true}
                                    />
                                </Grid>
                            </Grid>
                            </Grid>
                            <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                            <Grid item  md={6} className={classes.signatureContainer}>
                                <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                    
                                    <Field
                                        name={"signature1"}
                                        type={"text"}
                                        autoFocus={true}
                                        component={ImplementationFor["signature"]}
                                        id={"signature1"}
                                        onChange={(e) => handleSignature("signature1", e)}
                                    /></Grid>
                                    <Grid>Patient Signature</Grid>
                                </Grid>
                                <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                                    <Grid item md={12} className={classes.DateContainer1}>
                                        <Field
                                            name={"date1"}
                                            type={"text"}
                                            component={ImplementationFor["date"]}
                                            required={true}
                                        />
                                        
                                    </Grid>
                                    <Grid>Date</Grid>
                                    </Grid>
                            </Grid>

                            <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                            <Grid item  md={6} className={classes.signatureContainer}>
                                <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                    
                                    <Field
                                        name={"signature2"}
                                        type={"text"}
                                        autoFocus={true}
                                        component={ImplementationFor["signature"]}
                                        id={"signature2"}
                                        onChange={(e) => handleSignature("signature2", e)}
                                    /></Grid>
                                    <Grid> Doctor Signature</Grid>
                                </Grid>
                                <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                                    <Grid item md={12} className={classes.DateContainer1}>
                                        <Field
                                            name={"date2"}
                                            type={"text"}
                                            component={ImplementationFor["date"]}
                                            required={true}
                                        />
                                        
                                    </Grid>
                                    <Grid>Date</Grid>
                                    </Grid>
                            </Grid>

                             <Grid style={{ textAlign: "center", padding: '20px 0px' }}>
                                <Button className={classes.button}  type="submit" form="consent19">Submit</Button>
                             </Grid>
                         </Grid>
                     </Grid>
                 </form>
             </Grid >
         </Grid >
     </Grid>
 
 }
  
 Consent19.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent19',
     enableReinitialize: true,
     touchOnChange: true,
     touchOnBlur: true,
     destroyOnUnmount: true,
     forceUnregisterOnUnmount: true
 })(Consent19);
 