// import React, { Component } from 'react';
import axios from "axios";
import * as React from 'react';
 import { useState, useEffect } from 'react';
 import PropTypes from 'prop-types';
 import { Grid, Button, } from '@mui/material';
// class Upload extends React.Component {
    function Upload(props) {
    // API Endpoints
    console.log("uploading = =")
    const custom_file_upload_url = `http://192.168.0.83:9000/v4/signatureFile`;

    const [image_file, setImage] = useState(null);
    const [image_preview, setPreview] = useState('');
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         image_file: null,
    //         image_preview: '',
    //     }
    // }

    // Image Preview Handler
    const handleImagePreview = (e) => {
        let image_as_base64 = URL.createObjectURL(e.target.files[0])
        let image_as_files = e.target.files[0];

        setPreview(image_as_base64)
        setImage(image_as_files)
        // this.setState({
        //     image_preview: image_as_base64,
        //     image_file: image_as_files,
        // })
    }

    // Image/File Submit Handler
    const handleSubmitFile = () => {

        if (image_file !== null){

            let formData = new FormData();
            formData.append('customFile', image_file);
            // the image field name should be similar to your api endpoint field name
            // in my case here the field name is customFile

            axios.post(
                custom_file_upload_url,
                formData,
                {
                    headers: {
                        // "Authorization": "YOUR_API_AUTHORIZATION_KEY_SHOULD_GOES_HERE_IF_HAVE",
                        "Content-type": "multipart/form-data",
                    },                    
                }
            )
            .then(res => {
                console.log(`Success` + res.data);
            })
            .catch(err => {
                console.log(err);
            })
        }
    }


    // render from here
    // render() { 
        console.log("upload")
        return (
            <div>
                {/* image preview */}
                <img src={image_preview} alt="image preview"/>

                {/* image input field */}
                <input
                    type="file"
                    onChange={handleImagePreview}
                />
                <label>Upload file</label>
                <input type="submit" onClick={handleSubmitFile} value="Submit"/>
            </div>
        );
    // }
}

export default Upload;