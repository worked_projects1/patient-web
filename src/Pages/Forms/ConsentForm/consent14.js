
/**
 * 
 *  Consent14
 * 
 */
 import * as React from 'react';
 import { useState, useEffect, useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Grid,  Button,  } from '@mui/material';
 import Styles from './styles';
 import { ImplementationFor } from '../Components/CreateRecordForm/utils'
 import { Field, reduxForm, formValueSelector } from 'redux-form'
 // 
 /**
  * 
  * @param {object} props 
  * @returns 
  */   
  
 function Consent14(props) {
    const { handleSubmit, destroy,change,dispatch,initialValues } = props;
 
     const classes = Styles();
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState("");

     dispatch(change('staff1',initialValues.providerFirst+ ' ' + initialValues.providerLast))
     dispatch(change('staff2',initialValues.providerFirst+ ' ' + initialValues.providerLast))
     dispatch(change('staff3',initialValues.providerFirst+ ' ' + initialValues.providerLast))
     dispatch(change('staff4',initialValues.providerFirst+ ' ' + initialValues.providerLast))
     
     useEffect(() => {
         return () => destroy();
     }, []);
 
 
     const handleSignature = (val, eventValue) => {
         val == "signature1" && setSignature1(eventValue)
         val == "signature2" && setSignature2(eventValue)
         val == "signature3" && setSignature3(eventValue)
     }
 
     return <Grid className={classes.firstContainer}  >
         <Grid className={classes.secContainer} >
             <Grid item className={classes.EditForm} justify="center">
                 <span className={classes.formTitle}>Maxillary Ridge Expansion Surgery</span>
             </Grid>
             <Grid item className={classes.thirdContainer}>
                 <form onSubmit={handleSubmit} id='consent14'>
 
                     <Grid item spacing={12} className={classes.content} justify="space-between">
                         <Grid>
                                 
                             <p>
                                <Grid item xs={6} style={{ display: 'inline-block', margin: '5px 10px', lineHeight: '1rem' }}>
                                    <Field
                                        name={"staff1"}
                                        disabled={true}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                        
                                    />
                                </Grid>
                                has explained to me the various steps involved in my proposed surgery. Alternative treatment plans have been discussed, and I feel comfortable
                                in proceeding with the outlined surgery. </p>
                             <p> The following are some facts that pertain to my surgery have been explained to me.</p>
                             <p>I understand that surgery will be performed to place a bone graft material on top and within the crestal bone of the maxilla (upper jaw). The bone graft will be “sandwiched” between the existing bone. 
                             <Grid item xs={6} style={{ display: 'inline-block', margin: '5px 10px', lineHeight: '1rem' }}>
                                    <Field
                                        name={"staff2"}
                                        disabled={true}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                        
                                    />
                                </Grid>
                                has explained how this operation will be performed to my satisfaction. The graft material will consist of a bone substitute material (hydroxylapatite), tissue bank bone, or a combination of both. In approximately 5 to 6 months, after the graft has partially healed, a second procedure will be done to insert the implants into the maxilla (upper jaw) and the grafted material. In some cases it is possible to insert the implant and graft in the same operation. It is expected that the implants will become stable and act as anchors for fixed or fixeddetachable restorations. This graft is also being placed in an attempt to change the contour of the bone ridge, make it wider, or make it larger.
                             </p>
                             <p>The graft material consists of small particles. Some of the particles may work loose during the initial healing period. However, this should not influence the success of the surgery,and the particles will do no harm if swallowed. </p>
                             <p>Following the graft, it is sometimes necessary to have a second procedure called a vestibuloplasty. This operation provides more tissue to cover the grafted ridge.  </p>
                             <p>A custom surgical splint may need to be attached to the jaw (using surgical wire, surgical screws, or sutures) for 1 to 4 weeks to help keep the synthetic bone graft in place and to form it properly to the jaw bone. The patient’s existing partial or full denture can sometimes be modified for this purpose.</p>
                             <p>The surgical technique has been explained to me in detail. I understand that just as in any surgery complications can occur, including but not limited to: infection; bleeding; tissue damage; permanent numbness of the upper lip, face, or cheeks; and loss of the graft (requiring future surgical procedures).</p>
                             <p>I have been informed and understand that occasionally there are complications of surgery,drugs, and anesthesia, including but not limited to: 
                                 <ol>
                                    <li>
                                    Pain, swelling, and postoperative discoloration of the face, neck, and mouth 
                                    </li>
                                    <li>
                                    Numbness and tingling of the upper lip, teeth, gingiva (gum), cheek, and palate, which may be temporary or, rarely, permanent 
                                    </li>
                                    <li>
                                    Infection of the bone that might require further treatment, including
                                    hospitalization and surgery 
                                    </li>
                                    <li>
                                    Mal-union, delayed union, or nonunion of the bone graft replacement material to the normal bone 
                                    </li>
                                    <li>
                                    Lack of adequate bone growth into the bone graft replacement material
                                    </li>
                                    <li>
                                    Bleeding that may require extraordinary means to control hemorrhage
                                    </li>
                                    <li>
                                    Limitation of jaw function
                                    </li>
                                    <li>
                                    Stiffness of facial and jaw muscles 
                                    </li>
                                    <li>
                                    Injury to the teeth
                                    </li>
                                    <li>
                                    Referred pain to the ear, neck, and head 
                                    </li>
                                    <li>
                                    Postoperative complications involving the sinuses, nose, nasal cavity, sense of smell, infraorbital regions, and altered sensations of the cheeks and eyes 
                                    </li>
                                    <li>
                                    Postoperative unfavorable reactions to drugs, such as nausea, vomiting, and allergy
                                    </li>
                                    <li>
                                    Possible loss of teeth and bone segments 
                                    </li>
                                 </ol>
                             </p>

                             <p>Although unlikely, it is possible that the graft material will not “take,” i.e., not attach to the existing bone. The graft may be loose, and the gingival (gum) tissue over the graft may ulcerate. If this occurs the graft material will need to be removed. Further surgery, including mucosal or skin grafts, may be needed to repair lost oral tissues.</p>

                             <p>After surgery, there will be a certain amount of discomfort and swelling. I understand that I will need to be on a liquid to very soft diet for 2 to 3 weeks.</p>

                             <p>I agree to keep my teeth and mouth meticulously clean. I also agree to keep all postoperative appointments and checkups as required by my doctor. </p>

                             <p>I give my permission for  
                                <Grid item xs={6} style={{ display: 'inline-block', margin: '5px 10px', lineHeight: '1rem' }}>
                                    <Field
                                        name={"staff3"}
                                        disabled={true}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                        
                                    />
                                </Grid> 
                                to photograph and video this operation for purposes of education, publishing, and teaching. </p>

                            <p>Knowing the above facts, I freely give my consent to  
                                <Grid item xs={6} style={{ display: 'inline-block', margin: '5px 10px', lineHeight: '1rem' }}>
                                    <Field
                                        name={"staff4"}
                                        disabled={true}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                        
                                    />
                                </Grid> 
                                to perform a synthetic bone graft to my maxilla (upper jaw).
                            </p>

                        <Grid  className={classes.flexContainer}>
                        <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                            <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Name:</span></Grid>
                            <Grid  item style={{ margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                    name={"patientName"}
                                    type={"text"}
                                    required={true}
                                    component={ImplementationFor["input"]}
                                />
                            </Grid>
                        </Grid>
                        </Grid>
                        
                        <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                            <Grid item  md={6} className={classes.signatureContainer}>
                                <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                    
                                    <Field
                                        name={"signature1"}
                                        type={"text"}
                                        autoFocus={true}
                                        component={ImplementationFor["signature"]}
                                        id={"signature1"}
                                        onChange={(e) => handleSignature("signature1", e)}
                                    /></Grid>
                                <Grid> Patient Signature</Grid>
                            </Grid>
                            <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                                <Grid item md={12} className={classes.DateContainer1}>
                                    <Field
                                        name={"date1"}
                                        type={"text"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                    />
                                    
                                </Grid>
                                <Grid>Date</Grid>
                            </Grid>
                        </Grid>

                        <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                        <Grid item  md={6} className={classes.signatureContainer}>
                            <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                
                                <Field
                                    name={"signature2"}
                                    type={"text"}
                                    autoFocus={true}
                                    component={ImplementationFor["signature"]}
                                    id={"signature2"}
                                    onChange={(e) => handleSignature("signature2", e)}
                                    disabled={true}
                                /></Grid>
                                <Grid>Doctor Signature</Grid>
                            </Grid>
                            <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                                <Grid item md={12} className={classes.DateContainer1}>
                                    <Field
                                        name={"date2"}
                                        type={"text"}
                                        component={ImplementationFor["date"]}
                                        disabled={true}
                                        required={true}
                                    />
                                    
                                </Grid>
                                <Grid>Date</Grid>
                            </Grid>
                        </Grid>

                             <Grid style={{ textAlign: "center", padding: '20px 0px' }}>
                                 <Button className={classes.button}  type="submit" form="consent14">Submit</Button>
                             </Grid>
                         </Grid>
                     </Grid>
                 </form>
             </Grid >
         </Grid >
     </Grid>
 
 }
  
 Consent14.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent14',
     enableReinitialize: true,
     touchOnChange: true,
     touchOnBlur: true,
     destroyOnUnmount: true,
     forceUnregisterOnUnmount: true
 })(Consent14);
