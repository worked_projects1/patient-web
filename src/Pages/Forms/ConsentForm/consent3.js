/**
 * 
 *  Consent3
 * 
 */

 import React, { useState, useEffect, useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm } from 'redux-form';
 import { Grid, Button, } from '@mui/material';
 import Styles from './styles';
 import { ImplementationFor } from '../Components/CreateRecordForm/utils'
 
 /**
   * 
   * @param {object} props 
   * @returns 
   */
 
 function Consent3(props) {
     const { handleSubmit, destroy,change,dispatch,initialValues } = props;
     const classes = Styles();
     const [showModal, setModalOpen] = useState(false);
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState("");
 
     dispatch(change('staff',initialValues.providerFirst+ ' ' + initialValues.providerLast))
 
     useEffect(() => {
         return () => destroy();
     }, []);
 
     const onChange = (val, eventValue) => {
         val == "signature1" && setSignature1(eventValue)
     }
 
     return <Grid className={classes.firstContainer}  >
         <Grid className={classes.secContainer} >
             <Grid item className={classes.EditForm} justify="center">
                 <span className={classes.formTitle}>Block Graft Consent Form</span>
             </Grid>
             <Grid item className={classes.thirdContainer}>
                 <form onSubmit={handleSubmit} id={"consent3"}>
 
                     <Grid item spacing={12} className={classes.content} justify="space-between">
                         <Grid>
                             <p>
                                 <Grid item xs={12} md={6} style={{ display: 'inline-block', margin: '5px 10px',  lineHeight: '1rem' }}>
                                     <Field
                                         name={"staff"}
                                         disabled={true}
                                         type={"text"}
                                         component={ImplementationFor["input"]}
                                         required={true}
                                     />
                                 </Grid>
                                 has extensively discussed the proposed surgery noted above, including the expected benefits and the alternatives to treatment, if any. I have also been advised of the associated potential risks and possible complications of the proposed procedures, including, but not limited to:
                                 reaction or allergy to medications; bleeding; infection; swelling; pain; bruising; limited opening; temporomandibulr joint (TMJ) pain/dysfunction; involvement of the maxillary sinus; damage to other teeth or dental work; alveolar osteitis (dry socket or loss of the clot formed in the extracted tooth socket requiring treatment by irrigation and dressing placement); numbness of the tongue, lips, or face; nausea/vomiting; unplanned laceration, tear, burn, or abrasion of intraoral mucosa or skin with the need for additional treatment or
                                 surgical repair; and the possibility of the need for other surgery or hospitalization.
                             </p>
                             <p>
                                 I understand that if the planned procedure is performed by laser, a risk of burns to mucosa, skin, or eyes could exist. </p>
                            <p>If I am to receive medicines to relax me (IV sedation/general anesthesia, nitrous oxide–oxygen analgesia or oral sedative premedication), I have been advised of the additional risks and possible complications, i.e., nausea, vomiting, or an allergic or unexpected  reaction (if severe, allergic reactions might cause more serious respiratory [lung] or cardiovascular [heart] problems that may require treatment). In addition, there may be: pain, swelling, inflammation, or infection of the area of the injection; injury to nerves or blood vessels in the area; disorientation, confusion, or prolonged drowsiness after surgery; or cardiovascular or respiratory responses that could lead to heart attack, stroke, or death. Fortunately, these complications and side effects are not common. Wellmonitored anesthesia is generally very safe, comfortable, and well tolerated. If you have any questions, PLEASE ASK. </p>
                             <p>
                                 I understand I am to: <ul style={{ listStyleType: "circle" }}>
                                     <li>Have nothing to eat or drink 8 hours prior to surgery. </li><li>Have an escort, who is a responsible adult, drive me to the appointment, stay in the waiting room, and drive me home after my surgery. </li></ul>
                             </p>
                             <p>
                                I understand that I am to follow the oral and written instructions given to me, realizing  failure to do so may result in less than optimal results of the procedure and that I am to
                                present myself for postoperative appointments as scheduled. </p>
                             <p>I request the performance of the procedure named above and such additional procedures as may be found necessary in the judgment of my doctor during the course of this treatment. I understand unforeseen circumstances may necessitate a change in the desired procedure or, in rare cases, prevent completion of the planned procedure. </p>
                             <p>
                                 I request the administration of such anesthesia as may be considered necessary or advisable in the judgment of the doctor. </p>
                             <p>Exceptions to surgery or anesthesia, if any, are:<br></br>
                                 <Grid item xs={12} className={classes.textArea}>
                                     <Field
                                         name={"exception"}
                                         type={"text"}
                                         component={ImplementationFor["textArea"]}
                                         variant="standard"
                                         required={true}
                                     />
                                 </Grid>
                             </p>
                            <p>I request the disposal of any tissues or parts that may be necessary to remove.
                            I authorize photos, slides, x-rays, or any other viewing of my care and treatment during or after its completion to be used for the advancement of dentistry and for reimbursement purposes. However, my identity will not be revealed to the general public without my
                            permission. </p>
                             <p>I understand that there may be additional laboratory charges for specimens taken for biopsies and infections. </p>
                             <p>I have been advised of the risks of the planned procedure as noted above, the possible risks of nontreatment, and the procedures to be performed. I have the option of seeking additional opinions from other providers if desired. I have read and understand the  consent for surgery above and desire to proceed as planned. I acknowledge that no guarantees have been made to me concerning the outcome or results of the surgery or procedure. I have no unanswered questions concerning the proposed treatment.                 Please ask your doctor if you have any questions concerning this consent form. </p>
                                 <Grid className={classes.flexContainer}>
                             <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                                 <Grid item  style={{ display: 'inline-block'}}><span>Patient's Name:</span></Grid>
                                     <Grid style={{ display: 'inline-block', margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                         <Field
                                                 name={"patientName"}
                                                 type={"text"}
                                                 required={true}
                                                 component={ImplementationFor["input"]}
                                             />
                                 </Grid>
                             </Grid>
                             </Grid>
 
                             <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                             <Grid item  md={6} className={classes.signatureContainer}>
                                 <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                     
                                     <Field
                                         name={"signature1"}
                                         type={"text"}
                                         autoFocus={true}
                                         component={ImplementationFor["signature"]}
                                         id={"signature1"}
                                         onChange={(e) => onChange("signature1", e)}
                                     /></Grid>
                                     <Grid>Patient Signature</Grid>
                                 </Grid>
                                 <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>
 
                                     <Grid item md={12} className={classes.DateContainer1}>
                                         <Field
                                             name={"date1"}
                                             type={"text"}
                                             component={ImplementationFor["date"]}
                                             required={true}
                                         />
                                         
                                     </Grid>
                                     <Grid>Date</Grid>
                                     </Grid>
                             </Grid>
 
                             <Grid style={{ textAlign: "center", padding: '20px 0px' }}>
                                 <Button className={classes.button} form={"consent3"} type="submit">Submit</Button>
                             </Grid>
                         </Grid>
                     </Grid>
 
                 </form>
             </Grid >
         </Grid >
     </Grid>
 
 }
 
 Consent3.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent3',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true
 })(Consent3);