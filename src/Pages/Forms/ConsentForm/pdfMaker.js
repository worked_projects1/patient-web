
export const handlePdf = (props) => {
  const {form , windowSize} = props

  var consent1 = {
    info: {
      title: 'Biopsy and Soft Tissue Consent Form',
      filename: "myfilename.pdf"
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Biopsy and Soft Tissue Consent Form\n\n\n',
        style: 'header'
      },
      {text:[{text: 'SECTION I.',bold: true}, ' Patient Information\n']},

      { text: ['Patient\'s Name: ', { text: `${props.patientName ? "      " + props.patientName + "      " : "          "} \n\n`, style: 'liner' }] },
      {
        ol: [{ text: ['I hereby authorize and direct ', { text: `${props.staff ? "      " + props.staff + "      " : "        "}`, style: 'liner' }, ' and the assistants of his/her choice to perform the following operation(s) or procedure(s):\n\n'] },
          'I hereby authorize and direct the above named surgeon or other physician and/or associates and assistants to provide or arrange for the provision of such additional services or related procedures that are deemed necessary or advisable, including, but not limited to, pathology and radiology services.\n ',
          'All operations and procedures may involve risks of unsuccessful results, complications, injury, or even death, from known and unforeseen causes. I have the right to be informed of such risks, as well as the nature of the operation or procedure, the expected benefits or effects of such operation or procedure, and the available alternative methods of treatment and their risks and benefits. I also have the right to be informed as to whether my physician has any independent medical research or economic interests related to the performance of the proposed operation or procedure. Except in cases of emergency, I have the right to receive this information and to give my consent before operations or procedures are performed. I have the right to consent to or to refuse any proposed operation or procedure at any time prior to its performance. I understand that no warranty or guarantee is made as to the result or cure.\n\n',
          'My signature below confirms that I authorize the pathologist to use his/her discretion in the disposition or use of any member, organ, or other tissue removed from my person during the operation or procedure identified above.\n\n'
        ]
      },
      'My signature on this form indicates: (1) I have read and understood the information contained herein; (2) I have been verbally informed about this operation or procedure; (3) I have had the opportunity to ask questions regarding this operation or procedure; (4) I have received all of the information I desire; and (5) I authorize and consent to the performance of the operation or procedure. \n\n',
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},{ image: `${props.signature1}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.date1}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Patient Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
      'My signature below indicates that: (1) I have read and discussed the risks, benefits, and options of anesthesia (as listed on back of form) with a representative of the anesthesia department; (2) I have received answers to all of my questions; and (3) I authorize and consent to the anesthesia plan discussed. \n\n\n\n\n',
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},{ image: `${props.signature2}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.date2}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Patient Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
      {text:[{text: 'SECTION II.',bold: true}, ' Patient Declines to be Informed\n\n\n ']},
      'Although I have been given an opportunity to be advised as to the nature and purpose of the operation or procedure and the risks, benefits, and alternatives, I specifically decline to be so advised, but I give my consent to the operation. No warranty or guarantee has been made as to the result or cure.\n\n',
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},{ image: `${props.signature3}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.date3}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Patient Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline'
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
  };

  var consent2 = {
    info: {
      title: 'Bisphosphonate or Monoclonal Antibody Drugs Consent Form',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Bisphosphonate or Monoclonal Antibody Drugs Consent Form\n\n\n',
        style: 'header'
      },
      { text: 'CONSENT FOR DENTAL, PERIODONTAL, OR ORAL SURGICAL TREATMENT OF PATIENTS WHO HAVE RECEIVED BISPHOSPHONATE OR MONOCLONAL ANTIBODY ANTIRESORPTIVE DRUG THERAPY FOR THE TREATMENT OF OSTEOPENIA/ OSTEOPOROSIS OR CANCER \n\n', style: 'subHeader' },

      { text: ['Patient\'s Name: ', { text: `${props.patientName ? "      " + props.patientName + "      " : "          "} `, style: 'liner', margin: [20, 0, 40, 0] }, 'Date: ', { text: `${props.date1}\n\n`, style: 'liner', width: '30%', margin: [20, 20, 0, 0] }] },
      { text: 'Please initial each paragraph after reading. If you have any questions, please ask your doctor or dental assistant BEFORE initialing. \n ', style: 'bold' },
      { text: 'Due to your history of being treated with oral bisphosphonate or monoclonal antibody drugs, you should know that there is a small, but real risk of Medication Related Osteonecrosis of the Jaw (MRONJ) occurring that may be associated with dental treatment. Bone anti-resorptive drug therapies appear to adversely affect the vitality and health of Jawbones; thereby, reducing or eliminating the jawbones normal healing capacity.\n ' },

      { text: [{ text: 'Spontaneous exposure of the jawbone (Osteonecrosis) may result. This can be long-term destructive process involving the jawbone(s), that can be difficult or impossible to resolve orheal. Initials' }, { text: `${props.initials1 ? "      " + props.initials1 + "      " : "          "} \n\n`, style: 'liner', margin: [20, 0, 40, 0] }] },

      'The overall risk for patients taking an oral bisphosphonate to prevent or treat osteopenia/osteoporosis is currently estimated to be 0.01% to 0.04%; for Paget’s disease 0.26% to 1.8%;and for malignancies 0.88% to 1.15%. The incidence of dental related MRONJ appears to be dependent upon the dosage and duration of bisphosphonate therapy and the occurrence of an oral surgery or trauma event. Regardless of treatment duration, when surgery or trauma occurs, the incidence of MRONJ is as follows: for patients under treatment (or prevention) of osteoporosis the incidence is 0.09% to 0.34%; for Paget’s disease the incidence is 2.1% to 13.5%; and malignancies the incidence is 6.67% to 9.1%. \n\n',

      { text: [{ text: 'ef. J Oral Maxillofac. Surg. 65:415-423, 2007) The data and information for monoclonal antibodies appears to be similar. Initials' }, { text: `${props.initials2 ? "      " + props.initials2 + "      " : "          "} \n\n`, style: 'liner', margin: [20, 0, 40, 0] }] },

      { text: [{ text: 'Your medical/dental history is important. We must know the medications and drugs that you have taken or are currently taking. An accurate medical history, including namesof physicians is important. Initials' }, { text: `${props.initials3 ? "      " + props.initials3 + "      " : "          "} \n\n`, style: 'liner', margin: [20, 0, 40, 0] }] },

      {

        ul: [
          'If a complication occurs, short or long-term antibiotic therapy may be used to help control infection. For some patients, such therapy may cause allergic responses or have undesirable side effects such as gastric discomfort, diarrhea, colitis, etc.\n\n',
          'Despite all precautions, there may be delayed healing, osteonecrosis, loss of bone and sof tissues, pathologic fracture of the jaw, oral-cutaneous fistula (opendraining wound), or other significant complications.\n\n',
          'If osteonecrosis should occur; treatment may be prolonged and difficult, involving ongoing, intensive therapy including hospitalization, long-term antibiotics, and debridement to remove non-vital bone. Reconstructive surgery may be required, including bone grafting, metal plates and screws, and/or skin flaps and grafts.\n\n',
          'Even if there are no immediate complications from the proposed dental treatment, the area is always subject to spontaneous breakdown and infection due to the condition of the bone. Minimal trauma from a toothbrush, chewing hard food, or denture sores may trigger a complication.\n\n',
          'Long term postoperative monitoring may be required and cooperation in keeping scheduled appointments is important. Regular and frequent dental checkups with your dentist are important to monitor and attempt to prevent breakdown in your oral health.\n\n',
          { text: ['I have read the above paragraphs and understand the possible risks of undergoing my proposed planned treatment. I understand and agree to the following treatment plan:',] },
        ]
      },
      (props.exception) ? {
        margin: [20, 0, 0, 0],
        table: {
          widths: ['93%'],
          body: [
            [{ text: `${props.exception}`, border: [false, false, false, true] }]
          ]
        }, layout: {
          defaultBorder: false,
        }
      } : {
        margin: [20, 0, 0, 0],
        table: {
          widths: ['93%'],
          body: [
            [{ text: " ", border: [false, false, false, true], }]
          ]
        }, layout: {
          defaultBorder: false,
        },
      },

      '\n',
      {
        ul: [
          'I understand the importance of my health history and affirm that I havegiven any and all information that may impact my care. I understand that failure to give true health information may adversely affect my care and lead to unwanted complications.\n\n',
          'I realize that, despite all precautions that may be taken to avoidcomplications; there can be no guarantee as to the result of the proposed treatment.\n\n',
        ]
      },
      { text: 'CONSENT\n\n', bold: true, fontSize: 15 },
      'I certify that I speak, read and write English and have read and fully understand thisConsent for surgery, have had my questions answered and that all blanks were completed prior to placement of my initials and signature. \n\n ',


      {
        columns: [{ text: 'Patient’s (or Legal Guardian’s) Signature: ', margin: (windowSize > 480) ? [0, 50, 0, 0] : [0, 35, 0, 0], width: '27%' }, (props.signature1 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature1}`, fit: [150, 150], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 25, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }, { text: ['Date: ', { text: `${props.date1}`, style: 'liner' }, '\n\n'], width: '30%',margin: (windowSize > 480) ? [0, 65, 0, 0] : [0, 50, 0, 0] }]
      },

      {
        columns: [{ text: 'Doctor’s Signature: ',margin: [0, 30, 0, 0] , width: '27%' }, (props.signature2 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature2}`, fit: [150, 150], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 25, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }, { text: ['Date: ', { text: `${props.date2}`, style: 'liner' }, '\n\n'], width: '30%', margin: [0, 30, 0, 0] }]
      },
      // {text: '\n\n\n\n\n'},

      {
        columns: [{ text: 'Witness’ Signature: ', margin: [0, 30, 0, 0] , width: '27%' }, (props.signature3 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature3}`, fit: [150, 150], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 25, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }, { text: ['Date: ', { text: `${props.date3}`, style: 'liner' }, ], width: '30%',  margin: [0, 30, 0, 0] }]
      },
    ],
    styles: {
      header: {
        alignment: 'center',
        fontSize: 18,
        bold: true
      },
      subHeader: {
        alignment: 'center',
        opacity: 0.7
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',

      },
      bold: {
        bold: true,
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
  };

  var consent3 = {
    info: {
      title: 'Block Graft Consent Form',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Block Graft Consent Form\n\n\n',
        style: 'header'
      },
      { text: [{text: '  '},{ text: `${props.staff ? "      " + props.staff + "      " : "        "}`, width: '30%' ,style: 'liner', margin: [20,0,0,0] }, ' has extensively discussed the proposed surgery noted above, including the expected benefits and the alternatives to treatment, if any. I have also been advised of the associated potential risks and possible omplications of the proposed procedures, including, but not limited to: reaction or allergy to medications; leeding; infection; swelling; pain; bruising; limited opening; temporomandibulr joint (TMJ) pain/dysfunction; involvement of the maxillary sinus; damage to other teeth or dental work; alveolar osteitis (dry socket or loss of the clot formed in the extracted tooth socket requiring treatment by irrigation and dressing placement); numbness of the tongue, lips, or face; nausea/vomiting; unplanned laceration, tear, burn, or abrasion of intraoral mucosa or skin with the need for additional treatment or surgical repair; and the possibility of the need for other surgery or hospitalization. \n\n'] },
  
      'I understand that if the planned procedure is performed by laser, a risk of burns to mucosa, skin, or eyes could exist. \n ',
      'If I am to receive medicines to relax me (IV sedation/general anesthesia, nitrous oxide–oxygen analgesia or oral sedative premedication), I have been advised of the additional risks and possible complications, i.e., nausea, vomiting, or an allergic or unexpected reaction (if severe, allergic reactions might cause more serious respiratory [lung] or cardiovascular [heart] problems that may require treatment). In addition, there may be: pain, swelling, inflammation, or infection of the area of the injection; injury to nerves or blood vessels in the area; disorientation, confusion, or prolonged drowsiness after surgery; or cardiovascular or respiratory responses that could lead to heart attack, stroke, or death. Fortunately, these complications and side effects are not common. Wellmonitored anesthesia is generally very safe, comfortable, and well tolerated. If you have any questions, PLEASE ASK.\n ',
      'I understand I am to: \n',
      {
        type: 'circle',
        ul: [
          'Have nothing to eat or drink 8 hours prior to surgery.',
          'Have an escort, who is a responsible adult, drive me to the appointment, stay in the waiting room, and drive me home after my surgery.\n\n',
  
        ]
      },
  
  
      'I understand that I am to follow the oral and written instructions given to me, realizing failure to do so may result in less than optimal results of the procedure and that I am to present myself for postoperative appointments as scheduled. \n\n',
  
      'I request the performance of the procedure named above and such additional procedures as may be found necessary in the judgment of my doctor during the course of this treatment. I understand unforeseen circumstances may necessitate a change in the desired procedure or, in rare cases, prevent completion of the planned procedure. \n\n ',
  
      'I request the administration of such anesthesia as may be considered necessary or advisable in the judgment of the doctor. \n\n',
      'Exceptions to surgery or anesthesia, if any, are: ',
      (props.exception) ? {
        table: {
          widths: ['100%'],
          body: [
            [{ text: `${props.exception}`, border: [false, false, false, true] }]
          ]
        }, layout: {
          defaultBorder: false,
        }
      } : {
        table: {
          widths: ['100%'],
          body: [
            [{ text: " ", border: [false, false, false, true] }]
          ]
        }, layout: {
          defaultBorder: false,
        },
      },
      '\n',
  
      'I request the disposal of any tissues or parts that may be necessary to remove.\n I authorize photos, slides, x-rays, or any other viewing of my care and treatment during or after its completion to be used for the advancement of dentistry and for reimbursement purposes. However, my identity will not be revealed to the general public without my permission.\n\n',
      'I understand that there may be additional laboratory charges for specimens taken for biopsies and infections.\n\n ',
  
      'I have been advised of the risks of the planned procedure as noted above, the possible risks of nontreatment, and the procedures to be performed. I have the option of seeking additional opinions from other providers if desired. I have read and understand the consent for surgery above and desire to proceed as planned. I acknowledge that no guarantees have been made to me concerning the outcome or results of the surgery or procedure. I have no unanswered questions concerning the proposed treatment.Please ask your doctor if you have any questions concerning this consent form.\n \n \n \n ',
  
      { text: ['Patient\'s Name: ', { text: `${props.patientName ? "      " + props.patientName + "      " : "          "} \n\n`, style: 'liner' }], margin: [20, 0, 40, 0] },
  
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},{ image: `${props.signature1}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.date1}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Patient Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
  };

  var consent4 = {
    info: {
      title: 'Bone Grafting And Barrier Membrane Consent Form',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Bone Grafting And Barrier Membrane Consent Form\n\n\n',
        style: 'header'
      },
      { text: 'I understand that bone grafting and barrier membrane procedures include inherent risks such as, but not limited to, the following:\n\n' },
  
      {
        ul: [
          'Some discomfort is inherent in any oral surgery procedure. Grafting with materials that do not have to be harvested from your body is less painful because they do not require a donor site surgery. If the necessary bone is taken from your chin or wisdom tooth area in the back of your mouth, there will be more pain. It can be largely controlled with pain medications.\n\n',
          'No matter how carefully surgical sterility is maintained, it is possible, because of the existing nonsterile oral environment, for infections to occur postoperatively. At times these may be of a serious nature. Should severe swelling occur, particularly accompanied with fever or malaise, professional attention should be received as soon as possible.\n\n',
          "Bleeding, bruising, and swelling. Some moderate bleeding may last several hours. If bleeding is profuse, you must contact us as soon as possible. Some swelling is normal, but if it is severe, you should notify us. Swelling usually starts to subside after about 48 hours. Bruises may persist for a week or so.\n\n",
          'Success with bone and membrane grafting is high. Nevertheless, it is possible that the graft could fail. A block bone graft taken from somewhere else in your mouth may not adhere or could become infected. Despite meticulous surgery, particulate bone graft materials can migrate out of the surgery site and be lost. A membrane graft could start to dislodge. If so, the doctor should be notified. Your compliance is essential to assure success.\n\n',
          'Some bone graft and membrane materials commonly used are derived from human or other mammal sources. These grafts are thoroughly purified by different means to be free from contaminants. Signing this consent form gives your approval for the doctor to use such materials according to his/her knowledge and clinical judgment for your situation.\n\n',
          "This would include injuries causing numbness of the lips; the tongue; and/or any tissues of the mouth, cheeks, or face. This potential numbness may be of a temporary nature, lasting a few days, weeks, or months, or it could possibly be permanent. Numbness could be the result of surgical procedures or anesthetic administration.\n\n",
          'In some cases, the root tips of upper (maxillary) teeth lie in close proximity to the maxillary sinus. Occasionally, with extractions and/or grafting near the sinus, the sinus can become involved. If this happens, you will need to take special medications. Should sinus penetration occur, it might be necessary to later have the sinus surgically closed.\n\n',
          'It is your responsibility to seek attention should any undue circumstances occur postoperatively, and you should diligently follow any preoperative and postoperative instructions.\n\n'
  
        ]
      },
  
      { text: [{text: 'Informed Consent: ',bold:true},  'As a patient, I have been given the opportunity to ask any questions regarding the nature and purpose of surgical treatment and have received answers to my satisfaction. I do voluntarily assume any and all possible risks, including the risk of harm, if any, which may be associated with any phase of this treatment in hopes of obtaining the desired results, which may or may not be achieved. No guarantees or promises have been made to me concerning my recovery and the results of the treatment to be rendered to me. The fee(s) for this service have been explained to me and are satisfactory. By signing this form, I am freely giving my consent to allow and authorize ', { text: `${props.staff ? "      " + props.staff + "      " : "        "}`, style: 'liner' }, 'and his associates to render any treatments necessary or advisable to my dental conditions, including any and all anesthetics and/or medications.\n\n'] },
  
      { text: ['Patient\'s Name: ', { text: `${props.patientName ? "      " + props.patientName + "      " : "          "} \n\n`, style: 'liner' }], margin: [20, 0, 40, 0] },
  
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},{ image: `${props.signature1}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.date1}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Patient Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
  };

  var consent5 = {
    info: {
      title: 'Email Consent Form',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Email Consent Form\n\n\n',
        style: 'header'
      },
  
      {
        ul: [
          [{text: [{text: 'RISK OF USING EMIAL \n\n', bold:true}, 'Provider offers patients the opportunity to communicate by e-mail. Transmitting patient information by e-mail, however, has a number of risks that patients should consider before using e-mail. These include, but are not limited to, the following risks:\n\n']},
            {
              type: 'lower-alpha',
              ol: [
                'E-mail can be circulated, forwarded, and stored in numerous paper and electronic files.\n\n',
                'E-mail can be immediately broadcast worldwide and be received by many intended and unintended recipients.\n\n',
                'E-mail senders can easily misaddress an e-mail.\n\n',
                'E-mail is easier to falsify than handwritten or signed documents.\n\n',
                'Backup copies of e-mail may exist even after the sender or the recipient has deleted his or her copy.\n\n',
                'Employers and online services have a right to archive and inspect e-mails transmitted through their systems.\n\n',
                'E-mail can be intercepted, altered, forwarded, or used without authorization or detection.\n\n',
                'E-mail can be used to introduce viruses into computer systems.\n\n',
                'E-mail can be used as evidence in court.\n\n',
              ]
            }],
          [{text: [{text: 'CONDITIONS FOR THE USE OF E-MAIL  \n\n', bold:true}, 'Provider will use reasonable means to protect the security and confidentiality of e-mail information sent and received. However, because of the risks outlined above, Provider cannot guarantee the security and confidentiality of e-mail communication and will not be liable for improper disclosure of confidential information that is not caused by Provider’s  intentional misconduct. Thus, the patient must consent to the use of e-mail for patient information. Consent to the use of e-mail includes agreement with the following conditions:\n\n']},
            {
              type: 'lower-alpha',
              ol: [
                'All e-mails to or from the patient concerning diagnosis or treatment will be printed out and made part of the patient’s medical record. Because they are a part of the medical record, other individuals authorized to access the medical records, such as staff and billing personnel, will have access to those e-mails\n\n',

                'Provider may forward e-mails internally to Provider’s staff and agents as necessary for  diagnosis, treatment, reimbursement, and other handling. Provider will not, however, forward e-mails to independent third parties without the patient’s prior written consent, except as authorized or required by law.\n\n',
                
                'Although Provider will endeavor to read and respond promptly to an e-mail from the patient, Provider cannot guarantee that any particular e-mail will be read and responded to within any particular period of time. Thus, the patient shall not use e-mail for medical emergencies or other time-sensitive matters.\n\n',

                'If the patient’s e-mail requires or invites a response from Provider, and the patient has not received a response within a reasonable time period, it is the patient’s responsibility to follow up to determine whether the intended recipient received the email and when the recipient will respond. \n\n',

                'The patient should not use e-mail for communication regarding sensitive medical information, such as information regarding sexually transmitted diseases, AIDS/HIV, mental health, developmental disability, or substance abuse. \n\n',

                'The patient is responsible for informing the Provider of any types of information the patient does not want to be sent by e-mail in addition to those set out in 2(e) above. \n\n',

                'The patient is responsible for protecting his/her password or other means of access to email. Provider is not liable for breaches of confidentiality caused by the patient or any third party.\n\n',

                'Provider shall not engage in e-mail communication that is unlawful, such as unlawfully   practicing medicine across state lines. \n\n',

                'It is the patient’s responsibility to follow up and/or schedule an appointment if warranted. \n\n',
              ]
            }],
          [{text: [{text: "INSTRUCTIONS \n\n",bold:true}, "  To communicate by e-mail, the patient shall: \n\n",]},
            {
              type: 'lower-alpha',
              ol: [
                'Limit or avoid use of his/her employer’s computer.\n\n',

                'Inform Provider of changes in his/her e-mail address and put the patient’s name in the body of the e-mail.\n ',

                'Include the category of the communication in the e-mail’s subject line for routing purposes (e.g., billing questions).\n\n',

                'Review the e-mail to make sure it is clear and that all relevant information is provided before sending to Provider.\n\n',

                'Inform Provider that the patient received an e-mail from Provider.\n\n',

                'Take precautions to preserve the confidentiality of e-mails, such as using screen savers and safeguarding his/her computer password.\n\n',

                'Withdraw consent only by e-mail or written communication to provider.\n\n'
              ]
            }],
          {text:[{text: 'PATIENT ACKNOWLEDGMENT AND AGREEMENT \n\n', bold:true}, 'I acknowledge that I have read and fully understand this consent form. I understand the risks associated with the communication of e-mail between Provider and me and consent to the conditions outlined herein. In addition, I agree to the instructions outlined herein, as well as any other instructions that Provider may impose to communicate with patients by email. Any questions I may have had were answered.\n\n']},
  
        ]
      },
  
      { text: ['Patient\'s Name: ', { text: `${props.patientName ? "      " + props.patientName + "      " : "          "} \n\n`, style: 'liner' }], margin: [20, 0, 40, 0] },
  
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},{ image: `${props.signature1}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.date1}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Patient Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
  
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
  };

  var consent6 = {
    info: {
      title: 'Financial Policy',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Financial Policy\n\n\n',
        style: 'header'
      },
      { text: 'We are committed to providing you with the best possible dental care. If you have dental insurance, we will be happy to help you receive your maximum allowable benefits. In order to achieve these goals, we need your assistance and understanding of our financial policy. We will gladly discuss any insurance questions you may have. However, you must realize that your insurance plan is a contract between you and/or your employer and the insurance company. While the filing of claims is a courtesy that we extend to our patients, all charges are your responsibility on the date the services are rendered regardless of insurance benefits. \n\n' },
      { text: 'Payments for services are due in full at the time of services rendered.\n\n',style: 'liner' },
  
      { text: 'A 50% deposit is required for those patients who are starting any major dental treatment regardless of what your insurance will pay. We gladly accept Visa, Master Card, personal in-state checks, or cash. \n\n' },
  
      {text:'If there is still a balance owing after your insurance company pays, that balance will be due and payable within 30 days. If there is a credit on the account, we will issue a refund check. Return checks and balances older than 60 days are subject to additional collection fees and interest charges of 0.83% per month, or 10% per year.\n\n '},
      
      {text:'Charges may also be made for broken appointments and appointments cancelled without a 48 hour notice. A charge of $125.00 will be charged for any hygiene broken appointment and $200.00-$500.00 for a broken appointment with the Doctor.If an account is turned into collections, you will be responsible for all legal fees.\n\n'},
  
      { text: 'I have read the above and fully understand and accept the terms and conditions set forth. I authorize the release of any information relating to my dental claim. I understand I am responsible for all costs of dental treatment regardless of insurance.\n\n' },
  
      { text: ['Patient\'s Name: ', { text: `${props.patientName ? "      " + props.patientName + "      " : "          "} \n\n`, style: 'liner' }], margin: [20, 0, 40, 0] },

      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},{ image: `${props.signature1}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.date1}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Patient Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
  };

  var consent7 = {
    info: {
      title: 'Halcion Information and Consent Form',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Halcion Information and Consent Form\n\n\n',
        style: 'header'
      },
      { text: 'Taking Halcion one hour prior to your dental appointment is an excellent way to minimize or eliminate anxiety that may be associated with going to the dentist. Even though it is safe, effective, and wears off rapidly after the dental visit, you should be aware of some important precautions and considerations. \n\n' },
      { ol:[ 
        'This consent form and the dental treatment consent form should be signed before you take the medication. They are invalid if signed after you take the pills. \n\n',
        'The onset of Halcion is 15 to 30 minutes. Do not drive after you have taken the medication. The peak effect occurs between 1 and 2 hours. After that, it starts wearing off,and most people feel back to normal after 6 to 8 hours. For safety reasons and because people react differently, you should not drive or operate machinery the remainder of the day.\n\n',
        'This medication should not be used if',
        {
          type: 'lower-alpha',
          ol:[
            'You are hypersensitive to benzodiazepines (e.g., Valium, Ativan, Versa)\n\n',
            'You are pregnant or breast-feeding\n ',
            'You have liver or kidney disease\n\n',
            'You are taking the medicines nefazodone antidepressant (Serzone), cimetidine(Tagamet, Tagamet HB, Novocimetine, or Peptol), or levodopa (Dopar or Larodopa) for Parkinson\'s disease. The following substances may prolong the effects of the Halcion: Benadryl, Phenergan, Calan (verapamil), Cardizem(diltiazem), erythromycin, HIV drugs indinavir and nelfinavir, and alcohol. There may be unusual and dangerous reactions if you are currently taking illegal drugs\n\n'
          ]
        },
        'Side effects may include: light-headedness, headache, dizziness, visual disturbances, amnesia, and nausea. In some people, oral Halcion may not work as desired.\n\n',
        ' Smokers will probably notice a decrease in the medication’s ability to achieve desired results.\n\n',
        'You should not eat heavily prior to your appointment. You may take the medication with a small amount of food such as juice, toast, etc. Taking it with too much food can make absorption into your system unpredictable.\n\n',
        'Nitrous oxide (laughing gas) may be used in conjunction with the Halcion and local anesthetic. \n\n',
        'On the way home from the dentist, your seat in the car should be in the reclined position. When at home, lie down with the head slightly elevated. Someone should stay with you for the next several hours because of possible disorientation and possible injury from falling. \n\n'
      ]},
  
      { text: 'I understand these considerations and am willing to abide by the conditions stated above.\n\n'},
      { text: 'I have had an opportunity to ask questions and have had them answered to my satisfaction. \n\n' },
  
      { text: ['Patient\'s Name: ', { text: `${props.patientName ? "      " + props.patientName + "      " : "          "} \n\n`, style: 'liner' }], margin: [20, 0, 40, 0] },
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},{ image: `${props.signature1}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.date1}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Patient Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
      
      
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
  };

  var consent8 = {
    info: {
      title: 'Patient Health History / Financial Policy / HIPAA',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Patient Health History / Financial Policy / HIPAA\n\n\n',
        style: 'header'
      },
      {text: 'PERSONAL INFORMATION\n\n',color: '#ea6225',fontSize: '14'},
      { text: ['Patient\'s Name : ', { text: `${props.firstName ? "      " + props.firstName + "      " : "          "}`, style: 'liner' },' MI : ',{text: `${props.mi ? "      " + props.mi + "      " : "          "}`, style: 'liner'}, ' Last Name : ', { text: `${props.lastName ? "      " + props.lastName + "      " : "          "} \n\n`, style: 'liner' },], margin: [20, 0, 0, 0]},
  
      { text: [{text:'Cell Phone : '}, { text: `${props.cellPhone1 ? "      " + props.cellPhone1 + "      " : "          "}`, style: 'liner'},'\n\n'], margin: [20, 0, 0, 0] },

      { text: [{text:'  Initial Visit Date (MM/DD/YYYY) : '},{text: `${props.date1 ? "      " + props.date1 + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
     
      { text: ['Birth Date (MM/DD/YYYY) : ', { text: `${props.dob ? "      " + props.dob + "      " : "          "}`, style: 'liner' },'   Occupation : ',{text: `${props.occupation ? "      " + props.occupation + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
   
  
      { text: ['Address (Street, City, State, Zip, Country) : ', { text: `${props.address ? "      " + props.address + "      " : "          "} \n\n`, style: 'liner' },], margin: [20, 0, 0, 0] },
      
      { text: ['Email : ',{text: `${props.email ? "      " + props.email + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['Sex : ',{text: `${props.sex ? "      " + props.sex + "      " : "          "} \n\n`},], margin: [20, 0, 0, 0]},
  
      { text: ['How did you hear of us? ',{text: `${props.referredBy ? "      " + props.referredBy + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['Chief Complaint : ', { text: `${props.complaint ? "      " + props.complaint + "      " : "          "} `, style: 'liner' },'Treatment Goal : ',{text: `${props.goal ? "      " + props.goal + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      {text: 'EMERGENCY CONTACT :\n\n',color: '#ea6225',fontSize: '14'},
  
      { text: ['Full Name : ', { text: `${props.fullName ? "      " + props.fullName + "      " : "          "} `, style: 'liner' },' Relationship : ',{text: `${props.relationShip1 ? "      " + props.relationShip1 + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['Cell Phone : ', { text: `${props.cellPhone2 ? "      " + props.cellPhone2 + "      " : "          "} `, style: 'liner' },' Work Phone : ',{text: `${props.cellPhone3 ? "      " + props.cellPhone3 + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      {text: 'DENTAL INSURANCE\n\n',color: '#ea6225',fontSize: '14'},
  
      { text: ['Carrier Name : ', { text: `${props.carrier ? "      " + props.carrier + "      " : "          "}`, style: 'liner' },' Plan Name : ',{text: `${props.planName ? "      " + props.planName + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['Carrier Phone : ', { text: `${props.cellPhone4 ? "      " + props.cellPhone4 + "      " : "          "} `, style: 'liner' },' Group : ',{text: `${props.group ? "      " + props.group + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['Subscriber Name : ', { text: `${props.subscriber ? "      " + props.subscriber + "      " : "          "}`, style: 'liner' },' Subscriber ID : ',{text: `${props.subscriberId ? "      " + props.subscriberId + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Subscriber Birthday : ', { text: `${props.date3 ? "      " + props.date3 + "      " : "          "} `, style: 'liner' },' Relationship : ',{text: `${props.relationShip2 ? "      " + props.relationShip2 + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['Renewal Date : ', { text: `${props.date4 ? "      " + props.date4 + "      " : "          "} `, style: 'liner' },' Expiration Date : ',{text: `${props.date5 ? "      " + props.date5 + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      {text: 'MEDICAL INFORMATION\n\n',color: '#ea6225',fontSize: '14'},
  
      { text: ['Do You Have Medical Insurance Coverage? ',{text: `${props.insurance ? "      " + props.insurance + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Physician Name : ', { text: `${props.physician ? "      " + props.physician + "      " : "          "} `, style: 'liner' },' Last Exam Date : ',{text: `${props.date6 ? "      " + props.date6 + "      " : "          "} \n\n\n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      {text: 'MEDICAL CONDITION\n\n',color: '#ea6225',fontSize: '14'},
  
      (props.condition != undefined) ? { text: `${props.condition.join(', ') }\n\n`, margin: [20, 0, 0, 0]} : {text:''},
  
      {text: 'WOMEN\n\n',color: '#ea6225',fontSize: '14'},
  
      { text: ['Are You Pregnant or Think You May be Pregnant? ',{text: `${props.pregnant ? "      " + props.pregnant + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['If Yes, How Many Weeks? ',{text: `${props.referredBy1 ? "      " + props.referredBy1 + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Are You Nursing? ',{text: `${props.nursing ? "      " + props.nursing + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['Do You Take Birth Control Pills? ',{text: `${props.birth ? "      " + props.birth + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      {text: 'ALLERGIES\n\n',color: '#ea6225',fontSize: '14'},
  
      (props.allergies != undefined) ? { text: `${props.allergies.join(',') }\n\n`, margin: [20, 0, 0, 0]} : {text:''},
  
      {text: 'MEDICATIONS\n\n',color: '#ea6225',fontSize: '14'},
  
      { text: ['Are You Taking Any Medications At This Time? ',{text: `${props.medication ? "      " + props.medication + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['Preferred Pharmacy Information ',{text: `${props.pharmacy ? "      " + props.pharmacy + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      {text: 'DIABETIC PATIENTS \n\n',color: '#ea6225',fontSize: '14'},
  
      { text: ['Diabetes Type ',{text: `${props.diabetic ? "      " + props.diabetic + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['Type of Insulin ',{text: `${props.insulin ? "      " + props.insulin + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['Total Daily Insulin Dose ',{text: `${props.dose ? "      " + props.dose + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      {text: 'PATIENTS TAKING BISPHOSPHONATES\n\n',color: '#ea6225',fontSize: '14'},
  
      { text: ['Reason for Bisphosphonates ',{text: `${props.bisphosphonates ? "      " + props.bisphosphonates + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['Route of Administration ',{text: `${props.administration ? "      " + props.administration + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['How long you have been taking Bisphosphonates (in months) ',{text: `${props.period ? "      " + props.period + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['When was the last dose (in months) ',{text: `${props.lastDose ? "      " + props.lastDose + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['Type of osteoporosis medication ',{text: `${props.osteoporosis ? "      " + props.osteoporosis + "      " : "          "} \n\n\n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      {text: 'PATIENT DENTAL HISTORY\n\n',color: '#ea6225',fontSize: '14'},
  
      { text: ['Do Your Gums Bleed While Brushing or Flossing? ',{text: `${props.gumsBleed ? "      " + props.gumsBleed + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['Sensitive to Sweet or Sour Liquids/Foods? ',{text: `${props.sensitiveToSweet ? "      " + props.sensitiveToSweet + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['Are Your Teeth Sensitive to Hot or Cold Liquids/Foods? ',{text: `${props.sensitiveToHot ? "      " + props.sensitiveToHot + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['Have You Had any Head, Neck or Jaw Injuries? ',{text: `${props.injuries ? "      " + props.injuries + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['Do You Feel Anything Unusual in Your Mouth? ',{text: `${props.unusual ? "      " + props.unusual + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Have You Had Orthodontic Treatment? ',{text: `${props.orthodontic ? "      " + props.orthodontic + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['Do You Clench or Grind Your Teeth? ',{text: `${props.grind ? "      " + props.grind + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['Do You Like Your Smile? ',{text: `${props.smile ? "      " + props.smile + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      { text: ['any problems you have experienced in your jaw joint (TMJ) ',{text: `${props.jawJoint ? "      " + props.jawJoint + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      {text: 'AUTHORIZATION & RELEASE\n\n',color: '#ea6225',fontSize: '14'},
  
      {text: 'I consent to the diagnostic imaging procedures deemed necessary by this office. I understand that the cone beam CT scanner produces images that are intended only for evaluation of the mandibular & maxillary jawbones. This study is insufficient to detect intracranial and soft tissue disease processes. 3D CT scans are complimentary for diagnostic & treatment purposes for our in-house patients. There is a fee for replication of your scan onto a disc and/or print out, should you request for your personal use. I authorize my dentist and his/her designated staff to perform an oral examination for the purpose of diagnosis and treatment planning. Furthermore, I authorize the taking of all xrays required as a necessary part of this examination. In addition, by signing this document I understand and agree to the above policy.\n\n'},
  
      {text: 'I authorize the release of any information to third party payers and/or health practitioners for consultation purposes. I agree to be responsible for payment of full services rendered on my behalf, or my dependents behalf. I certify I have read and understand the above information and that the information I have provided is accurate.\n'},
  
      {
        columns: [{ text: 'Patient’s Signature: ', margin: (windowSize > 480) ? [0, 55, 0, 0] : [0, 50, 0, 0] , width: '27%' }, (props.signature1 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature1}`, fit: [150, 150], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 25, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }, { text: ['Date: ', { text: `${props.date7}`, style: 'liner' }], width: '30%', margin:(windowSize > 480) ? [0, 55, 0, 0] : [0, 50, 0, 0]}]
      },

      {text:'\n\n'},
  
      {text: 'FINANCIAL POLICY',color: '#ea6225',fontSize: '14'},
      
      {text:'We are committed to providing you with the best possible dental care. If you have dental insurance, we will be happy to help you receive your maximum allowable benefits. In order to achieve these goals, we need your assistance and understanding of our financial policy. We will gladly discuss any insurance questions you may have. However, you must realize that your insurance plan is a contract between you and/or your employer and the insurance company. While the filing of claims is a courtesy that we extend to our patients, all charges are your responsibility on the date the services are rendered regardless of insurance benefits\n\n'},
  
      {text: 'Payments for services are due in full at the time of services rendered.\n\n',decoration: 'underline'},
  
      {text: 'A 50% deposit is required for those patients who are starting any major dental treatment regardless of what your insurance will pay. We gladly accept Visa, Master Card, personal in-state checks, or cash.\n\n'},
  
      {text: 'If there is still a balance owing after your insurance company pays, that balance will be due and payable within 30 days. If there is a credit on the account, we will issue a refund check. Return checks and balances older than 60 days are subject to additional collection fees and interest charges of 0.83% per month, or 10% per year.\n\n'},
  
      {text: 'Charges may also be made for broken appointments and appointments cancelled without a 48 hour notice. A charge of $125.00 will be charged for any hygiene broken appointment and $200.00-$500.00 for a broken appointment with the Doctor.If an account is turned into collections, you will be responsible for all legal fees.\n\n'},
  
      {text: 'I have read the above and fully understand and accept the terms and conditions set forth. I authorize the release of any information relating to my dental claim. I understand I am responsible for all costs of dental treatment regardless of insurance. \n\n'},
  
      {
        columns: [{ text: 'Patient’s Signature: ', margin:(windowSize > 480) ? [0, 55, 0, 0] : [0, 50, 0, 0], width: '27%' }, (props.signature2 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature2}`, fit: [150, 150], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 25, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }, { text: ['Date: ', { text: `${props.date8}`, style: 'liner' }, '\n\n'], width: '30%', margin:(windowSize > 480) ? [0, 55, 0, 0] : [0, 50, 0, 0]}]
      },

      {text:'\n\n'},
  
      {text: 'NOTICE OF PRIVACY PRACTICES',color: '#ea6225',fontSize: '14'},
  
      {text: 'This notice describes how your health information may be used and disclosed by our office and how you can access this information. Please review this document and sign the acknowledgement attached to this form. At our office, we have always kept your health information secure and confidential. A new law requires us to continue maintaining your privacy in addition to providing you with this information and adhering to the new law set in place. This law allows us to:\n\n'},
  
      {text: 'Use or disclose your health information to those involved in your health treatment, i.e. a review of your file by a specialist whom we may involve in your care.\n\n'},
  
      {text: 'We may use or disclose your health information for payment services, i.e.we may send a report of progress with your claim to your insurance company.\n\n'},
  
      {text: 'We may disclose your health information with our business associates, such as a dental laboratory. Business associates have a written contract with the Doctor,which requires them to protect your privacy.\n\n'},
  
      {text: 'We may use your information to contact you, i.e. news letters, educational materials or other information. We may also contact you to confirm your appointments in lieu of your absence when the confirmation call is made our office may leave a message on your answering machine or with persons who answer the contact number provided.\n\n'},
  
      {text: 'In an emergency, we may also disclose your health information to a family member or another person responsible for your care.\n\n'},
  
      {text: 'We may also release your health information if required by law. Exceptions are as follows: we will not disclose your health information without prior written knowledge.\n\n'},
  
      {text: 'You may request in writing that we do not disclose your health information as described above, our office will inform you if that request cannot be met. You have the right to know any of the uses or disclosures of your health information beyond the normal uses. As we will need to contact you periodically we will use whatever address or telephone number you prefer.\n\n'},
  
      {text: 'You have the right to transfer copies of your information to another practice. You have the right to view and receive a copy of your health information. A written request is needed to prepare the documents requested. All copies of x-rays and/or records will ensue a reasonable fee for copies of documents.\n\n'},
  
      {text: 'You have the right to request an amendment or change to your health information. All requests shouldbe submitted in writing to our office. If you wish to include a statement in your file, please submit this in writing. The changes requested will be made at the discretion of our office, no documents shall be removed or altered the file can only be changed with new information.\n\n'},
  
      {text: 'All patients have a right to receive a copy of this notice. If any details in this notice have been changed or updated, we will notify of the changes in writing. Complaints can be filed with:\n\n'},
  
      {text: 'Department of Health and Human Services\n 200 Independent Avenue, S.W.,Room 5009F\n Washington, DC 20201\n\n'},
  
      {text: 'You will not be retaliated against for filing a complaint, however should you need assistance regarding your health information privacy, please contact our office.\n\n'},
  
      {text: 'By signing below,I acknowledge that I have received and read a copy of the Notice of Privacy Practices.I am in full understanding that the notice given to me may be updated in the future, and I will be notified of any amendments to this notice by telephone or mail.\n\n'},
    
      { text: ['Name: ', { text: `${props.patientName ? "      " + props.patientName + "      " : "          "} \n\n`, style: 'liner' }] },
  
      { text: ['Relationship to minor patient(if applicable): ', { text: `${props.relation ? "      " + props.relation + "      " : "          "} \n\n`, style: 'liner' }] },

      {text:'\n\n\n\n'},
  
      {
        columns: [{ text: 'Patient’s Signature: ', margin: (windowSize > 480) ? [0, 55, 0, 0] : [0, 50, 0, 0], width: '27%' }, (props.signature3 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature3}`, fit: [150, 150], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 25, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }, { text: ['Date: ', { text: `${props.date9}`, style: 'liner' }], width: '30%', margin:(windowSize > 480) ? [0, 55, 0, 0] : [0, 50, 0, 0] }]
      },
        
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
  };


  var consent9 = {
    info: {
      title: 'Patient Health History',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Patient Health History\n\n\n',
        style: 'header'
      },
      {text: 'PERSONAL INFORMATION\n\n',color: '#ea6225',fontSize: '14'},
      { text: ['Patient\'s Name: ', { text: `${props.firstName ? "      " + props.firstName + "      " : "          "}`, style: 'liner' },' MI: ',{text: `${props.mi ? "      " + props.mi + "      " : "          "}`, style: 'liner'}, ' Last Name: ', { text: `${props.lastName ? "      " + props.lastName + "      " : "          "} \n\n`, style: 'liner' },], margin: [20, 0, 0, 0] },
  
      { text: ['Cell Phone: ', { text: `${props.cellPhone1 ? "      " + props.cellPhone1 + "      " : "          "}`, style: 'liner' },'\n\n'], margin: [20, 0, 0, 0] },

      { text: ['Initial Visit Date (MM/DD/YYYY) : ',{text: `${props.date1 ? "     " + props.date1 + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
     
      { text: ['Birth Date (MM/DD/YYYY): ', { text: `${props.dob ? "      " + props.dob + "      " : "          "}`, style: 'liner' },'Occupation : ',{text: `${props.occupation ? "      " + props.occupation + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
   
  
      { text: ['Address (Street, City, State, Zip, Country): ', { text: `${props.address ? "      " + props.address + "      " : "          "} \n\n`, style: 'liner' },],margin: [20, 0, 0, 0] },
      
      { text: ['Email: ',{text: `${props.email ? "      " + props.email + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Sex: ',{text: `${props.sex ? "      " + props.sex + "      " : "          "} \n\n`},], margin: [20, 0, 0, 0] },
  
      { text: ['How did you hear of us? ',{text: `${props.referredBy ? "      " + props.referredBy + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Chief Complaint : ', { text: `${props.complaint ? "      " + props.complaint + "      " : "          "} `, style: 'liner' },'Treatment Goal : ',{text: `${props.goal ? "      " + props.goal + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      {text: 'EMERGENCY CONTACT\n\n',color: '#ea6225',fontSize: '14'},
  
      { text: ['Full Name : ', { text: `${props.fullName ? "      " + props.fullName + "      " : "          "} `, style: 'liner' },' Relationship : ',{text: `${props.relationShip1 ? "      " + props.relationShip1 + "      " : "          "} \n\n`, style: 'liner'},] , margin: [20, 0, 0, 0] },
  
      { text: ['Cell Phone : ', { text: `${props.cellPhone2 ? "      " + props.cellPhone2 + "      " : "          "} `, style: 'liner' },' Work Phone : ',{text: `${props.cellPhone3 ? "      " + props.cellPhone3 + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      {text: 'DENTAL INSURANCE\n\n',color: '#ea6225',fontSize: '14'},
  
      { text: ['Carrier Name : ', { text: `${props.carrier ? "      " + props.carrier + "      " : "          "}`, style: 'liner' },' Plan Name : ',{text: `${props.planName ? "      " + props.planName + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Carrier Phone : ', { text: `${props.cellPhone4 ? "      " + props.cellPhone4 + "      " : "          "} `, style: 'liner' },' Group : ',{text: `${props.group ? "      " + props.group + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Subscriber Name: ', { text: `${props.subscriber ? "      " + props.subscriber + "      " : "          "}`, style: 'liner' },' Subscriber ID : ',{text: `${props.subscriberId ? "      " + props.subscriberId + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Subscriber Birthday : ', { text: `${props.date3 ? "      " + props.date3 + "      " : "          "} `, style: 'liner' },' Relationship : ',{text: `${props.relationShip2 ? "      " + props.relationShip2 + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]  },
  
      { text: ['Renewal Date : ', { text: `${props.date4 ? "      " + props.date4 + "      " : "          "} `, style: 'liner' },'  Expiration Date : ',{text: `${props.date5 ? "      " + props.date5 + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      {text: 'MEDICAL INFORMATION\n\n',color: '#ea6225',fontSize: '14'},
  
      { text: ['Do You Have Medical Insurance Coverage? ',{text: `${props.insurance ? "      " + props.insurance + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Physician Name : ', { text: `${props.physician ? "      " + props.physician + "      " : "          "} `, style: 'liner' },'Last Exam Date : ',{text: `${props.date6 ? "      " + props.date6 + "      " : "          "} \n\n\n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      {text: 'MEDICAL CONDITION\n\n',color: '#ea6225',fontSize: '14'},
  
      (props.condition != undefined) ? { text: `${props.condition.join(', ') }\n\n`} : {text:''},
  
      {text: 'WOMEN\n\n',color: '#ea6225',fontSize: '14'},
  
      { text: ['Are You Pregnant or Think You May be Pregnant? ',{text: `${props.pregnant ? "      " + props.pregnant + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['How did you hear of us? ',{text: `${props.referredBy1 ? "      " + props.referredBy1 + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Are You Nursing? ',{text: `${props.nursing ? "      " + props.nursing + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Do You Take Birth Control Pills? ',{text: `${props.birth ? "      " + props.birth + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]},
  
      {text: 'ALLERGIES\n\n',color: '#ea6225',fontSize: '14'},
  
      (props.allergies != undefined) ? { text: `${props.allergies.join(', ') }\n\n`, margin: [20, 0, 0, 0] } : {text:''},
  
      {text: 'MEDICATIONS\n\n',color: '#ea6225',fontSize: '14'},
  
      { text: ['Are You Taking Any Medications At This Time? ',{text: `${props.medication ? "      " + props.medication + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0]  },
  
      { text: ['Preferred Pharmacy Information ',{text: `${props.pharmacy ? "      " + props.pharmacy + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      {text: 'DIABETIC PATIENTS \n\n',color: '#ea6225',fontSize: '14'},
  
      { text: ['Diabetes Type ',{text: `${props.diabetic ? "      " + props.diabetic + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Type of Insulin ',{text: `${props.insulin ? "      " + props.insulin + "      " : "          "} \n\n`, style: 'liner'},] , margin: [20, 0, 0, 0] },
  
      { text: ['Total Daily Insulin Dose ',{text: `${props.dose ? "      " + props.dose + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      {text: 'PATIENTS TAKING BISPHOSPHONATES\n\n',color: '#ea6225',fontSize: '14'},
  
      { text: ['Reason for Bisphosphonates ',{text: `${props.bisphosphonates ? "      " + props.bisphosphonates + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Route of Administration ',{text: `${props.administration ? "      " + props.administration + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['How long you have been taking Bisphosphonates (in months) ',{text: `${props.period ? "      " + props.period + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['When was the last dose (in months) ',{text: `${props.lastDose ? "      " + props.lastDose + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Type of osteoporosis medication ',{text: `${props.osteoporosis ? "      " + props.osteoporosis + "      " : "          "} \n\n\n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      {text: 'PATIENT DENTAL HISTORY\n\n',color: '#ea6225',fontSize: '14'},
  
      { text: ['Do Your Gums Bleed While Brushing or Flossing? ',{text: `${props.gumsBleed ? "      " + props.gumsBleed + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Sensitive to Sweet or Sour Liquids/Foods? ',{text: `${props.sensitiveToSweet ? "      " + props.sensitiveToSweet + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Are Your Teeth Sensitive to Hot or Cold Liquids/Foods? ',{text: `${props.sensitiveToHot ? "      " + props.sensitiveToHot + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Have You Had any Head, Neck or Jaw Injuries? ',{text: `${props.injuries ? "      " + props.injuries + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Do You Feel Anything Unusual in Your Mouth? ',{text: `${props.unusual ? "      " + props.unusual + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Have You Had Orthodontic Treatment? ',{text: `${props.orthodontic ? "      " + props.orthodontic + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Do You Clench or Grind Your Teeth? ',{text: `${props.grind ? "      " + props.grind + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['Do You Like Your Smile? ',{text: `${props.smile ? "      " + props.smile + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },
  
      { text: ['any problems you have experienced in your jaw joint (TMJ) ',{text: `${props.jawJoint ? "      " + props.jawJoint + "      " : "          "} \n\n`, style: 'liner'},], margin: [20, 0, 0, 0] },

      {text:'\n\n'},
  
      {text: 'AUTHORIZATION & RELEASE\n\n',color: '#ea6225',fontSize: '14'},
  
      {text: 'I consent to the diagnostic imaging procedures deemed necessary by this office. I understand that the cone beam CT scanner produces images that are intended only for evaluation of the mandibular & maxillary jawbones. This study is insufficient to detect intracranial and soft tissue disease processes. 3D CT scans are complimentary for diagnostic & treatment purposes for our in-house patients. There is a fee for replication of your scan onto a disc and/or print out, should you request for your personal use. I authorize my dentist and his/her designated staff to perform an oral examination for the purpose of diagnosis and treatment planning. Furthermore, I authorize the taking of all xrays required as a necessary part of this examination. In addition, by signing this document I understand and agree to the above policy.\n\n'},
  
      {text: 'I authorize the release of any information to third party payers and/or health practitioners for consultation purposes. I agree to be responsible for payment of full services rendered on my behalf, or my dependents behalf. I certify I have read and understand the above information and that the information I have provided is accurate.\n'},
  
      {
        columns: [{ text: 'Patient’s Signature: ', margin: (windowSize > 480) ? [0, 60, 0, 0] : [0, 50, 0, 0] , width: '27%' }, (props.signature1 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature1}`, fit: [150, 150], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 25, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }, { text: ['Date: ', { text: `${props.date7}`, style: 'liner' }, '\n\n'], width: '30%', margin:(windowSize > 480) ? [0, 60, 0, 0] : [0, 50, 0, 0]}]
      },
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
  };

  var consent10 = {
    info: {
      title: 'Notice of Privacy Practices/ HIPAA Consent Form',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Notice of Privacy Practices/ HIPAA Consent Form\n\n\n',
        style: 'header'
      },
      {text: 'This notice describes how your health information may be used and disclosed by our office and how you can access this information. Please review this document and sign the acknowledgement attached to this form. At our office, we have always kept your health information secure and confidential. A new law requires us to continue maintaining your privacy in addition to providing you with this information and adhering to the new law set in place. This law allows us to:\n\n'},
  
      {text: 'Use or disclose your health information to those involved in your health treatment, i.e. a review of your file by a specialist whom we may involve in your care.\n\n'},
  
      {text: 'We may use or disclose your health information for payment services, i.e.we may send a report of progress with your claim to your insurance company.\n\n'},
  
      {text: 'We may disclose your health information with our business associates, such as a dental laboratory. Business associates have a written contract with the Doctor,which requires them to protect your privacy.\n\n'},
  
      {text: 'We may use your information to contact you, i.e. news letters, educational materials or other information. We may also contact you to confirm your appointments in lieu of your absence when the confirmation call is made our office may leave a message on your answering machine or with persons who answer the contact number provided.\n\n'},
  
      {text: 'In an emergency, we may also disclose your health information to a family member or another person responsible for your care.\n\n'},
  
      {text: 'We may also release your health information if required by law. Exceptions are as follows: we will not disclose your health information without prior written knowledge.\n\n'},
  
      {text: 'You may request in writing that we do not disclose your health information as described above, our office will inform you if that request cannot be met. You have the right to know any of the uses or disclosures of your health information beyond the normal uses. As we will need to contact you periodically we will use whatever address or telephone number you prefer.\n\n'},
  
      {text: 'You have the right to transfer copies of your information to another practice. You have the right to view and receive a copy of your health information. A written request is needed to prepare the documents requested. All copies of x-rays and/orrecords will ensue a reasonable fee for copies of documents.\n\n'},
  
      {text: 'You have the right to request an amendment or change to your health information. All requests shouldbe submitted in writing to our office. If you wish to include a statement in your file, please submit this in writing. The changes requested will be made at the discretion of our office, no documents shall be removed or altered the file can only be changed with new information.\n\n'},
  
      {text: 'All patients have a right to receive a copy of this notice. If any details in this notice have been changed or updated, we will notify of the changes in writing. Complaints can be filed with:\n\n'},
  
      {text: 'Department of Health and Human Services\n 200 Independent Avenue, S.W.,Room 5009F\n Washington, DC 20201\n\n'},
  
      {text: 'You will not be retaliated against for filing a complaint, however should you need assistance regarding your health information privacy, please contact our office.\n\n'},
  
      {text: 'By signing below,I acknowledge that I have received and read a copy of the Notice of Privacy Practices.I am in full understanding that the notice given to me may be updated in the future, and I will be notified of any amendments to this notice by telephone or mail.\n\n'},
    
      { text: ['Name: ', { text: `${props.patientName ? "      " + props.patientName + "      " : "          "} \n\n`, style: 'liner' }] },
  
      { text: ['Relationship to minor patient(if applicable): ', { text: `${props.relation ? "      " + props.relation + "      " : "          "} \n\n`, style: 'liner' }] },
  
      {
        columns: [{ text: 'Patient’s Signature: ', margin: (windowSize > 480) ? [0, 60, 0, 0] : [0, 50, 0, 0], width: '27%' }, (props.signature1 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature1}`, fit: [150, 150], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 25, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }, { text: ['Date: ', { text: `${props.date1}`, style: 'liner' }, '\n\n'], width: '30%', margin:(windowSize > 480) ? [0, 60, 0, 0] : [0, 50, 0, 0] }]
      },
      
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
   };

   var consent11 = {
    info: {
      title: 'ICOI Implant Placement Consent Form',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'ICOI Implant Placement Consent Form\n\n\n',
        style: 'header'
      },
      {
        ol:[
          'I have been informed about and understand the purpose and the nature of the implant surgery procedure. I understand what is necessary to accomplish the placement of the implant under the gingiva (gum) or in the bone.\n\n',
          'My doctor has carefully examined my mouth. Alternatives to this treatment have been explained. I have tried or considered these methods, but I desire an implant to help secure the restorations for missing teeth.\n\n',
          'I have further been informed of the possible risks and complications involved with surgery, drugs, and anesthesia. Such complications include pain, swelling, infection, and discoloration. Numbness of the lip, tongue, chin, cheek, or teeth may occur. The exact duration may not be determinable and may be irreversible. Also possible are inflammation of a vein, injury to existing teeth, bone fractures, sinus penetration, delayed healing, allergic reactions to drugs or medications used, etc.\n\n',
          'I understand that if nothing is done, any of the following could occur: bone disease, loss of bone, gingival (gum) tissue inflammation, infection, sensitivity, and looseness of teeth, requiring subsequent extraction. Also possible are temporomandibular joint (TMJ) problems, headaches, referred pains to the back of the neck and facial muscles, and tired muscles when chewing.\n\n',
          'My doctor has explained that there is no method to accurately predict the gingival(gum) and bone healing capabilities in any given patient following the placement of the implant.\n\n',
          'It has been explained that in some instances implants fail and must be removed. I have been informed and understand that the practice of dentistry is not an exact science; no guarantees or assurance as to the outcome of treatment or surgery can be made.\n\n',
          'I understand that excessive smoking, alcohol, or sugar may affect gingival (gum) healing and may limit the success of the implant. I agree to follow my doctor\'s home care instructions. I agree to report to my doctor for regular examinations as instructed.\n\n',
          'I agree to the type of anesthesia chosen by the doctor. I agree not to operate a motor vehicle or hazardous device for at least 24 hours or more until fully recovered from the effects of the anesthesia or drugs given for my care. \n\n',
          'To my knowledge I have given an accurate report of my physical and mental health history. I have also reported any prior allergic or unusual reactions to drugs, food, insect bites, anesthetics, pollens, or dust; blood or body diseases; gingival (gum) or skin reactions; abnormal bleeding; or any other conditions related to my health.\n\n',
          'I consent to photography, filming, recording, and x-rays of the procedure to be performed for the advancement of implant dentistry, provided my identity is not revealed.\n\n',
          ' I request and authorize medical/dental services for me, including implants and other surgery. I fully understand that during and following the contemplated procedure, surgery, or treatment, conditions may become apparent that warrant, in the judgment of the doctor, additional or alternative treatment pertinent to the success of comprehensive treatment. I also approve any modification in design, materials, or care, if it is felt this is in my best interest. \n\n',
        ]},
  
      { text: ['Patient\'s Name: ', { text: `${props.patientName ? "      " + props.patientName + "      " : "          "} \n\n`, style: 'liner' }] },
  
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},{ image: `${props.signature1}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.date1}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Patient Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
      
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},(props.signature2 != undefined) ?{ image: `${props.signature2}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' }: {text: ' ',border: [false, false, false, true],},{},
             { text: `${props.date2}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Doctor Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
      
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
   };

   var consent12 = {
    info: {
      title: 'Implant Surgery And Anesthesia Consent Form',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Implant Surgery And Anesthesia Consent Form\n\n\n',
        style: 'header'
      },
      {text:[{text:'Instructions to Patient: ',bold: true},'Please take this document home and read it carefully. Note any  questions you might have in the area provided in paragraph 15. Bring this back to our office at your next appointment, and the doctor will review it with you before signing on page 4.\n\n']},
      {
        ol:[
          'My doctor has explained the various types of implants used in dentistry, and I have been  informed of the alternatives to implant surgery for replacement of my missing teeth. I have also been informed of the foreseeable risks of those alternatives. I understand what procedures are necessary to accomplish the placement of the implant(s) either on, in, or through the bone, and I understand that the most common types of implants available are subperiosteal (on), endosteal (in), and transosteal (through). The implant type recommended for my specific condition is circled above. I also understand that endosteal implants (more commonly known as root-form) generally have the most predictable prognosis. I further understand that subperiosteal implants, if an option for me, are not as widely used as root form implants but will negate the necessity of my undergoing the bone grafting and other surgical procedures that would be necessary for the placement of root-form implants. I understand that the risk associated with the use of a subperiosteal implant is the failure and loss of the implant, which could further reduce the minimal amount of existing bone that I now have, requiring more extensive bone grafting and other surgical procedures at some future time. I also understand that other dental practitioners may not be familiar or experienced in the use of subperiosteal implants, including their placement and maintenance as well as treatment of any problems that might arise involving the subperiosteal implant. I promise to, and accept responsibility for failing to, return to this office for examinations and any recommended treatment at least every 6 months. My failure to do so, for whatever reason, can jeopardize the clinical success of the implant system. Accordingly, I agree to release and hold my dentist harmless if my implant(s) fail as a result of my not maintaining an ongoing examination and  preventive maintenance routine as stated above.\n\n',
          'I have further been informed that if no treatment is elected to replace the missing teeth or existing dentures, the nontreatment risks include but are not limited to:\n\n',
          {
            type: 'lower-alpha',
            ol: [
              'Maintenance of the existing full or partial denture(s) with relines or remakes every 3 to 5 years or as otherwise may be necessary due to slow, but likely, progressive dissolution of the underlying denture-supporting jawbone\n\n',
              'Persistence or worsening of any present discomfort or chewing inefficiency with the existing partial or full denture over time \n\n',
              'Drifting, tilting, and/or extrusion of remaining teeth \n\n',
              'Looseness of teeth and periodontal disease (gingiva [gum] and bone), possibly followed by extraction(s)\n\n',
              'A potential temporomandibular joint (TMJ) problem caused by a deficient, collapsed, or otherwise improper bite. \n\n'
            ]
          },
  
          'I am aware that the practice of dentistry and dental surgery is not an exact science, and I acknowledge that no guarantees have been made to me concerning the success of my implant surgery, the associated treatment and procedures, or the postsurgical dental procedures. I am further aware that there is a risk that the implant placement may fail, which might require further corrective surgery associated with the removal. Such a failure and remedial procedures could also involve additional fees being assessed. \n\n',
  
          'I understand that implant success is dependent upon a number of variables, including but not limited to: operator experience, individual patient tolerance and health, anatomical variations, my home care of the implant, and habits such as grinding my teeth. I also understand that implants are available in a variety of designs and materials, and the choice of implant is determined in the professional judgment of my dentist\n\n',
  
          'I have further been informed of the foreseeable risks and complications of implant surgery, anesthesia, and related drugs, including but not limited to: failure of the implant(s), inflammation, swelling, infection, discoloration, numbness (exact extent and duration unknown), inflammation of blood vessels, injury to existing teeth, bone fractures, sinus penetration, delayed healing, or allergic reaction to the drugs or medications used. No one has made any promises or given me any guarantees about the outcome of this treatment or these procedures. I understand that these complications can occur even if all dental procedures are done properly. \n\n',
  
          'I have been advised that smoking and alcohol or sugar consumption may affect tissue healing and may limit the success of the implant. Because there is no way to accurately predict the gingival (gum) and bone healing capabilities of each patient, I know I must follow my dentist’s home care instructions and report to my dentist for regular examinations as instructed. I further understand that excellent home care, including brushing, flossing, and the use of any other device recommended by my dentist, is critical to the success of my treatment, and my failure to do what I am supposed to do at home will be, at a minimum, a partial cause of implant failure, should that occur. I understand that the more I smoke, the more likely it is that my implant treatment will fail, and I understand and accept that risk. \n\n',
  
          'I have also been advised that there is a risk that the implant may break, which may require additional procedures to repair or replace the broken implant. \n\n',
          
          'I authorize my dentist to perform dental services for me, including implants and other related surgery such as bone augmentation. I agree to the type of anesthesia that he/she has discussed with me, circled here (local, IV, or general sedation), and its potential side effects. I agree not to operate a motor vehicle or hazardous device for at least twenty-four(24) hours or more until fully recovered from the effects of the anesthesia or drugs given for my care. My dentist has also discussed the various kinds and types of bone augmentation material, and I have authorized him/her to select the material that he/she believes to be the best choice for my implant treatment. \n\n',
  
          'If an unforeseen condition arises in the course of treatment and calls for the performance of procedures in addition to or different from that now contemplated and I am under general anesthesia or IV sedation, I further authorize and direct my dentist and his/her associates or assistants of his/her choice to do whatever he/she/they deem necessary and advisable under the circumstances, including the decision not to proceed with the implant procedure(s). \n\n',
  
          'I approve any reasonable modifications in design, materials, or surgical procedures if my dentist, in his/her professional judgment, decides it is in my best interest to do so.\n\n',
  
          'To my knowledge, I have given an accurate report of my health history. I have also reported any past allergic or other reactions to drugs, food, insect bites, anesthetics, pollens, dust; blood diseases; gingival (gum) or skin reactions; abnormal bleeding; or any other condition relating to my physical or mental health or any problems experienced with any prior medical, dental, or other health care treatment on my medical history questionnaire. I understand that certain mental and/or emotional disorders may contraindicate implant therapy and have therefore expressly circled either YES or NO to indicate whether or not I have had any past treatment or therapy of any kind or type for any mental or emotional condition.\n\n',
  
          'I authorize my dentist to capture photos, slides, x-rays, or any other visual aids of my treatment to be used for the advancement of implant dentistry in any manner my dentist deems appropriate. However, no photographs or other records that identify me will be used without my express written consent. \n\n',
  
          'I realize and understand that the purpose of this document is to evidence the fact that I am knowingly consenting to the implant procedures recommended by my dentist. \n\n',
  
          'I agree that if I do not follow my dentist’s recommendations and advice for postoperative care, my dentist may terminate the dentist-patient relationship, requiring me to seek treatment from another dentist. I realize that postoperative care and maintenance treatment is critical for the ultimate success of dental implants. I accept responsibility for any adverse consequences that result from not following my dentist’s advice. \n\n',
  
          {text:['Questions I have to ask my dentist:',{text:`${props.exception ? "      " + props.exception + "      " : "          "} \n\n`, style: 'liner' }]},
  
          'I CERTIFY THAT I HAVE READ AND FULLY UNDERSTAND THE ABOVE AUTHORIZATION AND INFORMED CONSENT TO IMPLANT PLACEMENT AND SURGERY AND THAT ALL MY QUESTIONS, IF ANY, HAVE BEEN FULLY ANSWERED. I HAVE HAD THE OPPORTUNITY TO TAKE THIS FORM HOME AND REVIEW IT BEFORE SIGNING IT. I UNDERSTAND AND AGREE THAT MY INITIAL ON EACH PAGE ALONG WITH MY SIGNATURE BELOW WILL BE CONSIDERED CONCLUSIVE PROOF THAT I HAVE READ AND UNDERSTAND EVERYTHING CONTAINED IN THIS DOCUMENT AND I HAVE GIVEN MY CONSENT TO PROCEED WITH IMPLANT TREATMENT AND RELATED SURGERY, INCLUDING ANY ANCILLARY BONE GRAFTING PROCEDURES. \n\n',
        ]},
  
      { text: ['Patient\'s Name: ', { text: `${props.patientName ? "      " + props.patientName + "      " : "          "} \n\n`, style: 'liner' }] },
  
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},{ image: `${props.signature1}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.date1}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Patient Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},(props.signature2 != undefined) ?{ image: `${props.signature2}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' }: {text: ' ',border: [false, false, false, true],},{},
             { text: `${props.date2}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Doctor Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
      
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
   };

   var consent13 = {
    info: {
      title: 'Infection Screening',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Infection Screening\n\n\n',
        style: 'header'
      },
      {text:'As a safeguard to the coronavirus pandemic,we at this dental office would like our patients to know that your health and vitality are of utmost importance to us.We have implemented increased vigilance and safety protocols for clinician-patient interactions and workplace sanitation that take into account established universal precautions as set forth by major public health institutions.Please take the time to fill out the questionnaire below to help us better serve our patient base and ensure that we take maximal precautions to ensure that you receive the best care possible.\n\n'}, 
  
      { text: ['Are you experiencing fever,cough,runnynose,or shortness of breath? ', { text: `${props.ill ? "      " + props.ill + "      " : "          "} \n\n`, style: 'liner' }] },
  
      { text: ['Are you having any symptoms of respiratory infection? ', { text: `${props.symptoms ? "      " + props.symptoms + "      " : "          "} \n\n`, style: 'liner' }] },
  
      { text: ['Have you come in contact or been exposed to anyone with a respiratory infection? ','   ', { text: `${props.expose ? "      " + props.expose + "      " : "          "} \n\n`, style: 'liner' }] },
  
      { text: ['Have you or your family traveled outside of the US with in the last month? ', { text: `${props.travel ? "      " + props.travel + "      " : "          "} \n\n`, style: 'liner' }] },
  
      { text: ['Have you or anyone you have been in close contact with (within 6 feet) traveled to mainland China, Hong Kong, Macau, Iran, South Korea, or the Italian regions of Emilia-Romagna, Lombardy and Veneta? ', { text: `${props.closeContact ? "      " + props.closeContact + "      " : "          "} \n\n`, style: 'liner' }] },
  
      {text: 'Please take precautions: \n',fontSize: '14', bold: true},
      {text: 'Wash your hands often with soap and water for at least 20 seconds.\n'},
      {text: 'Avoid close contact with people who are sick.\n'},
      {text: 'Avoid touching your eyes,nose, and mouth.'},
      {text: 'Stay home when you are sick.\n\n'},
  
      {text: 'Thank you for assisting us in preventing the spread of infection.\n\n'},
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},{ image: `${props.signature1}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.date1}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Patient Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
      
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
   };

   var consent14 = {
    info: {
      title: 'Maxillary Ridge Expansion Surgery',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Maxillary Ridge Expansion Surgery\n\n\n',
        style: 'header'
      },
  
      { text: [{ text:' '},{ text: `${props.staff1 ? "      " + props.staff1 + "      " : "        "}`, style: 'liner' }, ' has explained to me the various steps involved in my proposed surgery. Alternative treatment plans have been discussed, and I feel comfortable in proceeding with the outlined surgery. \n\n'] },
  
      {text: 'The following are some facts that pertain to my surgery have been explained to me.\n\n'},
  
      { text: ['I understand that surgery will be performed to place a bone graft material on top and within the crestal bone of the maxilla (upper jaw). The bone graft will be “sandwiched” between the existing bone.',  { text: `${props.staff2 ? "      " + props.staff2 + "      " : "        "}`, style: 'liner' }, ' has explained how this operation will be performed to my satisfaction. The graft material will consist of a bone substitute material (hydroxylapatite), tissue bank bone, or a combination of both. In approximately 5 to 6 months, after the graft has partially healed, a second procedure will be done to insert the implants into the maxilla (upper jaw) and the grafted material. In some cases it is possible to insert the implant and graft in the same operation. It is expected that the implants will become stable and act as anchors for fixed or fixeddetachable restorations. This graft is also being placed in an attempt to change the contour of the bone ridge, make it wider, or make it larger. \n\n'] },
  
      {text: 'The graft material consists of small particles. Some of the particles may work loose during the initial healing period. However, this should not influence the success of the surgery,and the particles will do no harm if swallowed. \n\n'},
  
      {text: 'Following the graft, it is sometimes necessary to have a second procedure called a vestibuloplasty. This operation provides more tissue to cover the grafted ridge. \n\n'},
  
      {text: 'A custom surgical splint may need to be attached to the jaw (using surgical wire, surgical screws, or sutures) for 1 to 4 weeks to help keep the synthetic bone graft in place and to form it properly to the jaw bone. The patient’s existing partial or full denture can sometimes be modified for this purpose.\n\n'},
  
      {text: 'The surgical technique has been explained to me in detail. I understand that just as in any surgery complications can occur, including but not limited to: infection; bleeding; tissue damage; permanent numbness of the upper lip, face, or cheeks; and loss of the graft (requiring future surgical procedures).\n\n'},
  
      {text: 'I have been informed and understand that occasionally there are complications of surgery, drugs, and anesthesia, including but not limited to:\n\n'},
  
      {
        ol:[
          'Pain, swelling, and postoperative discoloration of the face, neck, and mouth\n\n',
  
          'Numbness and tingling of the upper lip, teeth, gingiva (gum), cheek, and palate, which may be temporary or, rarely, permanent \n\n',
  
          'Infection of the bone that might require further treatment, including hospitalization and surgery\n\n',
  
          'Mal-union, delayed union, or nonunion of the bone graft replacement material to the normal bone\n\n',
  
          'Lack of adequate bone growth into the bone graft replacement material\n\n',
  
          'Bleeding that may require extraordinary means to control hemorrhage \n\n',
  
          'Limitation of jaw function \n\n',
  
          'Stiffness of facial and jaw muscles \n\n',
  
          'Injury to the teeth\n\n',
  
          'Referred pain to the ear, neck, and head \n\n',
  
          'Postoperative complications involving the sinuses, nose, nasal cavity, sense of smell, infraorbital regions, and altered sensations of the cheeks and eyes \n\n',
  
          'Postoperative unfavorable reactions to drugs, such as nausea, vomiting, and allergy \n\n',
  
          'Possible loss of teeth and bone segments\n\n',
        ]},
  
      {text: 'Although unlikely, it is possible that the graft material will not “take,” i.e., not attach to the existing bone. The graft may be loose, and the gingival (gum) tissue over the graft may ulcerate. If this occurs the graft material will need to be removed. Further surgery, including mucosal or skin grafts, may be needed to repair lost oral tissues.\n\n'},
  
      {text: 'After surgery, there will be a certain amount of discomfort and swelling. I understand that I will need to be on a liquid to very soft diet for 2 to 3 weeks. \n\n'},
  
      {text:  'I agree to keep my teeth and mouth meticulously clean. I also agree to keep all postoperative appointments and checkups as required by my doctor.\n\n'},
  
      { text: ['I give my permission for ', { text: `${props.staff3 ? "      " + props.staff3 + "      " : "          "}`, style: 'liner' },'to photograph and video this operation for purposes of education, publishing, and teaching. \n\n'] },
  
      { text: ['Knowing the above facts, I freely give my consent to ', { text: `${props.staff4 ? "      " + props.staff4 + "      " : "          "} `, style: 'liner' }, 'to perform a synthetic bone graft to my maxilla (upper jaw).'] },
  
      { text: ['Patient\'s Name: ', { text: `${props.patientName ? "      " + props.patientName + "      " : "          "} \n\n`, style: 'liner' }] },
  
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},{ image: `${props.signature1}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.date1}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Patient Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
  
      {text: '\n\n\n\n'},
     
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},(props.signature2 != undefined) ?{ image: `${props.signature2}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' }: {text: ' ',border: [false, false, false, true],},{},
             { text: `${props.date2}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Doctor Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
      
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
   };

   var consent15 = {
    info: {
      title: 'Maxillary Sinus Bone Augmentation Consent Form',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Maxillary Sinus Bone Augmentation Consent Form\n\n\n',
        style: 'header'
      },
  
      { text: ['I authorize and request ', { text: `${props.staff1 ? "      " + props.staff1 + "      " : "        "}`, style: 'liner' }, ' to perform surgery on my maxilla (upper jaw).  \n\n'] },
  
      {text: 'I understand that surgery will be performed to place a bone graft material into the floor of the sinus to build up adequate bone height for the placement of implants. The bone graft will consist of a bone substitute material (hydroxylapatite), tissue bank bone, or a combination of both. In approximately 5 to 6 months, after the graft has partially healed, a second procedure will be done to insert the implant into the maxilla (upper jaw) and the grafted material. In some cases it is possible to insert the implants and sinus floor graft during the same operation. It is expected that the implants will become stable and act as anchors for fixed or fixed-detachable dental prostheses. \n\n'},
  
      { text: [{ text: ' '},{ text: `${props.staff2 ? "      " + props.staff2 + "      " : "        "}`, style: 'liner' }, 'has explained that if the new bone does not incorporate into the bone graft material, alternative prosthetic measures would have to be considered.', { text: `${props.staff3 ? "      " + props.staff3 + "      " : "        "}`, style: 'liner' },'has explained and described the procedure to my satisfaction.\n\n '] },
  
      {text: 'The likelihood for success of the suggested treatment plan is good. However, there are risks involved. The bone graft material has produced good results when placed on top of the maxillary (upper) or mandibular (lower) ridge. However, there are insufficient long-term studies to evaluate placement of the material on the sinus floor. This bone graft replacement material has previously been shown to be free from rejection or infection. However, there is no guarantee that your graft will not become infected or be rejected. \n\n'},
  
      {text: 'There have been some cases of failure of the graft to incorporate new bone or to sustain implants. Rarely, implants have failed and require removal; occasionally, the area can be re-grafted and implants re-inserted. \n\n'},
  
      {text: 'It is understood that although good results are expected, they cannot be and are not implied, guaranteed, or warrantable. There is also no guarantee against unsatisfactory or failed results,\n\n'},
  
      {text: 'I have been informed and understand that occasionally there are complications of surgery,drugs, and anesthesia, including but not limited to: \n\n'},
  
      {
        ol:[
          'Pain, swelling, and postoperative discoloration of the face, neck, and mouth\n\n',
  
          'Numbness and tingling of the upper lip, teeth, gingiva (gum), cheek, and palate, which may be temporary or, rarely, permanent \n\n',
  
          'Infection of the bone that might require further treatment, including hospitalization and surgery\n\n',
  
          'Mal-union, delayed union, or nonunion of the bone graft replacement material to the normal bone\n\n',
  
          'Lack of adequate bone growth into the bone graft replacement material\n\n',
  
          'Bleeding that may require extraordinary means to control hemorrhage ',
  
          'Limitation of jaw function \n\n',
  
          'Stiffness of facial and jaw muscles \n\n',
  
          'Injury to the teeth\n\n',
  
          'Referred pain to the ear, neck, and head \n\n',
  
          'Postoperative complications involving the sinuses, nose, nasal cavity, sense of smell, infraorbital regions, and altered sensations of the cheeks and eyes \n\n',
  
          'Postoperative unfavorable reactions to drugs, such as nausea, vomiting, and allergy \n\n',
  
          'Possible loss of teeth and bone segments\n\n',
        ]},
  
      { text: ['I understand that I am not to use alcohol or non-prescribed drugs during the treatment period. ', { text: `${props.staff4 ? "      " + props.staff4 + "      " : "          "} `, style: 'liner' },'has discussed with me that smoking is particularly harmful to the success of this operation. I have been requested to refrain from smoking. \n\n'] },
  
      { text: ['I understand that ', { text: `${props.staff5 ? "      " + props.staff5 + "      " : "          "} `, style: 'liner' },'will give his best professional care toward the accomplishment of the desired results. I understand that I can  ask for full recital of all possible risks attendant to phases of my care by asking. I further understand that I am free to withdraw from treatment at any time. \n\n'] },
  
      {text:'I give permission for persons other than the doctors involved in my care and treatment to observe this operation and for it to be photographed or otherwise recorded for the purposes of teaching and research.\n\n'},
  
      { text: ['I understand this consent form, and I request ', { text: `${props.staff6 ? "      " + props.staff6 + "      " : "          "}`, style: 'liner' },'to perform the surgery discussed. I hereby state that I read, speak, and understand English. \n\n'] },
  
      { text: ['Patient\'s Name: ', { text: `${props.patientName ? "      " + props.patientName + "      " : "          "} \n\n`, style: 'liner' }] },
  
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},{ image: `${props.signature1}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.date1}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Patient Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
    
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
   };

   var consent16 = {
    info: {
      title: 'Medical Clearance For Dental Treatment',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Medical Clearance For Dental Treatment\n\n\n',
        style: 'header'
      },
  
      { text: ['Date: ', { text: `${props.date1 ? "      " + props.date1 + "      " : "        "}`, style: 'liner' }, ' Dentist name: ', { text: `${props.dentistName ? "      " + props.dentistName + "      " : "        "} \n\n`, style: 'liner' }] },
  
      { text: ['Patient name: ', { text: `${props.patientName ? "      " + props.patientName + "      " : "        "}`, style: 'liner' }, 'DOB: ', { text: `${props.dob ? "      " + props.dob + "      " : "        "} \n\n`, style: 'liner' }] },
  
      {text: 'PATIENT CONSENT\n\n', bold: true, fontSize: 14},
  
      {text: 'I agree to the release of my medical information to the above named dentist office. \n\n'},
  
      {
        columns: [{ text: 'Patient’s Signature: ', margin:(windowSize > 480) ? [0, 55, 0, 0] : [0, 50, 0, 0] , width: '27%' }, (props.signature1 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature1}`, fit: [150, 150], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 25, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }, { text: ['Date: ', { text: `${props.date2}`, style: 'liner' }, '\n\n'], width: '30%', margin:(windowSize > 480) ? [0, 55, 0, 0] : [0, 50, 0, 0] }]
      },
  
      { text: ['Dear ', { text: `${props.staff1 ? "      " + props.staff1 + "      " : "        "}\n\n`, style: 'liner' }] },
  
      { text: ['This letter is regarding our mutual patient, ', { text: `${props.patient ? "      " + props.patient + "      " : "        "}\n\n`, style: 'liner' }] },
  
      {text: 'The patient is scheduled for dental treatment that will involve:\n  Tooth extraction\n   Bone graft\n  Placement of a dental implant\n  Maxillary sinus bone augmentation\n\n', bold: true},
  
      { text: ['Additional Prociedre (s): ', { text: `${props.procedure ? "      " + props.procedure + "      " : "        "}\n\n`, style: 'liner' }] },
  
      { text: ['Proposed date of dental treatment: ', { text: `${props.date3 ? "      " + props.date3 + "      " : "        "}\n\n`, style: 'liner' }] },
  
      {text: 'Most patients experience the following with the above planned procedures:\n\n', bold: true},
  
      { text: ['Bleeding: ', { text: `${props.minimal ? "      " + props.minimal + "      " : "        "}`, style: 'liner' },'minimal (<50ml) ', { text: `${props.significant ? "      " + props.significant + "      " : "        "}`, style: 'liner' }, 'significant(&gt;50ml)\n\n'] },
  
      { text: ['Stress and anxiety: ', { text: `${props.low ? "      " + props.low + "      " : "        "}`, style: 'liner' },'low ', { text: `${props.medium ? "      " + props.medium + "      " : "        "}`, style: 'liner' }, 'medium ',{ text: `${props.high ? "      " + props.high + "      " : "        "}`, style: 'liner' }, 'high\n\n'] },
  
      {text: 'As part of these procedures, the patient will be prescribed medications for dental surgery and pain. However, due to her current medical history, I’d like to get a medical clearance to proceed with the dental treatment. \n\n'},
  
      {text: 'THE PATIENT HAS INFORMED US THAT SHE IS CURRENTLY TAKING\n\n'},
      (props.exception) ? {
        margin: [20, 0, 0, 0],
        table: {
          widths: ['93%'],
          body: [
            [{ text: `${props.exception}`, border: [false, false, false, true] }]
          ]
        }, layout: {
          defaultBorder: false,
        }
      } : {
        margin: [20, 0, 0, 0],
        table: {
          widths: ['93%'],
          body: [
            [{ text: " ", border: [false, false, false, true], }]
          ]
        }, layout: {
          defaultBorder: false,
        },
      },
  {text: '\n\n'},
  
      {text: ['Please CHECK ', {text: 'ALL',decoration: 'underline' },' THAT APPLY: \n\n'], bold:true},
      {
        table: {
          widths: [20,60,'90%'],
          body: [
            [{},{ text: `${props.treatment ?  "      " + props.treatment + "      " : "          "}`, decoration: 'underline' },
            { text: [{text: 'OK',decoration: 'underline' }, ' to ', {text:'PROCEED',decoration: 'underline' }, ' with dental treatment; ',{text: 'NO',decoration: 'underline' }, ' special precautions and ', {text: 'NO',decoration: 'underline' }, ' prophylactic antibiotics are needed.\n\n' ]}]
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {
        table: {
          widths: [20,60,'90%'],
          body: [
            [{},{ text: `${props.antibiotic ?  "      " + props.antibiotic + "      " : "          "}`, decoration: 'underline' },
            { text: ['Antibiotic prophylaxis  ',{text: 'IS',decoration: 'underline' }, '  required for dental treatment according to the current American Heart Association and/or American Academy of Orthopedic Surgeons guidelines\n\n']}]
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      
      {
        table: {
          widths: [20,60,'90%'],
          body: [
            [{},{ text: `${props.precaution ?  "      " + props.precaution + "      " : "          "}`, decoration: 'underline' },
            { text: 'Other precautions are required: (please specify)\n\n'}]
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      (props.precaution1) ? {
        margin: [20, 0, 0, 0],
        table: {
          widths: ['75%'],
          body: [
            [{ text: `${props.precaution1}`, border: [false, false, false, true] }]
          ]
        }, layout: {
          defaultBorder: false,
        }
      } : {
        margin: [20, 0, 0, 0],
        table: {
          widths: ['93%'],
          body: [
            [{ text: " ", border: [false, false, false, true], }]
          ]
        }, layout: {
          defaultBorder: false,
        },
      },
      {text: '\n\n'},
      {
        table: {
          widths: [20,60,'90%'],
          body: [
            [{},{ text: `${props.noProceed ?  "      " + props.noProceed + "      " : "          "}`, decoration: 'underline' },
            { text: [{text: 'DO NOT ',decoration: 'underline'},'proceed with treatment. (Please specify reason)\n\n ']}]
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      (props.noProceed1) ? {
        margin: [20, 0, 0, 0],
        table: {
          widths: ['75%'],
          body: [
            [{ text: `${props.noProceed1}`, border: [false, false, false, true] }]
          ]
        }, layout: {
          defaultBorder: false,
        }
      } : {
        margin: [20, 0, 0, 0],
        table: {
          widths: ['93%'],
          body: [
            [{ text: " ", border: [false, false, false, true], }]
          ]
        }, layout: {
          defaultBorder: false,
        },
      },
      {text: '\n\n'},
      {text: 'Medications: \n\n', fontSize:14, bold:true},
      {
        ol:[
          {text: [' Amoxicillin 500 mg 24 Tabs: ',{ text: `${props.amoxicillin ?  "      " + props.amoxicillin + "      " : "          "}\n\n`, decoration: 'underline' }]},
  
          {text: ['Peridex Oral rinse: ',{ text: `${props.peridex ?  "      " + props.peridex + "      " : "          "}\n\n`, decoration: 'underline' }]},
  
          {text: ['Halcion 0.50 mg 2-4 Tabs: ',{ text: `${props.halcion ?  "      " + props.halcion + "      " : "          "}\n\n`, decoration: 'underline' }]},
  
          {text: ['Decadron 1.5 mg 7-12 Tabs: ',{text: `${props.decadron ?  "      " + props.decadron + "      " : "          "}\n\n`, decoration: 'underline'}]},
  
          {text: [' Motrin 600 mg15 Tabs: ',{ text: `${props.motrin ?  "      " + props.motrin + "      " : "          "}\n\n`, decoration: 'underline' }]},
        ]
      },
      
      {text: ['IF NECESSARY, CAN YOU PLEASE MODIFY THE DOSAGE (RX) AND LET US KNOW PRIOR TO HER SCHEDULED SURGERY FOR:',{ text: `${props.dosage ?  "      " + props.dosage + "      " : "          "} \n\n`, decoration: 'underline' }]},
  
      { text: ['Physician\'s Name printed: ', { text: `${props.physician ? "      " + props.physician + "      " : "          "} \n\n`, style: 'liner' }] },
  
      {
        columns: [{ text: 'Physician Signature: ', margin:[0, 30, 0, 0] , width: '27%' }, (props.signature2 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature2}`, fit: [150, 150], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 25, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }, { text: ['Date: ', { text: `${props.date4}`, style: 'liner' }, '\n\n'], width: '30%', margin:[0, 30, 0, 0] }]
      },
  
    
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
   };

   var consent17 = {
    info: {
      title: 'Necessary Follow-up Care Consent Form',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Necessary Follow-up Care Consent Form\n\n\n',
        style: 'header'
      },
  
      { text: ['I understand that ', { text: `${props.staff1 ? "      " + props.staff1 + "      " : "        "}`, style: 'liner' }, 'is evaluating me only for possible dental implant treatment, and thus the evaluation is limited to the edentulous spaces I have in my mouth. I understand that the oral exam/evaluation and the CT scan that might be required does not cover evaluation of the gums, the status of an existing restoration, or temporomandibular joint (TMJ) issues, nor does it include an oral cancer screening.\n\n'] },
  
      {text: 'I understand that it is important for me to continue to see my general dentist for routine dental care. This is only an evaluation of the edentulous spaces in both arches for potential implant treatment.\n\n'},
  
      { text: ['Patient name: ', { text: `${props.patientName ? "      " + props.patientName + "      " : "        "}`, style: 'liner' }, ] },
  
      {
        columns: [{ text: 'Patient Signature: ', margin:(windowSize > 480) ? [0, 50, 0, 0] : [0, 50, 0, 0] , width: '30%' }, (props.signature1 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature1}`, fit: [150, 150], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 25, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }]
      },
      {text: '\n\n'},
  
      { text: ['Date: ', { text: `${props.date1 ? "      " + props.date1 + "      " : "          "} \n\n`, style: 'liner' }], margin: [20, 0, 40, 0] },
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
   };

   var consent18 = {
    info: {
      title: 'Oral Surgery And Dental Extractions Consent Form',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Oral Surgery And Dental Extractions Consent Form\n\n\n',
        style: 'header'
      },
  
      {text:'I understand that oral surgery and/or dental extractions include inherent risks such as, but not limited to, the following:\n\n'},
  
      {
        ul:[
          'This would include injuries causing numbness of the lips; the tongue; and any tissues of the mouth, cheeks, and/or face. The potential numbness may be of a temporary nature, lasting a few days, weeks, or months, or could possibly be permanent. Numbness could be the result of surgical procedures or anesthetic administration.\n\n',
  
          'Some moderate bleeding may last several hours. If it is profuse, you must contact us as soon as possible. Some swelling is normal, but if it is severe, you should notify us. Swelling usually starts to subside after about 48 hours. Bruises may persist for a week or so.\n\n',
  
          'This occurs on occasion when teeth are extracted and is a result of a blood clot not forming properly during the healing process. Dry sockets can be extremely painful if not treated.\n\n',
  
          'In some cases, the root tips of maxillary (upper) teeth lie in close proximity to sinuses. Occasionally during extraction or surgical procedures the sinus membrane may be perforated. Should this occur, it may be necessary to have the sinus surgically closed. Root tips may need to be retrieved from the sinus.\n\n',
  
          'No matter how carefully surgical sterility is maintained, it is possible, because of the existing nonsterile oral environment, for infections to occur postoperatively. At times these may be of a serious nature. Should severe swelling occur, particularly if accompanied by fever or malaise, professional attention should be received as soon as possible.\n\n',
  
          'Although extreme care will be used, the jaw, tooth toots, bone spicules, or instruments used in the extraction procedure may fracture or be fractured, requiring retrieval and possibly referral to a specialist. A decision may be made to leave a small piece of root, bone fragment, or instrument in the jaw when removal may require additional extensive surgery that could cause more harm and add to the risk of complications.\n\n',
  
          'This could occur at times no matter how carefully surgical and/or extraction procedures are performed.\n\n',
  
          'Because of the normal existence of bacteria in the oral cavity, the tissues of the heart, as a result of reasons known or unknown, may be susceptible to bacterial infection transmitted through blood vessels, and bacterial endocarditis (an infection of the heart) could occur. It is my responsibility to inform the dentist of any known or suspected heart problems.\n\n',
  
          'Reactions, either mild or severe, may possibly occur from anesthetics or other medications administered or prescribed. All prescription drugs must be taken according to instructions. Women using oral contraceptives must be aware that antibiotics can render these contraceptives ineffective. Other methods of contraception must be utilized during the treatment period.\n\n',
  
          'It is my responsibility to seek attention should any undue circumstances occur postoperatively, and I shall diligently follow any preoperative and postoperative instructions given to me.\n\n'
        ]
      },
      { text: [{text: 'Informed Consent:', bold: true}, 'As a patient, I have been given the opportunity to ask any questions regarding the nature and purpose of surgical treatment and have received answers to my satisfaction. I do voluntarily assume any and all possible risks, including the risk of harm, if any, that may be associated with any phase of this treatment in hopes of obtaining the desired results, which may or may not be achieved. No guarantees or promises have been made to me concerning my recovery and results of the treatment to be rendered to me. The fee(s) for this service have been explained to me and are satisfactory. By signing this form, I am freely giving my consent to allow and authorize  ', { text: `${props.staff1 ? "      " + props.staff1 + "      " : "        "}`, style: 'liner' }, 'and his associates to render any treatments necessary or advisable to my dental conditions, including any and all anesthetics and/or medications.\n\n'] },
  
  
      { text: ['Patient name: ', { text: `${props.patientName ? "      " + props.patientName + "      " : "        "}\n\n`, style: 'liner' }, ] },
  
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},{ image: `${props.signature1}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.date1}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Patient Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
   };

   var consent19 = {
    info: {
      title: 'Physician-Patient Arbitration Agreement',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Physician-Patient Arbitration Agreement\n\n\n',
        style: 'header'
      },
  
      {text:[{text:'ARTICLE 1: ', bold:true, fontSize: 14},{text:'Agreement to Arbitrate: ', bold:true},'It is understood that any dispute as to medical malpractice, that is, as to whether any medical services rendered under this contract were unnecessary or unauthorized or were improperly, negligently, or incompetently rendered, will be determined by submission to arbitration as provided by California law and not by a lawsuit or resort to courts process except as California law provides for judicial review of arbitration proceedings. Both parties to this contract, by entering into it, are giving up their constitutional rights to have any such dispute decided in a court of law before a jury and instead are accepting the use of arbitration.\n\n']},
  
      {text:[{text:'ARTICLE 2: ', bold:true, fontSize: 14},{text:'All Claims Must Be Arbitrated: ', bold:true}, 'It is the intention of the parties that this agreement bind all parties whose claims may arise out of or relate to treatment or service provided by the physician, including any spouse or heirs of the patient and any children, whether born or unborn, at the time of the occurrence giving rise to any claim. In the case of a pregnant mother, the term “patient” herein shall mean both the mother and the mother’s expected child or children.\n',
      'All claims for monetary damages exceeding the jurisdictional limit of the small claims court against the physician; the physician’s partners, associates, association, corporation, or partnership; and the employees, agent, and estates of any of them must be arbitrated, including, without limitation, claims for loss of consortium, wrongful death, emotional distress, or punitive damages. Filing of any action in any court by the physician to collect any fee from the patient shall not waive the right to compel arbitration of any malpractice claim.\n\n']},
  
      {text:[{text:'ARTICLE 3: ', bold:true, fontSize: 14},{text:'Procedures and Applicable Law: ' ,bold:true,}, 'A demand for arbitration must be communicated in writing to all parties. Each party shall select an arbitrator (party arbitrator) within 30 days, and a third arbitrator (neutral arbitrator) shall be selected by the arbitrators appointed by the parties within 30 days of a demand for a neutral arbitrator by either party. Each party to the arbitration shall pay such party’s pro rata share of the expenses and fees of the neutral arbitrator, together with other expenses of the arbitration incurred or approved by the neutral arbitrator, not including counsel fees or witness fees, and together with expenses incurred by a party for such party’s own benefit. The parties agree that the arbitrators have the immunity of a judicial officer from civil liability when acting in the capacity of arbitrator under this contract. This immunity shall supplement, not supplant, any other applicable statutory or common law.\n',
      'Either party shall have the absolute right to arbitrate separately the issues of liability and damages upon written request to the neutral arbitrator.\n',
      'The parties agree that provisions of California law applicable to health care providers shall apply to disputes within this arbitration agreement, including, but not limited to, Code of Civil Procedure Sections 340.5 and 667.7 and Civil Code Sections 3333.1 and 3333.2. Any party may bring before the arbitrators a motion for summary judgment or summary adjudication in accordance with the Code of Civil Procedure. Discovery shall be conducted pursuant to Code of Civil Procedure section 1283.05; however, depositions may be taken without prior approval of the neutral arbitrator.\n\n']},
  
      {text:[{text:'ARTICLE 4: ', bold:true, fontSize: 14},{text: 'General Provisions: ',bold:true}, 'All claims based upon the same incident, transaction, or related circumstances shall be arbitrated in one proceeding. A claim shall be waived and forever barred if (1) on the date notice thereof is received, the claim, if asserted in a civil action, would be barred by the applicable California statute of limitations, or (2) the claimant fails to pursue the arbitration claim in accordance with the procedures prescribed herein with reasonable diligence. With respect to any matter not herein expressly provided for, the California Code of Civil Procedure provisions relating to arbitration shall govern the arbitrators.\n\n']},
  
      {text:[{text:'ARTICLE 5: ', bold:true, fontSize: 14},{text:'Revocation:' , bold:true},'This agreement may be revoked by written notice delivered to the physician within 30 days of signature. It is the intent of this agreement to apply to all medical services rendered anytime for any condition.\n\n']},
  
      {text:[{text:'ARTICLE 6: ', bold:true, fontSize: 14},{text:'Retroactive Effect:' , bold:true},'If patient intends this agreement to cover services rendered before the date it is signed (including, but not limited to, emergency treatment) patient should initial below: Effective as of the date of first medial services.\n\n']},
  
      (props.exception) ? {
        table: {
          widths: ['100%'],
          body: [
            [{ text: `${props.exception}`, border: [false, false, false, true] }]
          ]
        }, layout: {
          defaultBorder: false,
        }
      } : {
        table: {
          widths: ['100%'],
          body: [
            [{ text: " ", border: [false, false, false, true] }]
          ]
        }, layout: {
          defaultBorder: false,
        },
      },
      { text: 'Patient’s or Patient Representative’s Initials\n\n'},
  
      {text: 'If any provision of the arbitration agreement is held invalid or unenforceable, the remaining provisions shall remain in full force and shall not be affected by the invalidity of any other provision.\n\n'},
  
      {text: 'I understand that I have the right to receive a copy of this arbitration agreement. By my signature below, I acknowledge that I have received a copy.\n\n'},
  
      {text:[{text:'NOTICE: ', bold:true, fontSize: 14},'BY SIGNING THIS COTRACT YOU ARE AGREEING TO HAVE ANY ISSUE OF MEDICAL MALPRACTICE DECIDED BY NEUTRAL ARBITRATION, AND YOU ARE GIVING UP YOUR RIGHT TO A JURY OR COURT TRIAL. SEE ARTICLE 1 OF THIS CONTRACT.\n\n']},
  
      { text: ['Patient name: ', { text: `${props.patientName ? "      " + props.patientName + "      " : "        "}\n\n`, style: 'liner' }, ] },
  
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},{ image: `${props.signature1}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.date1}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Patient Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},(props.signature2 != undefined) ?{ image: `${props.signature2}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' }: {text: ' ',border: [false, false, false, true],},{},
             { text: `${props.date2}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Doctor Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
   };

   var consent20 = {
    info: {
      title: 'Prosthodontic Treatment Information And Consent Form',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Prosthodontic Treatment Information And Consent Form\n\n\n',
        style: 'header'
      },
  
  
      { text: ['This booklet has been prepared by  ',{ text: `${props.staff1 ? "      " + props.staff1 + "      " : "        "}`, style: 'liner' }, 'to familiarize you with facts about PROSTHETIC TREATMENT. Please read it and write any questions in  the margins so you can discuss them with ',{ text: `${props.staff2 ? "      " + props.staff2 + "      " : "        "}\n\n`, style: 'liner' },] },
  
      {text: 'Please bring this booklet with you to your appointments. Before any treatment is started, you will be asked to sign a statement that you have read and understand this information and have had the opportunity to have all of your questions answered.\n\n'},
      {text:'INTRODUCTION: \n\n', bold:true, fontSize: 14},
  
      {text: 'The following information is routinely discussed with patients considering prosthodontic treatment in our office.\n\n'},
  
      {text: 'While recognizing the benefits of a pleasing smile and well-functioning teeth, you should also be aware that prosthodontic treatment, like any treatment of the body, has some inherent risks and limitations. These risks and limitations seldom contraindicate treatment; however, they should be considered in making your decision to have prosthodontic treatment.\n\n',},
  
      {text:'Treatment of human biologic conditions will never reach a state of perfection despite technologic advancements; problems can occur. This pamphlet is intended to inform you of some of the potential problems of prosthodontic treatment. Many of the problems mentioned occur only occasionally and rarely. There may be other inherent risks not discussed in this brochure. Prosthodontic treatment usually proceeds as planned; however, like all other healing arts, results cannot be guaranteed.\n'},
  
      { text: ['I read and understand the above section ', { text: `${props.section1 ? "      " + props.section1 + "      " : "        "}\n\n`, style: 'liner' }, ] , alignment: 'right'},
  
      {text:'INITIAL DIAGNOSTIC PROCEDURES: \n\n', bold:true, fontSize: 14},
      {text: 'Diagnostic procedures help formulate treatment recommendations. When appropriate, these procedures may include:\n\n'},
  
      {
        ol:[
          'Medical and dental history',
          'Physical examination of the mouth and associated structures',
          'X-rays',
          'Models of the teeth and/or associated structures',
          'Photographs',
          'Conference with previously or currently treating health professionals',
        ]
      },
      {text: 'Additional diagnostic procedures may be indicated.\n\n'},
      { text: ['I read and understand the above section ', { text: `${props.section2 ? "      " + props.section2 + "      " : "        "}\n\n\n`, style: 'liner' }, ], alignment: 'right' },
  
      {text:'TREATMENT RECOMMENDATIONS: \n\n', bold:true, fontSize: 14},
      {text: 'You should be informed of the most appropriate and reasonable alterative treatment plans. Also, you should be informed of the dental prognosis if no prosthodontic treatment is initiated. If you have any questions I encourage you to discuss them with me.\n'},
      { text: ['I read and understand the above section ', { text: `${props.section3 ? "      " + props.section3 + "      " : "        "}\n\n`, style: 'liner' }, ] , alignment: 'right'},
  
      {text:'REFERRALS TO OTHER SPECIALISTS: \n\n', bold:true, fontSize: 14},
      {text: 'Sometimes the need for treatment by another dental specialist or health practitioner becomes evident during your prosthodontic treatment. If this need occurs, the appropriate referral and the reasons for the referral will be explained to you.\n'},
      { text: ['I read and understand the above section ', { text: `${props.section4 ? "      " + props.section4 + "      " : "        "}\n\n`, style: 'liner'}, ], alignment: 'right'  },
  
      {text:'REMOVABLE PROSTHODONTICS: \n\n', bold:true, fontSize: 14},
      {text: 'Removable prosthodontics refers to the replacement of missing teeth with a restoration that can be removed from the mouth. This can be accomplished with several types of dentures. These include: complete removable dentures supported by gingival (gum) tissues, removable partial dentures supported by gingival (gum) tissue and remaining teeth and implants, and overdentures supported by the roots of natural teeth or implants.\n'},
      { text: ['I read and understand the above section ', { text: `${props.section5 ? "      " + props.section5 + "      " : "        "}\n\n`, style: 'liner' }, ] , alignment: 'right'},
  
      {text: 'Potential Problems with Removable Prosthodontics:',bold:true},
    {text: 'Dentures are removable replacements of natural teeth. You should be aware of the following potential problems\n\n'},
      // {
      //   ol:[
          {text:['1, ',{text:'Mastication (chewing), stability', bold: true} , 'and retention: Removable dentures, under the best of circumstances, do not have the same chewing efficiency as natural teeth. The ability to chew food is further affected by the stability and retention of the dentures. \n The stability and retention of dentures depend on many factors, including the attachment of the dentures to natural teeth or implants, the amount and type of bone, gingival (gum) disease, saliva, your dexterity, and the fit of the dentures to your gingival (gum) tissue or implant support system.\n ',
          ]},
          { text: ['I read and understand the above section ', { text: `${props.section6 ? "      " + props.section6 + "      " : "        "}\n\n`, style: 'liner' }, ], alignment: 'right' },
          
          {text: ['2, ',{text: 'Appearance: ', bold: true},'Properly fitting dentures support the lips and facial contours. Dentures can provide additional facial support if desired or needed. Excessive lip and facial support from dentures can result in a “swollen” appearance.\n',
          ]},
          { text: ['I read and understand the above section ', { text: `${props.section7 ? "      " + props.section7 + "      " : "        "}\n\n`, style: 'liner' }, ] , alignment: 'right'},
  
          {text: [{text: '3, Speech: ', bold: true},'Removable dentures cover areas of the jaws and palate. The presence of acrylic resin, metal, or porcelain in these areas requires adaptation of the tongue and lips for proper speech.\n',
          ]},
          { text: ['I read and understand the above section ', { text: `${props.section8 ? "      " + props.section8 + "      " : "        "}\n\n`, style: 'liner' }, ], alignment: 'right' },
  
          {text: ['4, ',{text: 'Stain and cleaning: ', bold: true},'The amount of stain on dentures depends on oral hygiene, use of tobacco, and consumption of coffee and tea. Commercially available denture-cleaning solutions are usually sufficient to maintain clean dentures. Bleach should not be used to clean removable dentures. Bleach can corrode the metal portions of the dentures and/or fade the pink acrylic resin. Abrasive kitchen or bathroom cleaners should be avoided.\n',
          ]},
          { text: ['I read and understand the above section ', { text: `${props.section9 ? "      " + props.section9 + "      " : "        "}\n\n`, style: 'liner' }, ], alignment: 'right' }, 
  
          {text: ['5, ',{text: 'Chipping and wear: ', bold: true},'Porcelain denture teeth have the slowest rate of wear and the highest stain resistance. However, porcelain has a tendency to chip. Small chips can be polished. Larger chips usually require replacement of the porcelain tooth on the denture. Acrylic resin denture teeth are more resistant to chipping, but they have a tendency to wear down faster than porcelain. If the wear affects the appearance or occlusion (bite), the acrylic resin teeth can be replaced on the denture.\n',
          'Chips and cracks of the pink acrylic resin portion can usually be repaired without remaking the denture.\n',
          ] },
          { text: ['I read and understand the above section ', { text: `${props.section10 ? "      " + props.section10 + "      " : "        "}\n\n`, style: 'liner' },], alignment: 'right' }, 
  
          {text: ['6, ',{text: 'Relines: ', bold: true},'The shape and size of the gingival (gum) tissue and bone changes with time. A reline procedure readapts the pink acrylic resin portion of the denture to the new shape and size of your gingival (gum) tissue. A reline is usually necessary every 3 to 5 years.\n',
          ]},
          { text: ['I read and understand the above section .', { text: `${props.section11 ? "      " + props.section11 + "      " : "        "}\n\n`, style: 'liner' }, ], alignment: 'right' },
  
          {text: ['7, ',{text: 'Food retention: ', bold: true},'Removable dentures always have some space between the pink acrylic resin portion and the gingival (gum) tissue. There is always some movement of the removable denture during chewing. These factors create a situation where food can accumulate between the denture and the gingival (gum) tissue. Therefore, it is essential to remove the denture for cleaning on a daily basis. Removable partial dentures may have additional food retention problems.\n', ]},
          { text: ['I read and understand the above section .', { text: `${props.section12 ? "      " + props.section12 + "      " : "        "}\n\n`, style: 'liner' }, ], alignment: 'right' },
      //   ]
      // },
  
      {text: 'INFORMED CONSENT AND TREATMENT CONFIRMATION\n\n',fontSize: 14, bold: true, alignment: 'center'},
  
      {text: 'I certify that this "Prosthodontic Treatment" pamphlet outlining general treatment considerations and potential problems and hazards of prosthodontic treatment was presented to me and that I have read and understand its contents. I understand that potential hazards and problems may include, but are not limited to, those described in the brochure. I have had the opportunity to discuss my proposed treatment and the potential problems with the doctor to clarify any areas I did not understand. I authorize the doctor to provide prosthodontic treatment.\n\n'},
      { text: ['Patient\'s Name: ', { text: `${props.patientName1 ? "      " + props.patientName1 + "      " : "        "}\n\n`, style: 'liner' }, ] },
    
      {text: '\n\n'},{text: '\n\n'},{text: '\n\n'},

      {
        table: {
          dontBreakRows: true,
          unbreakable: true,
          widths: [20, 150, 20, 150],
          body: [
            [{},{ image: `${props.signature1}`,fit: [150,150],border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.date1}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Patient Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
      {text: 'IMPLANTS :\n\n',bold: true, fontSize: 14},
  
      {text: 'I understand that incisions will be made inside my mouth for the purpose of placing one or more titanium or coated titanium implants in my jaw(s). The implants should serve as anchor(s) for a missing tooth or teeth to stabilize a crown or dental prosthesis.\n'},
      { text: ['I read and understand the above section .', { text: `${props.section13 ? "      " + props.section13 + "      " : "        "}\n\n`, style: 'liner' }, ], alignment: 'right' },
  
      {text: 'I have been informed that the implant must remain covered, under the gingival (gum) tissue, for at least 3 months before it can be uncovered. A second surgical procedure is required to uncover the top of the implant. The crown or dental prosthesis and stabilizing bars will be attached to the implant(s) after it is uncovered.\n'},
      { text: ['I read and understand the above section .', { text: `${props.section14 ? "      " + props.section14 + "      " : "        "}\n\n`, style: 'liner' }, ], alignment: 'right' },
  
      {text: 'I understand that the implant should last for many years but that no guarantee that it will last for any specific period of time can be or has been given. It has been explained to me that once the implant is inserted, the entire dental treatment plan, including my personal oral hygiene and prophylaxis, must be followed and completed on schedule, or the implant may fail. \n'},
      { text: ['I read and understand the above section .', { text: `${props.section15 ? "      " + props.section15 + "      " : "        "}\n\n`, style: 'liner' }, ], alignment: 'right' },
  
      {text: 'The alternatives to implant treatment, including no treatment, construction of a new standard denture or partial denture, augmentation of the maxilla (upper jaw) or mandible (lower jaw), and soft tissue or bone grafting have been explained to me. The advantages and disadvantages of each of these procedures has also been discussed and explained to me.    \n'},
      { text: ['I read and understand the above section .', { text: `${props.section16 ? "      " + props.section16 + "      " : "        "}\n\n`, style: 'liner' }, ], alignment: 'right' },
  
      {text: 'INFORMED CONSENT AND TREATMENT CONFIRMATION\n\n',fontSize: 14,bold: true, alignment: 'center'},
  
      {text: 'I certify that this "Prosthodontic Treatment" pamphlet outlining general treatment considerations and potential problems and hazards of prosthodontic treatment was presented to me and that I have read and understand its contents. I understand that potential hazards and problems may include, but are not limited to, those described in the brochure. I have had the opportunity to discuss my proposed treatment and the potential problems with the doctor to clarify any areas I did not understand. I authorize the doctor to provide prosthodontic treatment.\n\n'},
      { text: ['Patient\'s Name: ', { text: `${props.patientName2 ? "      " + props.patientName2 + "      " : "        "}\n\n`, style: 'liner' }, ] },

      // {text: '\n\n\n'},
  
      {
        table: {
          dontBreakRows: true,
          unbreakable: true,
          widths: [20, 150, 20, 150],
          body: [
            [{},{ image: `${props.signature2}`,fit: [150,150],border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.date2}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Patient Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
  
      // {text: '\n\n\n'},
      {
        table: {
          dontBreakRows: true,
          unbreakable: true,
          widths: [20, 150, 20, 150],
          body: [
            [{},(props.signature3 != undefined) ?{ image: `${props.signature3}`,fit: [150,150],border: [false, false, false, true], alignment: 'center' }: {text: ' ',border: [false, false, false, true],},{},
             { text: `${props.date3}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Doctor Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      // {text: '\n\n'},
  
      
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
   };

   var consent21 = {
    info: {
      title: 'PRP/PRF Intravenous Access Consent Form',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'PRP/PRF Intravenous Access Consent Formn\n\n',
        style: 'header'
      },
  
  
      {text: 'PURPOSE\n\n',bold: true, fontSize: 14},
  
      {text: 'To obtain whole blood that will be centrifuged. This will facilitate the concentration of platelets, white blood cells, and the growth (healing) factors contained within these cells. These cells and the growth factors contained within them will be placed into the surgery sight to accelerate tissue repair and improve healing predictability.\n\n'},
  
      {text: 'PROCEDURES\n\n',bold: true, fontSize: 14},
  
      {text: 'For this purpose, 20 to 60 mL (approximately ½ to 2 ounces) of your blood will be needed. The procedure involves placing a needle in a vein in your arm to take blood. This will require no more than a few minutes.\n\n'},
  
      {text: 'RISKS\n\n',bold: true, fontSize: 14},
  
      {text: 'Occasionally there are complications, which typically resolve in 1 to 2 weeks. You may experience bruising, swelling, numbness, black and blue marks, fainting and/or infection at the site. Rarely do complications escalate into more serious problems. As with any procedure we cannot guarantee that you would not have an undesired event associated with the withdrawal of blood from your vein(s). This is similar to the withdrawal of blood for a blood test at the hospital.\n\n'},
  
      {text: 'BENEFITS\n\n',bold: true, fontSize: 14},
  
      {text: 'Healing of soft tissue and bone will be enhanced with the placement of concentrated platelets and white blood cells into the surgical area. Growth (healing) factors released from these cells will help attract other cells, such as stem cells, from your surrounding healthy tissues to the surgical area. In doing so, these attracted cell types will improve your chances of faster healing, and therefore fewer surgical complications.   \n\n'},
  
      {text: 'By signing below, you consent to participate in the procedure described above.\n\n '},
  
      { text: ['Patient’s Name:(Printed) ', { text: `${props.patientName ? "      " + props.patientName + "      " : "        "}`, style: 'liner' },' Date: ',{ text: `${props.date1 ? "      " + props.date1 + "      " : "        "}\n\n`, style: 'liner' }, ] },
  
      {
        columns: [{ text: 'Parent’s Signature: ', margin:(windowSize > 480) ? [0, 55, 0, 0] : [0, 50, 0, 0] , width: '27%' }, (props.signature1 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature1}`, fit: [170, 170], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 25, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        },]
      },
  
      {text: '\n\n'},
  
      {text: 'If the Patient is unable to sign or if the patient is a minor:\n\n',bold: true},
  
      { text: ['Name of Legal Representative(Printed): ', { text: `${props.legalRepresentate ? "      " + props.legalRepresentate + "      " : "        "}`, style: 'liner' },' Date: ',{ text: `${props.date2 ? "      " + props.date2 + "      " : "        "}\n\n`, style: 'liner' }, ] },

      {text: '\n\n\n'},
  
      {
        columns: [{ text: 'Signature of Legal Representative: ', margin:(windowSize > 480) ? [0, 55, 0, 0] : [0, 50, 0, 0] , width: '50%' }, (props.signature2 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature2}`, fit: [150, 150], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 25, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }]
      },
  
      {
        columns: [{ text: 'Dentist Signature: ', margin:[0, 30, 0, 0] , width: '27%' }, (props.signature3 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature3}`, fit: [170, 170], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 25, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }, { text: ['Date: ', { text: `${props.date3}`, style: 'liner' }, '\n\n'], width: '30%', margin:[0, 30, 0, 0] }]
      },
  
      {
        columns: [{ text: 'Witness Signature: ', margin:[0, 30, 0, 0] , width: '27%' }, (props.signature4 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature4}`, fit: [170, 170], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 25, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }, { text: ['Date: ', { text: `${props.date4}`, style: 'liner' }, '\n\n'], width: '30%', margin:[0, 30, 0, 0] }]
      },
  
      
  
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
   };

   var consent1a6 = {
    info: {
      title: 'Tooth Extractions And Related Surgery Consent Form',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Tooth Extractions And Related Surgery Consent Form\n\n\n',
        style: 'header'
      },
  
      {text: [{text:'Instructions to Patient:',bold: true, decoration: 'underline'}, '  Please take this document home and read it carefully. Note any questions you might have in the area provided. Bring this back to our office at your next appointment, and the doctor will review it with you before you sign it.\n\n']},
  
      {
        ul: [
          // {columns:[
            {text: 'My dentist has recommended the following procedures: ', width: '100%'},
  
          ]},
  
          (props.exception) ? {
            margin: [20, 0, 0, 0],
            table: {
              widths: ['93%'],
              body: [
                [{ text: `${props.exception}`, border: [false, false, false, true] }]
              ]
            }, layout: {
              defaultBorder: false,
            }
          } : {
            margin: [20, 0, 0, 0],
            table: {
              widths: ['93%'],
              body: [
                [{ text: " ", border: [false, false, false, true], }]
              ]
            }, layout: {
              defaultBorder: false,
            },
          },
          {text: '\n\n'},
    
        
  
         {
        ul: [
         
          'I have been informed of the risks and complications of the recommended oral surgical procedures, anesthesia, and the proposed drugs, including, but not limited to: pain, infection, swelling, heavy or prolonged bleeding, discoloration, numbness, and tingling of the lip, tongue, chin, gums, cheeks and teeth; pain, numbness and phlebitis (inflammation of a vein) from an intravenous and/or intramuscular injection; injury to and stiffening of the neck and facial muscles; malfunction of the adjacent facial muscles for an indefinite time; change in occlusion or temporomandibular joint (TMJ) difficulty; or injury to adjacent teeth, restorations in other teeth, or adjacent soft tissues.\n\n',
  
          'I  have further been informed of other potential complications, including, but not limited to: nausea, vomiting, allergic reaction, bone fractures, bruises, delayed healing, sinus complications, openings from the sinus into the mouth, apparent facial changes, nasal changes, the possibility of secondary surgical procedures, loss of bone and the invested teeth, non-healing of the bony segments, devitalization (nerve damage that may require root canal treatment) of teeth, and relapse.\n\n',
  
          'I am aware that the practice of dentistry and dental surgery is not an exact science, and I acknowledge that no guarantees have been made to me concerning the success of this procedure, the associated treatment and procedures, or the Tooth Extractions And Related Surgery Consent Form postsurgical dental procedures. I am further aware that there is a risk of failure and/or that further corrective surgery may be necessary. Such a failure and remedial procedures may involve additional fees being assessed.\n\n',
  
          'I agree and understand I am not to have anything to eat for 8 hours before my surgery or as instructed by the Dentist.\n\n',
  
          {text:['I authorize ',{ text: `${props.staff1 ? "      " + props.staff1 + "      " : "          "} `, style: 'liner' },' to perform the recommended dental procedures. I agree to the type of anesthesia that he/she has discussed with me, specifically (local) (IV sedation) or (general). I agree not to operate a motor vehicle or hazardous device for at least twenty-four (24) hours after the procedure or until fully recovered from the effects of the anesthesia or drugs given for my care. I agree not to drive home after my surgery and will have a responsible adult drive me or accompany me home after my discharge from surgery.\n\n']},
  
          {text:['If an unforeseen condition arises in the course of treatment that calls for the performance of procedures in addition to or different from that now contemplated and I am under general anesthesia or IV sedation, I further authorize and direct ',{ text: `${props.staff2 ? "      " + props.staff2 + "      " : "          "} `, style: 'liner' },' and the associates or assistants of his/her choice, to do whatever he/she/they deem necessary and advisable under the circumstances, including the decision not to proceed with the surgical procedure.\n\n']},
  
          'I agree to cooperate with the postoperative instructions of my dentist, realizing that any deviation from the instructions or lack of cooperation could result in less than optimal results. I further agree that if I do not follow my dentist’s recommendations and advice for postoperative care, my dentist may terminate the dentist-patient relationship, requiring me to seek treatment from another dentist.\n\n',
  
          'To my knowledge, I have given an accurate report of my health history. I have also reported any prior allergic or unusual reactions to drugs, food, insect bites, anesthetics, pollens, dust, blood or body diseases, gum or skin reactions,abnormal bleeding, or any other condition relating to my health or any problems experienced with any prior medical, dental, or other health care and treatment.\n\n',
  
          'The fee for services has been explained to me and is acceptable, and I understand that there is no warranty or guarantee as to the result of this treatment.\n\n',
  
          'I realize and understand that the purpose of this document is to evidence the fact that I am knowingly consenting to the oral surgical procedures recommended by my dentist.\n\n',
  
          
          {text:'Questions I have to ask mydentist: '},
          ]},
  
          (props.questions) ? {
            margin: [20, 0, 0, 0],
            table: {
              widths: ['93%'],
              body: [
                [{ text: `${props.questions}`, border: [false, false, false, true] }]
              ]
            }, layout: {
              defaultBorder: false,
            }
          } : {
            margin: [20, 0, 0, 0],
            table: {
              widths: ['93%'],
              body: [
                [{ text: " ", border: [false, false, false, true], }]
              ]
            }, layout: {
              defaultBorder: false,
            },
          },
          {text: '\n\n'},
          
          {
            ul: [
          'I CERTIFY THAT I HAVE READ AND FULLY UNDERSTAND THE ABOVE AUTHORIZATION AND INFORMED CONSENT TO THIS PROCEDURE AND THAT ALL OF MY QUESTIONS, IF ANY, HAVE BEEN ANSWERED. I HAVE HAD THE OPPORTUNITY TO TAKE THIS FORM HOME AND REVIEW IT BEFORE SIGNING IT.\n\n',
  
        ]
      },
  
      { text: ['Name: ', { text: `${props.patientName ? "      " + props.patientName + "      " : "          "} \n\n`, style: 'liner' }], margin: [20, 0, 40, 0] },
  
      { text: ['Relationship to minor patient (if applicable): ', { text: `${props.relationShip ? "      " + props.relationShip + "      " : "          "} \n\n`, style: 'liner' }], margin: [20, 0, 40, 0] },
  
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},{ image: `${props.signature1}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.date1}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
            ],
            [{}, { text: 'Patient Signature',alignment: 'center' },{},
            { text: 'Date',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
  };
  
  var consent23 = {
    info: {
      title: 'Zygomatic and Tubero-Pterygoid Consent Form',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Zygomatic and Tubero-Pterygoid Consent Form\n\n\n',
        style: 'header'
      },
   
      {text: 'Dental implants are performed as a basis for oral rehabilitation in cases one tooth or more are missing. The placement of a dental implant is a surgical procedure. Zygomatic and Tubero-Pteryoid implants were developed as rehabilitative solution for patients suffering from severe bone loss in the upper jaw that does not enable placement of conventional dental implants. The implants designated for placement in the zygomatic bone, in the tuberosity or in the pterygoid region for the purpose of dental rehabilitation.\n\n'},
   
     {
       table: {
         widths: [20, 150, 20, 150],
         body: [
           [{}, { text: `${props.firstName}`, border: [false, false, false, true], alignment: 'center' },{},
           { text: `${props.lastName}`, border: [false, false, false, true], alignment: 'center' },
           ],
           [{}, { text: 'First Name', alignment: 'center' },{},
           { text: 'Last Name',alignment: 'center' },
           ],
         ]
       }, layout: {
         defaultBorder: false,
       }
     },
   
       {text: 'declare and confirm that I received a detailed verbal information from:\n\n', bold: true, fontSize: '14'},
       {
         table: {
           widths: [20, 150, 20, 150],
           body: [
             [{},{ text: `${props.firstName1}`, margin: [0, 17, 0, 0], border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.lastName1}`, margin: [0, 17, 0, 0], border: [false, false, false, true], alignment: 'center' }
             ],
             [{}, { text: 'Last Name', alignment: 'center' },{},
           { text: 'First Name', alignment: 'center' },
           ],
           ]
         }, layout: {
           defaultBorder: false,
         }
       },
   
        { text: ['on treating me using zygomatic and/or Tubero-Pterygoid implants in the upper jaw.Details (type, location and quantity): ',{ text: `${props.quantity ? "      " + props.quantity + "      " : "          "} `,  width: '30%', margin: [20, 50, 0, 0],style: "liner" },'(Hereinafter: The Principal Treatment"). I was informed of the treatment necessary for the placement of a dental implant, including the hopeful results and possible alternative treatments under the circumstances of my condition. I have considered the alternative treatments before choosing this treatment.\n\n'] },
   
        {text: 'I was also informed of the importance of quitting smoking before and after the treatment, of treating gum disease and of controlling diabetes. It was explained to me that smoking, untreated gum disease and diabetes significantly increase the risk of dental implantation failure. It was further explained to me that the combination of surgical treatment and bisphosphonate medications, whether such medications are being taken now or had been taken in the past (medications for the treatment of osteoporosis and/or of bone diseases and/or useof steroids) increase the risk of chronic inflammation that can amount to necrosis of the jaw bones.\n\n'},
   
        {text: 'I was informed of the side effects of the Principal Treatment, including considerable swelling, pain,subcutaneous hematoma, and temporary limitation in mouth opening.\n\n'},
   
        {text: 'I was also informed of the risks and complications related to the Principal Treatment, including infection, injury to facial nerves during implantation, which means temporary or permanent impairment of sensation in the affected site, possible injury to the maxillary sinus during treatment of the upper jaw, which may require further treatment and the development of sinusitis shortly after the treatment and/or at a later date. There is also a risk of injury to major anatomical structures such as the eye orbit, the eyeball and brain, fractures of the zygomatic bone and injury to central blood vessels.\n\n'},
   
        {text: 'It has been further explained to me that the manner and duration of recovery of the bone and gums following insertion of dental implants are individual and unpredictable. I was informed of the possibility of dental implant failure, and I understand that in such case, it will become necessary to remove the implant and/ or to perform corrective treatment.\n\n'},
   
        {text: 'It has also been explained me, and I understand the importance of continuity of the treatment at the same place and of cooperation between the doctor performing the dental implant insertion and the doctor performing the rehabilitative treatment.\n\n'},
   
        {text: 'I understand the importance of providing accurate information regarding my health condition and of complying with all the instructions given to me by the treating staff/doctor, including maintaining oral hygiene, and receiving all necessary operative and prosthetic treatments and attending follow-up checkups on schedule as may be required.\n\n'},
   
        {text: 'I hereby give my consent to the Principal Treatment.\n\n'},
   
        {text: 'My consent is also given to local anesthesia, after being informed of the risks and complications of anesthesia including loss of sensation in the lip and/or tongue and/or chin and/or face, hematoma, swelling and temporary limitation in mouth opening. Should the Principal Treatment be performed under general anesthesia or under intravenous sedation, the anesthetic technique would be explained to me by an anesthesiologist.\n\n'},
   
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},{ text: `${props.date1}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },{},
             { image: `${props.signature1}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' },
            ],
            [{},{ text: 'Date',  alignment: 'center' },{},
             { text: 'Patient Signature',alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
        
       {
         table: {
           widths: [20, 150, 20, 150],
           body: [
             [{}, { text: `${props.guardian}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },{},
             { image: `${props.signature2}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' },
             ],
             [{}, { text: 'Name of Guardian (relationship)',alignment: 'center' },{},
             { text: 'Guardian’s Signature (when patient is legally incompetent, a minor or mentally ill)',  alignment: 'center' },
             ],
   
           ]
         }, layout: {
           defaultBorder: false,
         }
       },
   
        {text: 'I confirm that I explained verbally to the patient/the patient’s guardian/ all the aforementioned in detail as required and that he/she signed before me, after I was satisfied that he/she fully understood my explanation.\n\n'},
        // {text: '\n\n\n\n'},
       {
         table: {
           widths: [20, 150, 20, 150],
           body: [
             [{}, { text: `${props.lastName3}`, border: [false, false, false, true], alignment: 'center' },{},
             { text: `${props.firstName3}`,border: [false, false, false, true], alignment: 'center' },
             ],
             [{}, { text: 'Last Name',alignment: 'center' },{},
             { text: 'First Name',  alignment: 'center' },
             ],
   
           ]
         }, layout: {
           defaultBorder: false,
         }
       },
       {text: '\n\n\n\n'},
   
      {
        table: {
          widths: [20, 150, 20, 150],
          body: [
            [{},(props.signature3 != undefined) ?{ image: `${props.signature2}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' }: {text: ' ',border: [false, false, false, true],},{},
            (props.licence != undefined) ? { text: `${props.date3}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' } : {text: ' ',border: [false, false, false, true],},
            ],
            [{}, { text: 'Doctor\'s Signature',alignment: 'center' },{},
            { text: 'License No',  alignment: 'center' },
            ],
  
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {text: '\n\n'},
   
       
   
     ],
     styles: {
       header: {
         fontSize: 18,
         bold: true,
         alignment: 'center'
       },
       subheader: {
         fontSize: 15,
         bold: true
       },
       quote: {
         italics: true
       },
       small: {
         fontSize: 8
       },
       lining: {
         alignment: 'center',
       },
       liner: {
         decoration: 'underline',
   
       }
     },
     defaultStyle: {
       fontFamily: 'Avenir-Regular',
       fontSize: 12,
       color: '#313030',
     }

    };

  var consent24 = {
    info: {
      title: 'Endodontic Surgical Therapy Consent Form',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Endodontic Surgical Therapy Consent Form\n\n\n',
        style: 'header'
      },
      { text: ['Name of Patient: ', { text: `${props.patientName ? "      " + props.patientName + "      " : "          "} \n\n`, style: 'liner' }]},
      { text: ['Birth date: ', { text: `${props.dob ? "      " + props.dob + "      " : "          "} \n\n`, style: 'liner' }] },
      { text: ['I,', { text: `${props.ill ? "      " + props.ill + "      " : "          "} `, style: 'liner' },' authorize ',{ text: `${props.staff ? "      " + props.staff + "      " : "          "}`, style: 'liner' },'to perform an apical surgical procedure on tooth #',{ text: `${props.tooth ? "      " + props.tooth + "      " : "          "} \n\n`, style: 'liner' }]},
      { text: 'I understand that a surgical procedure has been recommended for me. If this treatment is not rendered, my present condition may possible deteriorate, resulting in loss of my tooth and/or damage to surrounding tissues. I have discussed the proposed procedure with the doctor named above before I was asked to consider this form which indicates my consent to the procedure\n\n' },
      {text: 'I understand the purpose of this procedure is to surgically treat the inflammatory or diseased conditions around the root of this tooth. Although surgical Endodontics has a high degree of success, individual patients have varying physiological responses, therefore, results cannot be guaranteed. As with all surgery, risks accompany this  procedure. The surgical risks include (patients are to initial each line as they acknowledge and accept these potential risks):\n\n'},
      {
        table: {
          widths: [20,60,'90%'],
          body: [
            [{},{ text: `${props.sideEffect1 ?  "      " + props.sideEffect1 + "      " : "          "}`, decoration: 'underline' },
            { text: 'Infection'}]
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {
        table: {
          widths: [20,60,'90%'],
          body: [
            [{},{ text: `${props.sideEffect2 ?  "      " + props.sideEffect2 + "      " : "          "}`, decoration: 'underline' },
            { text: 'Swelling'}]
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      
      {
        table: {
          widths: [20,60,'90%'],
          body: [
            [{},{ text: `${props.sideEffect3 ?  "      " + props.sideEffect3 + "      " : "          "}`, decoration: 'underline' },
            { text: 'Discomfort'}]
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {
        table: {
          widths: [20,60,'90%'],
          body: [
            [{},{ text: `${props.sideEffect4 ?  "      " + props.sideEffect4 + "      " : "          "}`, decoration: 'underline' },
            { text: 'Gum Recession (shrinkage in the area of the involved tooth)'}]
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {
        table: {
          widths: [20,60,'90%'],
          body: [
            [{},{ text: `${props.sideEffect5 ?  "      " + props.sideEffect5 + "      " : "          "}`, decoration: 'underline' },
            { text: 'Bleeding beneath the skin resulting in temporary black and blue discoloration of the face and/or gums.'}]
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {
        table: {
          widths: [20,60,'90%'],
          body: [
            [{},{ text: `${props.sideEffect6 ?  "      " + props.sideEffect6 + "      " : "          "}`, decoration: 'underline' },
            { text: 'Paresthesia (numbness or tingling in the area of the lip and/or gum which typically is temporary, but can be permanent).'}]
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
      {
        table: {
          widths: [20,60,'90%'],
          body: [
            [{},{ text: `${props.sideEffect7 ?  "      " + props.sideEffect7 + "      " : "          "}`, decoration: 'underline' },
            { text: 'It is possible the tooth may require extraction if the surgical procedure is not successful due to many causes, some of which are infection or root fractures.'}]
          ]
        }, layout: {
          defaultBorder: false,
        }
      },
  
  
      {text: 'If I have any questions with respect to these potential complications or others, I understand I must not sign this consent form until I have had all questions answered to my satisfaction.\n\n'},
      {text: 'I understand in the course of this procedure and during follow-up, developments may occur which call for the doctors judgment as to further diagnostic or therapeutic procedures. If such developments occur, I hereby authorize the additional procedures as indicated by the judgment of the dentist. \n\n'},
      {text: 'The nature of the above described procedure has been explained to me to my  satisfaction, and I have been provided the opportunity to ask any and all questions about my condition, treatment alternatives, and prognosis with or without surgery. I agree I have been given no guarantee or assurance of any specific result. I further understand that by signing this form, I provide my consent to the above procedure and I acknowledge I have been informed of the relevant benefits and risks prior to providing my consent.\n\n'},
      { text: ['Date: ', { text: `${props.date1 ? "      " + props.date1 + "      " : "          "} \n\n`, style: 'liner' }], margin: [20, 0, 40, 0] },
      {
        columns: [{ text: 'Patient, Parent of a Minor Parent’s Signature: ', margin: [0, 40, 0, 0], width: '50%' }, (props.signature1 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature1}`, fit: [150, 150], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 25, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }]
      },
      {
        columns: [{ text: 'WITNESS: ', margin: [0, 30, 0, 0], width: '50%' }, (props.signature2 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature2}`, fit: [150, 150], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 30, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }]
      },
      {
        columns: [{ text: 'Witness Signature: ', margin: [0, 30, 0, 0], width: '50%' }, (props.signature3 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature3}`, fit: [150, 150], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 30, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }]
      },
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      },
      linerForSide: {
        decoration: 'underline',
        margin: [20, 0, 40, 0],
        width: '30%'
      },
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
  };

  var consent25 = {
    info: {
      title: 'Endodontic Therapy Consent Form',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
      {
        text: 'Endodontic Therapy Consent Form\n\n\n',
        style: 'header'
      },
      { text: 'Please review the following consent form. You will be required to sign this form prior to the initiation of treatment. \n\n' },
      { text: 'I understand the root canal therapy is a procedure that retains a tooth, which may otherwise require extraction. Although root canal therapy has a very high degree of success, results cannot be guaranteed. Occasionally, a tooth, which has had root canal therapy, may require re-treatment, surgery, or even extraction. Following treatment, the tooth may be brittle and subject to fracture. A restoration (filling), crown and/or post and core will be necessary to restore the tooth, and your general dentist will perform these procedures. During endodontic treatment, there is the possibility of instrument separation within the root canals, perforations (extra openings), damage to bridges, existing fillings, crowns or porcelain veneers, missed canals, loss of tooth structure in gaining access to canals, and fractured teeth. Also, there are times when a minor surgical procedure may be indicated or when the tooth may not be amenable to endodontic treatment at all. Other treatment choices include no treatment, a waiting period for more definitive symptoms to develop, or tooth extraction. Risks involved in those choices might include, but are not limited to, pain, infection, swelling, loss of teeth, and infection to other areas. Occasionally, medication will be prescribed. Medications prescribed for discomfort and/or sedation may cause drowsiness, which can be increased by the use of alcohol or other drugs. We advise that you do not operate a motor vehicle or any hazardous device while taking such medications. In addition, certain medications may cause allergic reactions, such as hives or intestinal discomfort. If any of these problems occur, call the office immediately. It is the patient\'s responsibility to report any changes in his/her medical  history\n\n' },
  
      { text: 'All of my questions have been answered by the dentist and I fully understand the above statements in this consent form.\n\n' },
  
      { text: ['Furthermore, I give ', { text: `${props.staff ? "      " + props.staff + "      " : "        "}`, style: 'liner' }, 'my permission to voice record, tape digitally, videotape and/or take 35mm and/or digital photos of my procedure for purposes of completing my medical record and/or for patient education.    Note: All medical records will be kept strictly confidential. (If patient is under the age of 18, the signature of a parent or guardian is required.)\n\n'] },
  
      { text: ['Date: ', { text: `${props.date1 ? "      " + props.date1 + "      " : "          "} \n\n`, style: 'liner' }], margin: [20, 0, 40, 0] },
  
      {
        columns: [{ text: 'Patient, Parent of a Minor Parent’s Signature: ', margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], width: '50%' }, (props.signature1 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature1}`, fit: [150, 150], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }]
      },
      {
        columns: [{ text: 'WITNESS: ',margin:(windowSize > 480) ? [0, 30, 0, 0] : [0, 30, 0, 0],width: '50%' }, (props.signature2 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature2}`, fit: [150, 150], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 30, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }]
      },
      {text:'\n\n\n\n\n'},
      
      {
        columns: [{ text: 'Witness Signature: ',margin:(windowSize > 480) ? [0, 30, 0, 0] : [0, 30, 0, 0],width: '50%' }, (props.signature3 != undefined) ? {
          table: {
            widths: [150],
            body: [
              [{ image: `${props.signature3}`, fit: [150, 150], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        } : {
          table: {
            widths: [150],
            body: [
              [{ text: " ", margin: [0, 30, 0, 0], border: [false, false, false, true] }]
            ]
          }, layout: {
            defaultBorder: false,
          }
        }]
      },
      
    ],
    styles: {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center'
      },
      subheader: {
        fontSize: 15,
        bold: true
      },
      quote: {
        italics: true
      },
      small: {
        fontSize: 8
      },
      lining: {
        alignment: 'center',
      },
      liner: {
        decoration: 'underline',
  
      }
    },
    defaultStyle: {
      fontFamily: 'Avenir-Regular',
      fontSize: 12,
      color: '#313030',
    }
  };
  
  var consentletters1 = {
    info: {
        title: 'Implant Treatment Planning Letter',
    },
    pageSize: 'A4',
    pageOrientation: 'portait',
    pageMargins: [80, 60, 80, 40],
    content: [
        {
            text: 'Implant Treatment Planning Letter\n\n\n',
            style: 'header'
        },

        {text:[{text: 'Date: '},{ text:  "      " + `${props.date1}`+ "      \n\n", width: '30%', margin: [60, 50, 0, 0],style: 'liner' }]},

        {text:[{text: 'Dear '},{ text:  "      " + `${props.staff}`+ "      \n\n", width: '30%', margin: [60, 50, 0, 0],style: 'liner'}]},

        {text:[{text: 'Thank you for visiting our office on '},{ text:  "      " + `${props.date2}`+"      ", width: '30%', margin: [60, 50, 0, 0],style: 'liner'},'for consultation, evaluation, and implant treatment recommendations. I believe that through your collaboration with us we will be able to provide you with a successful treatment.\n\n']},

        {text: 'IMPORTANT READING\n\n',fontSize: 14, bold: true},

        {text: 'Please take the time to carefully read this letter and the material in the Patient Information Booklets / Brochures and Consent Forms that we gave you at your consultation. The letter explains the course of treatment and points out special considerations that are applicable to you.\n\n'},

        {text: 'DIAGNOSIS\n\n',fontSize: 14, bold: true},
        {text:[{text: 'The basic diagnosis is : '},{ text:  "      " + `${props.diagnosis}`+ "      \n\n", width: '30%', margin: [60, 50, 0, 0],style: 'liner' },]},
        {
          ol:[
            {text:[{text: 'You are missing : '},{ text:  "      " + `${props.missing}`+ "      \n\n", width: '30%', margin: [60, 50, 0, 0],style: 'liner' },]},
            {text:[{text: 'Bone status : '},{ text:  "      " + `${props.status}`+ "      \n\n", width: '30%', margin: [60, 50, 0, 0],style: 'liner'  },]},
            {text:[{text: 'Oral hygiene is : '},{ text:  "      " + `${props.oral}`+ "      \n\n", width: '30%', margin: [60, 50, 0, 0],style: 'liner'  },]},
        ]},
        {text: 'You have told me that you would prefer to wear a fixed appliance supported by implants rather than a removable one.\n\n'},
        { text: ['initials ', { text: `${props.section1 ? "      " + props.section1 + "      " : "        "}\n\n`, style: 'liner' }, ], alignment: 'right' },

        {text: 'SUGGESTIONS AND TREATMENT PLAN\n\n', fontSize: 14, bold: true},
        {text: 'As we discussed, I suggest:'},
        {
          ol:[
            {text:'Making models of your maxilla (upper arch) and mandible (lower arch) as well as a bite registration.\n\n'},
            {text:'Preparation of a study model and/or surgical template that will determine the most favorable position to place the implants if needed.\n\n'},

            {text:'Taking a CT Scan\n\n'},

            {text:[{text: 'Placing implants in the locations of : '},{ text:  "      " + `${props.implants1}`+ "      \n\n", width: '30%', margin: [60, 50, 0, 0],style: 'liner' },]},

            {text:'Healing process of approximately 4 to 6 months or as needed.\n\n'},

            {text:'After healing of the implant(s), I will place a temporary superstructure on top of the implant(s) and allow the gingiva (gum) to heal around the superstructure if needed (this point however does not apply to immediately loaded cases).\n\n'},

            {text:'After healing of the gingival (gum) tissues we will start the prosthetic treatment phase.\n\n'},
        ]},
        {text: 'After healing, these implants will act as anchors for your fixed appliance.\n\n'},
        { text: ['initials ', { text: `${props.section2 ? "      " + props.section2 + "      " : "        "}\n\n`, style: 'liner' }, ], alignment: 'right' },

        {text: 'POSSIBLE RISKS :\n\n',bold: true, fontSize: 14},
        {text: 'Reasonable foreseeable risks of implant treatment, as described in the Consent Form, may include mild to moderate pain or discomfort, swelling, and bruising. The remote risks of your treatment can include infection or failure of the implants to heal solidly in the bone.\n\n'},

        {text: 'I believe the likelihood of success is sufficiently high to justify this procedure, but only you can decide whether the risks are warranted. Many implants have been successfully placed and functioning for years: supporting crowns, partial dentures, and prosthetic appliances with no problems.\n\n'},

        {text: 'We are always concerned about the prognosis of dental implants and their restorations in patients who continue to smoke cigarettes, cigars, or pipes. It is well documented in our professional literature that smoking contributes to damage of the gingiva (gums) and bone around natural teeth as well as implants. This can result in infection, bone loss, and a lower immune resistance, requiring removal of teeth and implants. If you can eliminate smoking, maintain good home care, and have maintenance hygiene appointments on a regular three-month basis with your dentist, your prognosis can be expected to be very good.\n\n'},
        { text: ['initials ', { text: `${props.section3 ? "      " + props.section3 + "      " : "        "}\n\n`, style: 'liner' }, ], alignment: 'right' },

        {text: 'FEES for implants', bold: true},
        {text:[{text: 'My fee for the implant surgical procedure is $ '},{ text:  "      " + `${props.fee1}` + "      ", width: '30%',style: 'liner' , },'per implant. I will place ', {text:  "      " + `${props.order}`+ "      ", width: '30%',style: 'liner'  },'implants. The total estimated fee is $',{ text:  "      " + `${props.fee2}`+ "      ", width: '30%',style: 'liner'  },'. This fee includes the surgical procedure and the routine post surgical followup.\n\n']},
        { text: ['initials ', { text: `${props.section4 ? "      " + props.section4 + "      " : "        "}\n\n`, style: 'liner' }, ], alignment: 'right' },


        {text:[{text:'FEES for additional services', bold: true}, '(Extractions, Bone Grafting, etc.)']},
        {text:[{text: 'My fee for '},{ text:  "      " + `${props.implants2}`+ "      ",style: 'liner' },' $ ', {text:  "      " + `${props.fee3}`+ "      ", style: 'liner' },' The total estimated fee is $ ', {text:  "      " + `${props.fee4}`+ "      ", style: 'liner' },' . This fee includes the procedure (s) mentioned in this paragraph and the routine post surgical followup.\n\n']},
        { text: ['initials ', { text: `${props.section5 ? "      " + props.section5 + "      " : "        "}\n\n`, style: 'liner' }, ], alignment: 'right' },

        {text: 'INSURANCE\n\n',bold: true, fontSize: 14},
        {text: 'To date, most insurance companies exclude benefits for implants. We will assist you in the completion of any insurance forms; however, it is your responsibility to pay for this treatment and to communicate with your insurance company if your claim is disputed.\n\n'},
        { text: ['initials ', { text: `${props.section6 ? "      " + props.section6 + "      " : "        "}\n\n`, style: 'liner' }, ], alignment: 'right' },

        {text: 'ORAL HYGIENE\n\n',bold: true, fontSize: 14},
        {text: 'After the implants have healed, your cooperation is vital for long-term success. You must maintain good oral hygiene and visit our office for periodic examinations.\n\n'},
        { text: ['initials ', { text: `${props.section7 ? "      " + props.section7 + "      " : "        "}\n\n`, style: 'liner' }, ], alignment: 'right' },

        {text: 'TREATMENT OPTIONS', fontSize: 14, bold: true},
        {text: 'Implants are one option. Other options include:\n\n'},
        {
          ol:[
            { text: `${props.option1 ? "      " + props.option1 + "      " : "        "}\n\n`, style: 'liner',  },
            { text: `${props.option2 ? "      " + props.option2 + "      " : "        "}\n\n`, style: 'liner',},
            { text: `${props.option3 ? "      " + props.option3 + "      " : "        "}\n\n`, style: 'liner',},
          ]
        },
        { text: ['initials ', { text: `${props.section8 ? "      " + props.section8 + "      " : "        "}\n\n`, style: 'liner' }, ], alignment: 'right' },

        {text: 'The disadvantages and risks of the non-implant treatment alternatives are:\n\n'},
        {
          ol:[
            { text: `${props.disadvantage1 ? "      " + props.disadvantage1 + "      " : "        "}\n\n`, style: 'liner' },
            { text: `${props.disadvantage2 ? "      " + props.disadvantage2 + "      " : "        "}\n\n`, style: 'liner' },
            { text: `${props.disadvantage3 ? "      " + props.disadvantage3 + "      " : "        "}\n\n`, style: 'liner' },
          ]
        },
        { text: ['initials ', { text: `${props.section9 ? "      " + props.section9 + "      " : "        "}\n\n`, style: 'liner' }, ], alignment: 'right' },

        {text: 'INSTRUCTIONS\n\n', fontSize: 14, bold: true},
        {text: 'When you come for your next appointment, please bring this letter and the consent forms. After all your questions have been answered to your satisfaction, you will sign the forms. We\'ll make copies for your records; the originals will become part of your permanent file.'},
        {text: 'I feel confident that we can provide you with successful results. Please call me if you have any questions regarding this implant treatment plan.\n\n'},

        { text: ['Patient\'s Name: ', { text: `${props.patientName2 ? "      " + props.patientName2 + "      " : "          "} \n\n`, style: 'liner' }], margin: [20, 0, 40, 0] },

        {
          table: {
            widths: [20, 150, 20, 150],
            body: [
              [{},{ image: `${props.signature1}`,fit: [170,170],border: [false, false, false, true], alignment: 'center' },{},
               { text: `${props.date3}`, margin:(windowSize > 480) ? [0, 40, 0, 0] : [0, 50, 0, 0], border: [false, false, false, true], alignment: 'center' },
              ],
              [{}, { text: 'Patient Signature',alignment: 'center' },{},
              { text: 'Date',  alignment: 'center' },
              ],
    
            ]
          }, layout: {
            defaultBorder: false,
          }
        },

    ],
    styles: {
        header: {
            fontSize: 18,
            bold: true,
            alignment: 'center'
        },
        subheader: {
            fontSize: 15,
            bold: true
        },
        quote: {
            italics: true
        },
        small: {
            fontSize: 8
        },
        lining: {
            alignment: 'center',
        },
        liner: {
            decoration: 'underline',

        }
    },
    defaultStyle: {
        fontFamily: 'Avenir-Regular',
        fontSize: 12,
        color: '#313030',
    }
};



var docDefinition = (form == "consent1") ? consent1 : (form == "consent2") ? consent2 : (form == "consent3") ? consent3 : (form == "consent4") ? consent4 : (form == "consent5") ? consent5 : (form == "consent6") ? consent6 : (form == "consent7") ? consent7 : (form == "consent8") ? consent8 : (form == "consent9") ? consent9 : (form == "consent10") ? consent10 : (form == "consent11") ? consent11 : (form == "consent12") ? consent12 :(form == "consent13") ? consent13 : (form == "consent14") ? consent14 : (form == "consent15") ? consent15 : (form == "consent16") ? consent16 :(form == "consent17") ? consent17 : (form == "consent18") ? consent18 : (form == "consent19") ? consent19 : (form == "consent20") ? consent20 : (form == "consent21") ? consent21 : (form == "1a6") ? consent1a6 : (form == "consent23") ? consent23 : (form == "consent24") ? consent24 : (form == "consent25") ? consent25 : (form == "letters1") ? consentletters1 : {} ;
   if(Object.keys(docDefinition).length != 0){
     return  docDefinition
   }
  else{
    return ({})
  }
}
