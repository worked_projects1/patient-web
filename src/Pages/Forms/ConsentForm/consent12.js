
/**
 * 
 *  Consent12
 * 
 */
 import  * as React from 'react';
 import { useState, useEffect,useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm } from 'redux-form';
 import {  Grid,Button, } from '@mui/material';
 import Styles from './styles';
import {ImplementationFor} from '../Components/CreateRecordForm/utils'

 /**
  * 
  * @param {object} props 
  * @returns 
  */

 function Consent12(props) {
     const {  handleSubmit, destroy, } = props;
     const classes = Styles();
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState("");
     
     useEffect(() => {
        return () => destroy();
    }, []);
    
    const onChange = (val,eventValue) => {
    val=="signature1"&&setSignature1(eventValue)
    val=="signature2"&&setSignature2(eventValue)
    val=="signature3"&&setSignature3(eventValue)

    }

     return <Grid className={classes.firstContainer}  >
            <Grid className={classes.secContainer} >
                <Grid item className={classes.EditForm} justify="center">
                <span className={classes.formTitle}>Implant Surgery And Anesthesia Consent Form</span>
                </Grid>
                <Grid item className={classes.thirdContainer}>
                <form onSubmit={handleSubmit} id='consent12'>

                    <Grid item spacing={12} className={classes.content} justify="space-between">
                    <Grid>
                        <p><b>Instructions to Patient:</b> Please take this document home and read it carefully. Note any questions you might have in the area provided in paragraph 15. Bring this back to our office at your next appointment, and the doctor will review it with you before signing on page 4.</p>
                        <ol>
                            <li style={{padding: '10px 0px'}}>
                            My doctor has explained the various types of implants used in dentistry, and I have been informed of the alternatives to implant surgery for replacement of my missing teeth. I have also been informed of the foreseeable risks of those alternatives. I understand what procedures are necessary to accomplish the placement of the implant(s) either on, in, or through the bone, and I understand that the most common types of implants available are subperiosteal (on), endosteal (in), and transosteal (through). The implant type recommended for my specific condition is circled above. I also understand that endosteal
                            implants (more commonly known as root-form) generally have the most predictable prognosis. I further understand that subperiosteal implants, if an option for me, are not as widely used as root form implants but will negate the necessity of my undergoing the bone grafting and other surgical procedures that would be necessary for the placement of root-form implants. I understand that the risk associated with the use of a subperiosteal implant is the failure and loss of the implant, which could further reduce the minimal amount of existing bone that I now have, requiring more extensive bone grafting and other surgical procedures at some future time. I also understand that other dental practitioners may not be familiar or experienced in the use of subperiosteal implants, including their placement and maintenance as well as treatment of any problems that might arise involving the subperiosteal implant. I promise to, and accept responsibility for failing to, return to this office for examinations and any recommended treatment at least every 6 months. My failure to do so, for whatever reason, can jeopardize the clinical success of the implant system. Accordingly, I agree to release and hold my dentist harmless if my implant(s) fail as a result of my not maintaining an ongoing examination and preventive maintenance routine as stated above.  
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            I have further been informed that if no treatment is elected to replace the missing teeth or existing dentures, the nontreatment risks include but are not limited to: 
                                <ol className={classes.alphaSmall}>
                                    <li>
                                    Maintenance of the existing full or partial denture(s) with relines or remakes every 3 to 5 years or as otherwise may be necessary due to slow, but likely, progressive dissolution of the underlying denture-supporting jawbone
                                    </li>
                                    <li>
                                    Persistence or worsening of any present discomfort or chewing inefficiency with the existing partial or full denture over time 
                                    </li>
                                    <li>
                                    Drifting, tilting, and/or extrusion of remaining teeth 
                                    </li>
                                    <li>
                                    Looseness of teeth and periodontal disease (gingiva [gum] and bone), possibly followed by extraction(s) 
                                    </li>
                                    <li>
                                    A potential temporomandibular joint (TMJ) problem caused by a deficient, collapsed,or otherwise improper bite. 
                                    </li>
                                </ol>
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            I am aware that the practice of dentistry and dental surgery is not an exact science, and I acknowledge that no guarantees have been made to me concerning the success of my implant surgery, the associated treatment and procedures, or the postsurgical dental procedures. I am further aware that there is a risk that the implant placement may fail,which might require further corrective surgery associated with the removal. Such a failure and remedial procedures could also involve additional fees being assessed.  
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            I understand that implant success is dependent upon a number of variables, including but not limited to: operator experience, individual patient tolerance and health,anatomical variations, my home care of the implant, and habits such as grinding my teeth. I also understand that implants are available in a variety of designs and materials, and the choice of implant is determined in the professional judgment of my dentist. 
                            </li>
                           <li style={{padding: '10px 0px'}}>
                           I have further been informed of the foreseeable risks and complications of implant surgery, anesthesia, and related drugs, including but not limited to: failure of the implant(s), inflammation, swelling, infection, discoloration, numbness (exact extent and duration unknown), inflammation of blood vessels, injury to existing teeth, bone fractures, sinus penetration, delayed healing, or allergic reaction to the drugs or medications used. No one has made any promises or given me any guarantees about the outcome of this treatment or these procedures. I understand that these complications can occur even if all dental procedures are done properly.  
                            </li> 
                            <li style={{padding: '10px 0px'}}>
                            I have been advised that smoking and alcohol or sugar consumption may affect tissue healing and may limit the success of the implant. Because there is no way to accurately predict the gingival (gum) and bone healing capabilities of each patient, I know I must follow my dentist’s home care instructions and report to my dentist for regular examinations as instructed. I further understand that excellent home care, including brushing, flossing, and the use of any other device recommended by my dentist, is critical to the success of my treatment, and my failure to do what I am supposed to do at home will be, at a minimum, a partial cause of implant failure, should that occur. I understand that the more I smoke, the more likely it is that my implant treatment will fail, and I understand and accept that risk.
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            I have also been advised that there is a risk that the implant may break, which may require additional procedures to repair or replace the broken implant. 
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            I authorize my dentist to perform dental services for me, including implants and other related surgery such as bone augmentation. I agree to the type of anesthesia that he/she has discussed with me, circled here (local, IV, or general sedation), and its potential side effects. I agree not to operate a motor vehicle or hazardous device for at least twenty-four (24) hours or more until fully recovered from the effects of the anesthesia or drugs given for my care. My dentist has also discussed the various kinds and types of bone augmentation material, and I have authorized him/her to select the material that he/she believes to be the best choice for my implant treatment .
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            If an unforeseen condition arises in the course of treatment and calls for the
                            performance of procedures in addition to or different from that now contemplated and I am under general anesthesia or IV sedation, I further authorize and direct my dentist and his/her associates or assistants of his/her choice to do whatever he/she/they deem necessary and advisable under the circumstances, including the decision not to proceed with the implant procedure(s).  
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            I approve any reasonable modifications in design, materials, or surgical procedures if my dentist, in his/her professional judgment, decides it is in my best interest to do so.  
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            To my knowledge, I have given an accurate report of my health history. I have also reported any past allergic or other reactions to drugs, food, insect bites, anesthetics, pollens, dust; blood diseases; gingival (gum) or skin reactions; abnormal bleeding; or any other condition relating to my physical or mental health or any problems experienced with any prior medical, dental, or other health care treatment on my medical history questionnaire. I understand that certain mental and/or emotional disorders may contraindicate implant therapy and have therefore expressly circled either YES or NO to indicate whether or not I have had any past treatment or therapy of any kind or type for any mental or emotional condition. 
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            I authorize my dentist to capture photos, slides, x-rays, or any other visual aids of my treatment to be used for the advancement of implant dentistry in any manner my dentist deems appropriate. However, no photographs or other records that identify me will be used without my express written consent.
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            I realize and understand that the purpose of this document is to evidence the fact that I am knowingly consenting to the implant procedures recommended by my dentist. 
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            I agree that if I do not follow my dentist’s recommendations and advice for
                            postoperative care, my dentist may terminate the dentist-patient relationship, requiring me to seek treatment from another dentist. I realize that postoperative care and maintenance treatment is critical for the ultimate success of dental implants. I accept responsibility for any adverse consequences that result from not following my dentist’s advice. 
                            </li>
                            <li>
                            Questions I have to ask my dentist:
                            <Grid item xs={12} className={classes.textArea}>
                                <Field
                                    name={"exception"}
                                    type={"text"}
                                    component={ImplementationFor["textArea"]}
                                    variant="standard"
                                    required={true}
                                />
                            </Grid>
                            </li>
                            <li>
                            I CERTIFY THAT I HAVE READ AND FULLY UNDERSTAND THE ABOVE AUTHORIZATION AND INFORMED CONSENT TO IMPLANT PLACEMENT AND SURGERY AND THAT ALL MY QUESTIONS, IF ANY, HAVE BEEN FULLY ANSWERED. I HAVE HAD THE OPPORTUNITY TO TAKE THIS FORM HOME AND REVIEW IT BEFORE SIGNING IT. I UNDERSTAND AND AGREE THAT MY INITIAL ON EACH PAGE ALONG WITH MY SIGNATURE BELOW WILL BE CONSIDERED CONCLUSIVE PROOF THAT I HAVE READ AND UNDERSTAND EVERYTHING CONTAINED IN THIS DOCUMENT AND I HAVE GIVEN MY CONSENT TO PROCEED WITH IMPLANT TREATMENT AND RELATED SURGERY, INCLUDING ANY ANCILLARY BONE GRAFTING PROCEDURES.
                            </li>
                        </ol>
                        
                        <Grid  className={classes.flexContainer}>
                        <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                            <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Patient's Name:</span></Grid>
                            <Grid  item style={{ margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                    name={"patientName"}
                                    type={"text"}
                                    required={true}
                                    component={ImplementationFor["input"]}
                                />
                            </Grid>
                        </Grid>
                        </Grid>
                        
                        <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                            <Grid item  md={6} className={classes.signatureContainer}>
                                <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                    
                                    <Field
                                        name={"signature1"}
                                        type={"text"}
                                        autoFocus={true}
                                        component={ImplementationFor["signature"]}
                                        id={"signature1"}
                                        onChange={(e) => onChange("signature1", e)}
                                    /></Grid>
                                <Grid> Patient Signature</Grid>
                            </Grid>
                            <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                                <Grid item md={12} className={classes.DateContainer1}>
                                    <Field
                                        name={"date1"}
                                        type={"text"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                    />
                                    
                                </Grid>
                                <Grid>Date</Grid>
                            </Grid>
                        </Grid>

                        <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                        <Grid item  md={6} className={classes.signatureContainer}>
                            <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                
                                <Field
                                    name={"signature2"}
                                    type={"text"}
                                    autoFocus={true}
                                    component={ImplementationFor["signature"]}
                                    id={"signature2"}
                                    onChange={(e) => onChange("signature2", e)}
                                    disabled={true}
                                /></Grid>
                                <Grid>Doctor Signature</Grid>
                            </Grid>
                            <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                                <Grid item md={12} className={classes.DateContainer1}>
                                    <Field
                                        name={"date2"}
                                        type={"text"}
                                        component={ImplementationFor["date"]}
                                        disabled={true}
                                    />
                                    
                                </Grid>
                                <Grid>Date</Grid>
                            </Grid>
                        </Grid>


                            <Grid style={{textAlign:"center", padding: '20px 0px'}}>
                            <Button className={classes.button} type="submit" form="consent12">Submit</Button>
                            </Grid>
                        </Grid> 
                    </Grid>
              
            </form>
            </Grid >
            </Grid >
        </Grid>
      
 }
 
 Consent12.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent12',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true
 })(Consent12);