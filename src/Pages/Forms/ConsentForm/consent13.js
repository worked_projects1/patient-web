
/**
 * 
 *  Consent13
 * 
 */
 import  * as React from 'react';
 import { useState, useEffect,useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm } from 'redux-form';
 import {  Grid, Button } from '@mui/material';
 import Styles from './styles';
import {ImplementationFor} from '../Components/CreateRecordForm/utils'

 /**
  * 
  * @param {object} props 
  * @returns 
  */

 function Consent13(props) {
     const { handleSubmit, destroy, } = props;
     const classes = Styles();
     const [showModal, setModalOpen] = useState(false);
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState("");    
    
     useEffect(() => {
        return () => destroy();
    }, []);

    const onChange = (val,eventValue) => {
    val=="signature1"&&setSignature1(eventValue)
    val=="signature2"&&setSignature2(eventValue)
    val=="signature3"&&setSignature3(eventValue)

    }

    const option = [
        {
            "label": "Yes",
            "value": "Yes"
        },
        {
            "label": "No",
            "value": "No"
        }
    ]

     return <Grid className={classes.firstContainer}  >
            <Grid className={classes.secContainer} >
                <Grid item className={classes.EditForm} justify="center">
                <span className={classes.formTitle}>Infection Screening </span>
                </Grid>
                <Grid item className={classes.thirdContainer}>
                <form onSubmit={handleSubmit} id='consent13'>

                    <Grid item spacing={12} className={classes.content} justify="space-between">
                    <Grid>
                        <p>
                        As a safeguard to the coronavirus pandemic, we at this dental office would like our patients to know that your health and vitality are of utmost importance to us. We have implemented increased vigilance and safety protocols for clinician-patient interactions and workplace sanitation that take into account established universal precautions as set forth by major public health institutions. Please take the time to fill out the questionnaire below to help us better serve our patient base and ensure that we take maximal precautions to ensure that you receive the best care possible.
                        </p>
                        <Grid className={classes.radioGroup} container>
                            <Grid item  style={{ display: 'flex',}} >
                            Are you experiencing fever, cough, runnynose, or shortness of breath?
                            </Grid>
                            <Grid item style={{ display: 'inline-block',  margin: '5px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"ill"}
                                    options= {option}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                />
                                </Grid>
                        </Grid>

                        <Grid className={classes.radioGroup} container>
                            <Grid item  style={{ display: 'flex',}} >
                            Are you having any symptoms of respiratory infection? 
                            </Grid>
                            <Grid item style={{ display: 'inline-block',  margin: '5px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"symptoms"}
                                    options= {option}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                />
                                </Grid>
                        </Grid>

                        <Grid className={classes.radioGroup} container>
                            <Grid item  style={{ display: 'flex',}} >
                            Have you come in contact or been exposed to anyone with a respiratory infection? 
                            </Grid>
                            <Grid item style={{ display: 'inline-block',  margin: '5px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"expose"}
                                    
                                    options= {option}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                />
                                </Grid>
                        </Grid>

                        <Grid className={classes.radioGroup} container>
                            <Grid item  style={{ display: 'flex',}} >
                            Have you or your family traveled outside of the US with in the last month? 
                            </Grid>
                            <Grid item style={{ display: 'inline-block',  margin: '5px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"travel"}
                                    
                                    options= {option}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                />
                                </Grid>
                        </Grid>

                        <Grid className={classes.radioGroup} container>
                            <Grid item  style={{ display: 'flex',}} >
                            Have you or anyone you have been in close contact with (within 6 feet) traveled to mainland China, Hong Kong, Macau, Iran, South Korea, or the Italian regions of Emilia-Romagna, Lombardy and Veneta?
                            </Grid>
                            <Grid item style={{ display: 'inline-block',  margin: '5px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"closeContact"}
                                    required={true}
                                    options= {option}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                />
                                </Grid>
                        </Grid>

                        <b>Please take precautions:</b>
                        <p>
                        Wash your hands often with soap and water for at least 20 seconds.<br />
                        Avoid close contact with people who are sick.<br />
                        Avoid touching your eyes,nose, and mouth. <br />
                        Stay home when you are sick.
                        </p>
                        <p>Thank you for assisting us in preventing the spread of infection. </p>
                        
                        <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                            <Grid item  md={6} className={classes.signatureContainer}>
                                <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                    
                                    <Field
                                        name={"signature1"}
                                        type={"text"}
                                        autoFocus={true}
                                        component={ImplementationFor["signature"]}
                                        id={"signature1"}
                                        onChange={(e) => onChange("signature1", e)}
                                    /></Grid>
                                <Grid> Patient Signature</Grid>
                            </Grid>
                            <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                                <Grid item md={12} className={classes.DateContainer1}>
                                    <Field
                                        name={"date1"}
                                        type={"text"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                    />
                                    
                                </Grid>
                                <Grid>Date</Grid>
                            </Grid>
                        </Grid>

                            <Grid style={{textAlign:"center", padding: '20px 0px'}}>
                            <Button className={classes.button} type="submit" form="consent13">Submit</Button>
                            </Grid>
                        </Grid> 
                    </Grid>
              
            </form>
            </Grid >
            </Grid >
        </Grid>
      
 }
 
 Consent13.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent13',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true
 })(Consent13);