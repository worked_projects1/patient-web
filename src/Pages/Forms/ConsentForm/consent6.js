
/**
 * 
 *  Consent6
 * 
 */
 import  * as React from 'react';
 import { useState, useEffect,useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm } from 'redux-form';
 import {  Grid, Button,} from '@mui/material';
 import Styles from './styles';
 import {ImplementationFor} from '../Components/CreateRecordForm/utils'

 /**
  * 
  * @param {object} props 
  * @returns 
  */

 function Consent6(props) {
     const {  handleSubmit,destroy,} = props;
     const classes = Styles();
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState("");

     useEffect(() => {
        return () => destroy();
    }, []);

    const onChange = (val,eventValue) => {
    val=="signature1"&&setSignature1(eventValue)
    val=="signature2"&&setSignature2(eventValue)
    val=="signature3"&&setSignature3(eventValue)

    }

     return <Grid className={classes.firstContainer}  >
            <Grid className={classes.secContainer} >
                <Grid item className={classes.EditForm} justify="center">
                <span className={classes.formTitle}>Financial Policy</span>
                </Grid>
                <Grid item className={classes.thirdContainer}>
                <form onSubmit={handleSubmit} id='consent6'>

                    <Grid item spacing={12} className={classes.content} justify="space-between">
                    <Grid>
                        <p>We are committed to providing you with the best possible dental care. If you have dental
                        insurance, we will be happy to help you receive your maximum allowable benefits. In
                        order to achieve these goals, we need your assistance and understanding of our financial policy. We will gladly discuss any insurance questions you may have. However, you must realize that your insurance plan is a contract between you and/or your employer and the insurance company. While the filing of claims is a courtesy that we extend to our patients, all charges are your responsibility on the date the services are rendered regardless of insurance benefits
                        </p>
                        <p style={{textDecoration:'underline'}}>Payments for services are due in full at the time of services rendered.</p>
                        <p>
                        A 50% deposit is required for those patients who are starting any major dental treatment regardless of what your insurance will pay. We gladly accept Visa, Master Card, personal in-state checks, or cash. </p>
                        <p>
                        If there is still a balance owing after your insurance company pays, that balance will be
                        due and payable within 30 days. If there is a credit on the account, we will issue a
                        refund check. Return checks and balances older than 60 days are subject to additional
                        collection fees and interest charges of 0.83% per month, or 10% per year </p>
                        <p>Charges may also be made for broken appointments and appointments cancelled without a 48 hour notice. A charge of $125.00 will be charged for any hygiene broken appointment and $200.00-$500.00 for a broken appointment with the Doctor.If an account is turned into collections, you will be responsible for all legal fees.</p>
                        <p>
                        I have read the above and fully understand and accept the terms and conditions set forth. I authorize the release of any information relating to my dental claim. I understand I am responsible for all costs of dental treatment regardless of insurance. </p>
                        <Grid className={classes.flexContainer}>
                            <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                                <Grid item  style={{ display: 'inline-block'}}><span>Patient's Name:</span></Grid>
                                    <Grid style={{ display: 'inline-block', margin: '5px 10px',  lineHeight: '1rem' }} className={classes.blockToInline}>
                                        <Field
                                                name={"patientName"}
                                                type={"text"}
                                                required={true}
                                                component={ImplementationFor["input"]}
                                            />
                                </Grid>
                            </Grid>
                        </Grid>
                            <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                            <Grid item  md={6} className={classes.signatureContainer}>
                                <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                    
                                    <Field
                                        name={"signature1"}
                                        type={"text"}
                                        autoFocus={true}
                                        component={ImplementationFor["signature"]}
                                        id={"signature1"}
                                        onChange={(e) => onChange("signature1", e)}
                                    /></Grid>
                                    <Grid>Patient Signature</Grid>
                                </Grid>
                                <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                                    <Grid item md={12} className={classes.DateContainer1}>
                                        <Field
                                            name={"date1"}
                                            type={"text"}
                                            component={ImplementationFor["date"]}
                                            required={true}
                                        />
                                        
                                    </Grid>
                                    <Grid>Date</Grid>
                                    </Grid>
                            </Grid>

                        <Grid style={{textAlign:"center", padding: '20px 0px'}}>
                        <Button className={classes.button} type="submit" form="consent6">Submit</Button>
                        </Grid>
                    </Grid> 
                    </Grid>
              
            </form>
            </Grid >
            </Grid >
        </Grid>
      
 }
 
 Consent6.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent6',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true
 })(Consent6);