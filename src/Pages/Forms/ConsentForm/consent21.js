/**
 * 
 *  Consent21
 * 
 */

 import React, { useState, useEffect, useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm, formValueSelector } from 'redux-form';
 import {  Grid, Button,  } from '@mui/material';
 import Styles from './styles';
 import { ImplementationFor } from '../Components/CreateRecordForm/utils'
 
 /**
   * 
   * @param {object} props 
   * @returns 
   */
 function Consent21(props) {
     const {  handleSubmit, destroy, } = props;
     const classes = Styles();
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState("");
 
     useEffect(() => {
         return () => destroy();
     }, []);
 
     const onChange = (val, eventValue) => {
         val == "signature1" && setSignature1(eventValue)
         val == "signature2" && setSignature2(eventValue)
         val == "signature3" && setSignature3(eventValue)
     }
 
     return <Grid className={classes.firstContainer}  >
         <Grid className={classes.secContainer} >
             <Grid item className={classes.EditForm} justify="center">
                 <span className={classes.formTitle}>PRP/PRF Intravenous Access Consent Form</span>
             </Grid>
             <Grid item className={classes.thirdContainer}>
                 <form onSubmit={handleSubmit.bind(this)} id={"consent21"}>
 
                     <Grid item spacing={12} className={classes.content} justify="space-between">
                         <Grid>
                             <h4>PURPOSE </h4>
                             <p>To obtain whole blood that will be centrifuged. This will facilitate the concentration of platelets, white blood cells, and the growth (healing) factors contained within these cells. These cells and the growth factors contained within them will be placed into the surgery sight to accelerate tissue repair and improve healing predictability. </p>

                             <h4>PROCEDURES</h4>
                             <p>For this purpose, 20 to 60 mL (approximately ½ to 2 ounces) of your blood will be needed. The procedure involves placing a needle in a vein in your arm to take blood. This will require no more than a few minutes. </p>

                             <h4>RISKS </h4>
                             <p>Occasionally there are complications, which typically resolve in 1 to 2 weeks. You may experience bruising, swelling, numbness, black and blue marks, fainting and/or infection at the site. Rarely do complications escalate into more serious problems. As with any procedure we cannot guarantee that you would not have an undesired event associated with the withdrawal of blood from your vein(s). This is similar to the withdrawal of blood for a blood test at the hospital. </p>

                             <h4>BENEFITS </h4>
                             <p>Healing of soft tissue and bone will be enhanced with the placement of concentrated platelets and white blood cells into the surgical area. Growth (healing) factors released from these cells will help attract other cells, such as stem cells, from your surrounding healthy tissues to the surgical area. In doing so, these attracted cell types will improve your chances of faster healing, and therefore fewer surgical complications. </p>
                             
                             <p>By signing below, you consent to participate in the procedure described above.</p>
                             
                            <Grid  className={classes.flexContainer}>
                        
                            <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                                <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Patient’s Name:(Printed):</span></Grid>
                                <Grid  item style={{  margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                        name={"patientName"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid>
                            </Grid> 
                            <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                                <Grid item style={{ display: 'inline-block', paddingBottom: '10px'}}>
                                <span>Date:</span></Grid> 
                                <Grid item  className={classes.LowDateContainer} style={{ textAlign: 'center', margin: '0px 10px',lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                        name={"date1"}
                                        type={"text"}
                                        component={ImplementationFor["date"]}
                                        id={"date1"}
                                        required={true}
                                    />
                                </Grid>
                            </Grid>
                            </Grid>

                            <Grid >Parent’s Signature:
                            <Grid item xs={12} sm={5} style={{display:'inline-block',margin: '0px 10px'}}>
                            <Field
                                name={"signature1"}
                                type={"text"}
                                component={ImplementationFor["signature"]}
                                id={"signature1"}
                                onChange={(e) => onChange("signature1",e)}
                                />
                            </Grid>
                            </Grid>
                            
                            <p><b>If the Patient is unable to sign or if the patient is a minor:</b></p>

                            <Grid  className={classes.flexContainer}>
                        
                                <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                                <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Name of Legal Representative(Printed):</span></Grid>
                                    <Grid  item style={{  margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                        <Field
                                                name={"legalRepresentate"}
                                                type={"text"}
                                                component={ImplementationFor["input"]}
                                                required={true}
                                            />
                                    </Grid>
                                </Grid> 
                                <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                                <Grid item style={{ display: 'inline-block', paddingBottom: '10px'}}>
                                <span>Date:</span></Grid> 
                                <Grid item  className={classes.LowDateContainer} style={{ textAlign: 'center', margin: '0px 10px',lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                                name={"date2"}
                                                type={"text"}
                                                component={ImplementationFor["date"]}
                                                required={true}
                                                id={"date2"}
                                                required={true}
                                            />
                                </Grid>
                                </Grid>
                            </Grid>


                            <Grid >Signature of Legal Representative:
                            <Grid item xs={12} sm={5} style={{display:'inline-block',margin: '0px 10px'}}>
                            <Field
                                name={"signature2"}
                                type={"text"}
                                component={ImplementationFor["signature"]}
                                id={"signature2"}
                                onChange={(e) => onChange("signature2",e)}
                                
                                />
                            </Grid>
                            </Grid>

                             
                             <Grid style={{ textAlign: 'center' }}>
                             Dentist Signature:
                                 <Grid item xs={12} sm={5} style={{ display: 'inline-block', margin: '0px 10px' }}>
                                     <Field
                                         name={"signature3"}
                                         type={"text"}
                                         // value={value}
                                         component={ImplementationFor["signature"]}
                                         id={"signature3"}
                                         onChange={(e) => onChange("signature3", e)}
                                         disabled={true}
                                     />
                                 </Grid>
                                 <Grid style={{ display: 'inline-block', paddingBottom: '10px'}}>
                                 <span>Date:</span>
                                 <Grid item xs={6} md={6} className={classes.DateContainer} style={{ textAlign: 'center', display: 'inline-block', margin: '0px 10px' }}>
                                     <Field
                                         name={"date3"}
                                         type={"text"}
                                         component={ImplementationFor["date"]}
                                         required={true}
                                         id={"date3"}
                                         disabled={true}
                                     />
                                 </Grid></Grid>
                             </Grid>
                             <Grid style={{ textAlign: 'center' }}>
                                 Witness Signature:
                                 <Grid item xs={12} sm={5} style={{ display: 'inline-block', margin: '0px 10px' }}>
                                     <Field
                                         name={"signature4"}
                                         type={"text"}
                                         component={ImplementationFor["signature"]}
                                         id={"signature4"}
                                         onChange={(e) => onChange("signature4", e)}
                                         disabled={true}
                                     />
                                 </Grid>
                                 <Grid style={{ display: 'inline-block', paddingBottom: '10px'}}>
                                 <span>Date:</span>
                                 <Grid item xs={12} md={6} className={classes.DateContainer} style={{ textAlign: 'center', display: 'inline-block', margin: '0px 10px' }}>
                                     <Field
                                         name={"date4"}
                                         type={"text"}
                                         component={ImplementationFor["date"]}
                                         required={true}
                                         id={"date4"}
                                         disabled={true}
                                     />
                                 </Grid></Grid>
                             </Grid>
                             <Grid style={{ textAlign: "center", padding: '20px 0px' }}>
                                 <Button form="consent21" type="submit" className={classes.button}>Submit</Button>
                             </Grid>
                         </Grid>
                     </Grid>
 
                 </form>
             </Grid >
         </Grid >
     </Grid>
 
 }

 Consent21.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent21',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true,
 })(Consent21);