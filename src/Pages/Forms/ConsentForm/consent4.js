/**
 * 
 *  Consent4
 * 
 */

 import React, { useState, useEffect, useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm } from 'redux-form';
 import { Grid, Button, } from '@mui/material';
 import Styles from './styles';
 import { ImplementationFor } from '../Components/CreateRecordForm/utils'
 
 /**
   * 
   * @param {object} props 
   * @returns 
   */
 
 function Consent4(props) {
     const { handleSubmit, destroy,change,dispatch,initialValues } = props;
     const classes = Styles();
     const [signature1, setSignature1] = useState("");
 
     dispatch(change('staff',initialValues.providerFirst+ ' ' + initialValues.providerLast))
 
     useEffect(() => {
         return () => destroy();
     }, []);
 
     const onChange = (val, eventValue) => {
         val == "signature1" && setSignature1(eventValue)
     }
 
     return <Grid className={classes.firstContainer}  >
         <Grid className={classes.secContainer} >
             <Grid item className={classes.EditForm} justify="center">
                 <span className={classes.formTitle}>Bone Grafting And Barrier Membrane Consent Form</span>
             </Grid>
             <Grid item className={classes.thirdContainer}>
                 <form onSubmit={handleSubmit.bind(this)} id={"consent4"}>
 
                     <Grid item spacing={12} className={classes.content} justify="space-between">
                         <Grid>
                             <p>I understand that bone grafting and barrier membrane procedures include inherent risks
                                 such as, but not limited to, the following:
                                 <ul>
                                     <li style={{ padding: '10px 0px' }}>Some discomfort is inherent in any oral surgery procedure. Grafting with materials that do not have to be harvested from your body is less painful because they do not require a donor site surgery. If the necessary bone is taken from your chin or wisdom tooth area in the back of your mouth, there will be more pain. It can be
                                    largely controlled with pain medications.
                                     </li>
                                     <li style={{ padding: '10px 0px' }}>No matter how carefully surgical sterility is maintained, it is possible, because of the existing nonsterile oral environment, for infections to occur postoperatively. At times these may be of a serious nature. Should severe swelling occur,   particularly accompanied with fever or malaise, professional attention should be received as soon as possible.
                                     </li>
                                     <li style={{ padding: '10px 0px' }}>Bleeding, bruising, and swelling. Some moderate bleeding may last several hours. If   bleeding is profuse, you must contact us as soon as possible. Some swelling is normal, but if it is severe, you should notify us. Swelling usually starts to subside after about 48 hours. Bruises may persist for a week or so.
                                     </li>
                                     <li style={{ padding: '10px 0px' }}>Success with bone and membrane grafting is high. Nevertheless, it is possible that the graft could fail. A block bone graft taken from somewhere else in your mouth may not adhere or could become infected. Despite meticulous surgery, particulate bone graft materials can migrate out of the surgery site and be lost.  A membrane graft could start to dislodge. If so, the doctor should be notified. Your compliance is essential to assure success.
                                     </li>
                                     <li style={{ padding: '10px 0px' }}>Some bone graft and membrane materials commonly used are derived from human or other mammal sources. These grafts are thoroughly purified by different means to be free from contaminants. Signing this consent form gives your approval for the doctor to use such materials according to his/her knowledge and clinical judgment for your situation.
                                     </li>
                                     <li style={{ padding: '10px 0px' }}>This would include injuries causing numbness of the lips; the tongue; and/or any tissues of the mouth, cheeks, or face. This potential numbness may be of a temporary nature, lasting a few days, weeks, or months, or it could possibly be permanent. Numbness could be the result of surgical procedures or anesthetic administration.
                                     </li>
                                     <li style={{ padding: '10px 0px' }}>In some cases, the root tips of upper (maxillary) teeth lie in close proximity to the maxillary sinus. Occasionally, with extractions and/or grafting near the sinus, the sinus can become involved. If this happens, you will need to take special medications. Should sinus penetration occur, it might be necessary to later have the sinus surgically closed.
                                     </li>
                                     <li style={{ padding: '10px 0px' }}>It is your responsibility to seek attention should any undue circumstances occur postoperatively, and you should diligently follow any preoperative and postoperative instructions.
                                     </li>
                                 </ul>
                             </p>
                             <p>
                                 <b>Informed Consent: </b> As a patient, I have been given the opportunity to ask any questions
                                 regarding the nature and purpose of surgical treatment and have received answers to my satisfaction. I do voluntarily assume any and all possible risks, including the risk of harm, if any, which may be associated with any phase of this treatment in hopes of obtaining the
                                 desired results, which may or may not be achieved. No guarantees or promises have been made to me concerning my recovery and the results of the treatment to be rendered to me. The fee(s) for this service have been explained to me and are satisfactory. By signing
                                 this form, I am freely giving my consent to allow and authorize 
                                 <Grid item xs={12} style={{ display: 'inline-block', margin: '0px 10px 5px 10px', lineHeight: '1rem' }}>
                                     <Field
                                         name={"staff"}
                                         type={"text"}
                                         component={ImplementationFor["input"]}
                                         required={true}
                                         disabled={true}
                                     />
                                 </Grid>
                                 and his associates to render any treatments necessary or advisable to my dental conditions, including any and all anesthetics and/or medications.  </p>
                             
                             <Grid className={classes.flexContainer}>
                             <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                                 <Grid item  style={{ display: 'inline-block'}}><span>Patient's Name:</span></Grid>
                                     <Grid style={{ display: 'inline-block',  margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                         <Field
                                                 name={"patientName"}
                                                 type={"text"}
                                                 required={true}
                                                 component={ImplementationFor["input"]}
                                             />
                                 </Grid>
                             </Grid>
                             </Grid>
 
                             <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                             <Grid item  md={6} className={classes.signatureContainer}>
                                 <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                     
                                     <Field
                                         name={"signature1"}
                                         type={"text"}
                                         autoFocus={true}
                                         component={ImplementationFor["signature"]}
                                         id={"signature1"}
                                         onChange={(e) => onChange("signature1", e)}
                                     /></Grid>
                                     <Grid>Patient Signature</Grid>
                                 </Grid>
                                 <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>
 
                                     <Grid item md={12} className={classes.DateContainer1}>
                                         <Field
                                             name={"date1"}
                                             type={"text"}
                                             component={ImplementationFor["date"]}
                                             required={true}
                                         />
                                         
                                     </Grid>
                                     <Grid>Date</Grid>
                                     </Grid>
                             </Grid>
 
                             <Grid style={{ textAlign: "center", padding: '20px 0px' }}>
                                 <Button className={classes.button} form={"consent4"} type="submit">Submit</Button>
                             </Grid>
                         </Grid>
                     </Grid>
 
                 </form>
             </Grid >
         </Grid >
     </Grid>
 
 }
 
 Consent4.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent4',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true
 })(Consent4);