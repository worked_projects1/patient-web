/**
 * 
 *  Consent24
 * 
 */

 import React, { useState, useEffect,useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm, formValueSelector } from 'redux-form';
 import { Grid, Button, collapseClasses,} from '@mui/material';
 import Styles from './styles';
import {ImplementationFor} from '../Components/CreateRecordForm/utils'

/**
  * 
  * @param {object} props 
  * @returns 
  */
 function Consent24(props) {
    const { handleSubmit, destroy,change,dispatch,initialValues } = props;
     const classes = Styles();
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState("");

     useEffect(() => {
         return () => destroy();
     }, []);

     dispatch(change('staff',initialValues.providerFirst+ ' ' + initialValues.providerLast))

    const onChange = (val,eventValue) => {
    val=="signature1"&&setSignature1(eventValue)
    val=="signature2"&&setSignature2(eventValue)
    val=="signature3"&&setSignature3(eventValue)

    }

        return <Grid className={classes.firstContainer}  >
                    <Grid className={classes.secContainer} >
                        <Grid item className={classes.EditForm} justify="center">
                        <span className={classes.formTitle}>Endodontic Surgical Therapy Consent Form</span>
                        </Grid>
                    <Grid item className={classes.thirdContainer}>
                    <form onSubmit={handleSubmit.bind(this)} id={"consent24"}>

                    <Grid item spacing={12} className={classes.content} justify="space-between">
                    <Grid>
                   
                     <Grid  className={classes.flexContainer}>
                        <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                            <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Name of the Patient:</span></Grid>
                            <Grid  item style={{ margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                    name={"patientName"}
                                    type={"text"}
                                    required={true}
                                    component={ImplementationFor["input"]}
                                />
                            </Grid>
                        </Grid>
                        </Grid>

                        <Grid  className={classes.flexContainer}>
                            <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                            <Grid item style={{ display: 'inline-block', paddingBottom: '10px'}}>
                                    <span>Birth date:</span></Grid> 
                                    <Grid item  className={classes.LowDateContainer} style={{ textAlign: 'center', margin: '0px 10px',lineHeight: '1rem' }} className={classes.blockToInline}>
                                        <Field
                                            name={"dob"}
                                            type={"date"}
                                            component={ImplementationFor["date"]}
                                            required={true}
                                        />
                                    </Grid></Grid>
                                    </Grid>
                        <Grid>I,
                        <Grid item xs={12} md={6} style={{display:'inline-block',margin: '5px 10px',lineHeight: '1rem'}}>
                        <Field
                            name={"ill"}
                            type={"text"}
                            component={ImplementationFor["input"]}
                            required={true}
                            />
                        </Grid>
                        <span> authorize  </span>
                        <Grid  item xs={12} md={6} style={{display:'inline-block',margin: '5px 10px',lineHeight: '1rem'}}>
                        <Field
                            name={"staff"}
                            type={"text"}
                            component={ImplementationFor["input"]}
                            required={true}
                            disabled={true}
                            />
                        </Grid>
                        
                        <span>to perform an apical surgical procedure on tooth # </span>
                        <Grid  item xs={12} md={6} style={{display:'inline-block',margin: '5px 10px',lineHeight: '1rem'}}>
                        <Field
                            name={"tooth"}
                            type={"text"}
                            component={ImplementationFor["input"]}
                            required={true}
                            />
                        </Grid>
                        </Grid>
                        <p>I understand that a surgical procedure has been recommended for me. If this
                        treatment is not rendered, my present condition may possible deteriorate, resulting in loss of my tooth and/or damage to surrounding tissues. I have discussed the proposed
                        procedure with the doctor named above before I was asked to consider this form which
                        indicates my consent to the procedure
                        </p>
                        <p>
                        I understand the purpose of this procedure is to surgically treat the inflammatory
                        or diseased conditions around the root of this tooth. Although surgical Endodontics has a high degree of success, individual patients have varying physiological responses,
                        therefore, results cannot be guaranteed. As with all surgery, risks accompany this
                        procedure. The surgical risks include (patients are to initial each line as they acknowledge and accept these potential risks):
                        </p>
                        <Grid className={classes.initialContainer} container>
                        <Grid className={classes.initialItem}>
                            <Grid  item xs={4} md={3} className={classes.initialInput}>
                            <Field
                                name={"sideEffect1"}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                required={true}
                                />
                            </Grid>
                            <Grid item xs={8} md={8}>
                            Infection
                            </Grid> 
                        </Grid>
                        <Grid className={classes.initialItem}>
                            <Grid  item xs={4} md={3} className={classes.initialInput}>
                            <Field
                                name={"sideEffect2"}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                required={true}
                                />
                            </Grid>
                            <Grid item xs={8} md={8} >
                            Swelling
                            </Grid> 
                        </Grid>
                        <Grid className={classes.initialItem}>
                            <Grid  item xs={4} md={3} className={classes.initialInput}>
                            <Field
                                name={"sideEffect3"}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                required={true}
                                />
                            </Grid>
                            <Grid item xs={8} md={8} >
                            Discomfort
                            </Grid> 
                        </Grid>
                        <Grid className={classes.initialItem}>
                            <Grid  item xs={4} md={3} className={classes.initialInput}>
                            <Field
                                name={"sideEffect4"}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                required={true}
                                />
                            </Grid>
                        <Grid item xs={8} md={8} >
                        Gum Recession (shrinkage in the area of the involved tooth)
                        </Grid> 
                        </Grid>
                        <Grid className={classes.initialItem}>
                            <Grid  item xs={4} md={3} className={classes.initialInput}>
                            <Field
                                name={"sideEffect5"}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                required={true}
                                />
                            </Grid>
                        <Grid item xs={8} md={8} >
                        Bleeding beneath the skin resulting in temporary black and blue discoloration of the face and/or gums.
                        </Grid> 
                        </Grid>
                        <Grid className={classes.initialItem}>
                            <Grid  item xs={4} md={3} className={classes.initialInput}>
                            <Field
                                name={"sideEffect6"}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                required={true}
                                />
                            </Grid>
                        <Grid item xs={8} md={8} >
                        Paresthesia (numbness or tingling in the area of the lip and/or gum which typically is temporary, but can be permanent).
                        </Grid> 
                        </Grid>
                        <Grid className={classes.initialItem}>
                            <Grid  item xs={4} md={3} className={classes.initialInput}>
                            <Field
                                name={"sideEffect7"}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                required={true}
                                />
                            </Grid>
                        <Grid item xs={8} md={8} >
                        It is possible the tooth may require extraction if the surgical procedure is not successful due to many causes, some of which are infection or root fractures. 
                        </Grid> 
                        </Grid>
                    </Grid>

                    <p>
                    If I have any questions with respect to these potential complications or others, I
                    understand I must not sign this consent form until I have had all questions answered to my satisfaction.
                    </p>
                    <p>I understand in the course of this procedure and during follow-up, developments
                    may occur which call for the doctors judgment as to further diagnostic or therapeutic
                    procedures. If such developments occur, I hereby authorize the additional procedures as
                    indicated by the judgment of the dentist. 
                    </p>
                    <p>
                    The nature of the above described procedure has been explained to me to my  satisfaction, and I have been provided the opportunity to ask any and all questions about my condition, treatment alternatives, and prognosis with or without surgery. I agree I have been given no guarantee or assurance of any specific result. I further understand that by signing this form, I provide my consent to the above procedure and I acknowledge I have been informed of the relevant benefits and risks prior to providing my consent.
                    </p>

                    <Grid style={{paddingBottom:'10px'}}>Date:
                        <Grid  item xs={12} sm={5} className={classes.LowDateContainer} style={{textAlign: 'center',display:'inline-block',marginLeft: '10px'}}>
                        <Field
                        name={"date1"}
                        type={"text"}
                        component={ImplementationFor["date"]}
                        id={"date1"}
                        required={true}
                        />
                        </Grid> 
                    </Grid> 
                    <Grid style={{paddingBottom:'10px'}} className={classes.radioGroup}>
                        <Grid item xs={12} md={6} style={{display:'inline-block',alignSelf: 'flex-end'}}>Patient, Parent of a Minor Parent’s Signature:</Grid>
                        <Grid item xs={12} md={6} style={{display:'inline-block',margin: '0px 10px'}}>
                        <Field
                            name={"signature1"}
                            type={"text"}
                            component={ImplementationFor["signature"]}
                            onChange={(e) => onChange("signature1",e)}
                            />
                        </Grid>
                    </Grid>
                    <Grid style={{paddingBottom:'10px'}} className={classes.radioGroup}>
                        <Grid item xs={12} md={6} style={{display:'inline-block',alignSelf: 'flex-end'}}> WITNESS:</Grid>
                        <Grid item xs={12} md={6} style={{display:'inline-block',margin: '0px 10px'}}>
                        <Field
                            name={"signature2"}
                            type={"text"}
                            component={ImplementationFor["signature"]}
                            onChange={(e) => onChange("signature1",e)}
                            disabled={true}
                            />
                        </Grid>
                    </Grid>
                    <Grid style={{paddingBottom:'10px'}} className={classes.radioGroup}>
                        <Grid item xs={12} md={6} style={{display:'inline-block',alignSelf: 'flex-end'}}> Witness Signature:</Grid>
                        <Grid item xs={12} md={6} style={{display:'inline-block',margin: '0px 10px'}}>
                        <Field
                            name={"signature3"}
                            type={"text"}
                            component={ImplementationFor["signature"]}
                            onChange={(e) => onChange("signature1",e)}
                            disabled={true}
                            />
                        </Grid>
                    </Grid>
                    <Grid style={{textAlign:"center", padding: '20px 0px'}}>
                <Button  form="consent24" type="submit" className={classes.button}>Submit</Button>
            </Grid>
            </Grid>
        </Grid>
 
        </form>
    </Grid >
    </Grid >
    </Grid>
     
 }

Consent24.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent24',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true,
 })(Consent24);