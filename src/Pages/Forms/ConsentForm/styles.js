

import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({

    
    EditForm: {
        textAlign: 'center',
        background: '#818284!important',
        display: 'flex',
        alignItems: 'center',
        fontSize: '20px',
        justifyContent: 'center',
        padding: '20px',
        color: '#fff',
        fontWeight: '700'
    },
   
    formTitle: {
        fontSize: '25px',
        fontWeight: '700',
    },
    content: {
        marginTop: '25px',
        color: "#313030",
        fontSize: "24px",
        padding: "0px 30px",
        lineHeight: "1.5",
        ['@media (max-width: 480px)']: {
            fontSize: "18px !important",
        },
        '&::-webkit-scrollbar': {
            width: '0.3em'
        },
        '&::-webkit-scrollbar-thumb': {
            backgroundColor: '#89949b',
        }
    },
    
    title: {
        fontSize: '35px',
        textAlign: 'center',
    },
    form1: {
        margin: "10px 20px !important",
    },
    button: {
        border: "1px solid #89949b",
        color: "white",
        padding: "2px 15px",
        borderRadius: "20px",
        background: '#89949b',
        '&:hover': {
            background: 'rgba(0,0,0,.5)',
            opacity: '0.5',
        },
        fontSize: '18px',
        fontWeight: 'bold',
        // borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        // marginTop: '10px',
        outline: 'none',
        textTransform: 'capitalize',
        // marginRight: '50px'
    },
    
    lengthyField: {
        fontSize: '18px',
        height: '20px',
        margin: '15px 0px',
    },
    textArea: {
        fontSize: '18px',
        // height: '20px',
        margin: '10px 0px',
    },
    secContainer: {
        background: 'white',
    },
    firstContainer: {
        width: '100%',
        background: '#e7e7e8',
        padding: '0px 30px',
        ['@media (max-width: 480px)']: {
            padding: '0px 5px !important',

        },
    },
    thirdContainer: {
        padding: '30px',

        ['@media (max-width: 480px)']: {
            padding: '0px !important',

        },
    },
    alert: {
        fontSize: '16px',
        justifyContent: 'center'
    },
    alphaSmall: {
        listStyleType: 'lower-alpha',
    },
    dialogContent: {
        fontSize: "20px",
        padding:'25px',
        ['@media (max-width: 480px)']: {
            fontSize: '16px !important',

        },
    },
    dialogAction: {
        justifyContent: 'center'
    },
    radioGroup: {
        display: 'flex',
        ['@media (max-width: 480px)']: {
            display: 'block',
        },
    },
    checkBox: {
    maxWidth: '100%',
    },
    initialItem: {
        display: 'flex',
        flexDirection: 'row',
        padding: '10px 0px',

    },
    initialContainer: {
        display: 'flex',
        flexDirection: 'column',
    },
    initialInput: {
        margin: '0px 10px',
        padding: '10px 0px',
        ['@media (max-width: 480px)']: {
            padding: '5px 0px',

        },
    },
    underlinedSpan: {
        textDecoration: 'underline',
    },
    rightInput: {
        textAlign: 'right',
        ['@media (max-width: 480px)']: {
            textAlign: 'center',

        },
    },
    container: {
        textAlign: 'center',
        display: 'inline-block',
        margin: '0px 10px',
        lineHeight: '2rem',
        
        ['@media (max-width: 480px)']: {
            lineHeight: '1.5rem',

        },
    },
    flexItem: {
        display: 'inline-block',
        flexDirection: 'row',
    },
    flexContainer: {
        display: 'block',
        flexDirection: 'column',
        ['@media (max-width: 480px)']: {
            display: 'flex',

        },
    },
    singleContainer: {
        textAlign: 'left',
        ['@media (max-width: 480px)']: {
            textAlign: 'center',

        },
    },
    blockToInline: {
        display: 'inline-block',
        ['@media (max-width: 480px)']: {
            display: 'block !important',

        },
        
    },
    
    centeringText: {
        '& div': {
            '& div': {
                '& textarea': {
                textAlign: 'center',
                justifyContent: 'center'

                }
            }
        }
    },
    textContainer: {
        '& div': {
            height: '16vh',
            justifyContent: 'flex-end',
            verticalAlign: 'baseline'
        }
    },
    DateContainer: {
        textAlign: 'center',
        display: 'inline-block',
    // ['@media (max-width: 480px)']: {
    //     display: 'block',

    // },
        justifyContent: 'flex-end',
        verticalAlign:'baseline', 
        flexDirection: 'column',
    },
    DateContainer1: {
        textAlign: 'center', 
        display: 'flex', 
        margin: '0px 10px',
        justifyContent: 'flex-end',
        verticalAlign:'baseline', 
        flexDirection: 'column',
        height: '15.5vh'
    },
    signatureContainer: {
        textAlign: 'center',
        display: 'block', 
        justifyContent: 'flex-end',
        verticalAlign:'baseline', 
        flexDirection: 'column',
        paddingBottom: '10px' 
    }

}));

export default useStyles;

