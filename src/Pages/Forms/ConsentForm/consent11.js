
/**
 * 
 *  Consent11
 * 
 */
 import  * as React from 'react';
 import { useState, useEffect,useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm } from 'redux-form';
 import { Grid,Button, } from '@mui/material';
 import Styles from './styles';
import {ImplementationFor} from '../Components/CreateRecordForm/utils'

 /**
  * 
  * @param {object} props 
  * @returns 
  */

 function Consent11(props) {
     const {handleSubmit, destroy, } = props;
     const classes = Styles();
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState("");
     
     useEffect(() => {
        return () => destroy();
    }, []);
    
    const onChange = (val,eventValue) => {
    val=="signature1"&&setSignature1(eventValue)
    val=="signature2"&&setSignature2(eventValue)
    val=="signature3"&&setSignature3(eventValue)

    }

     return <Grid className={classes.firstContainer}  >
            <Grid className={classes.secContainer} >
                <Grid item className={classes.EditForm} justify="center">
                <span className={classes.formTitle}>ICOI Implant Placement Consent Form</span>
                </Grid>
                <Grid item className={classes.thirdContainer}>
                <form onSubmit={handleSubmit} id='consent11'>

                    <Grid item spacing={12} className={classes.content} justify="space-between">
                    <Grid>
                        <ol>
                            <li style={{padding: '10px 0px'}}>
                            I have been informed about and understand the purpose and the nature of the implant
                            surgery procedure. I understand what is necessary to accomplish the placement of the implant under the gingiva (gum) or in the bone. 
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            My doctor has carefully examined my mouth. Alternatives to this treatment have been explained. I have tried or considered these methods, but I desire an implant to help secure the restorations for missing teeth.
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            I have further been informed of the possible risks and complications involved with surgery, drugs, and anesthesia. Such complications include pain, swelling, infection, and discoloration. Numbness of the lip, tongue, chin, cheek, or teeth may occur. The exact duration may not be determinable and may be irreversible. Also possible are inflammation of a vein, injury to existing teeth, bone fractures, sinus penetration, delayed healing,
                            allergic reactions to drugs or medications used, etc. 
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            I understand that if nothing is done, any of the following could occur: bone disease, loss of bone, gingival (gum) tissue inflammation, infection, sensitivity, and looseness of teeth,
                            requiring subsequent extraction. Also possible are temporomandibular joint (TMJ)
                            problems, headaches, referred pains to the back of the neck and facial muscles, and tired muscles when chewing. 
                            </li>
                           <li style={{padding: '10px 0px'}}>
                           My doctor has explained that there is no method to accurately predict the gingival(gum) and bone healing capabilities in any given patient following the placement of the implant. 
                            </li> 
                            <li style={{padding: '10px 0px'}}>
                            It has been explained that in some instances implants fail and must be removed. I have been informed and understand that the practice of dentistry is not an exact science; no guarantees or assurance as to the outcome of treatment or surgery can be made
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            I understand that excessive smoking, alcohol, or sugar may affect gingival (gum) healing and may limit the success of the implant. I agree to follow my doctor's home care instructions. I agree to report to my doctor for regular examinations as instructed. 
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            I agree to the type of anesthesia chosen by the doctor. I agree not to operate a motor vehicle or hazardous device for at least 24 hours or more until fully recovered from the effects of the anesthesia or drugs given for my care.   
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            To my knowledge I have given an accurate report of my physical and mental health
                            history. I have also reported any prior allergic or unusual reactions to drugs, food, insect bites, anesthetics, pollens, or dust; blood or body diseases; gingival (gum) or skin reactions; abnormal bleeding; or any other conditions related to my health. 
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            I consent to photography, filming, recording, and x-rays of the procedure to be performed for the advancement of implant dentistry, provided my identity is not revealed. 
                            </li>
                            <li style={{padding: '10px 0px'}}>
                            I request and authorize medical/dental services for me, including implants and other surgery. I fully understand that during and following the contemplated procedure, surgery,or treatment, conditions may become apparent that warrant, in the judgment of the doctor, additional or alternative treatment pertinent to the success of comprehensive treatment. I also approve any modification in design, materials, or care, if it is felt this is in my best interest. 
                            </li>
                        </ol>
                        <Grid  className={classes.flexContainer}>
                        <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                            <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Patient's Name:</span></Grid>
                            <Grid  item style={{ margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                    name={"patientName"}
                                    type={"text"}
                                    required={true}
                                    component={ImplementationFor["input"]}
                                />
                            </Grid>
                        </Grid>
                        </Grid>
                        
                        <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                            <Grid item  md={6} className={classes.signatureContainer}>
                                <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                    
                                    <Field
                                        name={"signature1"}
                                        type={"text"}
                                        autoFocus={true}
                                        component={ImplementationFor["signature"]}
                                        id={"signature1"}
                                        onChange={(e) => onChange("signature1", e)}
                                    /></Grid>
                                    <Grid> Patient Signature</Grid>
                                </Grid>
                                <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                                    <Grid item md={12} className={classes.DateContainer1}>
                                        <Field
                                            name={"date1"}
                                            type={"text"}
                                            component={ImplementationFor["date"]}
                                            required={true}
                                        />
                                        
                                    </Grid>
                                    <Grid>Date</Grid>
                                    </Grid>
                            </Grid>

                            <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                            <Grid item  md={6} className={classes.signatureContainer}>
                                <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                    
                                    <Field
                                        name={"signature2"}
                                        type={"text"}
                                        autoFocus={true}
                                        component={ImplementationFor["signature"]}
                                        id={"signature2"}
                                        onChange={(e) => onChange("signature2", e)}
                                        disabled={true}
                                    /></Grid>
                                    <Grid>Doctor Signature</Grid>
                                </Grid>
                                <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                                    <Grid item md={12} className={classes.DateContainer1}>
                                        <Field
                                            name={"date2"}
                                            type={"text"}
                                            component={ImplementationFor["date"]}
                                            disabled={true}
                                            required={true}
                                        />
                                        
                                    </Grid>
                                    <Grid>Date</Grid>
                                    </Grid>
                            </Grid>

                            <Grid style={{textAlign:"center", padding: '20px 0px'}}>
                            <Button className={classes.button} type="submit" form="consent11">Submit</Button>
                            </Grid>
                        </Grid> 
                    </Grid>
              
            </form>
            </Grid >
            </Grid >
        </Grid>
      
 }
 
 Consent11.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent11',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true
 })(Consent11);