
/**
 * 
 *  Consent8
 * 
 */

 import  * as React from 'react';
 import { useState, useEffect,useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm } from 'redux-form';
 import {  Grid, Button, } from '@mui/material';
 import Styles from './styles';
import {ImplementationFor} from '../Components/CreateRecordForm/utils'

/**
  * 
  * @param {object} props 
  * @returns 
  */

 function Consent8(props) {
     const {  handleSubmit, destroy,change,dispatch, initialValues  } = props;
     const classes = Styles();
     const [showModal, setModalOpen] = useState(false);
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState(""); 

     dispatch(change('physician',initialValues.providerFirst+ ' ' + initialValues.providerLast))

     useEffect(() => {
        return () => destroy();
    }, []);

    const onChange = (val,eventValue) => {
    val=="signature1"&&setSignature1(eventValue)
    val=="signature2"&&setSignature2(eventValue)
    val=="signature3"&&setSignature3(eventValue)

    }
    const  sex =  [
        {
          "label": "Male",
          "value": "Male"
        },
        {
          "label": "Female",
          "value": "Female"
        }]

    const  insurance =  [
        {
            "label": "Yes",
            "value": "Yes"
        },
        {
            "label": "No",
            "value": "No"
        }]
        
    const allergies = [
        {
            "label": "Penicillin",
            "value": "Penicillin"
        },
        {
            "label": "Latex",
            "value": "Latex"
        },
        {
            "label": "Codeine",
            "value": "Codeine"
        },
        {
            "label": "Anesthetics",
            "value": "Anesthetics"
        },
        {
            "label": "Sulfa Drugs",
            "value": "Sulfa Drugs"
        },
        {
            "label": "Aspirin",
            "value": "Aspirin"
        },
        {
            "label": "Other",
            "value": "Other",
            "input": true
        },
        
    ]
    const condition = [
        {
            "label": "High Blood Pressure ",
            "value": "High Blood Pressure "
        },
        {
            "label": "Low Blood Pressure",
            "value": "Low Blood Pressure"
        },
        {
            "label": "Diabetes",
            "value": "Diabetes"
        },
        {
            "label": "Smoking ",
            "value": "Smoking "
        },
        {
            "label": "Sinus Problems",
            "value": "Sinus Problems"
        },
        {
            "label": "Bisphosphonate Medications",
            "value": "Bisphosphonate Medications"
        },
        {
            "label": "Cancer",
            "value": "Cancer"
        },
        {
            "label": "Radiation Therapy",
            "value": "Radiation Therapy"
        },
        {
            "label": "Chemo Therapy",
            "value": "Chemo Therapy"
        },
        {
            "label": "Bleeding Disorder",
            "value": "Bleeding Disorder"
        },
        {
            "label": "Anemia",
            "value": "Anemia"
        },
        {
            "label": "Angina",
            "value": "Angina"
        },
        {
            "label": "Arthritis",
            "value": "Arthritis"
        },
        {
            "label": "Artificial joints",
            "value": "Artificial joints"
        },
        {
            "label": "Low Blood Pressure",
            "value": "Low Blood Pressure"
        },
        {
            "label": "Alcohol/Drug Dependency ",
            "value": "Alcohol/Drug Dependency "
        },
        {
            "label": "Asthma",
            "value": "Asthma"
        },
        {
            "label": "Epilepsy",
            "value": "Epilepsy"
        },
        {
            "label": "Artificial Heart Valve ",
            "value": "Artificial Heart Valve "
        },
        {
            "label": "Congenital Heart Problems ",
            "value": "Congenital Heart Problems "
        },
        {
            "label": "Congenital Heart Problems ",
            "value": "Congenital Heart Problems "
        },
        {
            "label": "Heart Attack ",
            "value": "Heart Attack "
        },
        {
            "label": "Heart Disease",
            "value": "Heart Disease"
        },
        {
            "label": "Heart Murmur",
            "value": "Heart Murmur"
        },
        {
            "label": "Heart Pacemaker",
            "value": "Heart Pacemaker"
        },
        {
            "label": "Heart Surgery",
            "value": "Heart Surgery"
        },
        {
            "label": "Mitral Valve Prolapse",
            "value": "Mitral Valve Prolapse"
        },
        {
            "label": "Stroke",
            "value": "Stroke"
        },
        {
            "label": "Glaucoma",
            "value": "Glaucoma"
        },
        {
            "label": "HIV Infection (AIDS)",
            "value": "HIV Infection (AIDS)"
        },
        {
            "label": "Hay Fever",
            "value": "Hay Fever"
        },
        {
            "label": "Frequent Headaches ",
            "value": "Frequent Headaches "
        },
        {
            "label": "Ulcers",
            "value": "Ulcers"
        },
        {
            "label": "Tuberculosis",
            "value": "Tuberculosis"
        },
        {
            "label": "Thyroid Problems ",
            "value": "Thyroid Problems "
        },
        {
            "label": "Hepatitis A ",
            "value": "Hepatitis A "
        },
        {
            "label": "Hepatitis B",
            "value": "Hepatitis B"
        },
        {
            "label": "Hepatitis C ",
            "value": "Hepatitis C "
        },
        {
            "label": "Kidney Disease ",
            "value": "Kidney Disease "
        },
        {
            "label": "Leukemia ",
            "value": "Leukemia "
        },
        {
            "label": "Liver Disease",
            "value": "Liver Disease"
        },
        {
            "label": "Seizures/Fainting",
            "value": "Seizures/Fainting"
        },
        {
            "label": "Respiratory Problems",
            "value": "Respiratory Problems"
        },
        {
            "label": "Rheumatic Fever",
            "value": "Rheumatic Fever"
        },{
            "label": "Other",
            "value": "Other",
            "input": true
        },
    ]

    const  women =  [
        {
            "label": "Yes",
            "value": "Yes"
        },
        {
            "label": "No",
            "value": "No"
        }]

    const  diabetic =  [
        {
            "label": "Type I",
            "value": "Type I"
        },
        {
            "label": "Type II Insulin Dependent",
            "value": "Type II Insulin Dependent"
        },
        {
            "label": "Type II Non-Insulin Dependent ",
            "value": "Type II Non-Insulin Dependent "
        },
    ]

    const insulin = [
        {
            "label": "Intermediate",
            "value": "Intermediate"
        },
        {
            "label": "Long acting",
            "value": "Long acting "
        },
        {
            "label": "Short Acting",
            "value": "Short Acting"
        },
    ]

    const bisphosphonates = [
        {
            "label": "Osteopenia/Osteoporosis",
            "value": "Osteopenia/Osteoporosis"
        },
        {
            "label": "Cancer",
            "value": "Cancer"
        }
    ]
        
    const administration = [
        {
            "label": "Oral",
            "value": "Oral"
        },
        {
            "label": "IV",
            "value": "IV"
        }
    ]

    const osteoporosis = [
        {
            "label": "Bisphosphonates",
            "value": "Bisphosphonates"
        },
        {
            "label": "Monoclonal Antibody",
            "value": "Monoclonal Antibody"
        }
    ]

    const  jawJoint =  [
        {
            "label": "Clicking Pain",
            "value": "Clicking Pain"
        },
        {
            "label": "Difficulty in Opening ",
            "value": "Difficulty in Opening "
        },
        {
            "label": "Difficulty in Closing",
            "value": "Difficulty in Closing"
        },
        {
            "label": "Difficulty in Chewing",
            "value": "Difficulty in Chewing"
        }]
        const normalizePhone = (value, previousValue) => {
            if (!value) {
              return value
            }
            const onlyNums = value.replace(/[^\d]/g, '')
            if (!previousValue || value.length > previousValue.length) {
              if (onlyNums.length === 3) {
                return onlyNums + '-'
              }
              if (onlyNums.length === 6) {
                return onlyNums.slice(0, 3) + '-' + onlyNums.slice(3) + '-'
              }
            }
            if (onlyNums.length <= 3) {
              return onlyNums
            }
            if (onlyNums.length <= 6) {
              return onlyNums.slice(0, 3) + '-' + onlyNums.slice(3)
            }
            return onlyNums.slice(0, 3) + '-' + onlyNums.slice(3, 6) + '-' + onlyNums.slice(6, 10)
          }
        
return <Grid className={classes.firstContainer}  >
        <Grid className={classes.secContainer} >
            <Grid item className={classes.EditForm} justify="center">
                <span className={classes.formTitle}>Patient Health History / Financial Policy / HIPAA </span>
            </Grid>
            <Grid item className={classes.thirdContainer}>
            <form onSubmit={handleSubmit} id='consent8' validate>

                <Grid item spacing={12} xs={12} className={classes.content} justify="space-between">
                <Grid>
                <h4 style={{color: '#ea6225'}}>PERSONAL INFORMATION</h4>
                    <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>First Name:</span></Grid>
                                <Grid item style={{margin: '5px 10px',  lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"firstName"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid>
                                <Grid  className={classes.flexItem} style={{paddingBottom: '10px'}}> 
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}}><span>MI:</span></Grid>
                                <Grid item  style={{margin: '5px 10px',  lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"mi"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid> 
                                <Grid  className={classes.flexItem} style={{paddingBottom: '10px'}}> 
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}}><span>Last Name:</span></Grid>
                                <Grid item  style={{margin: '5px 10px',  lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"lastName"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid>
                        </Grid>

                    <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>Cell Phone :</span></Grid>
                        <Grid item  style={{margin: '5px 10px',  lineHeight: '1rem'}} className={classes.blockToInline}>
                        <Field
                                    name={"cellPhone1"}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    required={true}
                                    normalize={normalizePhone}
                                />
                                </Grid></Grid>
                                <Grid  className={classes.flexItem} style={{paddingBottom: '10px'}}> 
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}} ><span>Initial Visit Date (MM/DD/YYYY) :</span></Grid>
                                <Grid item className={classes.LowDateContainer} style={{ textAlign: 'center',  margin: '0px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"date1"}
                                        type={"date"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                        id={"date1"}
                                    />
                                </Grid></Grid> 
                        </Grid>
                       
                     <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>Birth Date (MM/DD/YYYY) :</span></Grid>
                        <Grid item  className={classes.LowDateContainer} style={{ textAlign: 'center', margin: '0px 10px',lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                        name={"dob"}
                                        type={"text"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                    />
                                </Grid></Grid>
                                <Grid  className={classes.flexItem} style={{paddingBottom: '10px'}}> 
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}}><span>Occupation :</span></Grid>
                                <Grid item  style={{margin: '5px 10px',  lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"occupation"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid> 
                        </Grid>
                        <Grid style={{  paddingBottom: '10px'}} >
                            <span>Address (Street, City, State, Zip, Country):</span>
                            <Grid item xs={12} md={12} className={classes.textArea}>
                                <Field
                                    name={"address"}
                                    type={"text"}
                                    component={ImplementationFor["textArea"]}
                                    variant="standard"
                                    required={true}
                                    />
                        </Grid>
                    </Grid>
                    <Grid xs={12} className={classes.flexContainer}>
                      <Grid  style={{paddingBottom: '10px'}}className={classes.radioGroup} >
                      <Grid  item item xs={12}  md={2} style={{display: 'inline-block', paddingBottom: '10px'}}><span>Email</span></Grid>
                            <Grid item xs={12} md={12} style={{  margin: '0px 15px 0px 10px', lineHeight: '1rem', padding: '10px 0px' }} className={classes.blockToInline}>
                                <Field
                                        name={"email"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                        </Grid></Grid>
                    </Grid>
                    <Grid style={{ display: 'flex',padding: '10px 0px'}}>
                        <Grid item  style={{ display: 'flex',lineHeight: '2rem'}}>
                        Sex :
                        </Grid>
                        <Grid item xs={6} md={9} style={{ display: 'inline-block', margin: '0px 10px',  lineHeight: '1rem' }}>
                            <Field
                                name={"sex"}
                                options= {sex}
                                type={"radio"}
                                component={ImplementationFor["radio"]}
                                required={true}
                            />
                        </Grid> 
                    </Grid>
                    <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>How did you hear of us?</span></Grid>
                                <Grid item style={{margin: '5px 10px',  lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"referredBy"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid>
                                </Grid>
                        <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>Chief Complaint :</span></Grid>
                                <Grid item style={{margin: '5px 10px',  lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"complaint"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid>
                                <Grid  className={classes.flexItem} style={{paddingBottom: '10px'}}> 
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}}><span>Treatment Goal :</span></Grid>
                                <Grid item  style={{margin: '5px 10px',  lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"goal"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid> 
                        </Grid>
                        <br />
                        <h4 style={{color: '#ea6225'}}>EMERGENCY CONTACT </h4>
                        <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>Full Name :</span></Grid>
                                <Grid item style={{margin: '5px 10px',  lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"fullName"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid>
                                <Grid  className={classes.flexItem} style={{paddingBottom: '10px'}}> 
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}}><span>Relationship :</span></Grid>
                                <Grid item  style={{margin: '5px 10px',  lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"relationShip1"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid> 
                        </Grid>
                        <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>Cell Phone :</span></Grid>
                                <Grid item  style={{ margin: '5px 10px',  lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                            name={"cellPhone2"}
                                            type={"text"}
                                            component={ImplementationFor["input"]}
                                            required={true}
                                            normalize={normalizePhone}
                                        />
                                </Grid></Grid>
                                <Grid  className={classes.flexItem} style={{paddingBottom: '10px'}}> 
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}}><span>Work Phone :</span></Grid>
                                <Grid item  style={{margin: '5px 10px',  lineHeight: '1rem'  }} className={classes.blockToInline}>
                                    <Field
                                            name={"cellPhone3"}
                                            type={"text"}
                                            component={ImplementationFor["input"]}
                                            required={true}
                                            normalize={normalizePhone}
                                        />
                                </Grid></Grid> 
                        </Grid>
                        
                        <br />
                        <h4 style={{color: '#ea6225'}}>DENTAL INSURANCE</h4>
                        <Grid xs={12} className={classes.flexContainer}>
                        <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                               
                                <Grid item  style={{ display: 'inline-block'}}><span>Carrier Name :</span> </Grid>
                                <Grid style={{ display: 'inline-block', margin:  '0px 15px 5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                            name={"carrier"}
                                            type={"text"}
                                            component={ImplementationFor["input"]}
                                            required={true}
                                        />
                                </Grid></Grid>
                                <Grid style={{ paddingBottom: '10px'}} className={classes.flexItem}>
                                <Grid item style={{ display: 'inline-block'}}><span> Plan Name :</span></Grid>
                                <Grid  style={{ display: 'inline-block', margin: '5px 10px',  lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"planName"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid> 
                        </Grid>
                        <Grid className={classes.flexContainer}>
                        <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                                <Grid item style={{ display: 'inline-block'}}><span>Carrier Phone :</span></Grid>
                                <Grid  style={{ display: 'inline-block', margin: '5px 10px',  lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                            name={"cellPhone4"}
                                            type={"number"}
                                            component={ImplementationFor["input"]}
                                            required={true}
                                            normalize={normalizePhone}
                                        />
                                </Grid></Grid>
                                <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                                <Grid item style={{ display: 'inline-block'}}><span> Group :</span></Grid>
                                <Grid  style={{ display: 'inline-block', margin: '5px 10px',  lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"group"}
                                        type={"text"}
                                        required={true}
                                        component={ImplementationFor["input"]}
                                    />
                                </Grid></Grid> 
                        </Grid>
                        <Grid className={classes.flexContainer}>
                        <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                            <Grid item  style={{ display: 'inline-block'}}><span>Subscriber Name :</span></Grid>
                                <Grid style={{ display: 'inline-block', margin: '5px 10px',  lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                            name={"subscriber"}
                                            type={"text"}
                                            required={true}
                                            component={ImplementationFor["input"]}
                                        />
                                </Grid></Grid>
                                <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                                <Grid item style={{ display: 'inline-block'}}><span> Subscriber ID :</span></Grid>
                                <Grid style={{ display: 'inline-block', margin: '5px 10px',  lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"subscriberId"}
                                        type={"text"}
                                        required={true}
                                        component={ImplementationFor["input"]}
                                    />
                                </Grid></Grid> 
                        </Grid>
                        <Grid  className={classes.flexContainer}>
                        <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                        <Grid item style={{ display: 'inline-block', paddingBottom: '10px'}}>
                                <span>Subscriber Birthday :</span></Grid> 
                                <Grid item  className={classes.LowDateContainer} style={{ textAlign: 'center', margin: '0px 10px',lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"date3"}
                                        type={"date"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                    />
                                </Grid></Grid>
                                <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                                <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Relationship :</span></Grid>
                                <Grid  item style={{  margin: '5px 10px',  lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"relationShip2"}
                                        type={"text"}
                                        required={true}
                                        component={ImplementationFor["input"]}
                                    />
                                </Grid></Grid> 
                        </Grid>
                        <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>Renewal Date :</span></Grid>
                                <Grid item className={classes.LowDateContainer} style={{ textAlign: 'center', margin: '0px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"date4"}
                                        type={"date"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                    />
                                </Grid></Grid>
                                <Grid  className={classes.flexItem} style={{paddingBottom: '10px'}}> 
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}} ><span>Expiration Date :</span></Grid>
                                <Grid item className={classes.LowDateContainer} style={{ textAlign: 'center',  margin: '0px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"date5"}
                                        type={"date"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                    />
                                </Grid></Grid> 
                        </Grid>

                        <br />
                        <h4 style={{color: '#ea6225'}}>MEDICAL INFORMATION</h4>
                        <Grid container xs={12} className={classes.radioGroup} style={{padding: '10px 0px'}}>
                            <Grid item xs={12} md={8} style={{ display: 'flex'}}>
                            Do You Have Medical Insurance Coverage? 
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block', margin: '0px 10px',  lineHeight: '1rem' ,maxWidth: '100%'}}>
                                <Field
                                    name={"insurance"}
                                    required={true}
                                    options= {insurance}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                />
                            </Grid> 
                        </Grid>
                        <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>Physician Name :</span></Grid>
                                <Grid item xs={12} md={6} style={{ margin: '5px 10px',  lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                            name={"physician"}
                                            type={"text"}
                                            component={ImplementationFor["input"]}
                                            required={true}
                                            disabled={true}
                                        />
                                </Grid></Grid>
                                <Grid style={{ paddingBottom: '10px'}} className={classes.flexItem}>
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}} ><span>Last Exam Date :</span></Grid>
                                <Grid item xs={12} md={6} className={classes.LowDateContainer} style={{ textAlign: 'center', margin: '0px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"date6"}
                                        type={"date"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                    />
                                </Grid></Grid> 
                        </Grid>
                        
                        <Grid>
                        <h4 style={{color: '#ea6225'}}>MEDICAL CONDITION</h4><span>Please circle if you have any of these conditions. </span>
                        <Grid item xs={12} md={12} style={{ margin: '5px 10px',  lineHeight: '1rem' , width: '100%'}}>
                                <Field
                                    name={"condition"}
                                    label={"condition"}
                                    options= {condition}
                                    type={"checkbox"}
                                    component={ImplementationFor["checkbox"]}
                                    required={true}
                                />
                            </Grid>
                            </Grid>

                            <Grid>
                            <h4 style={{color: '#ea6225'}}>WOMEN</h4>
                            <Grid className={classes.radioGroup} style={{padding: '10px 0px'}}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Are You Pregnant or Think You May be Pregnant?
                            </Grid>
                            <Grid item xs={12} md={3} style={{  margin: '0px 10px',  lineHeight: '1rem' }}>
                                <Field
                                    name={"pregnant"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid xs={12} className={classes.flexContainer}>
                      <Grid  style={{paddingBottom: '10px'}}className={classes.radioGroup} >
                      <Grid  item item xs={12}  md={8} style={{display: 'inline-block', paddingBottom: '10px'}}><span>If Yes, How Many Weeks? </span></Grid>
                            <Grid item xs={12} md={3} style={{  margin: '0px 15px 0px 10px', lineHeight: '1rem', padding: '10px 0px' }} className={classes.blockToInline}>
                                <Field
                                        name={"referredBy1"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        // required={true}
                                    />
                        </Grid></Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Are You Nursing?
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block', margin: '0px 10px',  lineHeight: '1rem' }}>
                                <Field
                                    name={"nursing"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Do You Take Birth Control Pills? 
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block', margin: '0px 10px',  lineHeight: '1rem' }}>
                                <Field
                                    name={"birth"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid></Grid>

                        <Grid>
                        <h4 style={{color: '#ea6225', display: 'inline-block'}}>ALLERGIES:</h4><span className={classes.infoSpan}>(Please circle if you’re allergic to) </span>
                        <Grid item xs={12} md={12} style={{ margin: '5px 10px',  lineHeight: '1rem' , width: '100%'}}>
                                <Field
                                    name={"allergies"}
                                    options= {allergies}
                                    type={"checkbox"}
                                    component={ImplementationFor["checkbox"]}
                                    required={true}
                                />
                            </Grid>
                        </Grid>

                        <Grid>
                        <h4 style={{color: '#ea6225', display: 'inline-block'}}>MEDICATIONS:</h4>
                        <Grid item xs={12} >
                        Are You Taking Any Medications At This Time? (Please list)
                        </Grid>
                        <Grid item xs={12} md={12} className={classes.textArea}>
                            <Field
                                name={"medication"}
                                type={"text"}
                                component={ImplementationFor["textArea"]}
                                variant="standard"
                                required={true}
                            />
                        </Grid>
                                
                        <Grid item xs={12} >
                        Preferred Pharmacy Information
                        </Grid>
                        <Grid item xs={12} md={12} className={classes.textArea}>
                            <Field
                                name={"pharmacy"}
                                type={"text"}
                                component={ImplementationFor["textArea"]}
                                variant="standard"
                                required={true}
                            />
                        </Grid>
                        </Grid>

                        <Grid>
                        <h4 style={{color: '#ea6225', display: 'inline-block'}}>DIABETIC PATIENTS :</h4>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={7}>
                            Diabetes Type : 
                            </Grid>
                            <Grid item xs={12} md={5} style={{ display: 'inline-block', margin: '0px 10px',  lineHeight: '1rem' }}>
                                <Field
                                    name={"diabetic"}
                                    options= {diabetic}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={7}>
                            Type of Insulin: 
                            </Grid>
                            <Grid item xs={12} md={5} style={{ display: 'inline-block', margin: '0px 10px',  lineHeight: '1rem' }}>
                                <Field
                                    name={"insulin"}
                                    options= {insulin}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={7}>
                            Total Daily Insulin Dose (in Units)
                            </Grid>
                            <Grid item xs={12} md={5} style={{ display: 'inline-block', margin: '5px 10px',  padding: '10px 0px' }}>
                                <Field
                                        name={"dose"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                        </Grid></Grid>
                        </Grid>

                        <Grid>
                        <h4 style={{color: '#ea6225'}}>PATIENTS TAKING BISPHOSPHONATES:</h4>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={7}>
                            Reason for Bisphosphonates:
                            </Grid>
                            <Grid item xs={12} md={5} style={{ display: 'inline-block', margin: '0px 10px',  lineHeight: '1rem' }}>
                                <Field
                                    name={"bisphosphonates"}
                                    options= {bisphosphonates}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={7}>
                            Route of Administration:
                            </Grid>
                            <Grid item xs={12} md={5} style={{ display: 'inline-block', margin: '0px 10px',  lineHeight: '1rem' }}>
                                <Field
                                    name={"administration"}
                                    options= {administration}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                        <Grid item  style={{ display: 'flex',}} xs={12} md={7}>
                        How long you have been taking Bisphosphonates (in months)
                        </Grid>
                        <Grid item xs={12} md={5} style={{ display: 'inline-block', margin: '0px 10px',  padding: '10px 0px'}}>
                            <Field
                                    name={"period"}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    required={true}
                                />
                        </Grid></Grid>
                            <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={7}>
                            When was the last dose (in months) 
                            </Grid>
                            <Grid item xs={12} md={5} style={{ display: 'inline-block', margin: '0px 10px',  padding: '10px 0px'}}>
                                <Field
                                        name={"lastDose"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                        </Grid></Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={7}>
                            Type of osteoporosis medication:
                            </Grid>
                            <Grid item xs={12} md={5} style={{ display: 'inline-block', margin: '0px 10px',  lineHeight: '1rem' }}>
                                <Field
                                    name={"osteoporosis"}
                                    options= {osteoporosis}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        </Grid>

                        <Grid>
                        <h4 style={{color: '#ea6225'}}>PATIENT DENTAL HISTORY:</h4>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Do Your Gums Bleed While Brushing or Flossing? 
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block', margin: '0px 10px',  lineHeight: '1rem' }}>
                                <Field
                                    name={"gumsBleed"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Sensitive to Sweet or Sour Liquids/Foods? 
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block', margin: '0px 10px',  lineHeight: '1rem' }}>
                                <Field
                                    name={"sensitiveToSweet"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Are Your Teeth Sensitive to Hot or Cold Liquids/Foods? 
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block', margin: '0px 10px',  lineHeight: '1rem' }}>
                                <Field
                                    name={"sensitiveToHot"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Have You Had any Head, Neck or Jaw Injuries? 
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block', margin: '0px 10px',  lineHeight: '1rem' }}>
                                <Field
                                    name={"injuries"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Do You Feel Anything Unusual in Your Mouth? 
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block', margin: '0px 10px',  lineHeight: '1rem' }}>
                                <Field
                                    name={"unusual"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid  style={{padding: '10px 0px'}}className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Have You Had Orthodontic Treatment? 
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block', margin: '0px 10px',  lineHeight: '1rem' }}>
                                <Field
                                    name={"orthodontic"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Do You Clench or Grind Your Teeth? 
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block', margin: '0px 10px',  lineHeight: '1rem' }}>
                                <Field
                                    name={"grind"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid className={classes.radioGroup} style={{padding: '10px 0px'}}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Do You Like Your Smile?  
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block', margin: '0px 10px',  lineHeight: '1rem' }}>
                                <Field
                                    name={"smile"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid >
                            <Grid item  style={{ display: 'flex',}} xs={12} md={12}>
                            Please circle any problems you have experienced in your jaw joint (TMJ): 
                            </Grid>
                        <Grid item xs={12} md={12} style={{ margin: '5px 10px',  lineHeight: '1rem' , width: '100%'}}>
                                <Field
                                    name={"jawJoint"}
                                    options= {jawJoint}
                                    type={"checkbox"}
                                    component={ImplementationFor["checkbox"]}
                                    required={true}
                                />
                            </Grid></Grid>
                        </Grid>
                        <Grid>
                        <h4 style={{color: '#ea6225'}}>AUTHORIZATION & RELEASE:</h4>
                        <p>I consent to the diagnostic imaging procedures deemed necessary by this office. I
                        understand that the cone beam CT scanner produces images that are intended only for
                        evaluation of the mandibular & maxillary jawbones. This study is insufficient to detect intracranial and soft tissue disease processes. 3D CT scans are complimentary for
                        diagnostic & treatment purposes for our in-house patients. There is a fee for replication of your scan onto a disc and/or print out, should you request for your personal use. I authorize my dentist and his/her designated staff to perform an oral examination for the purpose of diagnosis and treatment planning. Furthermore, I authorize the taking of all xrays required as a necessary part of this examination. In addition, by signing this document I understand and agree to the above policy. </p>
                        <p>I authorize the release of any information to third party payers and/or health practitioners for consultation purposes. I agree to be responsible for payment of full services rendered on my behalf, or my dependents behalf. I certify I have read and understand the above information and that the information I have provided is accurate.</p>
                        <Grid style={{ textAlign: 'center' }}>Patient’s Signature :
                            <Grid item xs={12} md={6} style={{ display: 'inline-block', margin: '0px 10px' }}>
                                <Field
                                    name={"signature1"}
                                    type={"text"}
                                    component={ImplementationFor["signature"]}
                                    id={"signature1"}
                                    onChange={(e) => onChange("signature1", e)}
                                />
                            </Grid>
                            <Grid style={{ display: 'inline-block', paddingBottom: '10px' }}>
                                    <span>Date:</span>
                                    <Grid item xs={6} md={6} className={classes.DateContainer} style={{ textAlign: 'center',  margin: '0px 10px' }}>
                                        <Field
                                            name={"date7"}
                                            type={"text"}
                                            component={ImplementationFor["date"]}
                                            id={"date7"}
                                        />
                                </Grid>
                            </Grid>
                        </Grid>
                        
                        <h4 style={{color: '#ea6225'}}>FINANCIAL POLICY :</h4>
                        <p>We are committed to providing you with the best possible dental care. If you have dental insurance, we will be happy to help you receive your maximum allowable benefits. In order to achieve these goals, we need your assistance and understanding of our financial policy. We will gladly discuss any insurance questions you may have. However, you must realize that your insurance plan is a contract  between you and/or your employer and the insurance company. While the filing of claims is a courtesy that we extend to our patients, all charges are your responsibility on the date the services are rendered regardless of insurance benefits
                        </p>
                        <p style={{textDecoration: 'underline'}}>Payments for services are due in full at the time of services rendered.</p>
                        <p>
                        A 50% deposit is required for those patients who are starting any major dental treatment regardless of what your insurance will pay. We gladly accept Visa, Master Card, personal in-state checks, or cash. </p>
                        <p>
                        If there is still a balance owing after your insurance company pays, that balance will be
                        due and payable within 30 days. If there is a credit on the account, we will issue a
                        refund check.
                        </p>
                        <p>Return checks and balances older than 60 days are subject to additional
                        collection fees and interest charges of 0.83% per month, or 10% per year. </p>
                        <p>Charges may also be made for broken appointments and appointments cancelled without a 48 hour notice. A charge of $125.00 will be charged for any hygiene broken appointment and $200.00-$500.00 for a broken appointment with the Doctor.If an account is turned into collections, you will be responsible for all legal fees.</p>
                        <p>
                        I have read the above and fully understand and accept the terms and conditions set forth. I authorize the release of any information relating to my dental claim. I understand I am responsible for all costs of dental treatment regardless of insurance. </p>
                        <Grid style={{ textAlign: 'center' }}>Patient’s Signature :
                            <Grid item xs={12} md={6} style={{ display: 'inline-block', margin: '0px 10px' }}>
                                <Field
                                    name={"signature2"}
                                    type={"text"}
                                    component={ImplementationFor["signature"]}
                                    id={"signature2"}
                                    onChange={(e) => onChange("signature2", e)}
                                />
                            </Grid>
                            <Grid style={{ display: 'inline-block', paddingBottom: '10px' }}>
                                    <span>Date:</span>
                                    <Grid item xs={6} md={6} className={classes.DateContainer} style={{ textAlign: 'center',  margin: '0px 10px' }}>
                                        <Field
                                            name={"date8"}
                                            type={"text"}
                                            component={ImplementationFor["date"]}
                                            id={"date8"}
                                        />
                                </Grid>
                            </Grid>
                        </Grid>

                        <h4 style={{color: '#ea6225'}}>NOTICE OF PRIVACY PRACTICES :</h4>
                        <p>This notice describes how your health information may be used and disclosed by our office and how you can access this information. Please review this document and sign the acknowledgement attached to this form. At our office, we have always kept your health information secure and confidential. A new law requires us to continue maintaining your privacy in addition to providing you with this information and adhering to the new law set in place. This law allows us to:</p>

                        <p>Use or disclose your health information to those involved in your health treatment, i.e. a review of your file by a specialist whom we may involve in your care. </p>

                        <p>We may use or disclose your health information for payment services, i.e.we may send a report of progress with your claim to your insurance company. </p>

                        <p>We may disclose your health information with our business associates, such as a dental laboratory. Business associates have a written contract with the Doctor,which requires them to protect your privacy.</p>

                        <p>We may use your information to contact you, i.e. news letters, educational materials or other information. We may also contact you to confirm your appointments in lieu of your absence when the confirmation call is made our office may leave a message on your answering machine or with persons who answer the contact number provided. </p>

                        <p> In an emergency, we may also disclose your health information to a family member or another person responsible for your care. </p>

                        <p>We may also release your health information if required by law. Exceptions are as follows: we will not disclose your health information without prior written knowledge.</p>

                        <p>You may request in writing that we do not disclose your health information as described above, our office will inform you if that request cannot be met. You have the right to know any of the uses or disclosures of your health information beyond the normal uses. As we will need to contact you periodically we will use whatever address or telephone number you prefer. </p>

                        <p>You have the right to transfer copies of your information to another practice. You have the right to view and receive a copy of your health information. A written request is needed to prepare the documents requested. All copies of x-rays and/or records will ensue a reasonable fee for copies of documents.</p>

                        <p>You have the right to request an amendment or change to your health information. All requests should be submitted in writing to our office. If you wish to include a statement in your file, please submit this in writing. The changes requested will be made at the discretion of our office, no documents shall be removed or altered the file can only be changed with new information. </p>

                        <p>All patients have a right to receive a copy of this notice. If any details in this notice have been changed or updated, we will notify of the changes in writing. Complaints can be filed with:</p>

                        <p> Department of Health and Human Services<br />
                        200 Independent Avenue, S.W.,Room 5009F<br />
                        Washington, DC 20201</p>

                        <p>You will not be retaliated against for filing a complaint, however should you need assistance regarding your health information privacy, please contact our office.</p>
                        
                        <p>By signing below,I acknowledge that I have received and read a copy of the Notice of Privacy Practices.I am in full understanding that the notice given to me may be updated in the future, and I will be notified of any amendments to this notice by telephone or mail.</p> 

                        <Grid  className={classes.flexContainer}>
                        
                        <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                            <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Name:</span></Grid>
                            <Grid  item style={{  margin: '5px 10px',  lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                    name={"patientName"}
                                    type={"text"}
                                    required={true}
                                    component={ImplementationFor["input"]}
                                />
                            </Grid>
                        </Grid>
                        </Grid>
                        <Grid  className={classes.flexContainer}>
                        
                        <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                            <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Relationship to minor patient(if applicable):</span></Grid>
                            <Grid  item style={{  margin: '5px 10px',  lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                    name={"relation"}
                                    type={"text"}
                                    // required={true}
                                    component={ImplementationFor["input"]}
                                />
                            </Grid>
                        </Grid>
                        </Grid>
                        <Grid style={{ textAlign: 'center' }}>Patient’s Signature :
                            <Grid item xs={12} md={6} style={{ display: 'inline-block', margin: '0px 10px' }}>
                                <Field
                                    name={"signature3"}
                                    type={"text"}
                                    component={ImplementationFor["signature"]}
                                    id={"signature3"}
                                    onChange={(e) => onChange("signature3", e)}
                                />
                            </Grid>
                            <Grid style={{ display: 'inline-block', paddingBottom: '10px' }}>
                                <span>Date:</span>
                                <Grid item xs={6} md={6} className={classes.DateContainer} style={{ textAlign: 'center',  margin: '0px 10px' }}>
                                    <Field
                                        name={"date9"}
                                        type={"text"}
                                        component={ImplementationFor["date"]}
                                        id={"date9"}
                                    />
                                </Grid>
                            </Grid>
                        </Grid>
                        </Grid>

                        <Grid style={{textAlign:"center", padding: '20px 0px'}}>
                        <Button className={classes.button} type="submit" form="consent8">Submit</Button>
                        </Grid>
                    </Grid> 
                    </Grid>
              
            </form>
            </Grid >
            </Grid >
        </Grid>
      
 }
 
 Consent8.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent8',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true
 })(Consent8);