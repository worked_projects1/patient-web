
/**
 * 
 *  Consent9
 * 
 */
 import  * as React from 'react';
 import { useState, useEffect,useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm } from 'redux-form';
 import {Grid, Button,} from '@mui/material';
 import Styles from './styles';
import {ImplementationFor} from '../Components/CreateRecordForm/utils'

/**
  * 
  * @param {object} props 
  * @returns 
  */

 function Consent9(props) {
     const {  handleSubmit, destroy, change,dispatch, initialValues  } = props;
     const classes = Styles();
     const [showModal, setModalOpen] = useState(false);
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState(""); 

     dispatch(change('physician',initialValues.providerFirst+ ' ' + initialValues.providerLast))

     useEffect(() => {
        return () => destroy();
    }, []);

    const onChange = (val,eventValue) => {
    val=="signature1"&&setSignature1(eventValue)
    val=="signature2"&&setSignature2(eventValue)
    val=="signature3"&&setSignature3(eventValue)

    }
    const  sex =  [
        {
          "label": "Male",
          "value": "Male"
        },
        {
          "label": "Female",
          "value": "Female"
        }]

    const  insurance =  [
        {
            "label": "Yes",
            "value": "Yes"
        },
        {
            "label": "No",
            "value": "No"
        }]
        
    const allergies = [
        {
            "label": "Penicillin",
            "value": "Penicillin"
        },
        {
            "label": "Latex",
            "value": "Latex"
        },
        {
            "label": "Codeine",
            "value": "Codeine"
        },
        {
            "label": "Anesthetics",
            "value": "Anesthetics"
        },
        {
            "label": "Sulfa Drugs",
            "value": "Sulfa Drugs"
        },
        {
            "label": "Aspirin",
            "value": "Aspirin"
        },
        {
            "label": "Other",
            "value": "Other",
            "input": true
        },
        
    ]
    const condition = [
        {
            "label": "High Blood Pressure ",
            "value": "High Blood Pressure "
        },
        {
            "label": "Low Blood Pressure",
            "value": "Low Blood Pressure"
        },
        {
            "label": "Diabetes",
            "value": "Diabetes"
        },
        {
            "label": "Smoking ",
            "value": "Smoking "
        },
        {
            "label": "Sinus Problems",
            "value": "Sinus Problems"
        },
        {
            "label": "Bisphosphonate Medications",
            "value": "Bisphosphonate Medications"
        },
        {
            "label": "Cancer",
            "value": "Cancer"
        },
        {
            "label": "Radiation Therapy",
            "value": "Radiation Therapy"
        },
        {
            "label": "Chemo Therapy",
            "value": "Chemo Therapy"
        },
        {
            "label": "Bleeding Disorder",
            "value": "Bleeding Disorder"
        },
        {
            "label": "Anemia",
            "value": "Anemia"
        },
        {
            "label": "Angina",
            "value": "Angina"
        },
        {
            "label": "Arthritis",
            "value": "Arthritis"
        },
        {
            "label": "Artificial joints",
            "value": "Artificial joints"
        },
        {
            "label": "Low Blood Pressure",
            "value": "Low Blood Pressure"
        },
        {
            "label": "Alcohol/Drug Dependency ",
            "value": "Alcohol/Drug Dependency "
        },
        {
            "label": "Asthma",
            "value": "Asthma"
        },
        {
            "label": "Epilepsy",
            "value": "Epilepsy"
        },
        {
            "label": "Artificial Heart Valve ",
            "value": "Artificial Heart Valve "
        },
        {
            "label": "Congenital Heart Problems ",
            "value": "Congenital Heart Problems "
        },
        {
            "label": "Congenital Heart Problems ",
            "value": "Congenital Heart Problems "
        },
        {
            "label": "Heart Attack ",
            "value": "Heart Attack "
        },
        {
            "label": "Heart Disease",
            "value": "Heart Disease"
        },
        {
            "label": "Heart Murmur",
            "value": "Heart Murmur"
        },
        {
            "label": "Heart Pacemaker",
            "value": "Heart Pacemaker"
        },
        {
            "label": "Heart Surgery",
            "value": "Heart Surgery"
        },
        {
            "label": "Mitral Valve Prolapse",
            "value": "Mitral Valve Prolapse"
        },
        {
            "label": "Stroke",
            "value": "Stroke"
        },
        {
            "label": "Glaucoma",
            "value": "Glaucoma"
        },
        {
            "label": "HIV Infection (AIDS)",
            "value": "HIV Infection (AIDS)"
        },
        {
            "label": "Hay Fever",
            "value": "Hay Fever"
        },
        {
            "label": "Frequent Headaches ",
            "value": "Frequent Headaches "
        },
        {
            "label": "Ulcers",
            "value": "Ulcers"
        },
        {
            "label": "Tuberculosis",
            "value": "Tuberculosis"
        },
        {
            "label": "Thyroid Problems ",
            "value": "Thyroid Problems "
        },
        {
            "label": "Hepatitis A ",
            "value": "Hepatitis A "
        },
        {
            "label": "Hepatitis B",
            "value": "Hepatitis B"
        },
        {
            "label": "Hepatitis C ",
            "value": "Hepatitis C "
        },
        {
            "label": "Kidney Disease ",
            "value": "Kidney Disease "
        },
        {
            "label": "Leukemia ",
            "value": "Leukemia "
        },
        {
            "label": "Liver Disease",
            "value": "Liver Disease"
        },
        {
            "label": "Seizures/Fainting",
            "value": "Seizures/Fainting"
        },
        {
            "label": "Respiratory Problems",
            "value": "Respiratory Problems"
        },
        {
            "label": "Rheumatic Fever",
            "value": "Rheumatic Fever"
        },{
            "label": "Other",
            "value": "Other",
            "input": true
        },
    ]

    const  women =  [
        {
            "label": "Yes",
            "value": "Yes"
        },
        {
            "label": "No",
            "value": "No"
        }]

    const  diabetic =  [
        {
            "label": "Type I",
            "value": "Type I"
        },
        {
            "label": "Type II Insulin Dependent",
            "value": "Type II Insulin Dependent"
        },
        {
            "label": "Type II Non-Insulin Dependent ",
            "value": "Type II Non-Insulin Dependent "
        },
    ]

    const insulin = [
        {
            "label": "Intermediate",
            "value": "Intermediate"
        },
        {
            "label": "Long acting",
            "value": "Long acting "
        },
        {
            "label": "Short Acting",
            "value": "Short Acting"
        },
    ]

    const bisphosphonates = [
        {
            "label": "Osteopenia/Osteoporosis",
            "value": "Osteopenia/Osteoporosis"
        },
        {
            "label": "Cancer",
            "value": "Cancer"
        }
    ]
        
    const administration = [
        {
            "label": "Oral",
            "value": "Oral"
        },
        {
            "label": "IV",
            "value": "IV"
        }
    ]

    const osteoporosis = [
        {
            "label": "Bisphosphonates",
            "value": "Bisphosphonates"
        },
        {
            "label": "Monoclonal Antibody",
            "value": "Monoclonal Antibody"
        }
    ]

    const  jawJoint =  [
        {
            "label": "Clicking Pain",
            "value": "Clicking Pain"
        },
        {
            "label": "Difficulty in Opening ",
            "value": "Difficulty in Opening "
        },
        {
            "label": "Difficulty in Closing",
            "value": "Difficulty in Closing"
        },
        {
            "label": "Difficulty in Chewing",
            "value": "Difficulty in Chewing"
        }]
        const normalizePhone = (value, previousValue) => {
            if (!value) {
              return value
            }
            const onlyNums = value.replace(/[^\d]/g, '')
            if (!previousValue || value.length > previousValue.length) {
              // typing forward
              if (onlyNums.length === 3) {
                return onlyNums + '-'
              }
              if (onlyNums.length === 6) {
                return onlyNums.slice(0, 3) + '-' + onlyNums.slice(3) + '-'
              }
            }
            if (onlyNums.length <= 3) {
              return onlyNums
            }
            if (onlyNums.length <= 6) {
              return onlyNums.slice(0, 3) + '-' + onlyNums.slice(3)
            }
            return onlyNums.slice(0, 3) + '-' + onlyNums.slice(3, 6) + '-' + onlyNums.slice(6, 10)
          }
        
    return <Grid className={classes.firstContainer}  >
            <Grid className={classes.secContainer} >
                <Grid item className={classes.EditForm} justify="center">
                    <span className={classes.formTitle}>Patient Health History</span>
                </Grid>
            <Grid item className={classes.thirdContainer}>
            <form onSubmit={handleSubmit} id='consent9'>

                <Grid item spacing={12} xs={12} className={classes.content} justify="space-between">
                <Grid>
                <h4 style={{color: '#ea6225'}}>PERSONAL INFORMATION</h4>
                <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>First Name:</span></Grid>
                                <Grid item style={{margin: '5px 10px',  lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"firstName"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid>
                                <Grid  className={classes.flexItem} style={{paddingBottom: '10px'}}> 
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}}><span>MI:</span></Grid>
                                <Grid item  style={{margin: '5px 10px',  lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"mi"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid> 
                                <Grid  className={classes.flexItem} style={{paddingBottom: '10px'}}> 
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}}><span>Last Name:</span></Grid>
                                <Grid item  style={{margin: '5px 10px',  lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"lastName"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid>
                        </Grid>

                    <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>Cell Phone :</span></Grid>
                        <Grid item  style={{ margin: '5px 10px', lineHeight: '1rem'}} className={classes.blockToInline}>
                        <Field
                                    name={"cellPhone1"}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    required={true}
                                    normalize={normalizePhone}
                                />
                                </Grid></Grid>
                                <Grid  className={classes.flexItem} style={{paddingBottom: '10px'}}> 
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}} ><span>Initial Visit Date (MM/DD/YYYY) :</span></Grid>
                                <Grid item className={classes.LowDateContainer} style={{ textAlign: 'center',  margin: '0px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"date1"}
                                        type={"date"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                    />
                                </Grid></Grid> 
                        </Grid>
                       
                     <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>Birth Date (MM/DD/YYYY) :</span></Grid>
                        <Grid item  className={classes.LowDateContainer} style={{ textAlign: 'center', margin: '0px 10px',lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                        name={"dob"}
                                        type={"text"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                    />
                                </Grid></Grid>
                                <Grid  className={classes.flexItem} style={{paddingBottom: '10px'}}> 
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}}><span>Occupation :</span></Grid>
                                <Grid item  style={{ margin: '5px 10px', lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"occupation"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid> 
                        </Grid>
                        <Grid style={{  paddingBottom: '10px'}} >
                            <span>Address (Street, City, State, Zip, Country):</span>
                            <Grid item xs={12} md={12} className={classes.textArea}>
                                <Field
                                    name={"address"}
                                    type={"text"}
                                    component={ImplementationFor["textArea"]}
                                    variant="standard"
                                    required={true}
                                    />
                        </Grid>
                    </Grid>
                    <Grid xs={12} className={classes.flexContainer}>
                      <Grid  style={{paddingBottom: '10px'}}className={classes.radioGroup} >
                      <Grid  item item xs={12}  md={2} style={{display: 'inline-block', paddingBottom: '10px'}}><span>Email</span></Grid>
                            <Grid item xs={12} md={12} style={{  margin: '0px 15px 0px 10px', lineHeight: '1rem', padding: '10px 0px' }} className={classes.blockToInline}>
                                <Field
                                        name={"email"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                        </Grid></Grid>
                    </Grid>
                    <Grid style={{ display: 'flex',padding: '10px 0px'}}>
                        <Grid item  style={{ display: 'flex',lineHeight: '2rem'}}>
                        Sex :
                        </Grid>
                        <Grid item xs={6} md={9} style={{ display: 'inline-block',  margin: '5px 10px', lineHeight: '1rem' }}>
                            <Field
                                name={"sex"}
                                options= {sex}
                                type={"radio"}
                                component={ImplementationFor["radio"]}
                                required={true}
                            />
                        </Grid> 
                    </Grid>
                    <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>How did you hear of us?</span></Grid>
                                <Grid item style={{ margin: '5px 10px', lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"referredBy"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid>
                                </Grid>

                        <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>Chief Complaint :</span></Grid>
                                <Grid item style={{ margin: '5px 10px', lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"complaint"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid>
                                <Grid  className={classes.flexItem} style={{paddingBottom: '10px'}}> 
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}}><span>Treatment Goal :</span></Grid>
                                <Grid item  style={{ margin: '5px 10px', lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"goal"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid> 
                        </Grid>
                        <br />
                        <h4 style={{color: '#ea6225'}}>EMERGENCY CONTACT </h4>
                        <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>Full Name :</span></Grid>
                                <Grid item style={{ margin: '5px 10px', lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"fullName"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid>
                                <Grid  className={classes.flexItem} style={{paddingBottom: '10px'}}> 
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}}><span>Relationship :</span></Grid>
                                <Grid item  style={{ margin: '5px 10px', lineHeight: '1rem'}} className={classes.blockToInline}>
                                    <Field
                                        name={"relationShip1"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid> 
                        </Grid>
                        <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>Cell Phone :</span></Grid>
                                <Grid item  style={{  margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                            name={"cellPhone2"}
                                            type={"text"}
                                            component={ImplementationFor["input"]}
                                            required={true}
                                            normalize={normalizePhone}
                                        />
                                </Grid></Grid>
                                <Grid  className={classes.flexItem} style={{paddingBottom: '10px'}}> 
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}}><span>Work Phone :</span></Grid>
                                <Grid item  style={{ margin: '5px 10px', lineHeight: '1rem'  }} className={classes.blockToInline}>
                                    <Field
                                            name={"cellPhone3"}
                                            type={"text"}
                                            component={ImplementationFor["input"]}
                                            required={true}
                                            normalize={normalizePhone}
                                        />
                                </Grid></Grid> 
                        </Grid>
                        
                        <br />
                        <h4 style={{color: '#ea6225'}}>DENTAL INSURANCE</h4>
                        <Grid xs={12} className={classes.flexContainer}>
                        <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                               
                                <Grid item  style={{ display: 'inline-block'}}><span>Carrier Name :</span> </Grid>
                                <Grid style={{ display: 'inline-block', margin:  '0px 15px 5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                            name={"carrier"}
                                            type={"text"}
                                            component={ImplementationFor["input"]}
                                            required={true}
                                        />
                                </Grid></Grid>
                                <Grid style={{ paddingBottom: '10px'}} className={classes.flexItem}>
                                <Grid item style={{ display: 'inline-block'}}><span> Plan Name :</span></Grid>
                                <Grid  style={{ display: 'inline-block',  margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"planName"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                                </Grid></Grid> 
                        </Grid>
                        <Grid className={classes.flexContainer}>
                        <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                                <Grid item style={{ display: 'inline-block'}}><span>Carrier Phone :</span></Grid>
                                <Grid  style={{ display: 'inline-block',  margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                            name={"cellPhone4"}
                                            type={"number"}
                                            component={ImplementationFor["input"]}
                                            required={true}
                                            normalize={normalizePhone}
                                        />
                                </Grid></Grid>
                                <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                                <Grid item style={{ display: 'inline-block'}}><span> Group :</span></Grid>
                                <Grid  style={{ display: 'inline-block',  margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"group"}
                                        type={"text"}
                                        required={true}
                                        component={ImplementationFor["input"]}
                                    />
                                </Grid></Grid> 
                        </Grid>
                        <Grid className={classes.flexContainer}>
                        <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                        <Grid item  style={{ display: 'inline-block'}}><span>Subscriber Name :</span></Grid>
                                <Grid style={{ display: 'inline-block',  margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                            name={"subscriber"}
                                            type={"text"}
                                            required={true}
                                            component={ImplementationFor["input"]}
                                        />
                                </Grid></Grid>
                                <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                                <Grid item style={{ display: 'inline-block'}}><span> Subscriber ID :</span></Grid>
                                <Grid style={{ display: 'inline-block',  margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"subscriberId"}
                                        type={"text"}
                                        required={true}
                                        component={ImplementationFor["input"]}
                                    />
                                </Grid></Grid> 
                        </Grid>
                        <Grid  className={classes.flexContainer}>
                        <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                        <Grid item style={{ display: 'inline-block', paddingBottom: '10px'}}>
                                <span>Subscriber Birthday :</span></Grid> 
                                <Grid item  className={classes.LowDateContainer} style={{ textAlign: 'center', margin: '0px 10px',lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"date3"}
                                        type={"date"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                    />
                                </Grid></Grid>
                                <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                                <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Relationship :</span></Grid>
                                <Grid  item style={{   margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"relationShip2"}
                                        type={"text"}
                                        required={true}
                                        component={ImplementationFor["input"]}
                                    />
                                </Grid></Grid> 
                        </Grid>
                        <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>Renewal Date :</span></Grid>
                                <Grid item className={classes.LowDateContainer} style={{ textAlign: 'center', margin: '0px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"date4"}
                                        type={"date"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                    />
                                </Grid></Grid>
                                <Grid  className={classes.flexItem} style={{paddingBottom: '10px'}}> 
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}} ><span>Expiration Date :</span></Grid>
                                <Grid item className={classes.LowDateContainer} style={{ textAlign: 'center',  margin: '0px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"date5"}
                                        type={"date"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                    />
                                </Grid></Grid> 
                        </Grid>

                        <br />
                        <h4 style={{color: '#ea6225'}}>MEDICAL INFORMATION</h4>
                        <Grid container xs={12} className={classes.radioGroup} style={{padding: '10px 0px'}}>
                            <Grid item xs={12} md={8} style={{ display: 'flex'}}>
                            Do You Have Medical Insurance Coverage? 
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block',  margin: '0px 10px', lineHeight: '1rem' ,maxWidth: '100%'}}>
                                <Field
                                    name={"insurance"}
                                    required={true}
                                    options= {insurance}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                />
                            </Grid> 
                        </Grid>
                        <Grid  className={classes.flexContainer}>
                        <Grid  className={classes.flexItem} style={{ paddingBottom: '10px'}}>
                        <Grid item style={{display: 'inline-block',  paddingBottom: '10px'}}><span>Physician Name :</span></Grid>
                                <Grid item xs={12} md={6} style={{  margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                            name={"physician"}
                                            type={"text"}
                                            component={ImplementationFor["input"]}
                                            required={true}
                                            disabled={true}
                                        />
                                </Grid></Grid>
                                <Grid style={{ paddingBottom: '10px'}} className={classes.flexItem}>
                                <Grid  item  style={{ display: 'inline-block', paddingBottom: '10px'}} ><span>Last Exam Date :</span></Grid>
                                <Grid item xs={12} md={6} className={classes.LowDateContainer} style={{ textAlign: 'center', margin: '0px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                    <Field
                                        name={"date6"}
                                        type={"date"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                    />
                                </Grid></Grid> 
                        </Grid>
                        
                        <Grid>
                        <h4 style={{color: '#ea6225'}}>MEDICAL CONDITION</h4><span>Please circle if you have any of these conditions. </span>
                        <Grid item xs={12} md={12} style={{  margin: '5px 10px', lineHeight: '1rem' , width: '100%'}}>
                                <Field
                                    name={"condition"}
                                    options= {condition}
                                    type={"checkbox"}
                                    component={ImplementationFor["checkbox"]}
                                    required={true}
                                />
                            </Grid>
                            </Grid>

                            <Grid>
                            <h4 style={{color: '#ea6225'}}>WOMEN</h4>
                        <Grid className={classes.radioGroup} style={{padding: '10px 0px'}}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Are You Pregnant or Think You May be Pregnant?
                            </Grid>
                            <Grid item xs={12} md={3} style={{   margin: '0px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"pregnant"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid xs={12} className={classes.flexContainer}>
                      <Grid  style={{paddingBottom: '10px'}}className={classes.radioGroup} >
                      <Grid  item item xs={12}  md={8} style={{display: 'inline-block', paddingBottom: '10px'}}><span>If Yes, How Many Weeks?</span></Grid>
                            <Grid item xs={12} md={3} style={{  margin: '0px 15px 0px 10px', lineHeight: '1rem', padding: '10px 0px' }} className={classes.blockToInline}>
                                <Field
                                        name={"referredBy1"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                        </Grid></Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Are You Nursing?
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block',  margin: '0px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"nursing"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Do You Take Birth Control Pills? 
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block',  margin: '0px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"birth"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid></Grid>

                        <Grid>
                        <h4 style={{color: '#ea6225', display: 'inline-block'}}>ALLERGIES:</h4><span className={classes.infoSpan}>(Please circle if you’re allergic to) </span>
                        <Grid item xs={12} md={12} style={{  margin: '5px 10px', lineHeight: '1rem' , width: '100%'}}>
                                <Field
                                    name={"allergies"}
                                    options= {allergies}
                                    type={"checkbox"}
                                    component={ImplementationFor["checkbox"]}
                                    required={true}
                                />
                            </Grid>
                        </Grid>

                        <Grid>
                        <h4 style={{color: '#ea6225', display: 'inline-block'}}>MEDICATIONS:</h4>
                        <Grid item xs={12} >
                        Are You Taking Any Medications At This Time? (Please list)
                        </Grid>
                        <Grid item xs={12} md={12} className={classes.textArea}>
                            <Field
                                name={"medication"}
                                type={"text"}
                                component={ImplementationFor["textArea"]}
                                variant="standard"
                                required={true}
                            />
                        </Grid>
                                
                        <Grid item xs={12} >
                        Preferred Pharmacy Information
                        </Grid>
                        <Grid item xs={12} md={12} className={classes.textArea}>
                            <Field
                                name={"pharmacy"}
                                type={"text"}
                                component={ImplementationFor["textArea"]}
                                variant="standard"
                                required={true}
                            />
                        </Grid>
                        </Grid>

                        <Grid>
                        <h4 style={{color: '#ea6225', display: 'inline-block'}}>DIABETIC PATIENTS :</h4>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={7}>
                            Diabetes Type : 
                            </Grid>
                            <Grid item xs={12} md={5} style={{ display: 'inline-block',  margin: '0px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"diabetic"}
                                    options= {diabetic}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={7}>
                            Type of Insulin: 
                            </Grid>
                            <Grid item xs={12} md={5} style={{ display: 'inline-block',  margin: '0px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"insulin"}
                                    options= {insulin}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={7}>
                            Total Daily Insulin Dose (in Units)
                            </Grid>
                            <Grid item xs={12} md={5} style={{ display: 'inline-block',  margin: '5px 10px', padding: '10px 0px' }}>
                                <Field
                                        name={"dose"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                        </Grid></Grid>
                        </Grid>

                        <Grid>
                        <h4 style={{color: '#ea6225'}}>PATIENTS TAKING BISPHOSPHONATES:</h4>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={7}>
                            Reason for Bisphosphonates:
                            </Grid>
                            <Grid item xs={12} md={5} style={{ display: 'inline-block',  margin: '0px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"bisphosphonates"}
                                    options= {bisphosphonates}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={7}>
                            Route of Administration:
                            </Grid>
                            <Grid item xs={12} md={5} style={{ display: 'inline-block',  margin: '0px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"administration"}
                                    options= {administration}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                            <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={7}>
                            How long you have been taking Bisphosphonates (in months)
                            </Grid>
                            <Grid item xs={12} md={5} style={{ display: 'inline-block',  margin: '5px 10px', padding: '10px 0px'}}>
                                <Field
                                        name={"period"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                        </Grid></Grid>
                            <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={7}>
                            When was the last dose (in months) 
                            </Grid>
                            <Grid item xs={12} md={5} style={{ display: 'inline-block',  margin: '5px 10px', padding: '10px 0px'}}>
                                <Field
                                        name={"lastDose"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                    />
                        </Grid></Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={7}>
                            Type of osteoporosis medication:
                            </Grid>
                            <Grid item xs={12} md={5} style={{ display: 'inline-block',  margin: '0px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"osteoporosis"}
                                    options= {osteoporosis}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        </Grid>

                        <Grid>
                        <h4 style={{color: '#ea6225'}}>PATIENT DENTAL HISTORY:</h4>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Do Your Gums Bleed While Brushing or Flossing? 
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block',  margin: '0px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"gumsBleed"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Sensitive to Sweet or Sour Liquids/Foods? 
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block',  margin: '0px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"sensitiveToSweet"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Are Your Teeth Sensitive to Hot or Cold Liquids/Foods? 
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block',  margin: '0px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"sensitiveToHot"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Have You Had any Head, Neck or Jaw Injuries? 
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block',  margin: '0px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"injuries"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Do You Feel Anything Unusual in Your Mouth? 
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block',  margin: '0px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"unusual"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid  style={{padding: '10px 0px'}}className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Have You Had Orthodontic Treatment? 
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block',  margin: '0px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"orthodontic"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid style={{padding: '10px 0px'}} className={classes.radioGroup}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Do You Clench or Grind Your Teeth? 
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block',  margin: '0px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"grind"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid className={classes.radioGroup} style={{padding: '10px 0px'}}>
                            <Grid item  style={{ display: 'flex',}} xs={12} md={8}>
                            Do You Like Your Smile?  
                            </Grid>
                            <Grid item xs={12} md={3} style={{ display: 'inline-block',  margin: '0px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"smile"}
                                    options= {women}
                                    type={"radio"}
                                    component={ImplementationFor["radio"]}
                                    required={true}
                                />
                                </Grid>
                        </Grid>
                        <Grid >
                            <Grid item  style={{ display: 'flex',}} xs={12} md={12}>
                            Please circle any problems you have experienced in your jaw joint (TMJ): 
                            </Grid>
                        <Grid item xs={12} md={12} style={{  margin: '5px 10px', lineHeight: '1rem' , width: '100%'}}>
                                <Field
                                    name={"jawJoint"}
                                    options= {jawJoint}
                                    type={"checkbox"}
                                    component={ImplementationFor["checkbox"]}
                                    required={true}
                                />
                            </Grid></Grid>
                        </Grid>
                        <Grid>
                        <h4 style={{color: '#ea6225'}}>AUTHORIZATION & RELEASE:</h4>
                        <p>I consent to the diagnostic imaging procedures deemed necessary by this office. I
                        understand that the cone beam CT scanner produces images that are intended only for
                        evaluation of the mandibular & maxillary jawbones. This study is insufficient to detect intracranial and soft tissue disease processes. 3D CT scans are complimentary for
                        diagnostic & treatment purposes for our in-house patients. There is a fee for replication of your scan onto a disc and/or print out, should you request for your personal use. I authorize my dentist and his/her designated staff to perform an oral examination for the purpose of diagnosis and treatment planning. Furthermore, I authorize the taking of all xrays required as a necessary part of this examination. In addition, by signing this document I understand and agree to the above policy. </p>
                        <p>I authorize the release of any information to third party payers and/or health practitioners for consultation purposes. I agree to be responsible for payment of full services rendered on my behalf, or my dependents behalf. I certify I have read and understand the above information and that the information I have provided is accurate.</p>
                        <Grid style={{ textAlign: 'center' }}>Patient’s Signature :
                            <Grid item xs={12} md={6} style={{ display: 'inline-block', margin: '0px 10px' }}>
                                <Field
                                    name={"signature1"}
                                    type={"text"}
                                    component={ImplementationFor["signature"]}
                                    id={"signature1"}
                                    onChange={(e) => onChange("signature1", e)}
                                />
                            </Grid>
                            <Grid style={{ display: 'inline-block', paddingBottom: '10px' }}>
                                    <span>Date:</span>
                                    <Grid item xs={6} md={6} className={classes.DateContainer} style={{ textAlign: 'center',  margin: '0px 10px' }}>
                                        <Field
                                            name={"date7"}
                                            type={"text"}
                                            component={ImplementationFor["date"]}
                                            id={"date7"}
                                        />
                                </Grid>
                            </Grid>
                        </Grid>
                        </Grid>

                        <Grid style={{textAlign:"center", padding: '20px 0px'}}>
                        <Button className={classes.button} type="submit" form="consent9">Submit</Button>
                        </Grid>
                    </Grid> 
                    </Grid>
              
            </form>
            </Grid >
            </Grid >
        </Grid>
      
 }
 
 Consent9.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent9',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true
 })(Consent9);