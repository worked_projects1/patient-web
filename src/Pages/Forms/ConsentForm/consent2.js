/**
 * 
 *  Consent2
 * 
 */

 import React, { useState, useEffect, useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm, } from 'redux-form';
 import { Grid, Button, } from '@mui/material';
 import Styles from './styles';
 import { ImplementationFor } from '../Components/CreateRecordForm/utils'
 
 /**
   * 
   * @param {object} props 
   * @returns 
   */
 function Consent2(props) {
     const { handleSubmit, destroy, } = props;
     const classes = Styles();
     const [showModal, setModalOpen] = useState(false);
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState("");
 
     useEffect(() => {
         return () => destroy();
     }, []);
 
     const onChange = (val, eventValue) => {
         val == "signature1" && setSignature1(eventValue)
         val == "signature2" && setSignature2(eventValue)
         val == "signature3" && setSignature3(eventValue)
     }
 
     return <Grid className={classes.firstContainer}  >
         <Grid className={classes.secContainer} >
             <Grid item className={classes.EditForm} justify="center">
                 <span className={classes.formTitle}>Bisphosphonate or Monoclonal Antibody Drugs Consent Form</span>
             </Grid>
             <Grid item className={classes.thirdContainer}>
                 <form onSubmit={handleSubmit.bind(this)} id={"consent2"}>
 
                     <Grid item spacing={12} className={classes.content} justify="space-between">
                         <Grid>
                             <p style={{ textAlign: 'center', opacity: '0.7' }}>CONSENT FOR DENTAL, PERIODONTAL, OR ORAL SURGICAL TREATMENT OF PATIENTS WHO HAVE RECEIVED BISPHOSPHONATE OR MONOCLONAL ANTIBODY    ANTIRESORPTIVE DRUG THERAPY FOR THE TREATMENT OF OSTEOPENIA/ OSTEOPOROSIS OR CANCER</p>
                         <Grid  className={classes.flexContainer}>
                         
                             <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                                 <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Patient’s Name:</span></Grid>
                                 <Grid  item style={{ margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                     <Field
                                         name={"patientName"}
                                         type={"text"}
                                         required={true}
                                         component={ImplementationFor["input"]}
                                     />
                                 </Grid>
                             </Grid>
                             <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                             <Grid item style={{ display: 'inline-block', paddingBottom: '10px'}}>
                                 <span>Date:</span></Grid> 
                                 <Grid item  className={classes.LowDateContainer} style={{ textAlign: 'center', margin: '0px 10px',lineHeight: '1rem' }} className={classes.blockToInline}>
                                     <Field
                                         name={"date1"}
                                         type={"date"}
                                         component={ImplementationFor["date"]}
                                         required={true}
                                     />
                                 </Grid>
                             </Grid> 
                         </Grid>
                             <p style={{ fontWeight: 'bold' }}>Please initial each paragraph after reading. If you have any questions, please ask
                            your doctor ordental assistant BEFORE initialing.</p>
                            <p>Due to your history of being treated with oral bisphosphonate or	monoclonal
                            antibody drugs, you should know that there is a small, but real risk of
                            Medication	Related Osteonecrosis of the Jaw (MRONJ) occurring that may be
                            associated with	dental treatment.Bone	anti-resorptive	drug therapies	appear to  adversely affect the vitality and health of Jawbones; thereby, reducing or eliminating the jawbones normal healing capacity.</p>
                            <p>Spontaneous exposure of the jawbone (Osteonecrosis)	may result. This can be
                            long-term destructive process involving	the jawbone(s),	that can be difficult or impossible to resolve orheal. Initials
                            <Grid item xs={12} md={6} style={{ display: 'inline-block', margin: '5px 10px', lineHeight: '1rem' }}>
                                <Field
                                    name={"initials1"}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    required={true}
                                />
                            </Grid>
                             </p>
                             <p>
                                 The overall risk for patients taking an oral bisphosphonate to prevent or treat osteopenia/osteoporosis is	currently estimated to be 0.01% to 0.04%; for Paget’s disease 0.26%	to	1.8%;and for malignancies 0.88%	to	1.15%. The incidence of dental related MRONJ appears to	be dependent upon the dosage and duration of bisphosphonate therapy	and the occurrence of an oral surgery or trauma event. Regardless of treatment duration, when surgery or
                                 trauma occurs, the incidence of MRONJ is as follows: for patients under treatment (or prevention) of osteoporosis the incidence is 0.09% to 0.34%; for Paget’s disease the incidence is 2.1% to 13.5%; and malignancies the incidence	is 6.67%
                                 to 9.1%.
                             </p>
                             <p>
                                 ef. J Oral Maxillofac. Surg. 65:415-423, 2007) The	data and information for monoclonal antibodies appears to be similar. Initials
                                 <Grid item xs={12} md={6} style={{ display: 'inline-block', margin: '5px 10px', lineHeight: '1rem' }}>
                                     <Field
                                         name={"initials2"}
                                         type={"text"}
                                         component={ImplementationFor["input"]}
                                         required={true}
                                     />
                                 </Grid>
                             </p>
                             <p>Your medical/dental history is important. We must know the medications and drugs that you have taken or are currently taking. An accurate medical history, including names of physicians is important. Initials
                                 <Grid item xs={12} md={6} style={{ display: 'inline-block', margin: '5px 10px', lineHeight: '1rem' }}>
                                     <Field
                                         name={"initials3"}
                                         type={"text"}
                                         component={ImplementationFor["input"]}
                                         required={true}
                                     />
                                 </Grid>
                             </p>
                             <p>
                                 The decision to continue or discontinue oral anti-resorptive drug therapy before or during dental treatment,should	be made by you in	consultation with your medical doctor.
                                 Initials
                                 <Grid item xs={12} md={6} style={{ display: 'inline-block', margin: '5px 10px', lineHeight: '1rem' }}>
                                     <Field
                                         name={"initials4"}
                                         type={"text"}
                                         component={ImplementationFor["input"]}
                                         required={true}
                                     />
                                 </Grid>
                             </p>
                             <ul>
                                 <li style={{ padding: '10px 0px' }}>If a complication occurs, short or long-term antibiotic therapy may be used to help control infection. For some patients, such therapy may	cause allergic responses or have undesirable side effects such as gastric discomfort, diarrhea,	colitis, etc.</li>
                                 <li style={{ padding: '10px 0px' }}>Despite all precautions, there may be delayed healing, osteonecrosis, loss of	bone and sof tissues, pathologic fracture of the jaw, oral-cutaneous fistula
                                (opendraining wound), or other significant complications.</li>
                                 <li style={{ padding: '10px 0px' }}>If osteonecrosis should occur; treatment may be prolonged and difficult, involving ongoing, intensive therapy including hospitalization, long-term     antibiotics, and debridement to remove non-vital bone. econstructive surgery may be required,including bone	grafting, metal plates and screws,and/or skin flaps and grafts.</li>
                                 <li style={{ padding: '10px 0px' }}>Even if there are no immediate complications from the proposed dental treatment, the area is always subject to spontaneous breakdown and infection due to the condition of	the bone.Minimal trauma from a toothbrush, chewing hard food, or denture sores may trigger a complication.</li>
                                 <li style={{ padding: '10px 0px' }}>Long term	postoperative monitoring may be	required and cooperation in keeping scheduled appointments is important. Regular and frequent dental checkups with your dentist are important to monitor and attempt to prevent
                                breakdown in your oral health.</li>
                                 <li style={{ padding: '10px 0px' }}>I have	read the above paragraphs and understand the possible risks	of
                                undergoing my proposed	planned treatment. I understand and agree to the  following	treatment plan:
                                     <Grid item xs={12} className={classes.textArea}>
                                         <Field
                                             name={"exception"}
                                             type={"text"}
                                             component={ImplementationFor["textArea"]}
                                             variant="standard"
                                             required={true}
                                         />
                                     </Grid>
                                 </li>
                                 <br />
                                 <li style={{ padding: '10px 0px' }}>I understand the importance of my health history and affirm that I have given any and all information that may	impact my care. I understand that failure to give true health information may adversely affect my care and lead to unwanted complications.</li>
                                 <li style={{ padding: '10px 0px' }}>I realize that, despite all precautions that may be taken to avoid complications;
                                there can be no guarantee as to the result of the proposed treatment.</li>
                             </ul>
                             <h3>CONSENT</h3>
                             <p>I certify that I speak, read and write English and have read and fully understand this Consent for surgery, have had my questions answered and that all blanks were completed prior	to placement of	my initials and signature.</p>
                             <Grid style={{ textAlign: 'center' }}>Patient’s (or	Legal	Guardian’s) Signature :
                                 <Grid item xs={12} md={6} style={{ display: 'inline-block', margin: '0px 10px' }}>
                                     <Field
                                         name={"signature1"}
                                         type={"text"}
                                         component={ImplementationFor["signature"]}
                                         id={"signature1"}
                                         onChange={(e) => onChange("signature1", e)}
                                     />
                                 </Grid>
                                 <Grid style={{ display: 'inline-block', paddingBottom: '10px' }}>
                                     <span>Date:</span>
                                     <Grid item xs={6} md={6} className={classes.DateContainer} style={{ textAlign: 'center',  margin: '0px 10px' }}>
                                         <Field
                                             name={"date2"}
                                             type={"text"}
                                             component={ImplementationFor["date"]}
                                             id={"date2"}
                                         />
                                     </Grid></Grid>
                             </Grid>
                             <Grid style={{ textAlign: 'center' }}>
                                 Doctor’s Signature:
                                 <Grid item xs={12} sm={5} style={{ display: 'inline-block', margin: '0px 10px' }}>
                                     <Field
                                         name={"signature2"}
                                         type={"text"}
                                         component={ImplementationFor["signature"]}
                                         id={"signature2"}
                                         onChange={(e) => onChange("signature2", e)}
                                         disabled={true}
                                     />
                                 </Grid>
                                 <Grid style={{ display: 'inline-block', paddingBottom: '10px' }}>
                                     <span>Date:</span>
                                     <Grid item xs={6} md={6} className={classes.DateContainer} style={{ textAlign: 'center',  margin: '0px 10px' }}>
                                         <Field
                                             name={"date3"}
                                             type={"text"}
                                             component={ImplementationFor["date"]}
                                             id={"date3"}
                                             disabled={true}
                                         />
                                     </Grid></Grid>
                             </Grid>
                             <Grid style={{ textAlign: 'center' }}>
                                 Witness’ Signature:
                                 <Grid item xs={12} sm={5} style={{ display: 'inline-block', margin: '0px 10px' }}>
                                     <Field
                                         name={"signature3"}
                                         type={"text"}
                                         component={ImplementationFor["signature"]}
                                         id={"signature3"}
                                         onChange={(e) => onChange("signature3", e)}
                                         disabled={true}
                                     />
                                 </Grid>
                                 <Grid style={{ display: 'inline-block', paddingBottom: '10px' }}>
                                     <span>Date:</span>
                                     <Grid item xs={6} md={6} className={classes.DateContainer} style={{ textAlign: 'center',  margin: '0px 10px' }}>
                                         <Field
                                             name={"date4"}
                                             type={"text"}
                                             component={ImplementationFor["date"]}
                                             id={"date4"}
                                             disabled={true}
                                         />
                                     </Grid></Grid>
                             </Grid>
                             <Grid style={{ textAlign: "center", padding: '20px 0px' }}>
                                 <Button form="consent2" type="submit" className={classes.button}>Submit</Button>
                             </Grid>
                         </Grid>
                     </Grid>
 
                 </form>
             </Grid >
         </Grid >
     </Grid>
 
 }
 
 Consent2.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent2',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true,
 })(Consent2);