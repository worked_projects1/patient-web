/**
 * 
 *  Consent17
 * 
 */

 import React, { useState, useEffect,useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm, formValueSelector } from 'redux-form';
 import Styles from './styles';
import {ImplementationFor} from '../Components/CreateRecordForm/utils'
import { Grid,Button,} from '@mui/material';

/**
  * 
  * @param {object} props 
  * @returns 
  */
 function Consent17(props) {
    const { handleSubmit, destroy,change,dispatch,initialValues } = props;
     const classes = Styles();
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState("");

     dispatch(change('staff1',initialValues.providerFirst+ ' ' + initialValues.providerLast))

     useEffect(() => {
         return () => destroy();
     }, []);

    const onChange = (val,eventValue) => {
    val=="signature1"&&setSignature1(eventValue)
    val=="signature2"&&setSignature2(eventValue)
    val=="signature3"&&setSignature3(eventValue)

    }

        return <Grid className={classes.firstContainer}  >
                    <Grid className={classes.secContainer} style={{height: '94vh'}}>
                        <Grid item className={classes.EditForm} justify="center">
                        <span className={classes.formTitle}>Necessary Follow-up Care Consent Form</span>
                        </Grid>
                    <Grid item className={classes.thirdContainer}>
                    <form onSubmit={handleSubmit.bind(this)} id={"consent17"}>

                    <Grid item spacing={12} className={classes.content} justify="space-between">
                    <Grid>
                   
                    <p>I understand that 
                        <Grid item xs={6} style={{ display: 'inline-block', margin: '5px 10px', lineHeight: '1rem' }}>
                            <Field
                                name={"staff1"}
                                type={"text"}
                                component={ImplementationFor["input"]}
                                disabled={true}
                            />
                        </Grid>
                        is evaluating me only for possible dental implant treatment, and thus the evaluation is limited to the edentulous spaces I have in my
                        mouth. I understand that the oral exam/evaluation and the CT scan that might be
                        required does not cover evaluation of the gums, the status of an existing restoration,
                        or temporomandibular joint (TMJ) issues, nor does it include an oral cancer screening. 
                    </p>
                    <p>
                    I understand that it is important for me to continue to see my general dentist for
                    routine dental care. This is only an evaluation of the edentulous spaces in both arches for potential implant treatment. 
                    </p>
                    <Grid  className={classes.flexContainer}>
                    <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                            <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Patient Name:</span></Grid>
                            <Grid  item style={{ margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                        <Field
                            name={"patientName"}
                            type={"text"}
                            component={ImplementationFor["input"]}
                            required={true}
                            />
                        </Grid>
                    </Grid>
                    </Grid>
                    <Grid >Patient Signature:
                    <Grid item xs={12} sm={5} style={{display:'inline-block',margin: '0px 10px'}}>
                    <Field
                        name={"signature1"}
                        type={"text"}
                        component={ImplementationFor["signature"]}
                        onChange={(e) => onChange("signature1",e)}
                        />
                    </Grid>
                    </Grid>
                    <Grid style={{marginTop: '20px'}}>Date:
                    <Grid  item xs={12} sm={5} className={classes.LowDateContainer} style={{textAlign: 'center',display:'inline-block',marginLeft: '10px'}}>
                        <Field
                        name={"date1"}
                        type={"text"}
                        component={ImplementationFor["date"]}
                        id={"date1"}
                        />
                    </Grid> 
                    </Grid> 

                <Grid style={{textAlign:"center", padding: '20px 0px'}}>
                    <Button  form="consent17" type="submit" className={classes.button}>Submit</Button>
                </Grid>
            </Grid>
        </Grid>
 
        </form>
    </Grid >
    </Grid >
    </Grid>
     
 }

Consent17.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent17',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true,
 })(Consent17);