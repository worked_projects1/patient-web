/**
 * 
 *  Consent5
 * 
 */

 import React, { useState, useEffect, useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm } from 'redux-form';
 import { Grid, Button, } from '@mui/material';
 import Styles from './styles';
 import { ImplementationFor } from '../Components/CreateRecordForm/utils'
 
 /**
   * 
   * @param {object} props 
   * @returns 
   */
 
 function Consent5(props) {
     const { handleSubmit, destroy, } = props;
     const classes = Styles();
     const [signature1, setSignature1] = useState("");
 
     useEffect(() => {
         return () => destroy();
     }, []);
 
     const onChange = (val, eventValue) => {
         val == "signature1" && setSignature1(eventValue)
     }
 
 
     return <Grid className={classes.firstContainer}  >
         <Grid className={classes.secContainer} >
             <Grid item className={classes.EditForm} justify="center">
                 <span className={classes.formTitle}>Email Consent Form</span>
             </Grid>
             <Grid item className={classes.thirdContainer}>
                 <form onSubmit={handleSubmit.bind(this)} id={"consent5"}>
 
                     <Grid item spacing={12} className={classes.content} justify="space-between">
                         <Grid>
                             <ul>
                                 <li style={{ padding: '10px 0px' }}>
                                     <b>RISK OF USING EMIAL</b> <br />
                                     Provider offers patients the opportunity to communicate by e-mail. Transmitting patient
                                     information by e-mail, however, has a number of risks that patients should consider before using e-mail. These include, but are not limited to, the following risks:
                                     <ol className={classes.alphaSmall}>
                                         <li style={{padding: '10px 0px'}}>
                                             E-mail can be circulated, forwarded, and stored in numerous paper and electronic files.
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                             E-mail can be immediately broadcast worldwide and be received by many intended and
                                             unintended recipients.
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                             E-mail senders can easily misaddress an e-mail.
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                             E-mail is easier to falsify than handwritten or signed documents.
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                             Backup copies of e-mail may exist even after the sender or the recipient has deleted his
                                             or her copy.
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                             Employers and online services have a right to archive and inspect e-mails transmitted
                                             through their systems.
                                         </li>
                                         <li>
                                             E-mail can be intercepted, altered, forwarded, or used without authorization or
                                             detection.
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                             E-mail can be used to introduce viruses into computer systems.
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                         E-mail can be used as evidence in court. 
                                         </li>
                                     </ol>
                                 </li>
                                 <li style={{ padding: '10px 0px' }}>
                                     <b>CONDITIONS FOR THE USE OF E-MAIL</b>  <br />
                                     Provider will use reasonable means to protect the security and confidentiality of e-mail
                                     information sent and received. However, because of the risks outlined above, Provider
                                     cannot guarantee the security and confidentiality of e-mail communication and will not be liable for improper disclosure of confidential information that is not caused by Provider’s intentional misconduct. Thus, the patient must consent to the use of e-mail for patient information. Consent to the use of e-mail includes agreement with the following conditions:
                                     <ol className={classes.alphaSmall}>
                                         <li style={{padding: '10px 0px'}}>
                                             All e-mails to or from the patient concerning diagnosis or treatment will be printed out and made part of the patient’s medical record. Because they are a part of the medical record, other individuals authorized to access the medical records, such as staff and billing personnel, will have access to those e-mails.
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                             Provider may forward e-mails internally to Provider’s staff and agents as necessary for diagnosis, treatment, reimbursement, and other handling. Provider will not, however, forward e-mails to independent third parties without the patient’s prior written consent, except as authorized or required by law.
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                             Although Provider will endeavor to read and respond promptly to an e-mail from the patient, Provider cannot guarantee that any particular e-mail will be read and responded to within any particular period of time. Thus, the patient shall not use e-mail for medical emergencies or other time-sensitive matters.
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                             If the patient’s e-mail requires or invites a response from Provider, and the patient has not received a response within a reasonable time period, it is the patient’s responsibility to follow up to determine whether the intended recipient received the email and when the recipient will respond.
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                             The patient should not use e-mail for communication regarding sensitive medical
                                             information, such as information regarding sexually transmitted diseases, AIDS/HIV,mental health, developmental disability, or substance abuse.
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                             The patient is responsible for informing the Provider of any types of information the patient does not want to be sent by e-mail in addition to those set out in 2(e) above.
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                             The patient is responsible for protecting his/her password or other means of access to email. Provider is not liable for breaches of confidentiality caused by the patient or any third party.
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                             Provider shall not engage in e-mail communication that is unlawful, such as unlawfully practicing medicine across state lines.
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                             It is the patient’s responsibility to follow up and/or schedule an appointment if warranted.
                                         </li>
                                     </ol>
                                 </li>
                                 <li style={{ padding: '10px 0px' }}>
                                     <b>INSTRUCTIONS</b> <br />
                                     To communicate by e-mail, the patient shall:
                                     <ol className={classes.alphaSmall}>
                                         <li style={{padding: '10px 0px'}}>
                                             Limit or avoid use of his/her employer’s computer.
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                             Inform Provider of changes in his/her e-mail address and put the patient’s name in the body of the e-mail.
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                             Include the category of the communication in the e-mail’s subject line for routing purposes (e.g., billing questions).
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                             Review the e-mail to make sure it is clear and that all relevant information is provided before sending to Provider.
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                         Inform Provider that the patient received an e-mail from Provider. 
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                             Take precautions to preserve the confidentiality of e-mails, such as using screen savers and safeguarding his/her computer password.
                                         </li>
                                         <li style={{padding: '10px 0px'}}>
                                             Withdraw consent only by e-mail or written communication to provider.
                                         </li>
                                     </ol>
                                 </li>
                                 <li style={{ padding: '10px 0px' }}>
                                     <b>PATIENT ACKNOWLEDGMENT AND AGREEMENT</b> <br />
                                     I acknowledge that I have read and fully understand this consent form. I understand the
                                     risks associated with the communication of e-mail between Provider and me and consent
                                     to the conditions outlined herein. In addition, I agree to the instructions outlined herein, as well as any other instructions that Provider may impose to communicate with patients by email. Any questions I may have had were answered.
                                 </li>
                             </ul>
                             <Grid className={classes.flexContainer}>
                             <Grid style={{paddingBottom: '10px'}} className={classes.flexItem}>
                                 <Grid item  style={{ display: 'inline-block'}}><span>Patient's Name:</span></Grid>
                                     <Grid style={{ display: 'inline-block', margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                         <Field
                                                 name={"patientName"}
                                                 type={"text"}
                                                 required={true}
                                                 component={ImplementationFor["input"]}
                                             />
                                 </Grid>
                             </Grid>
                             </Grid>
                             <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                             <Grid item  md={6} className={classes.signatureContainer}>
                                 <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                     
                                     <Field
                                         name={"signature1"}
                                         type={"text"}
                                         autoFocus={true}
                                         component={ImplementationFor["signature"]}
                                         id={"signature1"}
                                         onChange={(e) => onChange("signature1", e)}
                                     /></Grid>
                                     <Grid>Patient Signature</Grid>
                                 </Grid>
                                 <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>
 
                                     <Grid item md={12} className={classes.DateContainer1}>
                                         <Field
                                             name={"date1"}
                                             type={"text"}
                                             component={ImplementationFor["date"]}
                                             required={true}
                                         />
                                         
                                     </Grid>
                                     <Grid>Date</Grid>
                                     </Grid>
                             </Grid>
                             <Grid style={{ textAlign: "center", padding: '20px 0px' }}>
                                 <Button className={classes.button} form={"consent5"} type="submit">Submit</Button>
                             </Grid>
                         </Grid>
                     </Grid>
 
                 </form>
             </Grid >
         </Grid >
     </Grid>
 
 }
 
 Consent5.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent5',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true
 })(Consent5);