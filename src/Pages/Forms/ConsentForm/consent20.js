
/**
 * 
 *  Consent20
 * 
 */
 import * as React from 'react';
 import { useState, useEffect, useRef } from 'react';
 import PropTypes from 'prop-types';
 import {  Grid, Button, } from '@mui/material';
 import Styles from './styles';
 import { ImplementationFor } from '../Components/CreateRecordForm/utils'
 import { Field, reduxForm, formValueSelector } from 'redux-form'

 /**
  * 
  * @param {object} props 
  * @returns 
  */   
  
 function Consent20(props) {
    const { handleSubmit, destroy,change,dispatch,initialValues } = props;

     const classes = Styles();
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState("");
 
     useEffect(() => {
         return () => destroy();
     }, []);
 
     dispatch(change('staff1',initialValues.providerFirst+ ' ' + initialValues.providerLast))
     dispatch(change('staff2',initialValues.providerFirst+ ' ' + initialValues.providerLast))
 
     const handleSignature = (val, eventValue) => {
         val == "signature1" && setSignature1(eventValue)
         val == "signature2" && setSignature2(eventValue)
         val == "signature3" && setSignature3(eventValue)
     }

     return <Grid className={classes.firstContainer}  >
         <Grid className={classes.secContainer} >
             <Grid item className={classes.EditForm} justify="center">
                 <span className={classes.formTitle}>Prosthodontic Treatment Information And Consent Form</span>
             </Grid>
             <Grid item className={classes.thirdContainer}>
                 <form onSubmit={handleSubmit} id='consent20'>
 
                     <Grid item spacing={12} className={classes.content} justify="space-between">
                         <Grid>
                                 
                            <p>This booklet has been prepared by <Grid item xs={12} style={{display:'inline-block',margin: '5px 10px', lineHeight:'1rem'}}>
                                <Field
                                    name={"staff1"}
                                    required={true}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    disabled={true}
                                    />
                            </Grid>
                            to familiarize you with facts about PROSTHETIC TREATMENT. Please read it and write any questions in the margins so you can discuss them with 
                            <Grid item xs={12} style={{display:'inline-block',margin: '5px 10px', lineHeight:'1rem'}}>
                                <Field
                                    name={"staff2"}
                                    required={true}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    disabled={true}
                                    />
                            </Grid>
                            </p>

                            <p>Please bring this booklet with you to your appointments. Before any treatment is started, you will be asked to sign a statement that you have read and understand this information and have had the opportunity to have all of your questions answered. </p>

                            <h4>INTRODUCTION:  </h4>
                            <p>
                            The following information is routinely discussed with patients considering prosthodontic treatment in our office.  
                            </p>

                            <p>
                            While recognizing the benefits of a pleasing smile and well-functioning teeth, you should also be aware that prosthodontic treatment, like any treatment of the body, has some inherent risks and limitations. These risks and limitations seldom contraindicate treatment; however, they should be considered in making your decision to have prosthodontic treatment.
                            </p>

                            <p>
                            Treatment of human biologic conditions will never reach a state of perfection despite technologic advancements; problems can occur. This pamphlet is intended to inform you of some of the potential problems of prosthodontic treatment. Many of the problems
                            mentioned occur only occasionally and rarely. There may be other inherent risks not discussed in this brochure. Prosthodontic treatment usually proceeds as planned;
                            however, like all other healing arts, results cannot be guaranteed. 
                            </p>

                            <p className={classes.rightInput}>
                            I read and understand the above section
                            <Grid item xs={12} style={{display:'inline-block',margin: '5px 10px', lineHeight:'1rem'}}>
                                <Field
                                    name={"section1"}
                                    required={true}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    />
                            </Grid>
                            </p>

                            <h4>INITIAL DIAGNOSTIC PROCEDURES:</h4>
                            <p>
                            Diagnostic procedures help formulate treatment recommendations. When appropriate, these procedures may include:
                            </p>
                            
                            <ol>
                                <li>
                                Medical and dental history
                                </li>
                                <li>
                                Physical examination of the mouth and associated structures
                                </li>
                                <li>
                                X-rays
                                </li>
                                <li>
                                Models of the teeth and/or associated structures
                                </li>
                                <li>
                                Photographs
                                </li>
                                <li>
                                Conference with previously or currently treating health professionals 
                                </li>
                            </ol>

                            <p>Additional diagnostic procedures may be indicated. </p>

                            <p className={classes.rightInput}>
                            I read and understand the above section
                            <Grid item xs={12} style={{display:'inline-block',margin: '5px 10px', lineHeight:'1rem'}}>
                                <Field
                                    name={"section2"}
                                    required={true}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    />
                            </Grid>
                            </p>

                            <h4>TREATMENT RECOMMENDATIONS:</h4>
                            <p>You should be informed of the most appropriate and reasonable alterative treatment plans. Also, you should be informed of the dental prognosis if no prosthodontic treatment is initiated. If you have any questions I encourage you to discuss them with me. </p>

                            <p className={classes.rightInput}>
                            I read and understand the above section
                            <Grid item xs={12} style={{display:'inline-block',margin: '5px 10px', lineHeight:'1rem'}}>
                                <Field
                                    name={"section3"}
                                    required={true}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    />
                            </Grid>
                            </p>

                            <h4>REFERRALS TO OTHER SPECIALISTS:</h4>
                            <p>Sometimes the need for treatment by another dental specialist or health practitioner becomes evident during your prosthodontic treatment. If this need occurs, the appropriate referral and the reasons for the referral will be explained to you. </p>
                            <p className={classes.rightInput}>
                            I read and understand the above section
                            <Grid item xs={12} style={{display:'inline-block',margin: '5px 10px', lineHeight:'1rem'}}>
                                <Field
                                    name={"section4"}
                                    required={true}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    />
                            </Grid>
                            </p>

                            <h4>REMOVABLE PROSTHODONTICS </h4>
                            <p>Removable prosthodontics refers to the replacement of missing teeth with a restoration that can be removed from the mouth. This can be accomplished with several types of dentures. These include: complete removable dentures supported by gingival (gum) tissues, removable partial dentures supported by gingival (gum) tissue and remaining teeth and implants, and overdentures supported by the roots of natural teeth or implants. </p>
                            <p className={classes.rightInput}>
                            I read and understand the above section
                            <Grid item xs={12} style={{display:'inline-block',margin: '5px 10px', lineHeight:'1rem'}}>
                                <Field
                                    name={"section5"}
                                    required={true}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    />
                            </Grid>
                            </p>

                            <h5>Potential Problems with Removable Prosthodontics:</h5>
                            <p>
                            Dentures are removable replacements of natural teeth. You should be aware of the
                            following potential problems
                            </p>

                            <ol>
                                <li style={{padding: '10px 0px'}}>
                                <b>Mastication (chewing), stability, and retention:</b> Removable dentures, under the best of circumstances, do not have the same chewing efficiency as natural teeth. The ability to chew food is further affected by the stability and retention of the dentures. <br />
                                The stability and retention of dentures depend on many factors, including the attachment of the dentures to natural teeth or implants, the amount and type of bone, gingival (gum) disease, saliva, your dexterity, and the fit of the dentures to your gingival (gum) tissue or implant support system. 
                                <Grid className={classes.rightInput}>
                                I read and understand the above section
                                <Grid item xs={12} style={{display:'inline-block',margin: '5px 10px', lineHeight:'1rem'}}>
                                    <Field
                                        name={"section6"}
                                        required={true}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        />
                                </Grid>
                                </Grid>
                                </li>
                                <li style={{padding: '10px 0px'}}>
                                <b>Appearance:</b> Properly fitting dentures support the lips and facial contours. Dentures can provide additional facial support if desired or needed. Excessive lip and facial support
                                from dentures can result in a “swollen” appearance. 
                                <Grid className={classes.rightInput}>
                                I read and understand the above section
                                <Grid item xs={12} style={{display:'inline-block',margin: '5px 10px', lineHeight:'1rem'}}>
                                    <Field
                                        name={"section7"}
                                        required={true}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        />
                                </Grid>
                                </Grid>
                                </li>
                                <li style={{padding: '10px 0px'}}>
                                <b>Speech:</b> Removable dentures cover areas of the jaws and palate. The presence of acrylic resin, metal, or porcelain in these areas requires adaptation of the tongue and lips for
                                proper speech. 
                                <Grid className={classes.rightInput}>
                                I read and understand the above section
                                <Grid item xs={12} style={{display:'inline-block',margin: '5px 10px', lineHeight:'1rem'}}>
                                    <Field
                                        name={"section8"}
                                        required={true}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        />
                                </Grid>
                                </Grid>
                                </li>
                                <li style={{padding: '10px 0px'}}>
                                <b>Stain and cleaning:</b> The amount of stain on dentures depends on oral hygiene, use of tobacco, and consumption of coffee and tea. Commercially available denture-cleaning solutions are usually sufficient to maintain clean dentures. Bleach should not be used to
                                clean removable dentures. Bleach can corrode the metal portions of the dentures and/or fade the pink acrylic resin. Abrasive kitchen or bathroom cleaners should be avoided.
                                <Grid className={classes.rightInput}>
                                I read and understand the above section
                                <Grid item xs={12} style={{display:'inline-block',margin: '5px 10px', lineHeight:'1rem'}}>
                                    <Field
                                        name={"section9"}
                                        required={true}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        />
                                </Grid>
                                </Grid>
                                </li>
                                <li style={{padding: '10px 0px'}}>
                                <b>Chipping and wear:</b> Porcelain denture teeth have the slowest rate of wear and the highest stain resistance. However, porcelain has a tendency to chip. Small chips can be polished. Larger chips usually require replacement of the porcelain tooth on the denture. Acrylic resin denture teeth are more resistant to chipping, but they have a tendency to wear down faster than porcelain. If the wear affects the appearance or occlusion (bite), the acrylic resin teeth can be replaced on the denture. <br />
                                Chips and cracks of the pink acrylic resin portion can usually be repaired without remaking the denture. 
                                <Grid className={classes.rightInput}>
                                I read and understand the above section
                                <Grid item xs={12} style={{display:'inline-block',margin: '5px 10px', lineHeight:'1rem'}}>
                                    <Field
                                        name={"section10"}
                                        required={true}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        />
                                </Grid>
                                </Grid>
                                </li>
                                <li style={{padding: '10px 0px'}}>
                                <b>Relines:</b> The shape and size of the gingival (gum) tissue and bone changes with time. A reline procedure readapts the pink acrylic resin portion of the denture to the new shape and size of your gingival (gum) tissue. A reline is usually necessary every 3 to 5 years.
                                <Grid className={classes.rightInput}>
                                I read and understand the above section
                                <Grid item xs={12} style={{display:'inline-block',margin: '5px 10px', lineHeight:'1rem'}}>
                                    <Field
                                        name={"section11"}
                                        required={true}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        />
                                </Grid>
                                </Grid> 
                                </li>
                                <li style={{padding: '10px 0px'}}>
                                <b>Food retention:</b> Removable dentures always have some space between the pink acrylic resin portion and the gingival (gum) tissue. There is always some movement of the removable denture during chewing. These factors create a situation where food can accumulate between the denture and the gingival (gum) tissue. Therefore, it is essential to remove the denture for cleaning on a daily basis. Removable partial dentures may have additional food retention problems.
                                <Grid className={classes.rightInput}>
                                I read and understand the above section
                                <Grid item xs={12} style={{display:'inline-block',margin: '5px 10px', lineHeight:'1rem'}}>
                                    <Field
                                        name={"section12"}
                                        required={true}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        />
                                </Grid>
                                </Grid> 
                                </li>
                            </ol>

                            <h4>INFORMED CONSENT AND TREATMENT CONFIRMATION</h4>
                            <p>
                            I certify that this "Prosthodontic Treatment" pamphlet outlining general treatment considerations and potential problems and hazards of prosthodontic treatment was presented to me and that I have read and understand its contents. I understand that potential hazards and problems may include, but are not limited to, those described in the brochure. I have had the opportunity to discuss my proposed treatment and the potential
                            problems with the doctor to clarify any areas I did not understand. I authorize the doctor to provide prosthodontic treatment.
                            </p>
                            <Grid  className={classes.flexContainer}>
                            <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                                <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Patient's Name:</span></Grid>
                                <Grid  item style={{ margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                    name={"patientName1"}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    required={true}
                                    />
                                </Grid>
                            </Grid>
                            </Grid>
                            <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                            <Grid item  md={6} className={classes.signatureContainer}>
                                <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                    
                                    <Field
                                        name={"signature1"}
                                        type={"text"}
                                        autoFocus={true}
                                        component={ImplementationFor["signature"]}
                                        id={"signature1"}
                                        onChange={(e) => handleSignature("signature1", e)}
                                    /></Grid>
                                    <Grid>Patient Signature</Grid>
                                </Grid>
                                <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                                    <Grid item md={12} className={classes.DateContainer1}>
                                        <Field
                                            name={"date1"}
                                            type={"text"}
                                            component={ImplementationFor["date"]}
                                            required={true}
                                        />
                                        
                                    </Grid>
                                    <Grid>Date</Grid>
                                    </Grid>
                            </Grid>

                            <h4>IMPLANTS </h4>
                            <p>I understand that incisions will be made inside my mouth for the purpose of placing one or more titanium or coated titanium implants in my jaw(s). The implants should serve as anchor(s) for a missing tooth or teeth to stabilize a crown or dental prosthesis. </p>
                            <Grid className={classes.rightInput}>
                                I read and understand the above section
                                <Grid item xs={12} style={{display:'inline-block',margin: '5px 10px', lineHeight:'1rem'}}>
                                    <Field
                                        name={"section13"}
                                        required={true}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        />
                                </Grid>
                            </Grid> 

                            <p>I have been informed that the implant must remain covered, under the gingival (gum) tissue, for at least 3 months before it can be uncovered. A second surgical procedure is required to uncover the top of the implant. The crown or dental prosthesis and stabilizing bars will be attached to the implant(s) after it is uncovered. </p>
                            <Grid className={classes.rightInput}>
                                I read and understand the above section
                                <Grid item xs={12} style={{display:'inline-block',margin: '5px 10px', lineHeight:'1rem'}}>
                                    <Field
                                        name={"section14"}
                                        required={true}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        />
                                </Grid>
                            </Grid> 

                            <p>I understand that the implant should last for many years but that no guarantee that it will last for any specific period of time can be or has been given. It has been explained to me that once the implant is inserted, the entire dental treatment plan, including my personal
                            oral hygiene and prophylaxis, must be followed and completed on schedule, or the
                            implant may fail.</p>
                            <Grid className={classes.rightInput}>
                                I read and understand the above section
                                <Grid item xs={12} style={{display:'inline-block',margin: '5px 10px', lineHeight:'1rem'}}>
                                    <Field
                                        name={"section15"}
                                        required={true}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        />
                                </Grid>
                            </Grid>

                            <p>The alternatives to implant treatment, including no treatment, construction of a new standard denture or partial denture, augmentation of the maxilla (upper jaw) or mandible (lower jaw), and soft tissue or bone grafting have been explained to me. The advantages and disadvantages of each of these procedures has also been discussed and explained to me. </p>
                            <Grid className={classes.rightInput}>
                                I read and understand the above section
                                <Grid item xs={12} style={{display:'inline-block',margin: '5px 10px', lineHeight:'1rem'}}>
                                    <Field
                                        name={"section16"}
                                        required={true}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        />
                                </Grid>
                            </Grid>

                            <h4>INFORMED CONSENT AND TREATMENT CONFIRMATION </h4>
                            <p>I certify that this "Prosthodontic Treatment" pamphlet outlining general treatment considerations and potential problems and hazards of prosthodontic treatment was presented to me and that I have read and understand its contents. I understand that potential hazards and problems may include, but are not limited to, those described in the brochure. I have had the opportunity to discuss my proposed treatment and the potential
                            problems with the doctor to clarify any areas I did not understand. I authorize the doctor to provide prosthodontic treatment. </p>

                            
                            <Grid  className={classes.flexContainer}>
                            <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                                <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Patient's Name:</span></Grid>
                                <Grid  item style={{ margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                    name={"patientName2"}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    required={true}
                                    />
                                </Grid>
                            </Grid>
                            </Grid>
                            <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                            <Grid item  md={6} className={classes.signatureContainer}>
                                <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                    
                                    <Field
                                        name={"signature2"}
                                        type={"text"}
                                        autoFocus={true}
                                        component={ImplementationFor["signature"]}
                                        id={"signature2"}
                                        onChange={(e) => handleSignature("signature2", e)}
                                    /></Grid>
                                    <Grid>Patient Signature</Grid>
                                </Grid>
                                <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                                    <Grid item md={12} className={classes.DateContainer1}>
                                        <Field
                                            name={"date2"}
                                            type={"text"}
                                            component={ImplementationFor["date"]}
                                            required={true}
                                        />
                                        
                                    </Grid>
                                    <Grid>Date</Grid>
                                    </Grid>
                            </Grid>

                            <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                            <Grid item  md={6} className={classes.signatureContainer}>
                                <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                    
                                    <Field
                                        name={"signature3"}
                                        type={"text"}
                                        autoFocus={true}
                                        component={ImplementationFor["signature"]}
                                        id={"signature3"}
                                        onChange={(e) => handleSignature("signature3", e)}
                                        disabled={true}
                                    /></Grid>
                                    <Grid>Doctor Signature</Grid>
                                </Grid>
                                <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                                    <Grid item md={12} className={classes.DateContainer1}>
                                        <Field
                                            name={"date3"}
                                            type={"text"}
                                            component={ImplementationFor["date"]}
                                            required={true}
                                            disabled={true}
                                        />
                                        
                                    </Grid>
                                    <Grid>Date</Grid>
                                    </Grid>
                            </Grid>

                             <Grid style={{ textAlign: "center", padding: '20px 0px' }}>
                                <Button className={classes.button}  type="submit" form="consent20">Submit</Button>
                             </Grid>
                         </Grid>
                     </Grid>
                 </form>
             </Grid >
         </Grid >
     </Grid>
 
 }
  
 Consent20.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent20',
     enableReinitialize: true,
     touchOnChange: true,
     touchOnBlur: true,
     destroyOnUnmount: true,
     forceUnregisterOnUnmount: true
 })(Consent20);
 