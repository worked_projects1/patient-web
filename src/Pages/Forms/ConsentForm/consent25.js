/**
 * 
 *  Consent25
 * 
 */

 import React, { useState, useEffect, useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm } from 'redux-form';
 import { Grid, Button,} from '@mui/material';
 import Styles from './styles';
 import { ImplementationFor } from '../Components/CreateRecordForm/utils'

 /**
   * 
   * @param {object} props 
   * @returns 
   */
 
 function Consent25(props) {
    const { handleSubmit, destroy,change,dispatch,initialValues } = props;
     const classes = Styles();
     const [signature1, setSignature1] = useState("");

     useEffect(() => {
        return () => destroy();
    }, []);

     dispatch(change('staff',initialValues.providerFirst+ ' ' + initialValues.providerLast))
 
     const onChange = (val, eventValue) => {
         val == "signature1" && setSignature1(eventValue)
     }
     return <Grid className={classes.firstContainer}  >
         <Grid className={classes.secContainer} >
             <Grid item className={classes.EditForm} justify="center">
                 <span className={classes.formTitle}>Endodontic Therapy Consent Form</span>
             </Grid>
             <Grid item className={classes.thirdContainer}>
                 <form onSubmit={handleSubmit.bind(this)} id={"consent25"}>
 
                     <Grid item spacing={12} className={classes.content} justify="space-between">
                         <Grid>
                             <p>Please review the following consent form. You will be required to sign this form prior to
                                 the initiation of treatment. </p>
                             <p>I understand the root canal therapy is a procedure that retains a tooth, which may
                                 otherwise require extraction. Although root canal therapy has a very high degree of
                                 success, results cannot be guaranteed. Occasionally, a tooth, which has had root canal
                                 therapy, may require re-treatment, surgery, or even extraction. Following treatment, the
                                 tooth may be brittle and subject to fracture. A restoration (filling), crown and/or post and
                                 core will be necessary to restore the tooth, and your general dentist will perform these
                                 procedures. During endodontic treatment, there is the possibility of instrument separation
                                 within the root canals, perforations (extra openings), damage to bridges, existing fillings,
                                 crowns or porcelain veneers, missed canals, loss of tooth structure in gaining access to
                                 canals, and fractured teeth. Also, there are times when a minor surgical procedure may be
                                 indicated or when the tooth may not be amenable to endodontic treatment at all. Other
                                 treatment choices include no treatment, a waiting period for more definitive symptoms to
                                 develop, or tooth extraction. Risks involved in those choices might include, but are not
                                 limited to, pain, infection, swelling, loss of teeth, and infection to other areas.
                                 Occasionally, medication will be prescribed. Medications prescribed for discomfort and/or
                                 sedation may cause drowsiness, which can be increased by the use of alcohol or other
                                 drugs. We advise that you do not operate a motor vehicle or any hazardous device while
                                 taking such medications. In addition, certain medications may cause allergic reactions,
                                 such as hives or intestinal discomfort. If any of these problems occur, call the office
                                 immediately. It is the patient's responsibility to report any changes in his/her medical
                                 history</p>
                             <p>All of my questions have been answered by the dentist and I fully understand the above
                                 statements in this consent form.</p>
                             <p>
                                 Furthermore, I give 
                                 <Grid item xs={12} style={{ display: 'inline-block', margin: '5px 10px', lineHeight: '1rem' }}>
                                     <Field
                                         name={"staff"}
                                         type={"text"}
                                         component={ImplementationFor["input"]}
                                         required={true}
                                         disabled={true}
                                     />
                                 </Grid>
                                 my permission to voice record,
                                 tape digitally, videotape and/or take 35mm and/or digital photos of my procedure for
                                 purposes of completing my medical record and/or for patient education.
                                 Note: All medical records will be kept strictly confidential. (If patient is under the age of
                                 18, the signature of a parent or guardian is required.) </p>
                             <Grid style={{paddingBottom:'10px'}}>Date:
                             <Grid  item xs={12} sm={5} className={classes.LowDateContainer} style={{textAlign: 'center',display:'inline-block',marginLeft: '10px'}}>
                                 <Field
                                 name={"date1"}
                                 type={"text"}
                                 component={ImplementationFor["date"]}
                                 id={"date1"}
                                 />
                             </Grid> 
                             </Grid> 
                             <Grid style={{paddingBottom:'10px'}} className={classes.radioGroup}>
                        <Grid item xs={12} md={6} style={{display:'inline-block',alignSelf: 'flex-end'}}>Patient, Parent of a Minor Parent’s Signature:</Grid>
                        <Grid item xs={12} md={6} style={{display:'inline-block',margin: '0px 10px'}}>
                        <Field
                            name={"signature1"}
                            type={"text"}
                            component={ImplementationFor["signature"]}
                            onChange={(e) => onChange("signature1",e)}
                            />
                        </Grid>
                    </Grid>
                    <Grid style={{paddingBottom:'10px'}} className={classes.radioGroup}>
                        <Grid item xs={12} md={6} style={{display:'inline-block',alignSelf: 'flex-end'}}> WITNESS:</Grid>
                        <Grid item xs={12} md={6} style={{display:'inline-block',margin: '0px 10px'}}>
                        <Field
                            name={"signature2"}
                            type={"text"}
                            component={ImplementationFor["signature"]}
                            onChange={(e) => onChange("signature1",e)}
                            disabled={true}
                            />
                        </Grid>
                    </Grid>
                    <Grid style={{paddingBottom:'10px'}} className={classes.radioGroup}>
                        <Grid item xs={12} md={6} style={{display:'inline-block',alignSelf: 'flex-end'}}> Witness Signature:</Grid>
                        <Grid item xs={12} md={6} style={{display:'inline-block',margin: '0px 10px'}}>
                        <Field
                            name={"signature3"}
                            type={"text"}
                            component={ImplementationFor["signature"]}
                            onChange={(e) => onChange("signature1",e)}
                            disabled={true}
                            />
                        </Grid>
                    </Grid>
                         <Grid style={{ textAlign: "center", padding: '20px 0px' }}>
                             <Button className={classes.button} form={"consent25"} type="submit">Submit</Button>
                         </Grid>
                     </Grid>
                 </Grid>
 
                 </form>
             </Grid >
         </Grid >
     </Grid>
 
 }
 
 Consent25.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent25',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true
 })(Consent25);