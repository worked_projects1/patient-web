
/**
 * 
 *  Consent15
 * 
 */
 import * as React from 'react';
 import { useState, useEffect, useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Grid, Button, } from '@mui/material';
 import Styles from './styles';
 import { ImplementationFor } from '../Components/CreateRecordForm/utils'
 import { Field, reduxForm, formValueSelector } from 'redux-form'

 /**
  * 
  * @param {object} props 
  * @returns 
  */   
  
 function Consent15(props) {
    const { handleSubmit, destroy,change,dispatch,initialValues } = props;
 
     const classes = Styles();
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState("");
 
     useEffect(() => {
         return () => destroy();
     }, []);
 
     dispatch(change('staff1',initialValues.providerFirst+ ' ' + initialValues.providerLast))
     dispatch(change('staff2',initialValues.providerFirst+ ' ' + initialValues.providerLast))
     dispatch(change('staff3',initialValues.providerFirst+ ' ' + initialValues.providerLast))
     dispatch(change('staff4',initialValues.providerFirst+ ' ' + initialValues.providerLast))
     dispatch(change('staff5',initialValues.providerFirst+ ' ' + initialValues.providerLast))
     dispatch(change('staff6',initialValues.providerFirst+ ' ' + initialValues.providerLast))
 
     const handleSignature = (val, eventValue) => {
         val == "signature1" && setSignature1(eventValue)
         val == "signature2" && setSignature2(eventValue)
         val == "signature3" && setSignature3(eventValue)
     }
 
     return <Grid className={classes.firstContainer}  >
         <Grid className={classes.secContainer} >
             <Grid item className={classes.EditForm} justify="center">
                 <span className={classes.formTitle}>Maxillary Sinus Bone Augmentation Consent Form</span>
             </Grid>
             <Grid item className={classes.thirdContainer}>
                 <form onSubmit={handleSubmit} id='consent15'>
 
                     <Grid item spacing={12} className={classes.content} justify="space-between">
                         <Grid>
                                 
                             <p>I authorize and request 
                                <Grid item xs={6} style={{ display: 'inline-block', margin: '5px 10px',  lineHeight: '1rem' }}>
                                    <Field
                                        name={"staff1"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                        disabled={true}
                                    />
                                </Grid>
                                to perform surgery on my maxilla (upper jaw). </p>
                             <p> I understand that surgery will be performed to place a bone graft material into the floor of the sinus to build up adequate bone height for the placement of implants. The bone graft will consist of a bone substitute material (hydroxylapatite), tissue bank bone, or a combination of both. In approximately 5 to 6 months, after the graft has partially healed, a second procedure will be done to insert the implant into the maxilla (upper jaw) and the grafted material. In some cases it is possible to insert the implants and sinus floor graft during the same operation. It is expected that the implants will become stable and act as anchors for fixed or fixed-detachable dental prostheses. </p>
                             <p>
                                <Grid item xs={6} style={{ display: 'inline-block', margin: '5px 10px',  lineHeight: '1rem' }}>
                                    <Field
                                        name={"staff2"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                        disabled={true}
                                    />
                                </Grid> has explained that if the new bone does not incorporate into the bone graft material, alternative prosthetic measures would have to be considered. 
                                <Grid item xs={6} style={{ display: 'inline-block', margin: '5px 10px',  lineHeight: '1rem' }}>
                                    <Field
                                        name={"staff3"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                        disabled={true}
                                    />
                                </Grid>
                                has explained and described the procedure to my satisfaction. 
                             </p>
                             <p>The likelihood for success of the suggested treatment plan is good. However, there are risks involved. The bone graft material has produced good results when placed on top of the maxillary (upper) or mandibular (lower) ridge. However, there are insufficient long-term studies to evaluate placement of the material on the sinus floor. This bone graft replacement material has previously been shown to be free from rejection or infection.However, there is no guarantee that your graft will not become infected or be rejected. </p>
                             <p>There have been some cases of failure of the graft to incorporate new bone or to sustain implants. Rarely, implants have failed and require removal; occasionally, the area can be re-grafted and implants re-inserted. </p>
                             <p>It is understood that although good results are expected, they cannot be and are not implied, guaranteed, or warrantable. There is also no guarantee against unsatisfactory or failed results.</p>
                             <p>I have been informed and understand that occasionally there are complications of surgery,drugs, and anesthesia, including but not limited to: 
                                 <ol>
                                    <li>
                                    Pain, swelling, and postoperative discoloration of the face, neck, and mouth 
                                    </li>
                                    <li>
                                    Numbness and tingling of the upper lip, teeth, gingiva (gum), cheek, and palate, which may be temporary or, rarely, permanent 
                                    </li>
                                    <li>
                                    Infection of the bone that might require further treatment, including
                                    hospitalization and surgery 
                                    </li>
                                    <li>
                                    Mal-union, delayed union, or nonunion of the bone graft replacement material to the normal bone 
                                    </li>
                                    <li>
                                    Lack of adequate bone growth into the bone graft replacement material
                                    </li>
                                    <li>
                                    Bleeding that may require extraordinary means to control hemorrhage
                                    </li>
                                    <li>
                                    Limitation of jaw function
                                    </li>
                                    <li>
                                    Stiffness of facial and jaw muscles 
                                    </li>
                                    <li>
                                    Injury to the teeth
                                    </li>
                                    <li>
                                    Referred pain to the ear, neck, and head 
                                    </li>
                                    <li>
                                    Postoperative complications involving the sinuses, nose, nasal cavity, sense of smell, infraorbital regions, and altered sensations of the cheeks and eyes 
                                    </li>
                                    <li>
                                    Postoperative unfavorable reactions to drugs, such as nausea, vomiting, and allergy
                                    </li>
                                    <li>
                                    Possible loss of teeth and bone segments 
                                    </li>
                                 </ol>
                             </p>

                             <p>I understand that I am not to use alcohol or non-prescribed drugs during the treatment period.  
                                <Grid item xs={6} style={{ display: 'inline-block', margin: '5px 10px',  lineHeight: '1rem' }}>
                                    <Field
                                        name={"staff4"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                        disabled={true}
                                    />
                                </Grid> 
                                has discussed with me that smoking is particularly harmful to the success of this operation. I have been requested to refrain from smoking. </p>

                            <p>I understand that 
                                <Grid item xs={6} style={{ display: 'inline-block', margin: '5px 10px',  lineHeight: '1rem' }}>
                                    <Field
                                        name={"staff5"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                        disabled={true}
                                    />
                                </Grid> 
                                will give his best professional care toward the accomplishment of the desired results. I understand that I can ask for full recital of all possible risks attendant to phases of my care by asking. I further
                                understand that I am free to withdraw from treatment at any time.
                            </p>

                             <p>I give permission for persons other than the doctors involved in my care and treatment to observe this operation and for it to be photographed or otherwise recorded for the purposes of teaching and research. </p>

                             <p>I understand this consent form, and I request 
                                <Grid item xs={6} style={{ display: 'inline-block', margin: '5px 10px',  lineHeight: '1rem' }}>
                                    <Field
                                        name={"staff6"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                        disabled={true}
                                    />
                                </Grid> 
                                to perform the surgery discussed. I hereby state that I read, speak, and understand English. 
                            </p>

                            <Grid  className={classes.flexContainer}>
                        <Grid className={classes.flexItem} style={{paddingBottom: '10px'}}>
                            <Grid  item  style={{display: 'inline-block', paddingBottom: '10px'}}><span>Patient's Name:</span></Grid>
                            <Grid  item style={{ margin: '5px 10px', lineHeight: '1rem' }} className={classes.blockToInline}>
                                <Field
                                    name={"patientName"}
                                    type={"text"}
                                    required={true}
                                    component={ImplementationFor["input"]}
                                />
                            </Grid>
                        </Grid>
                        </Grid>
                        
                        <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                            <Grid item  md={6} className={classes.signatureContainer}>
                                <Grid item style={{textAlign: 'center', display: 'block', margin: '0px 10px' }}>
                                    
                                    <Field
                                        name={"signature1"}
                                        type={"text"}
                                        autoFocus={true}
                                        component={ImplementationFor["signature"]}
                                        id={"signature1"}
                                        onChange={(e) => handleSignature("signature1", e)}
                                    /></Grid>
                                <Grid> Patient Signature</Grid>
                            </Grid>
                            <Grid item style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                                <Grid item md={12} className={classes.DateContainer1}>
                                    <Field
                                        name={"date1"}
                                        type={"text"}
                                        component={ImplementationFor["date"]}
                                        required={true}
                                    />
                                    
                                </Grid>
                                <Grid>Date</Grid>
                            </Grid>
                        </Grid>

                             <Grid style={{ textAlign: "center", padding: '20px 0px' }}>
                                <Button className={classes.button} type="submit" form="consent15">Submit</Button>
                             </Grid>
                         </Grid>
                     </Grid>
                 </form>
             </Grid >
         </Grid >
     </Grid>
 
 }
  
 Consent15.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent15',
     enableReinitialize: true,
     touchOnChange: true,
     touchOnBlur: true,
     destroyOnUnmount: true,
     forceUnregisterOnUnmount: true
 })(Consent15);
 