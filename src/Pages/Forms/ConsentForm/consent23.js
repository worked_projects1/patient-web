/**
 * 
 *  Consent23
 * 
 */

 import React, { useState, useEffect, useRef } from 'react';
 import PropTypes from 'prop-types';
 import { Field, reduxForm, formValueSelector } from 'redux-form';
 import { Grid, Button, } from '@mui/material';
 import Styles from './styles';
 import { ImplementationFor } from '../Components/CreateRecordForm/utils'
 
 /**
   * 
   * @param {object} props 
   * @returns 
   */
 function Consent23(props) {
    const { handleSubmit, destroy,change,dispatch,initialValues } = props;
     const classes = Styles();
     const [signature1, setSignature1] = useState("");
     const [signature2, setSignature2] = useState("");
     const [signature3, setSignature3] = useState("");
 
     useEffect(() => {
         return () => destroy();
     }, []);

     dispatch(change('firstName1',initialValues.providerFirst))
     dispatch(change('lastName1',initialValues.providerLast))
 
     const onChange = (val, eventValue) => {
         val == "signature1" && setSignature1(eventValue)
         val == "signature2" && setSignature2(eventValue)
         val == "signature3" && setSignature3(eventValue)
     }
 
     return <Grid className={classes.firstContainer}  >
         <Grid className={classes.secContainer} >
             <Grid item className={classes.EditForm} justify="center">
                 <span className={classes.formTitle}>Zygomatic and Tubero-Pterygoid Consent Form</span>
             </Grid>
             <Grid item className={classes.thirdContainer}>
                 <form onSubmit={handleSubmit.bind(this)} id={"consent23"}>
 
                     <Grid item spacing={12} className={classes.content} justify="space-between">
                         <Grid>
                             <p>Dental implants are performed as a basis for oral rehabilitation in cases one tooth or more are missing. The placement of a dental implant is a surgical procedure. Zygomatic and Tubero-Pteryoid implants were developed as rehabilitative solution for patients suffering from severe bone loss in the upper jaw that does not enable placement of conventional dental implants. The implants designated for placement in the zygomatic bone, in the tuberosity or in the pterygoid region for the purpose of dental rehabilitation. </p>

                             <h4>Name of patient:</h4>
                            <Grid style={{ justifyContent:'center',  }} className={classes.radioGroup}>
                            <Grid item xs={12}  md={6} lg={4}  style={{ textAlign: 'center',}}>
                                <Grid item xs={12}  md={12}  style={{textAlign: 'center',   margin: '5px 10px', }} className={classes.centeringText}>
                                
                                    <Field
                                        name={"firstName"}
                                        type={"text"}
                                        required={true}
                                        component={ImplementationFor["input"]}
                                        
                                    />
                                    
                                </Grid>
                                <Grid>First Name</Grid>
                                </Grid>
                                <Grid item xs={12}  md={6} lg={4} style={{ textAlign: 'center',}}>
                                <Grid item xs={12}  md={12}  style={{    margin: '5px 10px', }} className={classes.centeringText}>
                                    <Field
                                        name={"lastName"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                        
                                    />
                                    
                                </Grid><Grid>Last Name</Grid>
                                </Grid>
                            </Grid>


                            <p><b> declare and confirm that I received a detailed verbal information from:</b></p>
                            <Grid style={{ justifyContent:'center',  }} className={classes.radioGroup}>
                            {/* <Grid item style={{ lineHeight: '1rem' }}> Dr.</Grid>  */}
                            <Grid item xs={12}  md={6} lg={4}  style={{ textAlign: 'center',}}>
                                <Grid item xs={12}  md={12}  style={{textAlign: 'center',   margin: '5px 10px', }} className={classes.centeringText}>
                                
                                    <Field
                                        name={"firstName1"}
                                        type={"text"}
                                        required={true}
                                        component={ImplementationFor["input"]}
                                        disabled={true}
                                        
                                    />
                                    
                                </Grid>
                                <Grid>First Name</Grid>
                                </Grid>
                                <Grid item xs={12}  md={6} lg={4} style={{ textAlign: 'center',}}>
                                <Grid item xs={12}  md={12}  style={{    margin: '5px 10px', }} className={classes.centeringText}>
                                    <Field
                                        name={"lastName1"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                        disabled={true}
                                    />
                                    
                                </Grid><Grid>Last Name</Grid>
                                </Grid>
                            </Grid>

                            <p>on treating me using zygomatic and/or Tubero-Pterygoid implants in the upper jaw.Details (type, location and quantity):
                            <Grid item xs={12} md={6} style={{ margin: '5px 10px',lineHeight: '1rem'}}
                            className={classes.blockToInline}>
                                <Field
                                    name={"quantity"}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    required={true}
                                    />
                            </Grid>
                            (Hereinafter: The Principal Treatment"). I was informed of the treatment necessary for the placement of a dental implant, including the hopeful results and possible alternative treatments under the circumstances of my condition. I have considered the alternative treatments before choosing this treatment.
                            </p>
                            <p>I was also informed of the importance of quitting smoking before and after the treatment, of treating gum disease and of controlling diabetes. It was explained to me that smoking, untreated gum disease and diabetes significantly increase the risk of dental implantation failure. It was further explained to me that the combination of surgical treatment and bisphosphonate medications, whether such medications are being taken now or had been taken in the past (medications for the treatment of osteoporosis and/or of bone diseases and/or useof steroids) increase the risk of chronic inflammation that can amount to necrosis of the jaw bones.</p>

                            <p>I was informed of the side effects of the Principal Treatment, including considerable swelling, pain,subcutaneous hematoma, and temporary limitation in mouth opening.</p>

                            <p>
                            I was also informed of the risks and complications related to the Principal Treatment, including infection, injury to facial nerves during implantation, which means temporary or permanent impairment of sensation in the affected site, possible injury to the maxillary sinus during treatment of the upper jaw, which may require further treatment and the development of sinusitis shortly after the treatment and/or at a later date. There is also a risk of injury to major anatomical structures such as the eye orbit, the eyeball and brain, fractures of the zygomatic bone and injury to central blood vessels.
                            </p>
                            <p>It has been further explained to me that the manner and duration of recovery of the bone and gums following insertion of dental implants are individual and unpredictable. I was informed of the possibility of dental implant failure, and I understand that in such case, it will become necessary to remove the implant and/ or to perform corrective treatment.</p>
                            <p>It has also been explained me, and I understand the importance of continuity of the treatment at the same place and of cooperation between the doctor performing the dental implant insertion and the doctor performing the rehabilitative treatment.</p>
                            <p>I understand the importance of providing accurate information regarding my health condition and of complying with all the instructions given to me by the treating staff/doctor, including maintaining oral hygiene, and receiving all necessary operative and prosthetic treatments and attending follow-up checkups on schedule as may be required.</p>
                            <p>I hereby give my consent to the Principal Treatment.</p>
                            <p>My consent is also given to local anesthesia, after being informed of the risks and complications of anesthesia including loss of sensation in the lip and/or tongue and/or chin and/or face, hematoma, swelling and temporary limitation in mouth opening. Should the Principal Treatment be performed under general anesthesia or under intravenous sedation, the anesthetic technique would be explained to me by an anesthesiologist.</p>
                            <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                            <Grid item  style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                                <Grid item md={12} className={classes.DateContainer1} style={{ margin: '5px 10px' }}>
                                    <Field
                                    name={"date1"}
                                    type={"text"}
                                    component={ImplementationFor["date"]}
                                    id={"date1"}
                                    />

                                </Grid>
                                <Grid>Date</Grid>
                            </Grid>
                            <Grid item  md={6} className={classes.signatureContainer}>
                                <Grid item  style={{textAlign: 'center', display: 'block',  margin: '5px 10px', }}>
                                    
                                    <Field
                                        name={"signature1"}
                                        type={"text"}
                                        autoFocus={true}
                                        component={ImplementationFor["signature"]}
                                        id={"signature1"}
                                        onChange={(e) => onChange("signature1", e)}
                                    /></Grid>
                                    <Grid>Signature</Grid>
                                </Grid>

                            </Grid>

                            <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                                 <Grid style={{ textAlign: 'center', display: 'inline-block', paddingBottom: '10px'}}>
                                 
                                 <Grid item xs={12} md={7} className={classes.textContainer} style={{ textAlign: 'center', display: 'inline-block',  margin: '5px 10px', }}>
                                     <Field
                                         name={"guardian"}
                                         type={"text"}
                                         component={ImplementationFor["input"]}
                                         required={true}
                                         id={"date1"}
                                     />
                                     <span>Name of Guardian (relationship)</span>
                                 </Grid></Grid>
                                 <Grid item xs={10} sm={5} style={{ textAlign: 'center', display: 'inline-block',  margin: '5px 10px', }}>
                                     <Field
                                         name={"signature2"}
                                         type={"text"}
                                         component={ImplementationFor["signature"]}
                                         id={"signature2"}
                                         onChange={(e) => onChange("signature2", e)}
                                     />
                                     <span>Guardian’s Signature (when patient is legally incompetent, a minor or mentally ill)</span>
                                 </Grid>
                            </Grid>
                             
                            <p>I confirm that I explained verbally to the patient/the patient’s guardian/ all the aforementioned in detail as required and that he/she signed before me, after I was satisfied that he/she fully understood my explanation.</p>
                            <Grid style={{ justifyContent:'center',  }} className={classes.radioGroup}>
                            
                                <Grid item xs={12}  md={6} lg={4} style={{ textAlign: 'center',}}>
                                <Grid item xs={12}  md={12}  style={{    margin: '5px 10px', }} className={classes.centeringText}>
                                    <Field
                                        name={"lastName3"}
                                        type={"text"}
                                        component={ImplementationFor["input"]}
                                        required={true}
                                        
                                    />
                                    
                                </Grid><Grid>Last Name</Grid>
                                </Grid>
                            <Grid item xs={12}  md={6} lg={4}  style={{ textAlign: 'center',}}>
                                <Grid item xs={12}  md={12}  style={{textAlign: 'center',   margin: '5px 10px', }} className={classes.centeringText}>
                                
                                    <Field
                                        name={"firstName3"}
                                        type={"text"}
                                        required={true}
                                        component={ImplementationFor["input"]}
                                        
                                        
                                    />
                                    
                                </Grid>
                                <Grid>First Name</Grid>
                                
                            </Grid>
                            </Grid>


                            <Grid style={{ justifyContent: 'center' }} className={classes.radioGroup}>
                            <Grid item  md={6} className={classes.signatureContainer}>
                                <Grid item style={{textAlign: 'center', display: 'block',  margin: '5px 10px', }}>
                                    
                                    <Field
                                        name={"signature3"}
                                        type={"text"}
                                        autoFocus={true}
                                        component={ImplementationFor["signature"]}
                                        id={"signature3"}
                                        onChange={(e) => onChange("signature3", e)}
                                        disabled={true}
                                    /></Grid>
                                    <Grid>Doctor’s Signature</Grid>
                                </Grid>
                            <Grid item  style={{ padding: '0px 10px' }} className={classes.DateContainer}>

                                <Grid item md={12} className={classes.DateContainer1}>
                                    <Field
                                    name={"licence"}
                                    type={"text"}
                                    component={ImplementationFor["input"]}
                                    disabled={true}
                                    />

                                </Grid>
                                <Grid>License No.</Grid>
                            </Grid>
                            

                            </Grid>

                             <Grid style={{ textAlign: "center", padding: '20px 0px' }}>
                                 <Button form="consent23" type="submit" className={classes.button}>Submit</Button>
                             </Grid>
                         </Grid>
                     </Grid>
 
                 </form>
             </Grid >
         </Grid >
     </Grid>
 
 }

 Consent23.propTypes = {
     title: PropTypes.string,
     children: PropTypes.func,
     handleSubmit: PropTypes.func,
     error: PropTypes.string,
     pristine: PropTypes.bool,
     submitting: PropTypes.bool,
     fields: PropTypes.array,
 };
 
 export default reduxForm({
     form: 'consent23',
     enableReinitialize: true,
     touchOnChange: true,
     destroyOnUnmount: false,
     forceUnregisterOnUnmount: true,
 })(Consent23);