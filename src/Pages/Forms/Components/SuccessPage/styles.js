
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  body: {
    display: 'flex',
    justifyContent: 'center',
    verticalAlign: 'middle',
    backgroundColor: '#98999a',
    height: '94vh',
    flexDirection: 'column'
  },
  status_confirm: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: 'white',
    height: '200px',
    boxShadow: '6px 6px 1px 2px #888888',
    margin: '25px',
  },
  schedule_icon: {
    color: "green",
    fontSize: "42px"
  },
  schedule_confirm: {
      paddingTop: '20px',
  },
}));

export default useStyles;
