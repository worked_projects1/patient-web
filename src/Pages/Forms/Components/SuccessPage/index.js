
/**
*
* Success Page
*
*/



import React from 'react';
import styled from 'styled-components';
import CheckCircleOutlinedIcon from '@mui/icons-material/CheckCircleOutlined';
import { Grid } from '@mui/material';
import styles from './styles';

const H1 = styled.h1`
  font-size: 2rem;
  font-weight: 400;
  text-align: center;
  color:white
`;

function SuccessPage(props) {
 const classes = styles();

  return (
    <Grid xs={12} justify="center" container className={classes.body}>
      <H1>Thanks for your response</H1>
      <Grid className={classes.status_confirm}>
        <Grid item align="center">
          <div><CheckCircleOutlinedIcon className={classes.schedule_icon} /></div>
          <Grid className={classes.schedule_confirm}> Consent form submitted successfully </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
export default SuccessPage;