
/***
 * 
 * Input Field
 * 
 */


import React from 'react';
import TextField from '@mui/material/TextField';
import styles from './styles';

export default function InputField(props) {
  const { input ,autoFocus,defaultBlur, required, disabled } = props;
  const classes = styles();
  const { name, value, onChange } = input;
  const InputChange = defaultBlur ? Object.assign({}, {
    onBlur: (e) => onChange(e.target.value),
    defaultValue: value || ''
  }) : Object.assign({}, {
    onChange: (e) => onChange(e.target.value),
    value: value || ''
  });

  return (
    <TextField
      className={classes.fieldColor && classes.Field}
      name={name}
      value={value || ''}
      rows={1}
      rowsMax={1}
      fullWidth
      {...InputChange}
      defaultValue={props.value ? props.value : ""}
      autoFocus={autoFocus}
      variant="standard"
      required={required}
      disabled={disabled}
    />

  )
}