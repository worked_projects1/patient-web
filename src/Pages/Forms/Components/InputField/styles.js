

import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  fieldColor: {
    '& :after': {
      borderBottomColor: '#2ca01c',
    },
    '& :before': {
      borderBottomColor: '#2ca01c',
    },
    color: 'green !important',
    '& label.Mui-focused': {
      color: '#2ca01c',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#2ca01c',
    }
  },
  textSize: {
    fontSize: '14px',
  },
  label: {
    fontSize: '18px',
    marginTop: '1em',
    display: 'block',
    lineHeight: '30px',
    fontWeight: '400'
  },
  Field: {
    // height: '30px',
    fontSize: '22px!important',
    '& div': {
      height: '1.2rem',
      color: 'gray',
      // padding: '6px 6px',
    },
    '& input': {
      color: 'gray',

    },
    '& input': {
      height: '30px!important',
      lineHeight: '30px',
      fontSize: '22px',
      ['@media (max-width: 480px)']: {
        fontSize: '20px',

      },
    },

  },
  Input: {
    padding: "6px 6px",
  }
}));


export default useStyles;



// import { makeStyles } from '@mui/styles';

// const useStyles = makeStyles((theme) => ({
//   fieldColor: {
//     '& :after': {
//       borderBottomColor: '#2ca01c',
//     },
//     '& :before': {
//       borderBottomColor: '#2ca01c',
//     },
//     color: 'green !important',
//     '& label.Mui-focused': {
//       color: '#2ca01c',
//     },
//     '&.Mui-focused fieldset': {
//       borderColor: '#2ca01c',
//     }
//   },
//   textSize: {
//     fontSize: '14px',
//   },
//   label: {
//     fontSize: '18px',
//     marginTop: '1em',
//     display: 'block',
//     lineHeight: '30px',
//     fontWeight: '400'
//   },
//   Field: {
//     height: '30px',
//     fontSize: '20px!important',
//     '& div': {
//       height: '30px',
//       color: 'gray',
//       // padding: '6px 6px',
//     },
//     '& input': {
//       color: 'gray',

//     },
//     '& textarea': {
//       height: '50px!important',
//       lineHeight: '60px',
//       fontSize: '20px',
//     }
//   },
//   Input: {
//     // padding: "6px 6px",
//   }
// }));


// export default useStyles;