import InputField from '../InputField';
import CheckboxField from '../CheckboxField';
import TextArea from '../TextArea';
import DatePicker from '../DatePicker';
import RadioBoxField from '../RadioBoxField';
import SignatureCard from '../SignatureCard';
export const ImplementationFor = {
    input: InputField,
    number: InputField,
    date: DatePicker,
    checkbox: CheckboxField,
    radio: RadioBoxField,
    signature: SignatureCard,
    textArea: TextArea
};

