/**
 * 
 * Radio Box
 * 
 */

 import React from 'react';
 import Styles from './styles';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import {Grid} from '@mui/material';

 
 export default function ({ input, options, disabled}) {
   const classes = Styles();
 
   const {  name, value,} = input;
   const isPreDefinedSet = Array.isArray(options);
 
   return (
     <Grid className={classes.checkboxField}>
       <FormControl component="fieldset">
        <RadioGroup
           row
           name={name}
           value={value}
           onChange={(e) => input.onChange(e.target.value)}>
           {isPreDefinedSet ? (options || []).map((opt, index) =>
             <FormControlLabel value={opt.value} name={opt.value} label={opt.label} control={<Radio  sx={{
              '&, &.Mui-checked': {
                color: '#ea6225',
              },
            }} />} disabled={disabled ? true : false} labelPlacement="end" className={classes.radioLabel} />
           ) : null}
         </RadioGroup> 
       </FormControl>
     </Grid>
   );
 }
 