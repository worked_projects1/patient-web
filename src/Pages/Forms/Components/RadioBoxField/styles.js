
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  checkboxField: {
    width: '100%',
    display: 'flex',
  },
  radioLabel: {
    fontSize: '18px',
    marginRight: '14px!important',
    '& span': {
      fontSize: '18px',
    }
  },
  label: {
    fontSize: '18px',
    marginTop: '1em',
    display: 'block',
    lineHeight: '30px',
    fontWeight: '400'
  },
}));


export default useStyles;