import React from 'react';
import PropTypes from 'prop-types';
import DatePicker from "react-datepicker";
import moment from 'moment';
import "react-datepicker/dist/react-datepicker.css";
import styles from './styles.css';

class Datepicker extends React.Component {
  static propTypes = {
    input: PropTypes.object,
   };

  constructor(props) {
    super(props);
    this.state = { startDate: new Date() };
    this.handleDate = this.handleDate.bind(this);
  }

  componentDidMount() {
    const { input } = this.props;
    if (input.value == "") { input.onChange(moment(new Date()).format("MM/DD/YYYY")) }

  }

  render() {
    const { startDate } = this.state;
    const { input, type, disabled } = this.props;
    return (<div className={'DatePicker'}>
    <DatePicker 
        selected={startDate ? startDate : input.value ? moment.utc(input.value) :  new Date() }
        format={type === 'calendar' ? 'DD MMM, YYYY' : 'MM/DD/YYYY'}    
        container="container"
        peekNextMonth
        showMonthDropdown
        showYearDropdown   
        dropdownMode="select"
        autoComplete="off"
        readOnly={disabled}
        className={type === 'calendar' ? styles.DatePickerWithNotes : styles.DatePicker}
        onChange={(selected) => this.handleDate(selected)}
        placeHolderTextStyle={{ textAlign: 'center' }}
        calendarClassName="rasta-stripes"
                    popperModifiers={{
                        offset: {
                          enabled: true,
                          offset: "0px, 0px"
                        },
                        preventOverflow: {
                          enabled: true,
                          escapeWithReference: false,
                          boundariesElement: "scrollParent"
                        }
                      }}
      />
      </div> );
        
  }


  handleDate(selected) {
    const { input, type } = this.props;
    this.setState({ startDate: selected });
    if(selected != "" && selected != null) {
      const pickedDate = moment(selected).format(type === 'calendar' ? 'DD MMM, YYYY' : 'MM/DD/YYYY');
    input.onChange(pickedDate);
    } else {
      input.onChange("");
    }
  }


}

export default Datepicker;