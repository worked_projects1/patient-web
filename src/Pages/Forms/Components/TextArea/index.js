
/***
 *
 * Textarea Field
 *
 */


import React from 'react';
import TextField from '@mui/material/TextField';
     import Styles from './styles';


export default function ({ input, label, autoFocus,disabled,className, placeholder, variant, style, defaultBlur, meta: { touched, error, warning }, required }) {

    const classes = Styles();
    const { id, name, value, onChange } = input;
    const InputChange = defaultBlur ? Object.assign({}, {
        onBlur: (e) => onChange(e.target.value),
        defaultValue: value || ''
    }) : Object.assign({}, {
        onChange: (e) => onChange(e.target.value),
        value: value || ''
    });


    return (
        <div style={style || {}}>
            <TextField

                className={className || classes.fieldColor && classes.Field}
                multiline
                name={name}
                id={id}
                label={variant && label || ''}
                variant={'filled'}
                placeholder={placeholder}
                type={"input"}
                fullWidth
                {...InputChange}
                autoFocus={autoFocus}
                disabled={disabled}
                required={required}
                size="small"
                InputProps={{
                    classes: {
                        input: classes.textSize,
                        padding: 0,
                    },

                }}
            />

            {/* <div className={classes.error} style={variant && { color: '#ffff' } || {}}>{touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</div> */}
        </div>

    )
}
