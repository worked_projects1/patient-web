
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  fieldColor: {
    marginTop: '10px',
    '& :after': {
      borderBottomColor: '#2ca01c',
    },
    '& :before': {
      borderBottomColor: '#2ca01c',
    },
    color: 'green !important',
    '& label.Mui-focused': {
      color: '#2ca01c',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#2ca01c',
    }
  },

  textSize: {
    fontSize: '22px',
    ['@media (max-width: 480px)']: {
      fontSize: '18px',

    },
  },
  error: {
    fontSize: '22px',
    color: 'red'
  },
  label: {
    fontSize: '22px',
    marginTop: '1em',
    display: 'block',
    lineHeight: '30px',
    fontWeight: '400'
  },
  Field: {
    // height: '55px',
    fontSize: '18px!important'
  }
}));


export default useStyles;