
/**
 * 
 * Checkbox Field
 * 
 */

 import React from 'react';
 import { FormControlLabel, FormControl,Grid } from '@mui/material'
 import Checkbox from '@mui/material/Checkbox';
 import styles from './styles';
 import CircleUnchecked from '@mui/icons-material/RadioButtonUnchecked';
 import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { useState } from 'react';
import TextField from '@mui/material/TextField';


 export default function CheckboxField({ input,options, meta: { touched, error }}) {
   const classes = styles();
   const inputRef = React.useRef();
   const isPreDefinedSet = Array.isArray(options);
  let inputValue = (input && input.value.length > 0 && Array.isArray(input.value) && input.value) || [];
  const [focus, setFocus] = useState(true);

  const handleChange = (val, checked) => {
       if (checked) {
         if(val === "Other"){
          setFocus(false)
            setTimeout(() => {
          inputRef.current.focus();
                  
                  inputRef.current.value = ""
                  
                }, 1000);

         }
          inputValue = inputValue.filter(el => el !== val);
          inputValue = inputValue.concat(val);
        } else {
            if(val === "Other"){
            setTimeout(() => {setFocus(true)},1000)
            inputRef.current.value = ""
            inputValue = inputValue.filter(el => el !== val);
            inputValue = inputValue.filter(el => !(el.includes(val)));
            }
            inputValue = inputValue.filter(el => el !== val);
            
        }
      if(Array.isArray(inputValue)){
      input.onChange(inputValue)
      }
    }

  const change = (val,value) => {
    if(Array.isArray(inputValue)){
      inputValue = inputValue.filter(el => el !== val);
      inputValue = inputValue.filter(el => !(el.includes(val)));
      inputValue = inputValue.concat(`${val}`+':'+`${value}`);
      input.onChange(inputValue)
      }
  }

   return (<div style={{width: '100%', padding: '20px', maxWidth: '100%'}}>
      <FormControl component="fieldset" className={classes.fieldSet} error={touched && !!error}> 
           {isPreDefinedSet ? (options || []).map((opt, index) =>
           <Grid item xs={12} md={4}>
           <Grid xs={12} >
            <FormControlLabel
            control={
                <Checkbox
                name={opt.value}
                onChange={(e) => handleChange(opt.value,e.target.checked)}
                size="large"
                icon={<CircleUnchecked style={{ color: '#ea6225', width: '25px' }} />}
                checkedIcon={<CheckCircleIcon style={{ color: '#ea6225',width: '25px' }} size="small" />}
                />
            }
            label={opt.label}
            className={classes.checkboxLabel}
            />
     
            {opt.input?
            <Grid xs={8} md={8}>
            <TextField className={classes.fieldColor && classes.Field}
                multiline
                name={opt.value}
                disabled={focus}
                rows={1}
                rowsMax={1}
                fullWidth 
                inputRef={inputRef}
                variant={"standard"}
                onChange={(e) => change(opt.value,e.target.value)}
                name={opt.value}/>
            </Grid> 
            : null }
        </Grid>
     </Grid>) 
     : null}
   </FormControl>
   </div>)
 };