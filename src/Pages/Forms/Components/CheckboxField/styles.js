

import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
     checkboxLabel: {
          '& span': {
               fontSize: '18px',
          },
          '& input + label > span': {
               display: 'flex',
               justifyContent: 'center',
               alignItems: 'center',
               marginRight: '1em',
               width: '2em',
               height: '2em',
               background: 'transparent',
               border: '2px solid #EA6225',
               borderRadius: '100px',
               cursor: 'pointer',
               transition: 'all 250ms cubic-bezier(.4,.0,.23,1)'
          }
     },
     fieldSet: {
          // height: '800px',
          flexDirection: 'row',
          width: '100%',
          flexWrap: 'wrap',

     },
     fieldColor: {
          '& :after': {
            borderBottomColor: '#2ca01c',
          },
          '& :before': {
            borderBottomColor: '#2ca01c',
          },
          color: 'green !important',
          '& label.Mui-focused': {
            color: '#2ca01c',
          },
          '&.Mui-focused fieldset': {
            borderColor: '#2ca01c',
          }
        },
        textSize: {
          fontSize: '14px',
        },
        label: {
          fontSize: '18px',
          marginTop: '1em',
          display: 'block',
          lineHeight: '30px',
          fontWeight: '400'
        },
        Field: {
          fontSize: '22px!important',
          '& div': {
            height: '1.2rem',
            color: 'gray',
          },
          '& input': {
            color: 'gray',
      
          },
          '& textarea': {
            height: '30px!important',
            lineHeight: '30px',
            fontSize: '22px',
            // ['@media (max-width: 480px)']: {
            //   fontSize: '20px',
      
            // },
          },
      
        },
        Input: {
          padding: "6px 6px",
        },
        hiding: {
          visibility: 'hidden'
        }
      
}));


export default useStyles;