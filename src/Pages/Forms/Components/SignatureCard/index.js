import React, { useRef, useState, useEffect } from "react";
import "./styles.css";
import SignaturePad from "react-signature-canvas";
import { Grid, Button, } from '@mui/material';
import Cancel from '@mui/icons-material/Cancel';

function SigCard(props) {
  const { input,disabled } = props;

  const sigCanvas = useRef(null);
  const [empty, setEmpty] = useState(false)

  useEffect(() => {
    if(disabled){
    sigCanvas.current.off()
    }
  }, [])

  const changes = (event) => {
    if (typeof props.input.onChange != undefined) {
      input.value = sigCanvas.current.toDataURL()
      props.input.onChange(sigCanvas.current.toDataURL())
      setEmpty(true)
    }
  }

  setTimeout(() => {

    if (input.value === "" && empty) {
      if (sigCanvas.current != null) {
        sigCanvas.current.clear();
      }
    }
  }, 1000);

  const clear = () => {
    if (sigCanvas.current != null) {
      sigCanvas.current.clear();
      props.input.onChange({})
      setEmpty(false)
    }
  }

  return (
    <Grid style={{display: 'flex'}}>
    <div className={"SigContainer"}>
      <SignaturePad penColor="black" ref={sigCanvas} clearOnResize={false} canvasProps={{ id: "sigPad", className: "SigPad" }} onStart={() => setEmpty(false)} onEnd={changes} />
    </div>
    <Grid className={"btn"}>
        {(sigCanvas.current != null && !(sigCanvas.current.isEmpty()) && empty) ? <Button onClick={clear} ><Cancel className={'cancel'}></Cancel></Button> : null}
      </Grid>
    </Grid>
  );
}

export default SigCard;


