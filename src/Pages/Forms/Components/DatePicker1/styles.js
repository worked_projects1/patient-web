
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  fieldColor: {
    width: '100%',
    fontSize: '10px',
    '& :after': {
      borderBottomColor: '#2ca01c',
      fontSize: '10px',
    },
    '& :before': {
      borderBottomColor: '#2ca01c',
      fontSize: '10px',
    },
    color: 'green !important',
    '& label.Mui-focused': {
      color: '#2ca01c',
      fontSize: '10px',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#2ca01c',
      fontSize: '10px',
    },
  },
  textSize: {
    fontSize: '14px',
  },
  label: {
    fontSize: '18px',
    marginTop: '1em',
    display: 'block',
    lineHeight: '30px',
    fontWeight: '400'
  },
  Field: {
    height: '50px',
    fontSize: '16px!important',
    color: 'gray',
    textAlign: 'center',
    '& div': {
    },
    '& input': {
      color: 'gray',
      textAlign: 'center',
      height: '10px',
      fontSize: '20px',
      ['@media (max-width: 480px)']: {
        fontSize: '16px',

      },
    }
  },
  checkboxField: {
    marginTop: '10px',
  },
  root: {
    "& .MuiInputBase-root": {
      padding: 0,
      "& .MuiButtonBase-root": {
        padding: 0,
        paddingLeft: 10
      },
      "& .MuiInputBase-input": {
        padding: 15,
        paddingLeft: 0
      }
    }
  },
}));


export default useStyles;
