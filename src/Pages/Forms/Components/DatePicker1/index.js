/**
 * 
 * Date Picker
 * 
 */
import React from 'react';
import Styles from './styles';
import moment from 'moment';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import MobileDatePicker from '@mui/lab/MobileDatePicker';
import { TextField, label } from '@mui/material';
import { Grid } from '@mui/material';

export default function DatePicker(props) {
  const { input, label, autoFocus, disabled, required } = props
  const classes = Styles();
  const { id, name, value, onChange } = input;
  const [selectedDate, handleDateChange] = React.useState(new Date());
  let value1 = moment(input.value).format("MM-DD-YYYY");

  function handleChange(val) {
    handleDateChange(moment(val).format("MM-DD-YYYY"));
    value1 = moment(val).format("MM-DD-YYYY");
  }

  if (value == "") { onChange(moment(selectedDate).format("MM/DD/YYYY")) }

  return (

    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <MobileDatePicker
        className={classes.fieldColor}
        key={id}
        margin="normal"
        name={name}
        onChange={(val) => onChange(moment(val).format("MM/DD/YYYY"))}
        value={value}
        format="MM/dd/yyyy"
        disabled={disabled}
        required={required}
        renderInput={(params) => <TextField {...params} style={{ width: '75%', height: '20px', display: 'unset' }} error={false} variant="standard" className={classes.Field} />}
        toolbarPlaceholder={`${moment().format("YYYY-MM-DD")}`}
      />
    </LocalizationProvider>
  );
}
