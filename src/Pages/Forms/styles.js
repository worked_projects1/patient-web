import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    dialog: {
        border: 'none',
        padding: '25px',
        minWidth: '40%',
        outline: 'none',
        width: '75%',
    },
    dialogContent: {
        fontSize: "20px",
        padding: '25px',
        ['@media (max-width: 480px)']: {
            fontSize: '16px !important',

        },
        textAlign:'center',
    },
    dialogAction: {
        justifyContent: 'center'
    },
    warning: {
        width: '100px',
        height: '60px',
        color: '#ea6225',
    },
    warningDiv: {
        textAlign: 'center'
    },
    successDiv: {
        textAlign: 'center',
    },
    success: {
        width: '100px',
        height: '60px',
        color: '#87b44d',
    },
}))

export default useStyles;
