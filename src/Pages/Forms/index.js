/**
 * 
 * Forms
 * 
 */

 import React, { useState } from 'react';
 import Styles from './styles';
 import { Grid , Button, Dialog, DialogActions,DialogContent,DialogContentText } from '@mui/material';
 import { handlePdf } from './ConsentForm/pdfMaker';
 import Consent1 from '../Forms/ConsentForm/consent1';
 import Consent3 from '../Forms/ConsentForm/consent3';
 import Consent2 from '../Forms/ConsentForm/consent2';
 import Consent4 from '../Forms/ConsentForm/consent4';
 import Consent5 from '../Forms/ConsentForm/consent5';
 import Consent6 from '../Forms/ConsentForm/consent6';
 import Consent7 from '../Forms/ConsentForm/consent7';
 import Consent8 from '../Forms/ConsentForm/consent8';
 import Consent9 from '../Forms/ConsentForm/consent9';
 import Consent10 from '../Forms/ConsentForm/consent10';
 import Consent11 from '../Forms/ConsentForm/consent11';
 import Consent12 from '../Forms/ConsentForm/consent12';
 import Consent13 from '../Forms/ConsentForm/consent13';
 import Consent14 from '../Forms/ConsentForm/consent14';
 import Consent15 from '../Forms/ConsentForm/consent15';
 import Consent16 from '../Forms/ConsentForm/consent16';
 import Consent17 from '../Forms/ConsentForm/consent17';
 import Consent18 from '../Forms/ConsentForm/consent18';
 import Consent19 from '../Forms/ConsentForm/consent19';
 import Consent20 from '../Forms/ConsentForm/consent20';
 import Consent21 from '../Forms/ConsentForm/consent21';
 import Consent1a6 from '../Forms/ConsentForm/consent1a6';
 import Consent23 from '../Forms/ConsentForm/consent23';
 import Consent24 from '../Forms/ConsentForm/consent24';
 import Consent25 from '../Forms/ConsentForm/consent25';
 import Upload from './ConsentForm/upload';
 import ConsentLetters1 from '../Forms/ConsentForm/consentletters1';
 import remotes from '../../Blocks/FileUpload/remotes'
 import Warning from '@mui/icons-material/Warning';
 import pdfMake from 'pdfmake/build/pdfmake';
 import pdfFonts from "pdfmake/build/vfs_fonts";
 import CheckCircleIcon from '@mui/icons-material/CheckCircle';
 import ConfirmPage from '../PatientConsultation/ConfirmPage';
 import SuccessPage from './Components/SuccessPage';
 import store2 from 'store2';
 
 function Forms(props) {
    const { consentName, inputs } = props
    const input = inputs.split('&')
    const patient = input[0].includes('patientId') ? input[0].substring(input[0].indexOf('=')+1) : input[1].includes('patientId') ? input[1].substring(input[1].indexOf('=')+1) : input[2].substring(input[2].indexOf('=')+1)
    
    const providerFirst = input[0].includes('providerFirstName') ? input[0].substring(input[0].indexOf('=')+1) : input[1].includes('providerFirstName') ? input[1].substring(input[1].indexOf('=')+1): input[2].substring(input[2].indexOf('=')+1)
    
    const providerLast = input[0].includes('providerLastName') ? input[0].substring(input[0].indexOf('=')+1) : input[1].includes('providerLastName') ? input[1].substring(input[1].indexOf('=')+1): input[2].substring(input[2].indexOf('=')+1)
    
    const initial = {providerFirst : providerFirst,
      providerLast : providerLast}

    const windowSize = window.innerWidth
    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);
    const [showDialog, setDialogOpen] = useState(false);
    const [error, setError] = useState("");
    const [one, setOne] = useState(true);
    const [upload, setUpload] = useState(false);
    const secret = store2.get('secret');
    const access_token = store2.get('access_token') || '';
    console.log("secret = ",secret," access_token = ",access_token)
    
    const handleSubmit = (data, dispatch, { form }) => {
        const docDefinition =  handlePdf(Object.assign({}, data, { "form": form,  "windowSize": windowSize }))
        if((Object.keys(docDefinition).length != 0) ){
            const pdfDocGenerator = pdfMake.createPdf(docDefinition, null, null, pdfFonts.pdfMake.vfs);
            const fileName = docDefinition.info ? docDefinition.info.title+'.pdf' : 'noConsent.pdf'
            
            pdfDocGenerator.getBlob((blob) => {
            const ajax = new XMLHttpRequest();
                remotes.getSignature(encodeURIComponent(fileName) ,"application/pdf")
            .then((result) => {
                  const { uploadUrl, publicUrl } = result;
                  if (uploadUrl && publicUrl) {
                  ajax.open("PUT", uploadUrl, true);
                  ajax.setRequestHeader("Content-Type", "application/pdf");
                  ajax.send(blob);
                  setTimeout(() =>  {
                    remotes.uploadPdf({ consentName: form,patientId:Number(patient),pdfUrl:publicUrl  }).then((result) => {
                    setOne(false)
  
                  setUpload(true)
                  setDialogOpen(true)
                  }).catch((error)=> {
                      setUpload(false)
                      setDialogOpen(true)
                    })
                  },1000)
                  } else {
                      setUpload(false)
                      setDialogOpen(true)
                  }
  
              })
            });
      
          //  pdfDocGenerator.open();
          }
    }
 
     const closeModal = () => {
         setDialogOpen(false)
         setModalOpen(false);
         setUpload(false)
         setOne(true)
     
     }
 
   
   return (
     <Grid container justify="center" style={{ justifyContent: 'center' }}>
       <Grid>
       {showModal?  
       <Dialog
          open={showModal}
          maxWidth={"md"}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogContent dividers>
            <DialogContentText id="alert-dialog-description" className={classes.dialogContent}>
            <Grid className={classes.warningDiv}>
            <Warning className={classes.warning}/>
            </Grid>
              <span dangerouslySetInnerHTML={{ __html: error }} />
            </DialogContentText>
          </DialogContent >
          <DialogActions className={classes.dialogAction}>
            <Button onClick={closeModal} color="primary" >
              Dismiss
            </Button>
            
          </DialogActions>
        </Dialog> : null }
        </Grid>
      {upload?
     <SuccessPage /> : null}



    {!upload ? <Grid>
       {
         consentName == "consent1" ?
         <Consent1
         initialValues={initial}
           onSubmit={(data, dispatch, { form }) => {
            if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
              setError("Please put sign in Signature1 !")
              setModalOpen(true)
            }
             else  if((data.signature2 == undefined) || (Object.keys(data.signature2).length == 0) ){
              setError("Please put sign in Signature2 !")
              setModalOpen(true)
            }
             else  if((data.signature3 == undefined) || (Object.keys(data.signature3).length == 0) ){
              setError("Please put sign in Signature3 !")
              setModalOpen(true)
            }
             else{
             handleSubmit(data, dispatch, { form })
             }
           // })
           }}
         />
         : consentName == "consent2" ?
           <Consent2 onSubmit={(data, dispatch, { form }) =>{
            if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
              setError("Please put sign in Signature1 !")
              setModalOpen(true)
            }
             else{
             handleSubmit(data, dispatch, { form })
             }
             }} />
           : consentName == "consent3" ?
             <Consent3 
             initialValues={initial}
             onSubmit={(data, dispatch, { form }) =>{
              if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                setError("Please put sign in Signature1 !")
                setModalOpen(true)
              }
               else{
                 handleSubmit(data, dispatch, { form })
                 }
               }} />
             : consentName == "consent4" ?
               <Consent4 
               initialValues={initial}
               onSubmit={(data, dispatch, { form }) =>{
                if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                  setError("Please put sign in Signature1 !")
                  setModalOpen(true)
                }
                 else{
                   handleSubmit(data, dispatch, { form })
                   }
                 }} />
               : consentName == "consent5" ?
                 <Consent5 onSubmit={(data, dispatch, { form }) =>{
                  if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                    setError("Please put sign in Signature1 !")
                    setModalOpen(true)
                  }
                   else{
                     handleSubmit(data, dispatch, { form })
                     }
                   }} />
                 : consentName == "consent6" ?
                    <Consent6 onSubmit={(data, dispatch, { form }) =>{
                      if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                        setError("Please put sign in Signature1 !")
                        setModalOpen(true)
                      }
                      else{
                        handleSubmit(data, dispatch, { form })
                        }
                      }} />
                      : consentName == "consent7" ?
                        <Consent7 onSubmit={(data, dispatch, { form }) =>{
                          if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                            setError("Please put sign in Signature1 !")
                            setModalOpen(true)
                          }
                          else{
                            handleSubmit(data, dispatch, { form })
                            }
                          }} />
                          : consentName == "consent8" ?
                            <Consent8 
                            initialValues={initial}
                            onSubmit={(data, dispatch, { form }) =>{
                              
                              if (data.sex == undefined || data.insurance == undefined || data.pregnant == undefined || data.nursing == undefined || data.birth == undefined || data.diabetic == undefined || data.insulin == undefined || data.bisphosphonates == undefined || data.administration == undefined || data.osteoporosis == undefined || data.gumsBleed == undefined || data.sensitiveToSweet == undefined || data.sensitiveToHot == undefined || data.injuries == undefined || data.unusual == undefined || data.orthodontic == undefined || data.grind == undefined || data.smile == undefined || data.condition == undefined || data.allergies == undefined || data.jawJoint == undefined){
                                setError("Please Fill All Fields !")
                                setModalOpen(true)
                              }
                              else if(data.condition.length == 0 || data.allergies.length == 0 || data.jawJoint.length == 0){
                                setError("Please Fill All Fields !")
                                setModalOpen(true)
                              }
                              else  if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                                setError("Please put sign in Signature1 !")
                                setModalOpen(true)
                              }
                              else  if((data.signature2 == undefined) || (Object.keys(data.signature2).length == 0) ){
                                setError("Please put sign in Signature2 !")
                                setModalOpen(true)
                              }
                              else  if((data.signature3 == undefined) || (Object.keys(data.signature3).length == 0) ){
                                setError("Please put sign in Signature3 !")
                                setModalOpen(true)
                              }
                              else{
                              handleSubmit(data, dispatch, { form })
                              }
                              }} />
                              : consentName == "consent9" ?
                              <Consent9 
                              initialValues={initial}
                              onSubmit={(data, dispatch, { form }) =>{
                                if (data.sex == undefined || data.insurance == undefined || data.pregnant == undefined || data.nursing == undefined || data.birth == undefined || data.diabetic == undefined || data.insulin == undefined || data.bisphosphonates == undefined || data.administration == undefined || data.osteoporosis == undefined || data.gumsBleed == undefined || data.sensitiveToSweet == undefined || data.sensitiveToHot == undefined || data.injuries == undefined || data.unusual == undefined || data.orthodontic == undefined || data.grind == undefined || data.smile == undefined || data.condition == undefined || data.allergies == undefined || data.jawJoint == undefined){
                                  setError("Please Fill All Fields !")
                                  setModalOpen(true)
                                }
                                else if(data.condition.length == 0 || data.allergies.length == 0 || data.jawJoint.length == 0){
                                  setError("Please Fill All Fields !")
                                  setModalOpen(true)
                                }
                                else  if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                                  setError("Please put sign in Signature1 !")
                                  setModalOpen(true)
                                }
                                else{
                                handleSubmit(data, dispatch, { form })
                                }
                                }} />
                                : consentName == "consent10" ?
                                  <Consent10 onSubmit={(data, dispatch, { form }) =>{
                                    if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                                      setError("Please put sign in Signature1 !")
                                      setModalOpen(true)
                                    }
                                    else{
                                      handleSubmit(data, dispatch, { form })
                                    }
                                  }} />
                                  : consentName == "consent11" ?
                                    <Consent11 onSubmit={(data, dispatch, { form }) =>{
                                      if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                                        setError("Please put sign in Signature1 !")
                                        setModalOpen(true)
                                      }
                                      else{
                                        handleSubmit(data, dispatch, { form })
                                      }
                                      }} />
                                      : consentName == "consent12" ?
                                        <Consent12 onSubmit={(data, dispatch, { form }) =>{
                                          if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                                            setError("Please put sign in Signature1 !")
                                            setModalOpen(true)
                                          }
                                          else{
                                            handleSubmit(data, dispatch, { form })
                                          }
                                        }} />
                                        : consentName == "consent13" ?
                                          <Consent13 onSubmit={(data, dispatch, { form }) =>{
                                            if (data.ill == undefined || data.symptoms == undefined || data.expose == undefined || data.travel == undefined || data.closeContact == undefined ){
                                              setError("Please Fill All Fields !")
                                              setModalOpen(true)
                                            }
                                            else  if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                                              setError("Please put sign in Signature1 !")
                                              setModalOpen(true)
                                            }
                                            else{
                                              handleSubmit(data, dispatch, { form })
                                            }
                                          }} />
                                          : consentName == "consent14" ?
                                            <Consent14 
                                            initialValues={initial}
                                            onSubmit={(data, dispatch, { form }) =>{
                                              if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                                                setError("Please put sign in Signature1 !")
                                                setModalOpen(true)
                                              }
                                              else{
                                                handleSubmit(data, dispatch, { form })
                                              }
                                              }} />
                                              : consentName == "consent15" ?
                                                <Consent15 
                                                initialValues={initial}
                                                onSubmit={(data, dispatch, { form }) =>{
                                                  if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                                                    setError("Please put sign in Signature1 !")
                                                    setModalOpen(true)
                                                  }
                                                  else{
                                                    handleSubmit(data, dispatch, { form })
                                                  }
                                                }} />
                                                : consentName == "consent16" ?
                                            <Consent16 
                                            initialValues={initial}onSubmit={(data, dispatch, { form }) =>{
                                              if (data.amoxicillin == undefined || data.peridex == undefined || data.halcion == undefined || data.decadron == undefined || data.motrin == undefined ){
                                                setError("Please Fill All Fields !")
                                                setModalOpen(true)
                                              }
                                              else if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                                                setError("Please put sign in Signature1 !")
                                                setModalOpen(true)
                                              }
                                              else{
                                                handleSubmit(data, dispatch, { form })
                                              }
                                            }} />
                                            : consentName == "consent17" ?
                                              <Consent17 
                                              initialValues={initial}
                                              onSubmit={(data, dispatch, { form }) =>{
                                                if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                                                  setError("Please put sign in Signature1 !")
                                                  setModalOpen(true)
                                                }
                                                else{
                                                  handleSubmit(data, dispatch, { form })
                                                }
                                              }} />
                                              : consentName == "consent18" ?
                                                <Consent18  initialValues={initial}
                                                onSubmit={(data, dispatch, { form }) =>{
                                                  if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                                                    setError("Please put sign in Signature1 !")
                                                    setModalOpen(true)
                                                  }
                                                  else{
                                                    handleSubmit(data, dispatch, { form })
                                                  }
                                                 }} />
                                                 : consentName == "consent19" ?
                                                  <Consent19 onSubmit={(data, dispatch, { form }) =>{
                                                    if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                                                      setError("Please put sign in Signature1 !")
                                                      setModalOpen(true)
                                                    }
                                                    else{
                                                      handleSubmit(data, dispatch, { form })
                                                    }
                                                  }} />
                                                  : consentName == "consent20" ?
                                                    <Consent20 
                                                    initialValues={initial}onSubmit={(data, dispatch, { form }) =>{
                                                      if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                                                        setError("Please put sign in Signature1 !")
                                                        setModalOpen(true)
                                                      }
                                                      else  if((data.signature2 == undefined) || (Object.keys(data.signature2).length == 0) ){
                                                        setError("Please put sign in Signature2 !")
                                                        setModalOpen(true)
                                                      }
                                                      else{
                                                        handleSubmit(data, dispatch, { form })
                                                      }
                                                    }} />
                                                    : consentName == "consent21" ?
                                                      <Consent21 onSubmit={(data, dispatch, { form }) =>{
                                                        if(data.signature1 == undefined){
                                                          setError("Please put sign in Signature1 !")
                                                          setModalOpen(true)
                                                        }
                                                        else{
                                                          handleSubmit(data, dispatch, { form })
                                                        }
                                                      }} />
                                                      : consentName == "consent1a6" ?
                                                      <Consent1a6 initialValues={initial}onSubmit={(data, dispatch, { form }) =>{
                                                        if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                                                          setError("Please put sign in Signature1 !")
                                                          setModalOpen(true)
                                                        }
                                                        else{
                                                          handleSubmit(data, dispatch, { form })
                                                        }
                                                      }} />
                                                      
                                                      : consentName == "consent23" ?
                                                      <Consent23 
                                                      initialValues={initial}onSubmit={(data, dispatch, { form }) =>{
                                                        if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                                                          setError("Please put sign in Signature1 !")
                                                          setModalOpen(true)
                                                        }
                                                        else if((data.signature2 == undefined) || (Object.keys(data.signature2).length == 0) ){
                                                          setError("Please put sign in Signature2 !")
                                                          setModalOpen(true)
                                                        }
                                                        else{
                                                          handleSubmit(data, dispatch, { form })
                                                        }
                                                      }} />
                                                      : consentName == "consent24" ?
                                                      <Consent24 
                                                      initialValues={initial}onSubmit={(data, dispatch, { form }) =>{
                                                        if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                                                          setError("Please put sign in Signature1 !")
                                                          setModalOpen(true)
                                                        }
                                                        else{
                                                        handleSubmit(data, dispatch, { form })
                                                        }
                                                        }} />
                                                      : consentName == "consent25" ?
                                                        <Consent25 initialValues={initial}
                                                        onSubmit={(data, dispatch, { form }) =>{
                                                          if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                                                            setError("Please put sign in Signature1 !")
                                                            setModalOpen(true)
                                                          }
                                                          else{
                                                          handleSubmit(data, dispatch, { form })
                                                          }
                                                          }} />
                                                      : consentName == "consentletters1" ?
                                                      <ConsentLetters1 
                                                      initialValues={initial}
                                                      onSubmit={(data, dispatch, { form }) =>{
                                                        if((data.signature1 == undefined) || (Object.keys(data.signature1).length == 0) ){
                                                          setError("Please put sign in Signature1 !")
                                                          setModalOpen(true)
                                                        }
                                                        else{
                                                          handleSubmit(data, dispatch, { form })
                                                        }
                                                      }} />
                                                      : (consentName == "consentload") ?
                                                      <Upload />
                       : null}
     </Grid> : null}
     </Grid>
   )
 
 }
 
 export default Forms;