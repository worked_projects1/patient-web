/**
 * 
 * Home
 * 
 */



 import React, { useEffect, useState } from 'react';
 import api from '../utils/api';
 import AppBar from '@mui/material/AppBar';
 import Toolbar from '@mui/material/Toolbar';
 import SVG from 'react-inlinesvg';
 import logo from './img/Logo.png';
 import PatientConsultation from './PatientConsultation';
 import PatientForm from './onlineRegistration/index';
 import Verification from './otpVerification/index';
 import Forms from './Forms'
 import {setAuthToken} from '../utils/api'
 
 var CryptoJS = require("crypto-js");
 
 
 function Home() {
   const [busy, setBusy] = useState(false);
   const [error, setError] = useState(false);
   const [encryptedBase64Key, setBase64Key] = useState(false);
   const text = window.location.href;
   const confirmAppointment = text.split('confirmAppointment/');
   const onlineform = text.split('onlineRegistration/');
   const consentForm = text.split('consent');
   const consentname = text.substring(text.indexOf("consent"), text.length);
   const name = consentname.substring(0, consentname.indexOf('/'));
   const saas = text.split('saas');
   const encryptedCipherText = text.includes("consent") ?
   consentForm[1].substring(consentForm[1].indexOf('/') + 1, consentForm[1].length) : text.includes("onlineRegistration") ? onlineform[1] : text.includes("confirmAppointment") ? confirmAppointment[1] : text.includes("saas") ? saas[1] :  null;
   useEffect(() => {
     setBusy(true);
     setAuthToken(null);
     api.get(`/getPEK`).then((response) => {
       setBase64Key(response && response.data);
     }).catch(err => setError('Failed to load!')).finally(() => setBusy(false));
   }, [api, setBase64Key, setBusy, setError])
 
   const decryptKey = () => {
     const parsedBase64Key = CryptoJS.enc.Base64.parse(encryptedBase64Key)
     const decryptedData = CryptoJS.AES.decrypt(encryptedCipherText, parsedBase64Key,
       {
         mode: CryptoJS.mode.ECB,
         padding: CryptoJS.pad.Pkcs7
       })
 
     const decryptedText = decryptedData.toString(CryptoJS.enc.Utf8)
     return decryptedText;
   }

   return (
     <div style={{ width: '100%' }}>
       <AppBar position="sticky">
         <Toolbar>
           <img src={logo} width="200px" />
         </Toolbar>
       </AppBar>
       {/* {busy && 'Loading...' || encryptedBase64Key && encryptedCipherText && <Verification decryptedText={decryptKey()} name={name} /> || null} */}
       {busy && 'Loading...' || encryptedBase64Key && encryptedCipherText && <Verification decryptedText={decryptKey()} name={name} /> || null}
     </div>
   );
 }
 export default Home;
