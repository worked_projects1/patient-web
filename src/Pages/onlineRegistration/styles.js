
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({

    button: {
        color: 'white !important',
        fontSize: '16px',
        '&:hover': {
            background: '#ea6225;',
        }
    },
    body: {
        backgroundColor:'#98999a',
        height: '650px',
    },
    formTitle: {
        fontSize: '20px',
        fontWeight: '700',
    },
    EditForm: {
        textAlign: 'center',
        background: '#818284!important',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: '20px',
        color: '#fff',
        fontWeight: '700'
    },
    mainDiv: {
        background: '#fff',
        margin: '0px 10px',
    },
    formActions: {
        fontWeight: '400',
    },
    btn: {
        color: '#fff!important',
        fontSize: '16px!important',
        border: 'none!important',
        textTransform: 'unset!important',
        '& :hover, :active, :focus': {
            color: '#fff!important',
            fontSize: '16px!important',
            border: 'none!important',
            textTransform: 'unset!important',
        }
      },
      link: {
        textDecoration: 'none',
      },
    dialog: {
        border: 'none',
        padding: '25px',
        minWidth: '40%',
        outline: 'none',
        width: '75%',
    },
    dialogContent: {
        fontSize: "20px",
        padding: '25px',
        ['@media (max-width: 480px)']: {
            fontSize: '16px !important',

        },
    },
    dialogAction: {
        justifyContent: 'center'
    },
    warning: {
        width: '100px',
        height: '60px',
        color: '#ea6225',
    },
    warningDiv: {
        textAlign: 'center'
    },
}));

export default useStyles;







