
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import { Grid, Alert, Link, Button } from '@mui/material';
import styles from './styles.css';
import { Map as iMap } from "immutable";
import ImageEditor from '../ImageEditor';
import { capitalize, getContentType, getFileType } from './utils';
import remotes from '../../../../Blocks/FileUpload/remotes';
//import Gallery from '../Gallery';
import { getFileName, getUploadDate } from '../Gallery/utils';
import style from '../Gallery/styles.css';
import pdf from '../../../../img/pdf3.svg';
import logo from '../../../../img/AddPhoto.svg';
import DeleteButton from '../DeleteButton/index';
import SVG from 'react-inlinesvg';


const entityNames = {
  images: 'image/PDF',
  videos: 'video',
  document: 'document',
  thumbnail: 'thumbnail',
  audios: 'Audio/Video',
};

const StatusInfo = ({ uploadInStart, uploadFailed, uploadLimit, uploadCompleted, uploadInProgress, entity, percentage, loaded, total }) => (
  <Grid id="alert" className={styles.statusInfo} style={{ whiteSpace: 'break-spaces' }}>
    {uploadInStart ? <Alert info>{capitalize(entity)} upload in progress.</Alert> : null}
    {uploadFailed ? <Alert danger>{capitalize(entity)} upload failed.</Alert> : null}
    {uploadLimit ? <Alert danger> Maximum {capitalize(entity)} size allowed 1.2 GB only.</Alert> : null}
    {uploadCompleted ? <Alert success>{capitalize(entity)} uploaded successfully. Submit the form to confirm changes.</Alert> : null}
    {uploadInProgress ? <Alert info>{percentage}% completed. upload in progress. <br /> {`Uploaded ${((loaded / 1024) / 1024).toFixed(2)} MB of ${((total / 1024) / 1024).toFixed(2)} MB`}</Alert> : null}
  </Grid>)



const FileView = ({ name, uploadDate, type, preview, handleOpen }) => {
  switch (type) {
    case 'image':
      return <Grid className={style.previewImg} onClick={handleOpen}>
        <img src={preview} responsive width='95px' style={{ height: '95px' }} />
        {/* <span className={styles.uploadDate}>{uploadDate || ''}</span>*/}
      </Grid>
    default:
      return <Grid className={style.previewFile} onClick={handleOpen}>
        <Grid>
          <SVG src={pdf} className={style.img} responsive style={{ height: '95px' }} />
          {/*<span>{name.length > 25 ? name.slice(0, 25) + '...' : name}</span>*/}
        </Grid>
        {/*<span className={styles.uploadDate}>{uploadDate || ''}</span>*/}
      </Grid>
  }
}

class ImageUpload extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      photoIndex: 0,
      isOpen: false,
      files: [],
      uploadInStart: false,
      uploadInProgress: false,
      uploadCompleted: false,
      uploadLimit: false,
      uploadFailed: false,
      publicUrl: false,
      percentage: 0,
      loaded: 0,
      total: 0
    };
    this.handleDrop = this.handleDrop.bind(this);
    this.handleCrop = this.handleCrop.bind(this);
    this.onUploadStart = this.onUploadStart.bind(this);
    this.onUploadProgress = this.onUploadProgress.bind(this);
    this.onUploadFailed = this.onUploadFailed.bind(this);
    this.onUploadCompleted = this.onUploadCompleted.bind(this);
    this.OnUploadDelete = this.OnUploadDelete.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handlePrev = this.handlePrev.bind(this);
  }

  render() {
    const { input, type, contentType, label, data, renderMode, formId, showtype } = this.props;
    const { openCrop, acceptedFiles, uploadInStart, uploadInProgress, uploadCompleted, isOpen, photoIndex } = this.state;
    const { attributes = {} } = data;
    const { singleImage } = attributes;
    const Input = iMap(input).toJS();
    const entity = data.imageLabel ? 'image/PDF/Video' : entityNames[type];
    const uniqueID = new Date().getUTCMilliseconds();
    const accept = (type === 'images') ? {} : { accept: contentType }
    let imageData = Input.value && Array.isArray(Input.value) && Input.value.length > 0 ? Input.value : Input.value && typeof Input.value === 'string' ? Input.value.split(',') : [];

    return (
      <Grid xs={12} className={styles.ImageUpload} style={{  }}>
        <StatusInfo {...this.state} entity={entity} />
        {!uploadInStart && !uploadInProgress && !uploadCompleted ?
          <Grid id="files" xs={12} style={{ display: "flex", overflowX: "auto", width: '85vw' }} className={showtype === 'button' ? style.galleryBtn : style.gallery}>
            {imageData.map((preview, index) => {
              const name = getFileName(preview);
              const uploadDate = getUploadDate(preview);
              const type = getFileType(preview);

              return <Grid style={{ marginLeft: '20px', marginTop: '60px' }} key={`${type}-${index}-${uniqueID}`} className={showtype ? style.showicon : style.preview}>
                <FileView
                  name={name}
                  type={type}
                  uploadDate={uploadDate}
                  preview={preview}
                  handleOpen={() => this.handleOpen(index)} />
                <DeleteButton type={type} preview={preview} OnDelete={(preview) => this.OnUploadDelete.bind(this, preview)} />
              </Grid>
            })}
            {singleImage && imageData.length > 0 ? null :
              <Dropzone onDrop={this.handleCrop.bind(this)} id={`dropzone-${uniqueID}`}>
                {({ getRootProps, getInputProps }) => (
                  <section>
                    <div className={styles.space} {...getRootProps()}>
                      <input {...getInputProps()} />
                      <Grid style={{ background: '#EFEFEF', paddingBottom: '12px', marginTop: '3.5em', marginBottom: '2em', paddingLeft: '12px', paddingRight: '7px', marginLeft: '20px' }} className={style.upload}>
                        <SVG src={logo} className={style.img} style={{ height: '95px' }} />
                        <Grid style={{ textTransform: 'capitalize', fontSize: '13px' }}>{`Add ${entity}`}</Grid>
                      </Grid>
                    </div>
                  </section>
                )}
              </Dropzone>}
          </Grid> : null}
        {acceptedFiles && openCrop ? this.handleDrop(acceptedFiles) : null}
        {/*{acceptedFiles && openCrop ? <ImageEditor acceptedFiles={acceptedFiles} onClose={this.closeCrop.bind(this)} onSubmit={this.handleDrop.bind(this)} /> : null}*/}
      </Grid>
    );
  }
  /**
* Tries to getSignature() and uploadFile(). Calls onUploadFailed() and onUploadCompleted() depending on outcomes.
* @param { Array } acceptedFiles
*/
  handleCrop(acceptedFiles) {
    const name = acceptedFiles[0].name.replace(/[^a-zA-Z.0-9]/g, "");
    const type = getFileType(name);
    if (type === 'image') {
      this.setState({
        openCrop: true,
        acceptedFiles
      })
    } else {
      this.handleDrop(acceptedFiles);
    }
  }

  handleOpen(i) {
    const { input } = this.props;
    const Input = iMap(input).toJS();
    let data = Input.value && Array.isArray(Input.value) && Input.value.length > 0 ? Input.value : Input.value && typeof Input.value === 'string' ? Input.value.split(',') : []
    const file = data[i] || '';
    const type = getFileType(file);
    window.open(file && file.url ? file.url : file, '_blank');
  }

  handleClose() {
    this.setState({ isOpen: false });
  }

  handlePrev(nextImage) {
    this.setState({ photoIndex: nextImage });
  }

  handleNext(prevImage) {
    this.setState({ photoIndex: prevImage });
  }

  closeCrop() {
    this.setState({
      openCrop: false,
      acceptedFiles: false
    })
  }

  /**
   * Tries to getSignature() and uploadFile(). Calls onUploadFailed() and onUploadCompleted() depending on outcomes.
   * @param { Array } acceptedFiles
   */
  handleDrop(acceptedFiles) {
    this.closeCrop();
    const { uploadInStart, uploadInProgress } = this.state;
    const fileSize = acceptedFiles[0] ? ((acceptedFiles[0]['size'] / 1024) / 1024).toFixed(2) : 0;
    const name = acceptedFiles[0].name.replace(/[^a-zA-Z.0-9]/g, "");
    const fileContentType = getContentType(name);

    if (uploadInStart || uploadInProgress) {
      return false;
    } else if (acceptedFiles.length == 0) {
      this.onUploadFailed();
      return false;
    } else if (fileSize > 1200) {
      this.onUploadLimit();
      return false;
    }

    this.onUploadStart();
    const ajax = new XMLHttpRequest();
    ajax.upload.addEventListener("progress", this.onUploadProgress, false);
    ajax.addEventListener("load", this.onUploadCompleted, false);
    ajax.addEventListener("error", this.onUploadFailed, false);
    ajax.addEventListener("abort", this.onUploadFailed, false);
    remotes.getSignature(name, fileContentType)
      .then((result) => {
        const { uploadUrl, publicUrl } = result;
        if (uploadUrl && publicUrl) {
          this.setState({ publicUrl: publicUrl })
          ajax.open("PUT", uploadUrl, true);
          ajax.setRequestHeader("Content-Type", fileContentType);
          ajax.send(acceptedFiles[0]);
        } else {
          this.onUploadFailed();
        }
      })
      .catch(() => {
        this.onUploadFailed();
      });
  }

  /**
   * Set upload start in local state.
   *
   */
  onUploadStart() {
    this.setState({
      uploadInStart: true,
      uploadInProgress: false,
      uploadFailed: false,
      uploadLimit: false,
      uploadCompleted: false,
    });
  }

  /**
   * Set upload progress in local state.
   * @param { Array } files
   */
  onUploadProgress(event) {
    this.setState({
      uploadInStart: false,
      uploadInProgress: true,
      uploadFailed: false,
      uploadLimit: false,
      uploadCompleted: false,
      percentage: Math.round((event.loaded / event.total) * 100),
      loaded: event.loaded,
      total: event.total,
    });
  }

  /**
   * Set upload error in local state.
   */
  onUploadFailed() {
    this.setState({
      uploadInStart: false,
      uploadInProgress: false,
      uploadFailed: true,
      uploadLimit: false,
      uploadCompleted: false
    });
    this.successAlertTimeout = setTimeout(() => {
      this.setState({
        uploadFailed: false
      });
    }, 3000);
  }

  /**
   * Set upload limit in local state.
   */
  onUploadLimit() {
    this.setState({
      uploadInStart: false,
      uploadInProgress: false,
      uploadFailed: false,
      uploadLimit: true,
      uploadCompleted: false
    });

    this.successAlertTimeout = setTimeout(() => {
      this.setState({
        uploadLimit: false,
      });
    }, 3000);
  }

  /**
   * Set upload success in local state.
   */
  onUploadCompleted() {
    const { input, contentType } = this.props;
    const Input = iMap(input).toJS();
    const inputValue = Input.value && Array.isArray(Input.value) && Input.value.length > 0 ? Input.value : Input.value && typeof Input.value === 'string' ? Input.value.split(',') : [];
    inputValue.push(contentType.indexOf('video') > -1 ? { url: this.state.publicUrl, thumbnail: 'https://s3.amazonaws.com/novadontics-uploads/dataurifile1584343276879-1584343277318.png' } : this.state.publicUrl);
    input.onChange(inputValue);

    this.setState({
      uploadInStart: false,
      uploadInProgress: false,
      uploadCompleted: true,
      uploadLimit: false,
      uploadFailed: false,
    });

    this.successAlertTimeout = setTimeout(() => {
      this.setState({
        uploadCompleted: false,
      });
    }, 2000);

  }

  /**
   * Set upload delete in local file.
   */
  OnUploadDelete(preview) {
    const { input, contentType } = this.props;
    const Input = iMap(input).toJS();
    const inputValue = Input.value && Array.isArray(Input.value) && Input.value.length > 0 ? Input.value : Input.value && typeof Input.value === 'string' ? Input.value.split(',') : [];
    input.onChange(inputValue.filter((val) => contentType.indexOf('video') > -1 ? val.url != preview.url : val != preview))
  }
}



export default ImageUpload;
