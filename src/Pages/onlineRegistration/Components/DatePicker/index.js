import React from 'react';
import PropTypes from 'prop-types';
import DatePicker from "react-datepicker";
import moment from 'moment';
import "react-datepicker/dist/react-datepicker.css";
import styles from './styles.css';

class Datepicker extends React.Component {
  static propTypes = {
    input: PropTypes.object,
   };

  constructor(props) {
    super(props);
    this.state = { startDate: false };
    this.handleDate = this.handleDate.bind(this);
    //this.initialDate = this.initialDate.bind(this);
  }

  componentDidMount() {
    const { input, type } = this.props;
  }
  
  

  render() {
    const { startDate } = this.state;
    const { input, type, title, data, flexStyle, disabled, label } = this.props;
    return (<div className={'DatePicker1'}>
        <label className={"label"}>
          {label ? label : ''}
          {data.required ? <span style={{color: "#ea6225", fontSize: "18px"}}>*</span> : null}
        </label>
        {/* <div className={"Field"}> */}
        <DatePicker 
        selected={startDate ? startDate : input.value ? moment.utc(input.value) :  null }
        format={type === 'calendar' ? 'DD MMM, YYYY' : 'MM/DD/YYYY'}    
        container="container"
        peekNextMonth
        showMonthDropdown
        showYearDropdown   
        dropdownMode="select"
        autoComplete="off"
        readOnly={disabled}
        // customInput={<TextField className={styles.Field} style={{width: '100%'}} error={false} variant="standard" />}
        className={type === 'calendar' ? styles.DatePickerWithNotes : styles.DatePicker}
        onChange={(selected) => this.handleDate(selected)}
      />
       {/* </div> */}
      </div>);
           
  }


  handleDate(selected) {
    const { input, type } = this.props;
    this.setState({ startDate: selected });
    if(selected != "" && selected != null) {
      const pickedDate = moment(selected).format(type === 'calendar' ? 'DD MMM, YYYY' : 'MM/DD/YYYY');
    input.onChange(pickedDate);
    } else {
      input.onChange("");
    }
  }


}

export default Datepicker;