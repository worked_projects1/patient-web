/**
 * 
 * Progress Provider
 * 
 */

 import React from "react";

 class ChangingProgressProvider extends React.Component {
   static defaultProps = {
     interval: 1000
   };
 
   state = {
     valuesIndex: 0
   };
 
   componentDidMount() {
     this.progressInterval = setInterval(() => {
       this.setState({
         valuesIndex: (this.state.valuesIndex + 1) % this.props.values.length
       });
     }, this.props.interval);
   }
 
   componentDidUpdate(oldProps) {
     if (this.props.clearInterval) {
       clearInterval(this.progressInterval);
     } else if (oldProps.changeInterval != this.props.changeInterval) {
       this.setState({ valuesIndex: 0 });
       clearInterval(this.progressInterval);
       this.progressInterval = setInterval(() => {
         this.setState({
           valuesIndex: (this.state.valuesIndex + 1) % this.props.values.length
         });
       }, this.props.interval);
     } else if (oldProps.values.length != this.props.values.length) {
       this.progressInterval = setInterval(() => {
         this.setState({
           valuesIndex: (this.state.valuesIndex + 1) % this.props.values.length
         });
       }, this.props.interval);
     } else if (this.props.values[this.state.valuesIndex] === this.props.values[this.props.values.length - 1]) {
       clearInterval(this.progressInterval);
     }
   }
 
   render() {
     return this.props.children(this.props.values[this.state.valuesIndex]);
   }
 }
 
 export default ChangingProgressProvider;
 