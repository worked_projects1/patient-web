
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  checkboxField: {
    width: '100%',
  },
  radioLabel: {
    fontSize: '14px',
    marginRight: '14px!important'
  },
  label: {
    fontSize: '18px',
    marginTop: '1em',
    display: 'block',
    lineHeight: '30px',
    fontWeight: '400'
  },
}));


export default useStyles;