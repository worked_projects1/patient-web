import React, { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import { Grid } from '@mui/material';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import Warning from '@mui/icons-material/Warning';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';


function Info (props) {
  const { info, showInfo, title, onClose } = props;
  return (
    <Dialog
      open={showInfo}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title" style={{ paddingBottom: '10px' }}>
        <div style={{fontSize:"16px"}}> {title} </div>
        {onClose ? (
          <IconButton
            aria-label="close"
            onClick={onClose}
            sx={{
              position: 'absolute',
              right: 8,
              top: 8,
              color: (theme) => theme.palette.grey[500],
            }}
          >
            <CloseIcon style={{ width: '15px', height: '15px'}} />
          </IconButton>
        ) : null}
      </DialogTitle>
      <DialogContent style={{ paddingBottom: '10px' }}>
        <DialogContentText id="alert-dialog-description">
          <span dangerouslySetInnerHTML={{ __html: info.text }} />
        </DialogContentText>
      </DialogContent>
      <DialogActions />
    </Dialog>
  );
}
export default Info;
