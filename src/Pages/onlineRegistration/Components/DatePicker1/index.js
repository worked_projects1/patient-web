/**
 * 
 * Date Picker
 * 
 */
import React,{useState} from 'react';
import Styles from './styles';
import moment from 'moment';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import MobileDatePicker from '@mui/lab/MobileDatePicker';
import {TextField, label} from '@mui/material';

export default function ({ input, label, autoFocus, disabled, required, meta: { touched, error } }) {
  const classes = Styles();
  const { id, name, onChange } = input;
  const [selectedDate, handleDateChange] = useState("");
  let value = moment(input.value).format("MM-DD-YYYY");

  function handleChange(val){
    handleDateChange(moment(val).format("MM-DD-YYYY"));
    value = moment(val).format("MM-DD-YYYY");
  }
   return (
    <div className={classes.checkboxField}>
    <label className={classes.label}>{label ? label : ''}</label>
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <MobileDatePicker
        className={classes.fieldColor}
        key={id}
        margin="normal"
        name={name}
        onChange={(val) => onChange(moment(val).format("MM/DD/YYYY"))}
        value={value}
        format="MM/dd/yyyy"
        disabled={disabled}
        renderInput={(params) => <TextField {...params} style={{width: '100%'}} error={false} variant="standard" className={classes.Field}/>}
        toolbarPlaceholder={`${moment().format("YYYY-MM-DD")}`}
      />
    </LocalizationProvider>
  </div>
   );
 }
