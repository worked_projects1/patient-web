
/***
 * 
 * Input Field
 * 
 */


 import React, { useState } from 'react';
 import TextField from '@mui/material/TextField';
 import styles from './styles';
 
 export default function InputField(props) {
     const { input, label, title, data = {}, autoFocus, type, defaultBlur, className, meta: { touched, error, warning }, initialValue } = props;
     const [initialData, setInitialData] = useState(true);

     const classes = styles();
     const { name, value, onChange, required } = input;
     const InputChange = defaultBlur ? Object.assign({}, {
        onBlur: (e) => onChange(e.target.value),
        defaultValue: value || ''
      }) : Object.assign({}, {
        onChange: (e) => onChange(e.target.value),
        value: value || ''
      });
     if (input.value === '' && initialData && initialValue && initialValue[name]) {
       input.onChange(initialValue[name]);
       setInitialData(false);
     }

     return (
         <div>
            <label className={classes.label}>
              {label ? label : title ? title : ''}
              {data.required ? <span style={{color: "#ea6225", fontSize: "18px"}}>*</span> : null}
            </label>
             <TextField
                 className={classes.fieldColor && classes.Field}
                 name={name}
                 type={type}
                 fullWidth
                 value={value || ''}
                 {...InputChange}
                 inputProps={{
                   maxLength: data.limit
                 }}
                 autoFocus={autoFocus}
                 variant="standard"
                 required={data.required ? true : false}
             />
             <div className={classes.error}>{touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</div>
         </div>
 
     )
 }