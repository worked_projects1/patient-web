/**
 * 
 * Select Field
 * 
 */

 import React from 'react';
 import styles from './styles';
 import { FormControl, InputLabel, Select, MenuItem } from '@mui/material';
 
 export default function selectField({ input, label, title, required, metaData, options, variant, disabled, children, style, meta: { touched, error, warning, data}, initialValue }) {

     const classes = styles();
 
     const { name, value } = input;
     const isPreDefinedSet = Array.isArray(options);
     if (input.value === '' && initialValue && initialValue[name]) {
       input.onChange(initialValue[name]);
     }
     return (
         <div className={classes.selectField} style={style || {}}>
             {children || ''}
             {/* <FormControl variant={variant || null} className={classes.formControl}> */}
                <label className={classes.label}>
                    {label ? label : title ? title : ''}
                    {required ? <span style={{color: "#ea6225", fontSize: "18px"}}>*</span> : null}
                 </label>
                 <Select
                     name={name}
                     fullWidth
                     disabled={disabled}
                     required={required}
                     value={value}
                     onChange={(e) => {input.onChange(e.target.value)}}
                     className={classes.Field}
                     variant="standard"
                     placeholder="Choose Option"
                     displayEmpty>
                    <MenuItem value="" disabled={input.value ? true : false } style={{color: 'black'}}>Choose Option</MenuItem>
                    {isPreDefinedSet ? (options || []).map((opt, index) =>{
                         return <MenuItem
                        key={index} disabled={opt.disabled || false} value={opt && opt.value ? opt.value : opt.name} style={{color: 'black'}}>{opt && opt.label ? opt.label : opt.title}</MenuItem>}) :
                         (metaData[options] || []).map((opt, index) =>{ return <MenuItem key={index} disabled={opt.disabled || false} value={opt && opt.value != null
                         && opt.value || opt}>{opt && opt.label != null && opt.label || opt}</MenuItem>})
                     }
                 </Select>
             {/* </FormControl> */}
             <div className={classes.error}>{touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</div>
         </div>
     )
 }
 
 