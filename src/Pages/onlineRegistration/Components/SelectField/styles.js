

import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  formControl: {
    width: '100%'
  },
  error: {
    fontSize: '14px',
    color: 'red'
  },
  fieldColor: {
    fontSize: '14px',
    '& label.Mui-focused': {
      color: '#2ca01c',
    },
    '&.MuiFormLabel-root.Mui-focused': {
      borderColor: '#2ca01c',
      color: '#2ca01c'
    }
  },
  selectField: {
    // '& .MuiInput-underline:after': {
    //   borderBottomColor: '#2ca01c',
    // },
    // '& .MuiInput-underline:before': {
    //   borderBottomColor: '#2ca01c',
    // },
    // '@global': {
    //   '.MuiOutlinedInput-notchedOutline': {
    //     border: 'none'
    //   }
    // }
  },
  label: {
    fontSize: '18px',
    marginTop: '1em',
    display: 'block',
    lineHeight: '30px',
    fontWeight: '400'
  },
  Field: {
    height: '50px',
    fontSize: '16px!important',
    alignItems: 'end!important',
    borderBottom: '1px solid #c8c8c8!important',
    '& :before, :active, :focus': {
      border: 'none!important',
      borderBottom: '1px solid #c8c8c8!important',
    },
    '& :after, :active, :focus':{
      border: 'none!important',
    },
    '& div:hover': {
      border: 'none!important',
      borderBottom: '1px solid #c8c8c8!important',
    },
    '& div:before, :active, :focus': {
      border: 'none!important',
      borderBottom: '1px solid #c8c8c8!important',
    },
    '& div:after, :active, :focus':{
      border: 'none!important',
    },
    '& div': {
      // height: '50px!important',
      color: 'black',
      background: 'none!important',
    },
    '& input': {
      color: 'black',
    },
    root: {
      border: 'none!important',
      "& :hover": {
        border: 'none!important',

      }
  }
  }
}));


export default useStyles;