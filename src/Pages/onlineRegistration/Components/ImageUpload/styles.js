

import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  progress: {
    height: '120px',
    width: '120px'
  },
  circle: {
    borderRadius: '50%',
    border: '1px solid #2ca01c',
    width: '130px',
    height: '130px',
    //boxShadow: theme.shadows[2],
    position: 'absolute',
    opacity: '0',
    top: '0',
    animation: `$scaleIn 4s infinite cubic-bezier(.36, .11, .89, .32)`,
  },
  '@keyframes scaleIn': {
    "from": {
      transform: 'scale(.5, .5)',
      opacity: '.5'
    },
    "to": {
      transform: 'scale(1.5, 1.5)',
      opacity: '0'
    }

  },
  container: {
    justifyContent: 'center',
    textAlign: 'center',
    marginTop: '25px',
    outline: 'none',
    position: 'relative'
  },
  upload: {
    boxShadow: 'none',
    width: '130px',
    height: '130px',
    zIndex: '100',
    padding: '5px',
    outline: 'none',
    display: 'inline-block',
    borderRadius: '100%',
    position: 'relative'
  },
  icon: {
    color: '#2ca01c',
    position: 'relative',
    display: 'flex',
    width: '100%',
    fontSize: '50px'
  },
remove: {
  position: 'relative',
  top: '5px',
  width: '0',
  height: '0',
  right: '-60px',
  cursor: 'pointer'
},
  button: {
    fontWeight: 'bold',
    borderRadius: '28px',
    textTransform: 'none',
    fontFamily: 'Avenir-Regular',
    marginTop: '10px',
    outline: 'none'
  },
  animate: {
    display: 'flex',
    width: '100%',
    justifyContent: 'center'
  },
  image: {
    width: '100px',
    height: '100px',
    display: 'inline-block',
    borderRadius: '100%',
  },
  percentage: {
    color: '#2ca01c',
    fontWeight: 'bold',
    fontSize: '18px'
  },
  template: {
      color: 'gray',
      cursor: 'pointer',
      fontSize: '0.84rem'
  },
  description: {
      color: '#2ca01c'
  }
}));


export default useStyles;