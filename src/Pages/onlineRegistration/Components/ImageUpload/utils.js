/**
 * 
 * Utils
 * 
 */

 import React, { useState } from 'react';
 import styles from './styles';
 import CloseIcon from '@mui/icons-material/Close';
 import { Backdrop, Modal, Paper, Fade, Grid, Typography, Button } from '@mui/material';
 import SVG from 'react-inlinesvg';
 import logo from '../../../../img/trash.svg';
 
 export function getSteps() {
   return ['Uploading Document', 'Converting Document', 'Creating Template'];
 }
 
 /**
  * 
  * @param {string} name 
  * @returns 
  */
 export function getContentType(name) {
  
   const extension = name && name.substring(name.lastIndexOf('.') + 1).toLowerCase() || '';
   if (extension === 'pdf') {
     return 'application/pdf'
   } else if (extension === 'mp4') {
     return 'video/mp4'
   } else if (extension === 'gif') {
     return 'image/gif'
   } else if (extension === 'png') {
     return 'image/png'
   } else if (extension === 'ico') {
     return 'image/x-icon'
   } else if (extension === 'jng') {
     return 'image/x-jng'
   } else if (extension === 'bmp') {
     return 'image/x-ms-bmp'
   } else if (extension === 'webp') {
     return 'image/webp'
   } else if (extension === 'wbmp') {
     return 'image/vnd.wap.wbmp'
   } else if (extension === 'jpeg' || extension === 'jpg') {
     return 'image/jpeg'
   } else if (extension === 'svg' || extension === 'svgz') {
     return 'image/svg+xml'
   } else if (extension === 'tif' || extension === 'tiff') {
     return 'image/tiff'
   } else if (extension === '3gpp' || extension === '3gp') {
     return 'video/3gpp'
   } else if (extension === 'mpeg' || extension === 'mpg') {
     return 'video/mpeg'
   } else if (extension === 'asx' || extension === 'asf') {
     return 'video/x-ms-asf'
   } else if (extension === 'mov') {
     return 'video/quicktime'
   } else if (extension === 'ogg') {
     return 'video/ogg'
   } else if (extension === 'wmv') {
     return 'video/x-ms-wmv'
   } else if (extension === 'webm') {
     return 'video/webm'
   } else if (extension === 'flv') {
     return 'video/x-flv'
   } else if (extension === 'avi') {
     return 'video/x-msvideo'
   } else if (extension === 'm4v') {
     return 'video/x-m4v'
   } else if (extension === 'mng') {
     return 'video/x-mng'
   } else if (extension === 'doc' || extension === 'dot') {
     return 'application/msword'
   } else if (extension === 'docx') {
     return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
   } else if (extension === 'dotx') {
     return 'application/vnd.openxmlformats-officedocument.wordprocessingml.template'
   } else if (extension === 'docm') {
     return 'application/vnd.ms-word.document.macroEnabled.12'
   } else {
     return extension;
   }
 }
 
 /**
  * 
  * @param {integer} step 
  * @returns 
  */
 export function getStepContent(step) {
   switch (step) {
     case 0:
       return `For each ad campaign that you create, you can control how much
                 you're willing to spend on clicks and conversions, which networks
                 and geographical locations you want your ads to show on, and more.`;
     case 1:
       return 'An ad group contains one or more ads which target a shared set of keywords.';
     case 2:
       return `Try out different ad text to see what brings in the most customers,
                 and learn how to enhance your ads using features like ad extensions.
                 If you run into any problems with your ads, find out how to tell if
                 they're running and how to resolve approval issues.`;
     default:
       return 'Unknown step';
   }
 }
 
 export function FilesList({ files, className, style, onDelete, children }) {
   const classes = styles();
   const [showModal, setModalOpen] = useState(false);
 
   const handleClose = () => {
     setModalOpen(false);
   }
   
   return <Grid container className={className} style={style}>
     {children && children(() => setModalOpen(!showModal))}
     <Modal
       aria-labelledby="transition-modal-title"
       aria-describedby="transition-modal-description"
       open={showModal || false}
       className={classes.modal}
       onClose={handleClose}
       closeAfterTransition
       BackdropComponent={Backdrop}
       BackdropProps={{
         timeout: 500,
       }}>
       <Fade in={showModal}>
         <Paper className={classes.paper} style={{ maxWidth: '50%' }}>
           <Grid container className={classes.header} justify="flex-end">
             <CloseIcon onClick={handleClose} className={classes.closeIcon} />
           </Grid>
           <Grid className={classes.gridList}>
             {files && Array.isArray(files) && files.length > 0 && files.map((file, index) => {
               return <Grid key={index} container className={classes.gridContainer}>
                 <Grid item xs={10} className={classes.fileName}>
                   <Typography variant="subtitle1" >{file.name}</Typography>
                 </Grid>
                 <Grid item xs={2} className={classes.gridDelete}>
                   <SVG src={logo} className={classes.attachDelete} onClick={() => onDelete(index)} />
                 </Grid>
                 <Grid item xs={12} ><hr className={classes.hr} /></Grid>
               </Grid>
             }) || <Grid>No Files Found.</Grid>}
           </Grid>
         </Paper>
       </Fade>
     </Modal>
   </Grid>
 
 }