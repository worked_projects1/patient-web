/*
 Image Upload 
 */


 import React, { useCallback, useState, useRef } from 'react';
 import PropTypes from 'prop-types';
 import { useDropzone } from 'react-dropzone';
 import PublishIcon from '@mui/icons-material/Publish';
 import ProgressProvider from '../ProgressProvider';
 import DoneIcon from '@mui/icons-material/Done';
 import styles from './styles';
 import CloseSharpIcon from '@mui/icons-material/CloseSharp';
 import remotes from '../../../../Blocks/FileUpload/remotes';
 import DescriptionIcon from '@mui/icons-material/Description';
 import { Grid, Button, Paper, Typography } from '@mui/material';
 import { CircularProgressbarWithChildren, buildStyles } from 'react-circular-progressbar';
 import { getContentType, FilesList } from './utils';
 import {capitalizeFirstLetterOnly, capitalizeFirstWord} from '../../../../utils/tools';
 
 function FileUpload({ input, contentType = '*', label, upload, multiple, max, secret, viewFiles, meta: { touched, error, warning }, errorType }) {
     const inputValue = input && input.value.length > 0 && Array.isArray(input.value) && input.value || [];
     const [uploadFiles, setuploadFiles] = useState(false);
     const [uploadError, setuploadError] = useState(false);
     const [uploadPercentage, setPercentage] = useState(false);
     const [uploadedFile, setUploadedFile] = useState(1);
     const [totalFiles, setTotalFiles] = useState(false);
 
     const inputRef = useRef(false);
     const labelValue = label === 'MODIFIED TEMPLATE' || 'Custom Document Template' ? capitalizeFirstWord(label) : capitalizeFirstLetterOnly(label);
     const accept = contentType;
 
     const onUpload = (files, index) => {
         if (!files || files.length <= index) {
             return false;
         }
 
         const ajax = new XMLHttpRequest();
         const fileName = files[index].name;
         const fileContentType = getContentType(fileName);
         remotes.getSignature(fileName, fileContentType)
             .then((result) => {
                const { uploadUrl, publicUrl } = result;
                 if (uploadUrl) {
                     ajax.upload.addEventListener("progress", (e) => setPercentage(Math.round((e.loaded / e.total) * 100)), false);
                     ajax.addEventListener("load", () => {
                         const inputVal =  publicUrl;

                         inputRef.current.push(inputVal);                       
                        
                         if ((files.length - 1) === index) {
                             input.onChange(multiple ? inputRef.current : inputRef.current[0]);
                             inputRef.current = false;
                             setUploadedFile(1);
                             setPercentage(false);
                             setTotalFiles(false);
                         } else {
                             setUploadedFile(index + 2);
                             setPercentage(false);
                             onUpload(files, index + 1);
                         }
                     }, false);
                     ajax.addEventListener("error", () => setuploadError('Upload Failed'), false);
                     ajax.addEventListener("abort", () => setuploadError('Upload Failed'), false);
                     ajax.open("PUT", uploadUrl, true);
                     ajax.setRequestHeader("Content-Type", fileContentType);
                     ajax.send(files[index]);
                 } else {
                     setuploadError('Upload Failed');
                 }
             })
             .catch(() => {
                 setuploadError('Upload Failed');
             });
     }
 
     const onDrop = useCallback((uploadedFiles) => {
         const acceptedFiles = uploadedFiles.map(file => new File([file], file.name.replace(/[^a-zA-Z.0-9]/g, ""), { type: file.type, lastModified: file.lastModified}));
 
         setuploadFiles(false);
         setuploadError(false);
         setPercentage(false);
         setUploadedFile(1);
         setTotalFiles(false);
         input.onChange(false);
         inputRef.current = false;
 
         if (max && acceptedFiles && max < acceptedFiles.length) {
             errorType ? setuploadError(`You cannot upload more than ${max} summary files`) : setuploadError(`If you need to upload more than ${max} files, do it in batches of upto ${max}`);
         } else if (upload) {
             inputRef.current = [];
             setTotalFiles(acceptedFiles.length);
             onUpload(acceptedFiles, 0);
         } else {
             setuploadFiles(acceptedFiles);
             setTimeout(() => input.onChange(acceptedFiles), 1000);
         }
     }, [setuploadFiles, onUpload, setuploadError, upload, input, uploadFiles]);
 
     const onDropRejected = useCallback((error) => {
         setuploadError(error[0].errors[0].message);
     }, []);
 
     const handleFileDelete = (fileIndex) => {
         const Files = input.value && input.value.length > 0 && input.value.filter((e, i) => i !== fileIndex) || [];
         if(Files && Array.isArray(Files) && Files.length > 0){
             setuploadFiles(Files);
             input.onChange(Files);
         } else {
             setuploadFiles(false);
             input.onChange(false);
         }
     }
 
     const { getRootProps, open, getInputProps } = useDropzone({ onDrop, accept, multiple, onDropRejected });
     const classes = styles();
 
     return <Grid container style={{ width: '100%' }}>
         {input.value && contentType.indexOf('image') > -1 &&

             <Grid item xs={12} {...getRootProps({ className: 'dropzone' })} className={classes.container}>
                 <input {...getInputProps()} />
                 <Grid>{input && input.value && input.value.map(el => (
                    <img key={el} src={el} className={classes.image} />
                 ))}</Grid>
             </Grid> }
            <Grid item xs={12} {...getRootProps({ className: 'dropzone' })} className={classes.container}>
                 <input {...getInputProps()} />
                 <Paper className={classes.upload}>
                     <ProgressProvider values={input.value ? [100] : uploadFiles && !uploadError ? [0, 50, 100] : [0]}>
                         {percentage => (<CircularProgressbarWithChildren
                             value={uploadPercentage || percentage}
                             className={classes.progress}
                             styles={buildStyles({
                                 pathColor: '#2ca01c',
                                 pathTransition:
                                     percentage === 0
                                         ? "none"
                                         : "stroke-dashoffset 0.5s ease 0s"
                             })}>
                             {(uploadPercentage || percentage) && !input.value ? <div className={classes.percentage}>{uploadPercentage || percentage}%</div> : input.value && upload ? <DoneIcon className={classes.icon} /> : <PublishIcon className={classes.icon} />}
                         </CircularProgressbarWithChildren>)}
                     </ProgressProvider>
                 </Paper>
                {/*<Grid className={classes.animate}>
                     <div className={classes.circle} style={{ animationDelay: '0s' }} />
                     <div className={classes.circle} style={{ animationDelay: '1s' }} />
                     <div className={classes.circle} style={{ animationDelay: '2s' }} />
                 </Grid>*/}
                </Grid>
         {/*<Grid item xs={12}>
             <Grid container className={classes.container} justify="center" direction="column">
                 {!input.value ? <Typography component="span">{uploadPercentage || inputRef.current ? `Uploading ${uploadedFile} of ${totalFiles} ${label === 'MODIFIED TEMPLATE' ? labelValue : label}` : `Drag ${label === 'MODIFIED TEMPLATE' ? labelValue : label} to upload, or`}</Typography> : null}
                 {!inputRef.current ?
                     <Typography component="span">
                         <Button type="button" variant="contained" color="primary" onClick={open} className={classes.button}>
                             {input.value && `Change ${labelValue}` || `Choose ${labelValue}`}
                         </Button>
                     </Typography> : null}
                 {input.value && !upload && viewFiles ? <FilesList
                     style={{ justifyContent: 'center', marginTop: '18px' }}
                     onDelete={handleFileDelete}
                     files={input.value}>
                     {(openModal) =>
                         <Typography onClick={openModal} component="span" variant="subtitle2" className={classes.template}>
                         {`View ${labelValue} To Upload:`} <DescriptionIcon type="Description" className={classes.description} />
                     </Typography>}
                 </FilesList> : null}
             </Grid>
         </Grid>*/}
         {/*{(uploadError || (touched && (error || warning))) && !uploadFiles && !uploadPercentage && uploadedFile ? <Grid container className={classes.container}>
             <Error errorMessage={uploadError || error || warning} />
         </Grid> : null}*/}
     </Grid>
 }
 export default FileUpload;
 