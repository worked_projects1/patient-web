import React, { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import { Grid } from '@mui/material';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import SuccessIcon from '@mui/icons-material/CheckCircleOutlined';
import Warning from '@mui/icons-material/Warning';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import styles from './styles.js';


function Alert (props) {
  const { alertText, onClose, showAlert, warning, success, emailAlert } = props;
  const classes = styles();
  return (
     <Dialog
        open={showAlert}
        maxWidth={"sm"}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <DialogContent dividers>
          <DialogContentText id="alert-dialog-description" className={classes.dialogContent}>
            <Grid className={classes.warningDiv}>
              {warning ? <Warning className={classes.warning}/> : success ? <SuccessIcon className={classes.success}/> : null}
            </Grid>
            {emailAlert ?
              <span>Due to some reason OTP could not be sent. Please try again. If the problem persists contact support team <a href="mailto:team@novadontics.com" style={{ color: '#ea6225', textDecoration: 'none' }} target="_blank" data-rel="external">team@novadontics.com</a></span>
             : <span>{alertText}</span>}
          </DialogContentText>
        </DialogContent>
        <DialogActions className={classes.dialogAction} onClick={onClose}>
          <Button style={{ color: '#ea6225' }}>
            Dismiss
          </Button>
        </DialogActions>
      </Dialog>
  );
}
export default Alert;
