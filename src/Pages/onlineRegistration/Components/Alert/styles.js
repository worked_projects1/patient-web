
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    dialog: {
        border: 'none',
        padding: '25px',
        minWidth: '40%',
        outline: 'none',
        width: '75%',
    },
    dialogContent: {
        fontSize: "20px",
        padding: '25px',
        ['@media (max-width: 480px)']: {
            fontSize: '16px !important',

        },
    },
    dialogAction: {
        justifyContent: 'center'
    },
    warning: {
        width: '100px',
        height: '60px',
        color: '#ea6225',
    },
    success: {
        width: '100px',
        height: '50px',
        color: "green",
    },
    warningDiv: {
        textAlign: 'center'
    },
}));

export default useStyles;