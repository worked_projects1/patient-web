import InputField from '../InputField';
import CheckboxField from '../CheckboxField';
import TextArea from '../TextArea';
import selectField from '../SelectField';
import DatePicker from '../DatePicker';
import RadioBoxField from '../RadioBoxField';
import ImageUpload from '../FileUpload';

export const ImplementationFor = {
    input: InputField,
    number: InputField,
    checkbox: CheckboxField,
    textarea: TextArea,
    select: selectField,
    date: DatePicker,
    radiobox: RadioBoxField,
    upload:ImageUpload

};
export const ContentTypes = {
    Images: 'image/*,application/pdf,.stl',
    Videos: 'video/mp4, video/*',
    Audios: 'audio/mp3, audio/*,video/mp4,video/*'
  };

