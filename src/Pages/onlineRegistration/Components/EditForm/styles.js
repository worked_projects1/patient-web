import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({

    button: {
        color: 'white !important',
        fontSize: '16px',
        '&:hover': {
            background: '#ea6225;',
        }
    },
    body: {
        backgroundColor:'#98999a',
        height: '650px',
    },
    Fields: {
        padding: '0 0 4px',
    },
    groupTitle: {
        fontSize: '20px',
        fontWeight: '500',
        color: '#ea6225',
        // height: '30px'
    },
    emptyTitle: {
        fontSize: '20px',
        fontWeight: '500',
        color: '#ea6225',
        ['@media (max-width: 480px)']: {
            fontSize: '0px !important',
        },
        ['@media (min-width: 481px) and (max-width: 768px)']: {
            fontSize: '0px !important',
        },
    },
    columnItems: {
        ['@media (max-width: 480px)']: {
            paddingTop: '0px !important',
        },
        ['@media (min-width: 481px) and (max-width: 768px)']: {
            paddingTop: '0px !important',
        },
    },
    rowItem: {
        margin: 'auto!important',
        
    },
    column: {
        padding: '0 20px!important',
    },
    EditForm: {
        textAlign: 'center',
        background: '#818284!important',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '20px',
        color: '#fff',
        fontWeight: '700'
    },
    btn: {
        color: '#fff!important',
        fontSize: '16px!important',
        border: 'none!important',
        textTransform: 'unset!important',
        '& :hover, :active, :focus': {
            color: '#fff!important',
            fontSize: '16px!important',
            border: 'none!important',
            textTransform: 'unset!important',
        }
      },
    saveButton: {
        border: "1px solid #ea6225",
        color: "white",
        padding: "2px 15px",
        borderRadius: "20px",
        background: '#ea6225',
        '&:hover': {
            background: '#ef8b5d',
        },
        fontSize: '18px',
        fontWeight: 'bold',
        // borderRadius: '28px',
        textTransform: 'none',
        fontFamily: "Arial",
        // marginTop: '10px',
        outline: 'none',
        textTransform: 'capitalize',
        // marginRight: '50px'
    },
      link: {
        textDecoration: 'none',
      },
      formTitle: {
        fontSize: '20px',
        fontWeight: '700',
    },
}));

export default useStyles;
