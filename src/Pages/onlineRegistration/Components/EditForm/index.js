




/**
*
* EditPatientForm
*
*/

import React from 'react';
import styles from './styles';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form/immutable';
import { ImplementationFor } from '../CreateRecordForm/utils'
import { Grid ,Link ,Button } from '@mui/material';



function EditForm (props) {
  const classes = styles();
  const { fields, handleSubmit, metaData, initialValues } = props;

  const renderSimpleField = (field) => {
    const InputComponent = ImplementationFor[field.type];

    return (<Grid item xs={12} className={classes.Fields}>
    <Field
    data={field}
    metaData={metaData}
    name={field.value ? field.value : field.name}
    label={field.label ? field.label : field.title}
    type={field.type}
    component={InputComponent}
    initialValue={initialValues && initialValues.toJS() || {}}
    {...field} />
    </Grid> )                                    
  }


  return (
        <form onSubmit={handleSubmit}>
            <Grid item className={classes.EditForm}>
              <span className={classes.formTitle}>Online Registration</span>
            </Grid>
          <Grid container spacing={5} className={classes.header} justify="space-between">
          {fields ? fields.map((section) =>{
            return section.forms.map((form) =>{
               return <Grid item style={{ width: '100%' }}>
                   {form.schema.map((schemaItem, schemaIndex) =>{
                      return <Grid key={schemaIndex} item>
                     {schemaItem.rows.map((row, rowIndex) =>{
                       return <Grid container spacing={12} xs={12} className={classes.rowItem} id={`row-${rowIndex}`} key={`row-${rowIndex}`}>
                      {row.columns.map((column, columnIndex) =>{
                          return <Grid key={columnIndex} item md={row.columns.length === 2 ? 12 : row.columns.length === 3 ? 4 : 12} xs={12} className={classes.column}>
                            <h4 className={columnIndex == 0 ? classes.groupTitle : null} dangerouslySetInnerHTML={{ __html: column.title }} />
                            <Grid container spacing={3} item key={`column-${columnIndex}-${columnIndex}`} className={column.noteStyle ? classes.noteWidth : null}>
                              {column.columnItems.map((columnItem, columnItemIndex) =>{
                                return <Grid item xs={12} lg={column.noWrap ? 12 : 6} className={classes.columnItems} key={`columnItems-${columnItemIndex}-${columnItemIndex}`}>
                                   {(columnItem.group && columnItem.group.title ?
                                  <h4 className={columnIndex == 0 ? classes.groupTitle : null} dangerouslySetInnerHTML={{ __html: columnItem.group.title }} />
                                  : columnItem.group && !columnItem.group.title ? <h4 className={columnItem.group.removeTitle ? classes.emptyTitle : classes.groupTitle} dangerouslySetInnerHTML={{ __html: '&nbsp;' }} /> : null)}
                                  {(columnItem.group ? columnItem.group.fields.map((group) => {
                                    return <Grid item >{group.type? renderSimpleField(group) : null}</Grid>
                                  }) : null)}

                                  {(columnItem.field && columnItem.field.type) || (columnItem.group && columnItem.group.radio) ?
                                    renderSimpleField(columnItem.field) : null }
                                </Grid>
                              })}
                            </Grid>
                          </Grid>
                     })}
                    </Grid>
                   })}
                  </Grid>
                 })}
              </Grid>
                  
               })}) :<div>Hello</div>}
              <Grid style={{ textAlign: "center", padding: '20px 0px', width: '100%' }}>
                <Button className={classes.saveButton} type="submit" disableRipple={true}>Save</Button>
              </Grid>
          </Grid>
        </form>
  )
}

  EditForm.propTypes = {
    fields: PropTypes.array,
};


export default reduxForm({
  form: 'editPatient',
  enableReinitialize: true
})(EditForm);
