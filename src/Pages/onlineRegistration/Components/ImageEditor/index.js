import React from 'react';
import { Grid ,Modal ,Button,Typography,Box } from '@mui/material';
import styles from './styles.css';
import LockIcon from '@mui/icons-material/Lock';
import LockOpenIcon from '@mui/icons-material/LockOpen';
import RestartAltIcon from '@mui/icons-material/RestartAlt';
import SwapHorizontalCircleIcon from '@mui/icons-material/SwapHorizontalCircle';
import SwapVerticalCircleIcon from '@mui/icons-material/SwapVerticalCircle';
import RotateLeftIcon from '@mui/icons-material/RotateLeft';
import RotateRightIcon from '@mui/icons-material/RotateRight';
import ArrowLeftIcon from '@mui/icons-material/ArrowLeft';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import ZoomOutIcon from '@mui/icons-material/ZoomOut';
import ZoomInIcon from '@mui/icons-material/ZoomIn';


class ImageEditor extends React.Component {

    constructor(props) {
        super(props);
        this.state = { showModal: true };
    }
    componentDidMount() {
       // new novaEditor(document.getElementById('novaImage'), this.props);
    }

    render() {
        const { acceptedFiles = [] } = this.props;
        const [file] = acceptedFiles;
        return <Grid>
            <Modal show={this.state.showModal} bsSize="large" className="novaEditor" onHide={this.close.bind(this)}>
            <Box>
            <Typography>
                    <Grid sm={12} className={`novaEditor-header ${styles.header}`}>
                        <Button onClick={this.close.bind(this)} type="button" className={styles.btn}>Cancel</Button>
                        <h4 className={styles.subtitle}>Image Editor</h4>
                        <Button action="crop" type="button" className={styles.btn}>Done</Button>
                    </Grid>
                </Typography>
                <Typography>
                <Grid className="novaEditor-options">
                        <ZoomInIcon action="zoomIn"/> 
                        <ZoomOutIcon action="zoomOut"/>
                        <ArrowLeftIcon action="moveLeft" />
                        <ArrowRightIcon action="moveRight" />
                        <ArrowDropUpIcon action="moveUp" />
                        <ArrowDropDownIcon action="moveDown" />
                        <RotateLeftIcon  action="rotateLeft"/>
                        <RotateRightIcon action="rotateRight"/>
                        <SwapHorizontalCircleIcon action="flipHorizontal"/>
                        <SwapVerticalCircleIcon  action="flipVertical" />
                        <LockIcon action="disable" />
                        <LockOpenIcon action="enable" />
                        <RestartAltIcon action="reset" />
                    </Grid>
                    <Grid>
                        <img id="novaImage" src={file && file.preview || ''} alt="Picture" width="400" height="400" />
                    </Grid>
                    </Typography>
                </Box>
            </Modal>
        </Grid>
    }

    close() {
        this.setState({ showModal: false });
        this.props.onClose();
    }
}

export default ImageEditor;