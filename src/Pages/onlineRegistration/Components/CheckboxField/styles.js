

import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
     checkboxLabel: {
     '& span': {
          fontSize: '18px',
          },
     '& input + label > span': {
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          marginRight: '1em',
          width: '2em',
          height: '2em',
          background: 'transparent',
          border: '2px solid #EA6225',
          borderRadius: '100px',
          cursor: 'pointer',  
          transition: 'all 250ms cubic-bezier(.4,.0,.23,1)'
        }        
     },
      imgDelete: {
        width: '25px !important',
        height: '25px !important'
      },
}));


export default useStyles;