
/**
 * 
 * Checkbox Field
 * 
 */

 import React, { useState } from 'react';
 import { FormControlLabel } from '@mui/material'
 import Checkbox from '@mui/material/Checkbox';
 import styles from './styles';
 import SelectField from '../SelectField';
 import { Field } from 'redux-form/immutable';
 import TextArea from '../TextArea';
 import { ContentTypes } from '../CreateRecordForm/utils';
 import ImageUpload from '../FileUpload';
 import CircleUnchecked from '@mui/icons-material/RadioButtonUnchecked';
 import CheckCircleIcon from '@mui/icons-material/CheckCircle';
 import InfoIcon from '@mui/icons-material/Info';
 import { IconButton } from '@mui/material';
 import Info from '../Info';

 const ImplementationFor = {
  note: TextArea,
  options: SelectField,
  images : ImageUpload,
};

 export default function CheckboxField (props) {

  const [isTrue, setisTrue] = useState(false);
  const [showInfo, setShowInfo] = useState(false);

  const handleOpen = () => {
    setShowInfo(true);
  }

  const handleClose = () => {
    setShowInfo(false);
  }

   const {input, id, label, required, warning, meta: { touched, error }, data} = props;
   const classes = styles();
   const { attributes = {}, upload, emptyText, labelStyle } = data;
   const { options, info } = attributes;
  const handleCheck = (e) =>
   {
     input.onChange({checked : e.target.checked})
     setisTrue(e.target.checked);
   }
   const renderAttributeFields = (click) => {
    const InputComponent  = attributes.note && !attributes.options && !attributes.calendar ? ImplementationFor['note'] :  attributes.options ? ImplementationFor['options'] : attributes.images ? ImplementationFor['images'] : null;
    const contentType = attributes.images ? ContentTypes['Images'] : attributes.note ? 'note' : attributes.options ? 'options' : null;
    const type = contentType != undefined ? contentType.indexOf('image') > -1 ? 'images' : contentType.indexOf('note') > -1 ? 'notes' : contentType.indexOf('options') > -1 ? 'selectedOption' : null : null;

    return (InputComponent && attributes ?
      <Field
        data={data}
        name={`${data.name}.${type}`}
        id={`${data.name}.${type}`}
        type={type}
        showtype={'button'}
        upload={upload}
        options={options}
        contentType={contentType}
        component={InputComponent}
      /> : null);
  }

     return (<div style={labelStyle || {}}>
           <FormControlLabel
              control={
                <Checkbox
                id={id}
                onChange={handleCheck}
                size="large"
                icon={<CircleUnchecked style={{color:'#ea6225'}}/>}
                checkedIcon={<CheckCircleIcon style={{color:'#ea6225'}} size="small"/>}
                />
              }
              label={label}
              className={classes.checkboxLabel}
           />
          {info ?
            <IconButton onClick={handleOpen}>
              <InfoIcon width="30px" className={classes.imgDelete} />
            </IconButton> : null}
          {showInfo ? <Info title={label} info={info} showInfo={showInfo} onClose={handleClose} /> : null}
          {attributes && isTrue ? renderAttributeFields(): null}
         </div>)
 };