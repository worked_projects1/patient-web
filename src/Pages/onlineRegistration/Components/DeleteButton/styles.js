import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  rowStandard: {
    marginBottom:'2em',
    paddingLeft:'25px',
    paddingRight:'25px',
    fontWeight: '400',
    textAlign: 'center'
  },
  imgDelete: {
    width: '25px !important',
    height: '25px !important'
  },
  submitButton: {
    background: 'none !important',
    border: 'none !important',  
    color: '#ea6225 !important',
    textTransform: 'capitalize !important',
    fontSize: '15px !important',
    marginTop: '0px !important'
  },
  footer:{
    display:'flex',
    padding: '0px !important',
    height: '45px',
    marginTop: '0px !important'
  },
  vr: {
    borderLeft: '0.1em solid #1e1b1b38',
    marginTop: '0px'
  },
  hr:{
    borderTop: '1px solid #1e1b1b38',
    marginTop:'44px',
  },
  deletePlayer:{
    position: 'relative',
    top: '-158px',
    bottom: '0',
    cursor: 'pointer',
    left: '-121px',
    display: 'inline-block',
  },

}));

export default useStyles;