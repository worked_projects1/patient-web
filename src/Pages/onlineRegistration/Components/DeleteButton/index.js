/**
*
* DeleteButton
*
*/

import React from 'react';
import PropTypes from 'prop-types';
//import shallowCompare from 'react-addons-shallow-compare';
import { Grid ,Box,Typography,Modal,Button } from '@mui/material';
import styles from './styles.js';
import CloseIcon from '@mui/icons-material/Close';
import CancelIcon from '@mui/icons-material/Cancel';
import { IconButton } from '@mui/material';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  bgcolor: 'background.paper',
  borderRadius: '15px',
  boxShadow: 24,
  padding: '12px',
  width: '300px'
};


function DeleteButton (props)
{
  const { type ,preview,OnDelete} = props; 
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => { 
    const {preview} = props;
    OnDelete(preview);
    setOpen(true);
  }
  const classes = styles();
  const handleClose = () => setOpen(false);

  return (
    <Grid style={{ position:"relative",top:"-130px",cursor:"pointer",left:'-18px'}}>
    <IconButton onClick={handleOpen}>
      <CancelIcon width="30px" className={classes.imgDelete} />
    </IconButton> 
    <Modal
        open={open}
        close={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
    >
          <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h4">
          <Grid style={{color:'#ea6225',fontSize:'16px',textAlign:'center', paddingTop: '10px', fontWeight: 'bold'}}>
            Confirm
          </Grid><br></br>
          <Grid style={{fontSize:'16px',textAlign:'center'}}>
            Are you sure want to delete this image ?
          </Grid>
          </Typography>
          <Grid className={classes.hr}></Grid>
          <Grid id="modal-modal-description" className={classes.footer} sx={{ mt: 2 }}>
            <div style={{ width: '49%', textAlign: 'center', paddingTop: '5px' }}>
              <Button bsStyle="standard" type="button" onClick={handleClose} className={classes.submitButton}>No</Button>
            </div>
            <Grid className={classes.vr}></Grid>
            <div style={{ width: '49%', textAlign: 'center', paddingTop: '5px' }}>
              <Button bsStyle="standard" type="button" onClick={OnDelete(preview)} className={classes.submitButton}>Yes</Button>
            </div>
          </Grid>
        </Box>
      </Modal>
      </Grid>
  )
}
export default DeleteButton;


