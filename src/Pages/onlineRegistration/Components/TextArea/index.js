
/***
 *
 * Textarea Field
 *
 */


 import React from 'react';
 import TextField from '@mui/material/TextField';
 import { Grid, Button } from '@mui/material';
 import Styles from './styles';
 
 
 export default function ({ input, label, autoFocus, type, disabled, prefix, rows, className, placeholder, variant, style, defaultBlur, meta: { touched, error, warning } }) {
 
     const classes = Styles();
     const { id, name, value, onChange } = input;
     const InputChange = defaultBlur ? Object.assign({}, {
         onBlur: (e) => onChange(e.target.value),
         defaultValue: value || ''
     }) : Object.assign({}, {
         onChange: (e) => onChange(e.target.value),
         value: value || ''
     });
 
 
     return (
         <div style={style || {}}>
             <label className={classes.label}>{label}</label>
             <TextField
                 className={className || classes.fieldColor && classes.Field}
                 multiline
                 name={name}
                 id={id}
                 label={variant && label || ''}
                 variant={variant || 'filled'}
                 placeholder={placeholder}
                 type={"input"}
                 rows={2}
                 rowsMax={6}
                 fullWidth
                 {...InputChange}
                 autoFocus={autoFocus}
                 disabled={disabled}
                 InputProps={{
                     classes: {
                         input: classes.textSize,
                         padding: 0,
                     },
 
                 }}
             />

           
             {/* <TextField
                 className={classes.fieldColor}
                 multiline
                 name={name}
                 type={type}
                 label={label}
                 rows={1}
                 rowsMax={1}
                 fullWidth
                 onChange={(e) => onChange(e.target.value)}
                 value={value || ''}
                 autoFocus={autoFocus}/>*/}
             <div className={classes.error} style={variant && { color: '#ffff' } || {}}>{touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</div>
         </div>
 
     )
 }
 