
/**
 * 
 * online
 * 
 */


 import React, { useEffect, useState } from 'react';
 import Button from '@mui/material/Button';
 import styles from './styles.js';
 import { Grid } from '@mui/material';
 import api from '../../utils/api';
 import DialogActions from '@mui/material/DialogActions';
 import DialogContent from '@mui/material/DialogContent';
 import DialogContentText from '@mui/material/DialogContentText';
 import DialogTitle from '@mui/material/DialogTitle';
 import Dialog from '@mui/material/Dialog';
 import EditForm from './Components/EditForm'; 
 import Alert from './Components/Alert';
 import schema from '../../routes/schema';
 import SuccessPage from './Components/SuccessPage';
 import Warning from '@mui/icons-material/Warning';
 import remotes from '../../Blocks/patients/remotes'

 const patients = schema().Online_form().columns;

 
 function PatientForm (props)
 {
  const {onlinedata} =props;
  const id = onlinedata[0].split('=');
  const patientId = id[1];
  const [busy, setBusy] = useState(false);
  const [error, setError] = useState(false);
  const [isOpen, setIsOpen] = useState(true);
  const [referral, setReferral] = useState(true);
  const [patientDetails, setPatientDetails] = useState(true);
  const [success, setSuccess] = useState(false);
  const [showAlert, setShowAlert] = useState(false);
  const appMetaData = {};
  appMetaData.referalList =  referral && referral.length > 0 ? referral.map(el => Object.assign({}, { label: el.value, value: `${el.id}` })) : [];

  useEffect(() => {
    setBusy(true);
    remotes.loadReferralSource(patientId)
      .then((response) => { setReferral(response); })
      .catch(err => setError('Failed to load!')).finally(()=>setBusy(false));
    remotes.loadPatientDetails(patientId)
      .then((response) => { setPatientDetails(response); })
      .catch(err => setError('Failed to load!')).finally(()=>setBusy(false))
  }, [api,setBusy,setError])

   function handleCreate(data, dispatch, {form}) {
      let record = data;
      let submitRecord = {}
      const { email, phone, birth_date } = record;
      record.callingFrom = "patientModule";
      record.onlineRegStatus = "Completed";
      submitRecord.id = parseInt(patientId);
      submitRecord.emailAddress = record.email;
      submitRecord.phoneNumber = record.phone;
      submitRecord.steps = [{'formId': '1A'}];
      submitRecord.steps[0]['state'] = 'Completed';
      submitRecord.steps[0]['formVersion'] = 1;
      const stepValues = Object.keys(record).length > 0 ?
        Object.keys(record)
        .filter(key => key)
        .reduce((obj, key) => {
          obj[key] = (typeof(record[key]) === 'object') ? JSON.stringify(record[key]).replace(/"/g,'★').replace(/\//g,"\\/") : record[key];
          return obj;
        }, {}) : {};
      submitRecord.steps[0]['value'] = stepValues;
      if(birth_date != undefined) {
       remotes.updatePatientDetails(submitRecord)
          .then((response) => {
            setSuccess(true);
          })
          .catch(err => {
            setShowAlert(true);
            setError('There was an error saving your changes. Please try again.')
          }).finally(()=>setBusy(false));
      } else {
        setShowAlert(true);
        setError('Please enter Birth Date')
      }
    };

    const closeModal = () => {
      setIsOpen(false)
     }

     const closeAlertModal = () => {
         setShowAlert(false);
     }
    const classes = styles();
         return (
           <div>
                    
                        {isOpen ? 
                         <Dialog
                            open={isOpen}
                            aria-labelledby="alert-dialog-title"
                            aria-describedby="alert-dialog-description"
                             >
                            <DialogTitle id="alert-dialog-title">
                            {<div style={{textAlign:'center',fontSize:"26px"}}>Welcome To Patient Online Registration</div>}
                            </DialogTitle>
                            <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                            <div>
                                  We need your information and health history in order to provide you the best care possible. Please fill out this online questionnaire. It is secure and confidential.
                            </div>
                            <hr></hr>
                           <div>
                            Please make sure you have the following information available:<br/><br/>
                            <ul>
                              <li>Names and dosage of each prescription medication you take.</li><br/>
                              <li>Names and dosage of each over-the-counter medication you take regularly.</li><br/>
                              <li>Allergies you may have.</li><br/>
                              <li>Previous surgeries and the year they were performed.</li>
                            </ul>
                            </div> 
                            <div>
                              Upon receiving this information, a staff member may call you for further clarification.<br/>
                              We look forward to providing you with the highest quality dental care and service.
                            </div>                          
                            </DialogContentText>
                            </DialogContent>
                            <DialogActions>
                            <Grid item xs={12} align="center"><Button onClick={closeModal} style={{color:'white',backgroundColor:'#ea6225',fontSize:'16px'}}> Continue </Button></Grid>
                            </DialogActions>
                        </Dialog>:
                    success ?
                      <SuccessPage /> :
                  <div className={classes.mainDiv}>
                   <Grid item xs={12} lg={12} className={classes.mainGrid}>
                       <EditForm
                         fields= {patients}
                         initialValues={patientDetails}
                         btnLabel="Submit"
                         metaData={{referalSource:appMetaData.referalList}}
                         title= "Invite Patient for Online Registration"
                         onSubmit={handleCreate}
                         />
                   </Grid>
             </div>}
             <Grid>
               {showAlert ? <Alert alertText={error} showAlert={showAlert} onClose={closeAlertModal} /> : null}
             </Grid>
           </div>

              
         );
       
 }
 export default PatientForm;

