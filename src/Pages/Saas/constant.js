export const GET_SAAS = "getSaas";
export const PUT_SAAS = "updateSaas";
export const POST_SAAS = "postSaas";
export const DELETE_SAAS = "deleteSaas";
export const LOAD_RECORD_ERROR = "error";
export const GET_Number = "number";
export const GET_SAAS_SUCCESS = "success;"