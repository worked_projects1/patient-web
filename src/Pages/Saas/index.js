import React, { useEffect, useState } from 'react';
// import Button from '@mui/material/Button';
// import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { GET_SAAS,GET_Number,GET_SAAS_SUCCESS } from './constant';
import {loadGlobalSettingsSaga} from './sagas'
import {connect} from 'react-redux';
import getstore from './store';

function Saas(props) {
  console.log("saas = ",props," store = ",getstore)
  const {saas=[]} = props

  // getstore.subscribe(() => {
    // this.setState({
      // console.log("store -= ",getstore.getState())
    // });
  // });

  // useEffect(() => {
  //   props.getSaas()
  // },[])
    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
          backgroundColor: '#ea6229',
          color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
          fontSize: 14,
        },
      }));
      
      const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
          backgroundColor: '#eee',
        },
        // hide last border
        '&:last-child td, &:last-child th': {
          border: 0,
        },
      }));

      // const {saas = []}= props

      const rows = saas.saas && saas.saas.data || saas
      console.log("rows = ",rows)
    return (
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 700 }} aria-label="customized table"s>
          <TableHead>
            <TableRow>
              <StyledTableCell>Name</StyledTableCell>
              <StyledTableCell >Logo</StyledTableCell>
              <StyledTableCell >SaasType</StyledTableCell>
              <StyledTableCell >Subscription Fee</StyledTableCell>
              <StyledTableCell >Subscription Type</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <StyledTableRow key={row.name}>
                <StyledTableCell component="th" scope="row">
                  {row.name}
                </StyledTableCell>
                <StyledTableCell >{row.logo}</StyledTableCell>
                <StyledTableCell >{row.saasType}</StyledTableCell>
                <StyledTableCell >{row.subscriptionFee}</StyledTableCell>
                <StyledTableCell >{row.subscriptionType}</StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );

}

const mapStateToProps = state => {
  console.log("state = ",state)
  return{
    saas: state
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getSaas: () => {
      dispatch({type: GET_SAAS_SUCCESS})
    }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Saas);

// export default Saas;