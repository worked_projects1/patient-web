import {createStore, applyMiddleware} from  'redux';
import rootReducer from './reducer';
import reduxSaga from 'redux-saga';
import {loadGlobalSettingsSaga} from './sagas';

const getStore = () => {
    console.log("getstore")
    const initialState = {};
    const reduxSagaMiddleware = reduxSaga();
    const store = createStore(rootReducer,initialState,applyMiddleware(reduxSagaMiddleware));
    store.runSaga = reduxSagaMiddleware.run(loadGlobalSettingsSaga)
    return store;
}
export default getStore