import saas from './actions';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
    saas: saas
});

export default rootReducer;