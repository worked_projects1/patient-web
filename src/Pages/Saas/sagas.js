import {GET_SAAS, PUT_SAAS, LOAD_RECORD_ERROR,GET_SAAS_SUCCESS } from './constant';
import {takeEvery, call, put, race, delay ,take,all} from 'redux-saga/effects';
import {getRecord} from '../../Blocks/Saas/remotes';

export const watchGetSaas = function(filter){
    console.log("filter = ",filter)
    return {
    type: GET_SAAS, 
    data: filter
    };
}

export const watchPutSaas = function(record){
    return { 
        type: PUT_SAAS,
         record
        };
}

export function loadRecordError(error) {
    console.log("errorinsaga = ",error && error.response && error.response.data && error.response.data.message)
    const errorMessage = error && error.response && error.response.data && error.response.data.message ? error.response.data.message : ""
    return {
      type: LOAD_RECORD_ERROR,
      data: errorMessage
    };
  }

export function* loadGlobalSettingsSaga() {
    // while (true) { // eslint-disable-line no-constant-condition
        console.log("enteredsaga")
        // const { load } = yield race({
        //     load: take(GET_SAAS),
        // });
        // console.log("load = ",load)
        // if (load) {
            try {
                const result = yield call(getRecord);
                
                if (result) {
                    yield put(watchGetSaas(result));
                    // yield put({type: GET_SAAS, data: result})
                    // break;
                }
                yield delay(500);
            } catch (error) {
                console.log("error = ",error)
                // yield put(loadRecordError(error));
                // yield put({type: LOAD_RECORD_ERROR, data: error})
              }
        // }
    // }
}

export function* getSaas1(){
    yield takeEvery(GET_SAAS_SUCCESS,loadGlobalSettingsSaga)
}

// export default function* rootSaga(){
// yield all([getSaas])
// }