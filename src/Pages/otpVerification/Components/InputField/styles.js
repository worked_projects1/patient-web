

import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  fieldColor: {
    '& :after': {
      borderBottomColor: '#2ca01c',
    },
    '& :before': {
      borderBottomColor: '#2ca01c',
    },
    color: 'green !important',
    '& label.Mui-focused': {
      color: '#2ca01c',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#2ca01c',
    }
  },
  textSize: {
    fontSize: '14px',
  },
  label: {
    fontSize: '18px',
    marginTop: '1em',
    display: 'block',
    lineHeight: '30px',
    fontWeight: '400'
  },
  Field: {
    height: '50px',
    fontSize: '16px!important',
    '& div': {
      height: '50px',
      color: 'gray',
      alignItems: 'end',
    },
    '& div:before, :active, :focus': {
      border: 'none!important',
      borderBottom: '1px solid #c8c8c8!important',
    },
    '& div:after, :active, :focus': {
      border: 'none!important',
    },
    '& input:hover': {
      border: 'none!important',
      borderBottom: '1px solid #c8c8c8!important',
    },
    '& input': {
      color: 'gray',
    },
    '& :hover': {
      borderColor: '#c8c8c8!important',
    }
    // '& textarea': {
    //   height: '50px!important',
    //   lineHeight: '60px'
    // }
  }
}));


export default useStyles;