

import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    form: {
        width: '100%', // Fix IE 11 issue.

    },
    submitBtn: {
        fontWeight: 'bold',
        borderRadius: '20px',
        fontFamily: 'Avenir-Regular',
        paddingLeft: '25px',
        paddingRight: '25px',
        marginTop: '20px',
        marginRight: '15px',
        marginRight: '12px'
    },
    verify:
    {
        fontWeight: 'bold',
        fontFamily: 'Avenir-Regular',
        paddingLeft: '25px',
        width: '695px',
        paddingRight: '25px',
        marginTop: '-27px',
        marginRight: '4px',
        marginRight: '12px',
        paddingTop: '13px',
    },
    cancelBtn: {
        fontWeight: 'bold',
        borderRadius: '20px',
        fontFamily: 'Avenir-Regular',
        paddingLeft: '25px',
        paddingRight: '25px',
        marginTop: '20px',
        backgroundColor: 'gray !important'
    },
    footer: {
        width: "100%",
        paddingTop: '25px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    }
}));


export default useStyles;