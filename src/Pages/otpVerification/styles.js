import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    body: {
        backgroundColor: 'white',
        height: '650px',
    },

    button: {
        color: 'white !important',
        fontSize: '16px',
        background: '#ea6225 !important',
    },
    appointment: {
        width: "100%",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
}));

export default useStyles;