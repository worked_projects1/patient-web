/*
otp
*/




import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import LockRoundedIcon from '@mui/icons-material/LockRounded';
import styles from './styles';
import { Grid,Button } from '@mui/material';
import api from '../../utils/api';
import PatientForm from '../onlineRegistration/index';
import PatientConsultation from '../PatientConsultation/index';
import  Forms from '../Forms';
import schema from '../../routes/schema';
import EditRecordForm from './Components/EditRecordForm/index';
import Alert from '../onlineRegistration/Components/Alert/index';
import Saas from '../Saas/index'

const patients = schema().Verification().columns;

const H4 = styled.h4`
font-size: 2rem;
font-weight: 400;
text-align: center;
color: 'black'
`;
const H6 = styled.h6`
font-size: 1rem;
font-weight: 400;
text-align: center;
color: 'black',
`;

function Verification (props)
{
   const {decryptedText,name} = props;
   const data = decryptedText.split('/');
   const text = window.location.pathname; 
   console.log("text = ",text)
   const [busy, setBusy] = useState(false);
   const [error, setError] = useState(false);
   const [verifyOtp, setVerifyOtp] = useState(false);
   const [sendService, setSendService] = useState(false);
   const [verify_num, setVerify_num] = useState(false);
   const [response_otp, setResponse_otp] = useState(false);
   const [sendNumber, setSendNumber] = useState(false);
   const [otpFailure, setOtpFailure] = useState(false);
   const [failureAlert, setFailureAlert] = useState(false);
   const [successAlert, setSuccessAlert] = useState(false);
   const [message, setMessage] = useState(false);
   const classes = styles();


   useEffect(() => {
    if (!text.includes("confirmAppointment") && !text.includes("consent")  && !text.includes("onlineRegistration") && !text.includes("saas")) {
     setBusy(true);
     api.put(`/sendOtp?${data[0]}`)
       .then((response) => {
         setVerify_num(true);
         setSendService(response && response.data && response.data.serviceId);
         setSendNumber(response && response.data && response.data.patientMobileNumber)
       })
       .catch(err => {
         setOtpFailure(true);
       }).finally(()=>setBusy(false));
      }
   }, [api,verifyOtp,setVerify_num,setSendService,setSendNumber, setBusy, setError])
   
   const handleResend = () => {
     setBusy(true);
     api.put(`/sendOtp?${data[0]}`)
       .then((response) => {
         setVerify_num(true);
         setSendService(response && response.data && response.data.serviceId);
         setSendNumber(response && response.data && response.data.patientMobileNumber)
         setSuccessAlert(true);
         setMessage('OTP resent successfully');
       })
       .catch(err => {
         setOtpFailure(true);
       }).finally(()=>setBusy(false));
   }

   const handleVerify =  async (submitData) => {
     const otp = submitData.otp;
     setBusy(true);
     await api.get(`/verifyOtp?${data[0]}&serviceId=${sendService}&otp=${otp}`)
       .then((response) => {
         const schedule_data =  response.data.status;
         setResponse_otp(schedule_data);
         if(schedule_data == 'pending') {
           setFailureAlert(true);
           setError('Please enter valid OTP')
         }
       })
       .catch(err => {
         setFailureAlert(true);
         setError('PIN is not valid, please try again.')
       }).finally(()=>setBusy(false));
    }

     const closeAlertModal = () => {
       setOtpFailure(false);
       setFailureAlert(false);
       setSuccessAlert(false);
     }

   return (
       <div>
                 {
                //  response_otp === 'approved' && text.includes("onlineRegistration")  ? <PatientForm onlinedata={data}/> :
                //  response_otp === 'approved' && text.includes("confirmAppointment") ? <PatientConsultation  decryptedText={decryptedText} /> :
                //   response_otp === 'approved' && name ? <Forms inputs={decryptedText} consentName={name} /> :
                // response_otp === 'approved' && text.includes("onlineRegistration") ? <PatientForm onlinedata={data} /> :
          text.includes("confirmAppointment") ? <PatientConsultation decryptedText={decryptedText} /> :
          text.includes("consent") ? <Forms inputs={decryptedText} consentName={name} /> :
          text.includes("saas") ? <Saas/> :
          text.includes("onlineRegistration") ? <PatientForm onlinedata={data} /> :
                 <div className={classes.body} style={{padding: '25px',textAlign: 'center'}}> 
                  <LockRoundedIcon style={{ color: "white",fontSize:"40px",backgroundColor:'#ccccb3' }}/>
                  <H4>Two step verification </H4>
                  <H6>In order to keep your data safe we need to verify your identity. Thank you for your understanding!</H6>
                  <Grid align="center" className={classes.mainGrid}>
                   <EditRecordForm
                        fields= {patients}
                        btnLabel2="Verify"
                        placeholder="Enter Verification Code"
                        onSubmit={handleVerify}
                        sendNumber={sendNumber}
                        verify_num={verify_num}
                        verify_btn = {true}
                        className={classes.appointment}
                   />

                  </Grid>
                  <div>
                      <H6>Code not Working? <Button onClick={() =>handleResend()} style={{color:'#ea6225'}} > Resend code </Button> </H6>
                  </div>
                  <Grid>
                    {otpFailure ? <Alert warning={true} emailAlert={true} showAlert={otpFailure} onClose={closeAlertModal} /> : null}
                    {failureAlert ? <Alert warning={true} alertText={error} showAlert={failureAlert} onClose={closeAlertModal} /> : null}
                    {successAlert ? <Alert success={true} alertText={message} showAlert={successAlert} onClose={closeAlertModal} /> : null}
                  </Grid>
           </div>
          }
       </div>
     );
}
export default Verification;
