
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({

    button: {
        color: 'white !important',
        fontSize: '16px',
        '&:hover': {
            background: '#ea6225 !important',
        }
    },
    body: {
        backgroundColor: '#98999a',
        height: '650px',
    },
    schedule_time: {
        backgroundColor: 'white',
        height: '145px',
        boxShadow: '6px 6px 1px 2px #888888',
        margin: '25px',
    },
    //status 
    status_confirm: {
        backgroundColor: 'white',
        height: '200px',
        boxShadow: '6px 6px 1px 2px #888888',
        margin: '25px',
    },
    schedule_confirm: {
        paddingTop: '20px',
    },
    status_cancel: {
        backgroundColor: 'white',
        height: '150px',
        boxShadow: '6px 6px 1px 2px #888888',
        margin: '25px',
    },
    schedule_cancel: {
        paddingTop: '20px',
    },
    status_schedule: {
        backgroundColor: 'white',
        height: '150px',
        boxShadow: '6px 6px 1px 2px #888888',
        margin: '25px',
    },
    schedule_schedule: {
        paddingTop: '20px',
    },
    space: {
        paddingTop: '75px',
    },
    schedule_table: {
        paddingTop: '20px',
    },
    Dialog: {
        color: '#ea6225',
    },
    button1: {
        color: 'white !important',
        background: '#ea6225;',
        fontSize: '12px',
        marginTop: '23px',
        marginBottom: '15px',
        marginRight: '16px',
        '&:hover': {
            background: '#ea6225;',
        }
    },

}));

export default useStyles;







