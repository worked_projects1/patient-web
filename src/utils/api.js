import axios from 'axios';

const api = axios.create({
  baseURL: process.env.NODE_ENV === 'development' ? 'http://192.168.0.83:9000/v4/' : 'http://192.168.0.83:9000/v4/',
  timeout: 40000,
  headers: { Accept: 'application/json', "Authorization" : '6356f7d63b40e354000d65ca9a36bf00199d86ecf40cbf64f4f94dc4e589eee8da0bfd484d88053c17ab3f98b2ecf359caaf49795489f723d8a301a4cfd0148ea6759c88f1d7ac3c1e4327dbca4493b8d5054a4da8ec303980a317cc66058f6b12026160c03dbbdb186bf8ac1223c871e8d3f0c060c594861b8183df301584c7' },
});


export function setAuthToken(authToken) {
  api.defaults.headers.common['X-Auth-Token'] = authToken;
}

export default api;

