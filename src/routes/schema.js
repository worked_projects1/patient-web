
import React from 'react';
import moment from 'moment';

export default function schema() {

  function Online_form() {
    return {
      columns: [
        {
          "title": "Online Form",
          "forms": [
            {
              "id": "1Z",
              "version": 1,
              "title": "Online Form",
              "icon": "PersonalInfo",
              "schema": [
                {
                  "rows": [
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "field": {
                                "max": 35,
                                "value": "full_name",
                                "type": "input",
                                "label": "First Name",
                                "required": true
                              }
                            },
                            {
                              "field": {
                                "max": 35,
                                "name": "mi",
                                "type": "input",
                                "title": "MI"
                              }
                            },
                            {
                              "field": {
                                "max": 35,
                                "value": "last_name",
                                "type": "input",
                                "label": "Last Name",
                                "required": true
                              }
                            },
                            {
                              "field": {
                                "name": "preferredName",
                                "type": "input",
                                "title": "Preferred Name"
                              }
                            },
                            {
                              "field": {
                                "name": "business_phone",
                                "type": "input",
                                "limit": 10,
                                "label": "Business Phone"
                              }
                            },
                            {
                              "field": {
                                "name": "phone",
                                "type": "input",
                                "title": "Cell Phone",
                                "limit": 10,
                                "required": true
                              }
                            },
                            {
                              "field": {
                                "name": "birth_date",
                                "type": "date",
                                "label": "Birthdate (MM/DD/YYYY)",
                                "required": true
                              }
                            },
                            {
                              "field": {
                                "max": 35,
                                "name": "street",
                                "type": "input",
                                "label": "Street",
                                "required": true
                              }
                            },
                            {
                              "field": {
                                "max": 35,
                                "name": "city",
                                "type": "input",
                                "title": "City",
                                "required": true
                              }
                            },
                            {
                              "field": {
                                "name": "state",
                                "type": "input",
                                "label": "State",
                                "required": true
                              }
                            },
                            {
                              "field": {
                                "name": "zip",
                                "type": "input",
                                "title": "Zip",
                                "required": true
                              }
                            },
                            {
                              "field": {
                                "name": "country",
                                "type": "select",
                                "title": "Country",
                                "options": [
                                  {
                                    "title": "USA",
                                    "value": "USA"
                                  },
                                  {
                                    "title": "Canada",
                                    "value": "Canada"
                                  },
                                  {
                                    "title": "Australia",
                                    "value": "Australia"
                                  },
                                  {
                                    "title": "England",
                                    "value": "England"
                                  },
                                  {
                                    "title": "India",
                                    "value": "India"
                                  },
                                ],
                                "required": true
                              }
                            },

                          ]
                        },
                        {
                          "columnItems": [
                            {
                              "field": {
                                "name": "employer",
                                "type": "input",
                                "title": "Employer"
                              }
                            },
                            {
                              "field": {
                                "name": "ss#",
                                "type": "input",
                                "title": "SS#"
                              }
                            },
                            {
                              "field": {
                                "name": "email",
                                "type": "input",
                                "title": "Email",
                                "required": true
                              }
                            },
                            {
                              "field": {
                                "name": "position",
                                "type": "select",
                                "title": "Marital Status",
                                "options": [
                                  {
                                    "title": "Single",
                                    "value": "single"
                                  },
                                  {
                                    "title": "Married",
                                    "value": "married"
                                  },
                                  {
                                    "title": "Child",
                                    "value": "child"
                                  },
                                  {
                                    "title": "Widowed",
                                    "value": "widowed"
                                  },
                                  {
                                    "title": "Divorced",
                                    "value": "divorced"
                                  }
                                ]
                              }
                            },
                            {
                              "field": {
                                "name": "driversLicense",
                                "type": "input",
                                "title": "Driver's License"
                              }
                            },
                            {
                              "field": {
                                "name": "preferredContactMethod",
                                "type": "select",
                                "title": "Preferred Contact Method",
                                "options": [
                                  {
                                    "title": "Phone",
                                    "value": "phone"
                                  },
                                  {
                                    "title": "Email",
                                    "value": "email"
                                  },
                                  {
                                    "title": "Text",
                                    "value": "text"
                                  },
                                  {
                                    "title": "Mail",
                                    "value": "mail"
                                  }
                                ]
                              }
                            },
                            {
                              "field": {
                                "name": "preferredConfirmationMethod",
                                "type": "select",
                                "title": "Preferred Appt. Confirmation Method",
                                "options": [
                                  {
                                    "title": "Phone",
                                    "value": "phone"
                                  },
                                  {
                                    "title": "Email",
                                    "value": "email"
                                  },
                                  {
                                    "title": "Text",
                                    "value": "text"
                                  },
                                  {
                                    "title": "Mail",
                                    "value": "mail"
                                  }
                                ]
                              }
                            },
                            {
                              "field": {
                                "name": "gender",
                                "type": "select",
                                "title": "Gender",
                                "options": [
                                  {
                                    "title": "Male",
                                    "value": "male"
                                  },
                                  {
                                    "title": "Female",
                                    "value": "female"
                                  },
                                  {
                                    "title": "Unknown",
                                    "value": "unknown"
                                  }
                                ],
                                "required": true
                              }
                            },
                            {
                              "field": {
                                "name": "patient_referer",
                                "type": "select",
                                "title": "Referral Source",
                                "options":"referalSource",
                                "required": true
                              }
                            },
                            {
                              "field": {
                                "name": "chiefComplaint",
                                "type": "select",
                                "title": "Patient's Chief Complaint",
                                "options": [
                                  {
                                    "title": "Bad taste",
                                    "value": "bad taste"
                                  },
                                  {
                                    "title": "Broken tooth",
                                    "value": "broken tooth"
                                  },
                                  {
                                    "title": "Cavity",
                                    "value": "cavity"
                                  },
                                  {
                                    "title": "Cold sensitivity",
                                    "value": "cold sensitivity"
                                  },
                                  {
                                    "title": "Discoloration",
                                    "value": "discoloration"
                                  },
                                  {
                                    "title": "Fistula",
                                    "value": "fistula"
                                  },
                                  {
                                    "title": "History of pain",
                                    "value": "history of pain"
                                  },
                                  {
                                    "title": "Hot sensitivity",
                                    "value": "hot sensitivity"
                                  },
                                  {
                                    "title": "Hot & cold sensitivity",
                                    "value": "hot & cold sensitivity"
                                  },
                                  {
                                    "title": "Implant",
                                    "value": "implant"
                                  },
                                  {
                                    "title": "Implants",
                                    "value": "implants"
                                  },
                                  {
                                    "title": "Missing teeth",
                                    "value": "missing teeth"
                                  },
                                  {
                                    "title": "Missing tooth",
                                    "value": "missing tooth"
                                  },
                                  {
                                    "title": "Mouth sores",
                                    "value": "mouth sores"
                                  },
                                  {
                                    "title": "Needs a quote",
                                    "value": " needs a quote"
                                  },
                                  {
                                    "title": " Needs a root canal",
                                    "value": " needs a root canal"
                                  },
                                  {
                                    "title": "None",
                                    "value": "none"
                                  },
                                  {
                                    "title": "Pain",
                                    "value": "pain"
                                  },
                                  {
                                    "title": "Pain on biting",
                                    "value": "pain on biting"
                                  },
                                  {
                                    "title": "Pain on percussion",
                                    "value": "pain on percussion"
                                  },
                                  {
                                    "title": "Problem with swallowing",
                                    "value": "problem with swallowing"
                                  },
                                  {
                                    "title": "Sinus drainage",
                                    "value": "sinus drainage"
                                  },
                                  {
                                    "title": "Swelling",
                                    "value": "swelling"
                                  },
                                  {
                                    "title": "Yellow teeth",
                                    "value": "yellow teeth"
                                  }
                                ],
                                "required": true
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "columns": [
                        {
                          "title": "Emergency Contact",
                          "columnItems": [
                            {
                              "field": {
                                "max": 35,
                                "value": "fullName",
                                "type": "input",
                                "label": "Full Name",
                              }
                            },
                            {
                              "field": {
                                "name": "relationship",
                                "type": "select",
                                "title": "Relationship",
                                "options": [
                                  {
                                    "title": "Brother (br)",
                                    "value": "brother"
                                  },
                                  {
                                    "title": "Caregiver (cg)",
                                    "value": "caregiver"
                                  },
                                  {
                                    "title": "Child (c)",
                                    "value": "child"
                                  },
                                  {
                                    "title": "Father (d)",
                                    "value": "father"
                                  },
                                  {
                                    "title": "Foster Child (fc)",
                                    "value": "fosterChild"
                                  },
                                  {
                                    "title": "Friend (f)",
                                    "value": "friend"
                                  },
                                  {
                                    "title": "Grandchild (gc)",
                                    "value": "grandchild"
                                  },
                                  {
                                    "title": "Grandfather (gf)",
                                    "value": "grandfather"
                                  },
                                  {
                                    "title": "Grandmother (gm)",
                                    "value": "grandmother"
                                  },
                                  {
                                    "title": "Grandparent (gp)",
                                    "value": "grandparent"
                                  },
                                  {
                                    "title": "Guardian (g)",
                                    "value": "guardian"
                                  },
                                  {
                                    "title": "Life Partner (lp)",
                                    "value": "lifePartner"
                                  },
                                  {
                                    "title": "Mother (m)",
                                    "value": "mother"
                                  },
                                  {
                                    "title": "Other (o)",
                                    "value": "other"
                                  },
                                  {
                                    "title": "Parent (p)",
                                    "value": "parent"
                                  },
                                  {
                                    "title": "Self (se)",
                                    "value": "self"
                                  },
                                  {
                                    "title": "Sibling (sb)",
                                    "value": "sibling"
                                  },
                                  {
                                    "title": "Sister (ss)",
                                    "value": "sister"
                                  },
                                  {
                                    "title": "Sitter (s)",
                                    "value": "sitter"
                                  },
                                  {
                                    "title": "Spouse (sp)",
                                    "value": "spouse"
                                  },
                                  {
                                    "title": "Stepchild (sc)",
                                    "value": "stepchild"
                                  },
                                  {
                                    "title": "Stepfather (sf)",
                                    "value": "stepfather"
                                  },
                                  {
                                    "title": "Stepmother (sm)",
                                    "value": "stepmother"
                                  }
                                ]
                              }
                            },
                            {
                              "field": {
                                "value": "cellPhone",
                                "type": "input",
                                "label": "Cell Phone",
                                "limit": 10,
                              }
                            },
                            {
                              "field": {
                                "value": "workPhone",
                                "type": "input",
                                "label": "Work Phone",
                                "limit": 10,
                              }
                            },
                          ]
                        },
                      ]
                    },
                    {
                      "columns": [
                        {
                          "noWrap": true,
                          "columnItems": [
                            {
                              "group": {
                                "title": "Medical Conditions",
                                "fields": [
                                  {
                                    "name": "medicalCondition21",
                                    "type": "checkbox",
                                    "title": "High Blood Pressure",
                                  },
                                  {
                                    "name": "medicalCondition22",
                                    "type": "checkbox",
                                    "title": "Low Blood Pressure"
                                  },
                                  {
                                    "name": "medicalCondition9",
                                    "type": "checkbox",
                                    "title": "Diabetes"
                                  },
                                  {
                                    "name": "medicalCondition40",
                                    "type": "checkbox",
                                    "title": "Smoking"
                                  },
                                  {
                                    "name": "medicalCondition39",
                                    "type": "checkbox",
                                    "title": "Sinus Problems"
                                  },
                                  {
                                    "name": "medicalCondition42",
                                    "type": "checkbox",
                                    "title": "Taking Bisphosphonate Medication"
                                  },
                                  {
                                    "name": "medicalCondition8",
                                    "type": "checkbox",
                                    "title": "Cancer"
                                  },
                                  {
                                    "name": "medicalCondition43",
                                    "type": "checkbox",
                                    "title": "Radiation Therapy"
                                  },
                                  {
                                    "name": "medicalCondition44",
                                    "type": "checkbox",
                                    "title": "Chemotherapy",
                                  },
                                  {
                                    "name": "medicalCondition45",
                                    "type": "checkbox",
                                    "title": "Bleeding Disorder"
                                  },
                                  {
                                    "name": "medicalCondition3",
                                    "type": "checkbox",
                                    "title": "Anemia"
                                  },
                                  {
                                    "name": "medicalCondition4",
                                    "type": "checkbox",
                                    "title": "Angina"
                                  },
                                  {
                                    "name": "medicalCondition5",
                                    "type": "checkbox",
                                    "title": "Arthritis"
                                  },
                                  {
                                    "name": "medicalCondition6",
                                    "type": "checkbox",
                                    "title": "Artificial Joints or Implants"
                                  },
                                  {
                                    "name": "medicalCondition1",
                                    "type": "checkbox",
                                    "title": "Daily Alcoholic Beverages Number",
                                    "attributes": {
                                      "note": false,
                                      "images": false,
                                      "options": [
                                        {
                                          "title": "0",
                                          "value": "0"
                                        },
                                        {
                                          "title": "1",
                                          "value": "1"
                                        },
                                        {
                                          "title": "2",
                                          "value": "2"
                                        },
                                        {
                                          "title": "3",
                                          "value": "3"
                                        },
                                        {
                                          "title": "4",
                                          "value": "4"
                                        },
                                        {
                                          "title": "5",
                                          "value": "5"
                                        },
                                        {
                                          "title": "6",
                                          "value": "6"
                                        },
                                        {
                                          "title": "7",
                                          "value": "7"
                                        },
                                        {
                                          "title": "8",
                                          "value": "8"
                                        },
                                        {
                                          "title": "9",
                                          "value": "9"
                                        },
                                        {
                                          "title": "10",
                                          "value": "10"
                                        },
                                        {
                                          "title": "10+",
                                          "value": "11"
                                        }
                                      ]
                                    }
                                  },
                                  {
                                    "name": "medicalCondition47",
                                    "type": "checkbox",
                                    "title": "Blood Disease"
                                  },
                                  {
                                    "name": "medicalCondition7",
                                    "type": "checkbox",
                                    "title": "Asthma"
                                  },
                                ]
                              }
                            }
                          ]
                        },
                        {
                          "noWrap": true,
                          "columnItems": [
                            {
                              "group": {
                                "removeTitle": true,
                                "label": "Medical conditions",
                                "title": "",
                                "fields": [
                                  {
                                    "name": "medicalCondition10",
                                    "type": "checkbox",
                                    "title": "Epilepsy"
                                  },
                                  {
                                    "name": "medicalCondition11",
                                    "type": "checkbox",
                                    "title": "Artificial Heart Valve"
                                  },
                                  {
                                    "name": "medicalCondition12",
                                    "type": "checkbox",
                                    "title": "Congenital Heart Problems"
                                  },
                                  {
                                    "name": "medicalCondition13",
                                    "type": "checkbox",
                                    "title": "Congestive Heart Disease"
                                  },
                                  {
                                    "name": "medicalCondition14",
                                    "type": "checkbox",
                                    "title": "Heart Attack"
                                  },
                                  {
                                    "name": "medicalCondition15",
                                    "type": "checkbox",
                                    "title": "Heart Disease"
                                  },
                                  {
                                    "name": "medicalCondition16",
                                    "type": "checkbox",
                                    "title": "Heart Murmur"
                                  },
                                  {
                                    "name": "medicalCondition17",
                                    "type": "checkbox",
                                    "title": "Heart Pacemaker"
                                  },
                                  {
                                    "name": "medicalCondition18",
                                    "type": "checkbox",
                                    "title": "Heart Surgery"
                                  },
                                  {
                                    "name": "medicalCondition19",
                                    "type": "checkbox",
                                    "title": "Mitral Valve Prolapse"
                                  },
                                  {
                                    "name": "medicalCondition20",
                                    "type": "checkbox",
                                    "title": "Stroke"
                                  },
                                  {
                                    "name": "medicalCondition23",
                                    "type": "checkbox",
                                    "title": "Glaucoma"
                                  },
                                  {
                                    "name": "medicalCondition24",
                                    "type": "checkbox",
                                    "title": "HIV Infection (AIDS)"
                                  },
                                  {
                                    "name": "medicalCondition25",
                                    "type": "checkbox",
                                    "title": "Hay Fever"
                                  },
                                  {
                                    "name": "medicalCondition48",
                                    "type": "checkbox",
                                    "title": "Psychological Disorder"
                                  },
                                  {
                                    "name": "medicalCondition26",
                                    "type": "checkbox",
                                    "title": "Frequent Headaches"
                                  },
                                  {
                                    "name": "medicalCondition27",
                                    "type": "checkbox",
                                    "title": "Ulcers"
                                  },
                                ]
                              }
                            }
                          ]
                        },
                        {
                          "noWrap": true,
                          "columnItems": [
                            {
                              "group": {
                                "label": "Medical conditions",
                                "removeTitle": true,
                                "title": "",
                                "fields": [
                                  {
                                    "name": "medicalCondition28",
                                    "type": "checkbox",
                                    "title": "Lung Disease (Tuberculosis)"
                                  },
                                  {
                                    "name": "medicalCondition29",
                                    "type": "checkbox",
                                    "title": "Thyroid Problems"
                                  },
                                  {
                                    "name": "medicalCondition30",
                                    "type": "checkbox",
                                    "title": "Hepatitis A"
                                  },
                                  {
                                    "name": "medicalCondition31",
                                    "type": "checkbox",
                                    "title": "Hepatitis B"
                                  },
                                  {
                                    "name": "medicalCondition32",
                                    "type": "checkbox",
                                    "title": "Hepatitis C"
                                  },
                                  {
                                    "name": "medicalCondition33",
                                    "type": "checkbox",
                                    "title": "Kidney Disease"
                                  },
                                  {
                                    "name": "medicalCondition34",
                                    "type": "checkbox",
                                    "title": "Leukemia"
                                  },
                                  {
                                    "name": "medicalCondition35",
                                    "type": "checkbox",
                                    "title": "Liver Disease"
                                  },
                                  {
                                    "name": "medicalCondition36",
                                    "type": "checkbox",
                                    "title": "Seizures /Fainting"
                                  },
                                  {
                                    "name": "medicalCondition37",
                                    "type": "checkbox",
                                    "title": "Respiratory Problems"
                                  },
                                  {
                                    "name": "medicalCondition38",
                                    "type": "checkbox",
                                    "title": "Rheumatic Fever"
                                  },
                                  {
                                    "name": "medicalCondition50",
                                    "type": "checkbox",
                                    "title": "Pre-Med - Clind"
                                  },
                                  {
                                    "name": "medicalCondition46",
                                    "type": "checkbox",
                                    "title": "Pre-Med - Amox"
                                  },
                                  {
                                    "name": "medicalCondition49",
                                    "type": "checkbox",
                                    "title": "Dizziness"
                                  },
                                  {
                                    "name": "medicalCondition41",
                                    "type": "checkbox",
                                    "title": "Other",
                                    "attributes": {
                                      "note": true
                                    }
                                  }
                                ]
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "columns": [
                        {
                          "noWrap": true,
                          "columnItems": [
                            {
                              "group": {
                                "title": "Allergies",
                                "fields": [
                                  {
                                    "name": "allergy1",
                                    "type": "checkbox",
                                    "title": "Penicillin"
                                  },
                                  {
                                    "name": "allergy2",
                                    "type": "checkbox",
                                    "title": "Latex"
                                  },
                                  {
                                    "name": "allergy3",
                                    "type": "checkbox",
                                    "title": "Codeine"
                                  }
                                ]
                              }
                            }
                          ]
                        },
                        {
                          "noWrap": true,
                          "columnItems": [
                            {
                              "group": {
                                "removeTitle": true,
                                "label": "Allergies",
                                "title": "",
                                "fields": [
                                  {
                                    "name": "allergy4",
                                    "type": "checkbox",
                                    "title": "Anesthetics"
                                  },
                                  {
                                    "name": "allergy5",
                                    "type": "checkbox",
                                    "title": "Sulfa Drugs"
                                  },
                                  {
                                    "name": "allergy6",
                                    "type": "checkbox",
                                    "title": "Aspirin"
                                  }
                                ]
                              }
                            }
                          ]
                        },
                        {
                          "noWrap": true,
                          "columnItems": [
                            {
                              "group": {
                                "label": "Allergies",
                                "removeTitle": true,
                                "title": "",
                                "fields": [
                                  {
                                    "name": "allergy8",
                                    "type": "checkbox",
                                    "title": "Erythromycin"
                                  },
                                  {
                                    "name": "allergy7",
                                    "type": "checkbox",
                                    "title": "Other",
                                    "attributes": {
                                      "note": true
                                    }
                                  }
                                ]
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "columns": [
                        {
                          "title": "Patient Dental History",
                          "columnItems": [
                            {
                              "field": {
                                "value": "previousDentist",
                                "type": "input",
                                "label": "Name of Previous Dentist",
                              }
                            },
                            {
                              "field": {
                                "value": "lastExamXray",
                                "type": "date",
                                "label": "Date of Last Exam and X-Rays",
                              }
                            },
                            {
                              "field": {
                                "value": "gumsBleed",
                                "type": "input",
                                "label": "Do Your Gums Bleed While Brushing or Flossing?",
                              }
                            },
                            {
                              "field": {
                                "value": "sensitiveTeethSweetSour",
                                "type": "input",
                                "label": "Sensitive to Sweet or Sour Liquids/Foods?",
                              }
                            },
                            {
                              "field": {
                                "value": "sensitiveTeethHotCold",
                                "type": "input",
                                "label": "Are Your Teeth Sensitive to Hot or Cold Liquids/Foods?",
                              }
                            },
                            {
                              "field": {
                                "value": "head_neck_jaw_injuries",
                                "type": "input",
                                "label": "Have You Had any Head, Neck or Jaw Injuries?",
                              }
                            },
                            {
                              "field": {
                                "value": "feelAnythingUnusual",
                                "type": "input",
                                "label": "Do You Feel Anything Unusual in Your Mouth?",
                              }
                            },
                            {
                              "field": {
                                "value": "orthodonticTreatment",
                                "type": "input",
                                "label": "Have You Had Orthodontic Treatment?",
                              }
                            },
                            {
                              "field": {
                                "value": "clenchTeeth",
                                "type": "input",
                                "label": "Do You Clench or Grind Your Teeth?",
                              }
                            },
                            {
                              "field": {
                                "value": "likeSmile",
                                "type": "input",
                                "label": "Do You Like Your Smile?",
                              }
                            },
                          ]
                        },
                      ]
                    },
                    {
                      "columns": [
                        {
                          "noWrap": true,
                          "columnItems": [
                            {
                              "group": {
                                "title": "Have you experienced any problems with you jaw joint (TMJ):",
                                "fields": [
                                  {
                                    "name": "clickingPain",
                                    "type": "checkbox",
                                    "title": "Clicking Pain"
                                  },
                                  {
                                    "name": "difficultyOpening",
                                    "type": "checkbox",
                                    "title": "Difficulty in Opening or Closing"
                                  },
                                  {
                                    "name": "difficultyChewing",
                                    "type": "checkbox",
                                    "title": "Difficulty in Chewing"
                                  }
                                ]
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "columns": [
                        {
                          "title": "Rx",
                          "columnItems": [
                            {
                              "field": {
                                "value": "pharmacy_information1",
                                "type": "input",
                                "label": "Current Active Medication",
                              }
                            },
                            {
                              "field": {
                                "value": "pharmacy_information2",
                                "type": "input",
                                "label": "Preferred Pharmacy Information",
                              }
                            },
                          ]
                        },
                      ]
                    },
                    {
                      "columns": [
                        {
                          "columnItems": [
                            {
                              "group": {
                                "title": "",
                                "fields": [
                                  {
                                    "name": "medicalInsurance",
                                    "type": "checkbox",
                                    "title": "Do you have medical insurance(s)? If you do please check the box then upload photos of the insurance card(s)",
                                    "upload": true,
                                    "attributes": {
                                      "images": true
                                    }
                                  },
                                  {
                                    "name": "dentalInsurance",
                                    "type": "checkbox",
                                    "title": "Do you have dental insurance(s)? If you do please check the box then upload photos of the insurance card(s)",
                                    "upload": true,
                                    "labelStyle": { marginTop: '20px' },
                                    "attributes": {
                                      "images": true
                                    }
                                  }
                                ]
                              }
                            }
                          ]
                        },
                      ]
                    },
                  ]
                }
              ]
            }
          ]
        }
      ]
    }
  }
  function Appoinment_form() {
    return {
      columns: [
        {
          id: 2,
          value: 'reason',
          label: 'Reason',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'select',
          options: [
            {
              label: 'Conflict of schedule',
              value: 'Conflict of schedule'
            },
            {
              label: 'Out of town',
              value: 'Out of town'
            },
            {
              label: 'Not feeling well',
              value: 'Not feeling well'
            },
            {
              label: 'My insurance expired',
              value: 'My insurance expired'
            }
          ],
        },
        {
          id: 3,
          value: 'other',
          label: 'Other',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'textarea',
        },
      ],
    }
  }
  function Verification() {
    return {
      columns: [
        {
          id: 2,
          value: 'otp',
          label: '',
          visible: true,
          editRecord: false,
          viewRecord: true,
          viewMode: true,
          type: 'input',
        },
      ]
    }
  }

  return {
    Online_form,
    Verification,
    Appoinment_form
  };

}
