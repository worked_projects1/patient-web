import api from '../../utils/api';

function loadReferralSource(id) {
  return api.get(`/unsecuredReferralSource/${id}`).then((response) => response.data);
}

function loadPatientDetails(id) {
  return api.get(`/patientDetails/${id}`).then((response) => response.data);
}

function updatePatientDetails(record) {
  return api.put(`/onlinePatientUpdate/${record.id}`, record).then((response) => response.data);
}

export default {
  loadReferralSource,
  loadPatientDetails,
  updatePatientDetails
};
