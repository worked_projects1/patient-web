import api from '../../utils/api';

function getSignature(filename, contentType) {
  return api.get(`unSecuredSignature/${filename}` + `?Content-Type=${contentType}`).then((response) => response.data);
}

function uploadPdf(record) {
  return api.put(`updatePdfUrl`, record).then((response) => response.data);
}

export default {
  getSignature,
  uploadPdf
};
