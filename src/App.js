import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers,applyMiddleware } from 'redux';
// import { reducer as formReducer } from 'redux-form';
import rootReducer from './Pages/Saas/reducer';
import Home from './Pages/Home';
import reduxSaga from 'redux-saga';
import {loadGlobalSettingsSaga} from './Pages/Saas/sagas'
// const reducers = {
//   form: formReducer
// };

const initialState = {};
const reduxSagaMiddleware = reduxSaga();
// const reducer = combineReducers(rootReducer);
const store = createStore(rootReducer,initialState,applyMiddleware(reduxSagaMiddleware));
store.runSaga = reduxSagaMiddleware.run(loadGlobalSettingsSaga)

store.subscribe(() => {console.log("store -= ",store.getState())})

const App = ({ history }) => (
      <Provider store={store}>
        <Router>
          <Switch>
            <Route path="/" component={Home}/>
          </Switch>
        </Router>
      </Provider>
);

export default App;
