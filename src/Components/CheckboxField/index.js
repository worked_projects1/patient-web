
/**
 * 
 * Checkbox Field
 * 
 */

import React from 'react';
import { FormControlLabel } from '@mui/material'
import Checkbox from '@mui/material/Checkbox';
import styles from './styles';


export default function CheckboxField({ input, id, label, required, warning, meta: { touched, error } }) {
  const classes = styles();

  return (<div>
    <FormControlLabel
      control={
        <Checkbox
          id={id}
          // checked={input.value || false}
          // onChange={(e) => input.onChange({checked : e.target.checked})} 
          size="large"
        />
      }
      label={label}
      className={classes.checkboxLabel}
    />
  </div>)
};