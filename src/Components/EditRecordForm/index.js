
/***
 * 
 * EditRecordForm
 * 
 */


import React from 'react';
import styles from './styles';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { ImplementationFor } from '../CreateRecordForm/utils';
import { Grid, Button } from '@mui/material';
import AlertDialog from '../AlertDialog';


function EditRecordForm(props) {

    const classes = styles();
    const { handleSubmit, pristine, submitting, fields, path, className, placeholder, error, metaData, locationState, confirmButton, confirmMessage, btnLabel, invalid, btnLabel1, btnLabel2, destroy, spinner, formSchema, verify_btn } = props;

    return (
        <div>
            <form onSubmit={handleSubmit} className={classes.form} noValidate >
                <Grid container className={className} spacing={3} >
                    {(fields || []).map((field, index) => {
                        const InputComponent = ImplementationFor[field.type];
                        return <Grid key={index} item xs={12} lg={6}>
                            <Field
                                name={field.value}
                                label={field.label}
                                verify_btn={verify_btn}
                                type="text"
                                placeholder={placeholder}
                                metaData={metaData}
                                component={InputComponent}
                                required={field.required}
                                disabled={field.disableOptons && field.disableOptons.edit}
                                {...field} />
                        </Grid>
                    })}
                </Grid>
                <Grid className={classes.footer}>
                    {confirmButton ? <AlertDialog
                        description={confirmMessage}
                        onConfirm={() => handleSubmit()}
                        onConfirmPopUpClose={true}
                        btnLabel1='Yes'
                        btnLabel2='No' >
                        {(open) => <Button
                            type="button"
                            disabled={pristine || submitting || (!pristine && invalid)}
                            variant="contained"
                            onClick={!invalid ? open : null}
                            color="primary"
                            className={classes.submitBtn}>
                            {btnLabel1}
                        </Button>}
                    </AlertDialog> :
                        <Button
                            type="submit"
                            disabled={pristine || submitting || (!pristine && invalid)}
                            variant="contained"
                            color="primary"
                            className={verify_btn ? classes.verify : classes.submitBtn}>
                            {btnLabel2}
                        </Button>}
                </Grid>
            </form>
        </div>
    )
}
export default reduxForm({
    form: 'EditRecord',
    enableReinitialize: true,
    touchOnChange: true,
})(EditRecordForm);