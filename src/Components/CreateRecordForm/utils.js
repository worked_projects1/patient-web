import InputField from '../InputField';
import CheckboxField from '../CheckboxField';
import TextArea from '../TextArea';
import selectField from '../SelectField';
import DatePicker from '../DatePicker';
import RadioBoxField from '../RadioBoxField';


export const ImplementationFor = {
    input: InputField,
    number: InputField,
    checkbox: CheckboxField,
    textarea: TextArea,
    select: selectField,
    date: DatePicker,
    radiobox: RadioBoxField,
};

