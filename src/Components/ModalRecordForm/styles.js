

import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        // backgroundColor: theme.palette.background.paper,
        backgroundColor: "white",
        border: 'none',
        // boxShadow: theme.shadows[5],
        boxShadow: "1px 1px 10px #d0d0d0",
        padding: '25px',
        minWidth: '35%',
        outline: 'none',
        width: '25%',
        zIndex: 1
    },
    header: {
        justifyContent: 'space-between',
    },
    body: {
        marginTop: '25px',
        marginBottom: '25px'
    },
    footer: {
        borderTop: '1px solid lightgray',
        paddingTop: '10px',
        flexDirection: 'row-reverse!important'
    },
    button: {
        fontWeight: 'bold',
        borderRadius: '28px',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginTop: '10px',
        outline: 'none',
        textTransform: 'capitalize',
        marginRight: '12px'
    },
    title: {
        fontSize: '22px!important',
        color: '#ea6225',
    },
    message: {
        fontFamily: 'Avenir-Bold',
        fontSize: '16px',
        paddingTop: '18px',
    },
    messageRegular: {
        fontSize: '16px',
        paddingTop: '18px',
    },
    closeIcon: {
        cursor: 'pointer'
    },
    messageGrid: {
        paddingTop: '10px'
    },
    error: {
        marginTop: '10px'
    },
    note: {
        fontSize: '16px',
    },
}));


export default useStyles;