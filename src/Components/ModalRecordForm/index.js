/**
 * 
 *  Modal Record Form
 * 
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@mui/icons-material/Close';
import { Field, reduxForm } from 'redux-form';
import { ImplementationFor } from '../CreateRecordForm/utils';
import { Modal, Paper, Fade, Grid, Typography } from '@mui/material';
import Button from '@mui/material/Button';
import Backdrop from '@mui/material/Backdrop';

//import validate from 'utils/validation';
//import ButtonSpinner from 'components/ButtonSpinner';
//import Error from 'components/Error';
import AlertDialog from '../AlertDialog';
import { useTheme } from '@mui/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
//import { normalize } from 'utils/tools';
import Styles from './styles';

/**
 * 
 * @param {object} props 
 * @returns 
 */

function ModalRecordForm(props) {
    const { title, message, messageRegular, handleSubmit, submitting, children, fields, metaData, className, style, btnLabel, onOpen, onClose, error, show, confirmButton, confirmMessage, pristine, invalid, onSubmitClose, footerStyle, destroy, notes, disableContainer, enableSubmitBtn, disableCancelBtn, progress, btnLabel1, btnLabel2, footerBtn, confirmPopUpClose, paperClassName, enableScroll } = props;
    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);

    //  const theme = useTheme();
    //  const md = useMediaQuery(theme.breakpoints.up('md'));
    //  const sm = useMediaQuery(theme.breakpoints.up('sm'));

    const closeModal = () => {
        setModalOpen(false);
        if (onClose)
            onClose();
    }

    useEffect(() => {
        return () => destroy();
    }, []);

    return <Grid align="center" item container={disableContainer ? false : true} className={className} style={style}>
        {children && children(() => setModalOpen(!showModal))}
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={showModal || show || false}
            className={classes.modal}
            onClose={closeModal}
            closeAfterTransition
            BackdropComponent={Backdrop}
            onRendered={onOpen}
            BackdropProps={{
                timeout: 500,
            }}>
            {/* !sm ? { width: '90%' } : '' */}
            <Fade in={showModal || show || false}>
                <Paper className={`${classes.paper} ${paperClassName}`} style={''}>
                    <form onSubmit={handleSubmit.bind(this)} className={classes.form} noValidate>
                        <Grid container className={classes.header} justify="space-between">
                            <Typography component="span" className={classes.title}>{title || ''}</Typography>
                            {!disableCancelBtn ? <CloseIcon onClick={closeModal} className={classes.closeIcon} /> : null}
                        </Grid>
                        {message ?
                            <Grid className={classes.messageGrid}>
                                <Typography component="span" className={classes.message}>{message || ''}</Typography>
                            </Grid> : null}
                        {messageRegular ?
                            <Grid className={classes.messageGrid}>
                                <Typography component="span" className={classes.messageRegular}>{messageRegular || ''}</Typography>
                            </Grid> : null}
                        <Grid container className={classes.body}>
                            <Grid item xs={6} className={enableScroll ? enableScroll : null}>
                                <Grid container spacing={3}>
                                    {(fields || []).map((field, index) => {
                                        const InputComponent = ImplementationFor[field.type];
                                        return <Grid key={index} item xs={12}>
                                            <Field
                                                name={field.value}
                                                label={field.label}
                                                type={field.type}
                                                metaData={metaData}
                                                component={InputComponent}
                                                required={field.required}
                                                disabled={field.disableOptons && field.disableOptons.edit}
                                                {...field} />
                                        </Grid>
                                    })}
                                </Grid>
                            </Grid>
                            {error ? <Grid item xs={12} className={classes.error}></Grid> : null}
                        </Grid>
                        {notes ? <Grid >
                            <Typography component="span" className={classes.note}>{notes || ''}</Typography>
                        </Grid> : null}
                        <Grid container justify="flex-end" style={footerStyle} className={classes.footer}>
                            {footerBtn && React.createElement(footerBtn) || null}
                            {confirmButton ?
                                <AlertDialog
                                    description={confirmMessage}
                                    onConfirm={() => handleSubmit()}
                                    onConfirmPopUpClose={confirmPopUpClose}
                                    btnLabel1='Yes'
                                    btnLabel2='No' >
                                    {(open) => <Button
                                        type="button"
                                        variant="contained"
                                        onClick={open}
                                        disabled={!enableSubmitBtn && (pristine || invalid)}
                                        color="primary"
                                        className={classes.button}>
                                        {btnLabel1}
                                    </Button>}
                                </AlertDialog> :
                                <Button
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    disabled={!enableSubmitBtn && (pristine || invalid)}
                                    className={classes.button}
                                    onClick={!invalid && onSubmitClose ? closeModal : null}>
                                    {btnLabel2}
                                </Button>}
                        </Grid>
                    </form>
                </Paper>
            </Fade>
        </Modal>
    </Grid>
}

ModalRecordForm.propTypes = {
    title: PropTypes.string,
    children: PropTypes.func,
    handleSubmit: PropTypes.func,
    error: PropTypes.string,
    pristine: PropTypes.bool,
    submitting: PropTypes.bool,
    fields: PropTypes.array,
};

export default reduxForm({
    form: 'modalRecord',
    enableReinitialize: true,
    touchOnChange: true,
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true
})(ModalRecordForm);