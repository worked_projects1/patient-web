/**
 * 
 * Radio Box
 * 
 */

import React from 'react';
import Styles from './styles';
import { FormControl, FormLabel, FormControlLabel, Radio, RadioGroup, label } from '@mui/material';

export default function ({ input, label, options, autoFocus, disabled, required, meta: { touched, error } }) {
  const classes = Styles();

  const { id, name, value, onChange } = input;
  const isPreDefinedSet = Array.isArray(options);

  return (
    <div className={classes.checkboxField}>
      <FormControl component="fieldset">
        <label className={classes.label}>{label ? label : ''}</label>
        <RadioGroup
          row
          name={name}
          value={value}
          onChange={(e) => input.onChange(e.target.value)}>
          {isPreDefinedSet ? (options || []).map((opt, index) =>
            <FormControlLabel value={opt.value} name={opt.value} label={opt.label} control={<Radio />} disabled={disabled ? true : false} className={classes.radioLabel} />
          ) : null}
        </RadioGroup>
      </FormControl>
    </div>
  );
}
