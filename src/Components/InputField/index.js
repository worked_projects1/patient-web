
/***
 * 
 * Input Field
 * 
 */


import React from 'react';
import TextField from '@mui/material/TextField';
import styles from './styles';

export default function InputField(props) {
  const { input, label, title, data = {}, placeholder, autoFocus, verify_btn, type, defaultBlur, className, meta: { touched, error, warning } } = props;

  const classes = styles();
  const { name, value, onChange, required } = input;
  const InputChange = defaultBlur ? Object.assign({}, {
    onBlur: (e) => onChange(e.target.value),
    defaultValue: value || ''
  }) : Object.assign({}, {
    onChange: (e) => onChange(e.target.value),
    value: value || ''
  });

  return (
    <div>
      <label className={classes.label}>
        {label ? label : title ? title : ''}
        {data.required ? <span style={{ color: "#ea6225", fontSize: "18px" }}>*</span> : null}
      </label>
      <TextField
        className={classes.fieldColor && classes.Field}
        multiline
        name={name}
        type={type}
        placeholder={placeholder}
        rows={1}
        rowsMax={1}
        fullWidth
        {...InputChange}
        value={value || ''}
        autoFocus={autoFocus}
        variant={verify_btn ? "filled" : "standard"}
      />
      <div className={classes.error}>{touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</div>
    </div>

  )
}