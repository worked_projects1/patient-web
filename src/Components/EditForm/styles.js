
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({

    button: {
        color: 'white !important',
        fontSize: '16px',
        '&:hover': {
            background: '#ea6225;',
        }
    },
    body: {
        backgroundColor: '#98999a',
        height: '650px',
    },
    Fields: {
        padding: '0 0 4px',
    },
    groupTitle: {
        fontSize: '20px',
        fontWeight: '500',
        color: '#ea6225',
        // height: '30px'
    },
    rowItem: {
        margin: 'auto!important',
    },
    column: {
        padding: '0 20px!important',
    }

}));

export default useStyles;







